using LevelOfAchievement;
using ScoreManager;
using UnityEngine;
using TMPro;

//エンディングシーン
public class EndingSceneManager : MonoBehaviour
{
	//Zキーを押した回数
	private int cnt = 0;

	//キャンバス(画像など)
	public Canvas canvas = null;

	//キャンバス(テキスト)
	public Canvas textCanvas = null;

	//テキスト(オブジェクト)
	private TextMeshProUGUI text = null;

	//初期化
	void Start()
	{
		//生成
		Instantiate(canvas);
		Instantiate(textCanvas);

		//取得
		text = GameObject.Find("Text").GetComponent<TextMeshProUGUI>();

		//フェードイン
		FadeScript.FadeIn();

		//サウンドマネージャー
		SoundManager.SoundManagerGeneration();

		//BGM再生
		SoundManager.PlayBGM("GameClear");

		//テキストボックス生成
		MessageWindowManager.ImageGeneration();

		//ボックスの透明度変更
		MessageWindowManager.AlphaChenge(1.0f);

		//最初に表示されるメッセージ
		MessageWindowManager.MessageTextChange(
		"\n" +
		"		階層				：" + Score.GetFloor().ToString() + "\n" +
		"		与えたダメージ	：" + Score.GetGivingDamage() + "\n" +
		"		倒した敵			：" + Score.GetKillNum() + "\n"
		);

		//テキスト表示
		text.text = "\n" + "プレイ時間			：" + Score.GetTime().ToString() + "\n" +
					"受けたダメージ		：" + Score.GetCoveredDamage() + "\n" +
					"受けた状態異常の回数	：" + Score.GetCoveredTrap();

		//使用武器の保存
		Score.WriteWeaponFlgs();

		//アチーブ確認
		AchievementList.AchievementCheck();

		//生成
		AchievementPeaple.AppealGeneration();
	}

	//更新
	void Update()
	{
		//Zキーを押したら
		if (Input.GetKeyUp(KeyCode.Z))
		{
			//何回目か
			if (++cnt == 1)
			{
				//--------
				SoundManager.PlaySE("Choice");

				//1つ目のメッセージ削除
				MessageWindowManager.MessageImageHide();

				//メッセージ切り替え
				MessageWindowManager.MessageTextChange(
									"\n" +
									"		Lv				：" + Score.GetLv() + "                  " + "\n" +
									"		攻撃力			：" + Score.GetAttack() + "\n" +
									"		獲得スキルポイント	：" + Score.GetSkillPt()
									);

				//テキスト表示
				text.text = "\n" + "HP					：" + Score.GetHp() + "\n" +
							"防御力				：" + Score.GetDefence() + "\n" +
							"獲得経験値			：" + Score.GetHaveAllExp();
			}
			if (cnt == 2)
			{
				//--------
				SoundManager.PlaySE("Choice");

				//2つ目のメッセージ削除
				MessageWindowManager.MessageImageHide();

				//メッセージ切り替え
				MessageWindowManager.MessageTextChange(
				"\n" +
				"		緑色ポーションの使用回数：" + Score.GetUseGreenPotion() + "\n" +
				"\n" +
				"		青色ポーションの使用回数：" + Score.GetUseBluePotion()
				);

				//テキスト表示
				text.text = "\n" + "赤色ポーションの使用回数：" + Score.GetUseRedPotion() + "\n" + "\n" +
							"白色ポーションの使用回数：" + Score.GetUseWhitePotion();

				if (!AchievementList.GetDungeonTraversal_Flg())
				{
					cnt++;
					if (!AchievementList.GetMonsterHunter_Flg())
					{
						cnt++;
						if (!AchievementList.GetDr_Potion_Flg())
						{
							cnt++;
							if (!AchievementList.GetClumsyPerson_Flg())
							{
								cnt++;
								if (!AchievementList.GetDiamond_Flg())
								{
									cnt++;
									if (!AchievementList.GetGameOver_Flg())
									{
										cnt++;
										if (!AchievementList.GetTalented_Flg())
										{
											cnt++;
											if (!AchievementList.GetGreatAdventurer_Flg())
											{
												cnt++;
											}
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 3)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetDungeonTraversal_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("TreasureChest");
					if (!AchievementList.GetMonsterHunter_Flg())
					{
						cnt++;
						if (!AchievementList.GetDr_Potion_Flg())
						{
							cnt++;
							if (!AchievementList.GetClumsyPerson_Flg())
							{
								cnt++;
								if (!AchievementList.GetDiamond_Flg())
								{
									cnt++;
									if (!AchievementList.GetGameOver_Flg())
									{
										cnt++;
										if (!AchievementList.GetTalented_Flg())
										{
											cnt++;
											if (!AchievementList.GetGreatAdventurer_Flg())
											{
												cnt++;
											}
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 4)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetMonsterHunter_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Trophy");
					if (!AchievementList.GetDr_Potion_Flg())
					{
						cnt++;
						if (!AchievementList.GetClumsyPerson_Flg())
						{
							cnt++;
							if (!AchievementList.GetDiamond_Flg())
							{
								cnt++;
								if (!AchievementList.GetGameOver_Flg())
								{
									cnt++;
									if (!AchievementList.GetTalented_Flg())
									{
										cnt++;
										if (!AchievementList.GetGreatAdventurer_Flg())
										{
											cnt++;
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 5)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetDr_Potion_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Apple");
					if (!AchievementList.GetClumsyPerson_Flg())
					{
						cnt++;
						if (!AchievementList.GetDiamond_Flg())
						{
							cnt++;
							if (!AchievementList.GetGameOver_Flg())
							{
								cnt++;
								if (!AchievementList.GetTalented_Flg())
								{
									cnt++;
									if (!AchievementList.GetGreatAdventurer_Flg())
									{
										cnt++;
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 6)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetClumsyPerson_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Rhombus");
					if (!AchievementList.GetDiamond_Flg())
					{
						cnt++;
						if (!AchievementList.GetGameOver_Flg())
						{
							cnt++;
							if (!AchievementList.GetTalented_Flg())
							{
								cnt++;
								if (!AchievementList.GetGreatAdventurer_Flg())
								{
									cnt++;
								}
							}
						}
					}
				}
			}

			else if (cnt == 7)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetDiamond_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Heart");
					if (!AchievementList.GetGameOver_Flg())
					{
						cnt++;
						if (!AchievementList.GetTalented_Flg())
						{
							cnt++;
							if (!AchievementList.GetGreatAdventurer_Flg())
							{
								cnt++;
							}
						}
					}
				}
			}

			else if (cnt == 8)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetGameOver_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Weak");
					if (!AchievementList.GetTalented_Flg())
					{
						cnt++;
						if (!AchievementList.GetGreatAdventurer_Flg())
						{
							cnt++;
						}
					}
				}
			}

			else if (cnt == 9)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetTalented_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Medal");
					if (!AchievementList.GetGreatAdventurer_Flg())
					{
						cnt++;
					}
				}
			}

			else if (cnt == 10)
			{
				//--------
				SoundManager.PlaySE("Choice");

				if (AchievementList.GetGreatAdventurer_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Crown");
				}

			}
		}

		//終了
		if (cnt > 10)
		{
			//フェードアウト
			FadeScript.FadeOut();

			//完全にフェードアウトが終了した場合
			if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
			{
				//タイトルシーンに移動
				UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
			}
		}
	}
}
