using SceneManager;
using UnityEngine;

public class GaidoButton : MonoBehaviour
{
	private Scene Scene = null;

	private GameObject GaidoArea = null;

	//設定画面
	private GameObject Gaido = null;

	//ほかのボタン
	private ConfigButton ConfigButton = null;
	private AchievementButton AchievementButton = null;
	private GameEndButton GameEndButton = null;

	//------------
	//マネージャー
	private TitleSceneManager manager = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//現在のシーンを登録
		Scene = new Scene();
		Scene.SetNowScene(SCENES.TITLE_SCENE);

		//取得
		GaidoArea = GameObject.Find("GaidoArea");
		Gaido = GameObject.Find("Gaido");
		ConfigButton = GameObject.Find("ConfigButton").GetComponent<ConfigButton>();
		AchievementButton = GameObject.Find("AchievementButton").GetComponent<AchievementButton>();
		GameEndButton = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();

		//------------
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();

		//ひとつ前のシーンがスプラッシュシーンの場合
		if (Scene.GetBeforeScene() == SCENES.SPLASH_SCENE)
		{
			flg = true;
		}

		GaidoArea.SetActive(flg);
		Gaido.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		GaidoArea.SetActive(flg);
		Gaido.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//ほかのボタンがOnじゃない
		if (ConfigButton.flg != true && AchievementButton.flg != true && GameEndButton.flg != true)
		{
			//フラグ切り替え
			if (flg)
				flg = false;
			else
				flg = true;

			//------------
			manager.SelectMenu();
		}
	}
}
