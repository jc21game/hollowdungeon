using UnityEngine;

public class YesButton : MonoBehaviour
{
	//ボタン
	private GaidoButton button = null;

	//------------
	//マネージャー
	private TitleSceneManager manager = null;

	public void Start()
	{
		//取得
		button = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();

		//------------
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	public void OnClick()
	{
		Application.OpenURL("https://youtu.be/ROS1XS_mUd4");

		//フラグ変更
		button.flg = false;

		//------------
		manager.SelectMenu();
	}
}
