using UnityEngine;

public class NoButton : MonoBehaviour
{
	//ボタン
	private GaidoButton button = null;

	//------------
	//マネージャー
	private TitleSceneManager manager = null;

	//初期化
	void Start()
	{
		//取得
		button = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();

		//------------
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	void Update()
	{
	}

	//ボタンをクリックされた
	public void OnClick()
	{

		//フラグ変更
		button.flg = false;

		//------------
		manager.SelectMenu();
	}
}
