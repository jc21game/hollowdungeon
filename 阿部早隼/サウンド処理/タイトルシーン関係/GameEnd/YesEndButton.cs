using UnityEngine;

public class YesEndButton : MonoBehaviour
{
	//------------
	//マネージャー
	private TitleSceneManager manager = null;

	//初期化
	void Start()
	{
		//------------
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	void Update()
	{

	}

	public void OnClick()
	{
		//------------
		manager.SelectMenu();

		//ゲーム終了
		Application.Quit();
	}
}
