using UnityEngine;

public class LeftArrowButton : MonoBehaviour
{
	//左矢印
	private GameObject LeftArrow = null;

	//オブジェクト
	private TitleAchievement TitleAchievement = null;

	//------------
	//マネージャー
	private TitleSceneManager manager = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//取得
		LeftArrow = GameObject.Find("LeftArrow");
		TitleAchievement = GameObject.Find("Achievement").GetComponent<TitleAchievement>();

		//------------
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();

		//アクティブ反映
		LeftArrow.SetActive(flg);
	}

	//更新
	void Update()
	{
		if (TitleAchievement.Icon == IconBase.Icon.DUNGEON_1)
		{
			flg = false;
		}
		else if (TitleAchievement.Icon == IconBase.Icon.DUNGEON_2)
		{
			flg = true;
		}

		//表示切り替え
		LeftArrow.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		TitleAchievement.Icon = IconBase.Icon.DUNGEON_1;

		//------------
		manager.ChangePage();
	}
}
