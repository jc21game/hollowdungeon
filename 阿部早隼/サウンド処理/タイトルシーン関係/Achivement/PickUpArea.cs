using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpArea : MonoBehaviour
{
    //ボタン
    private PickUp button = null;

    //------------
    //マネージャー
    private TitleSceneManager manager = null;

    //初期化
    void Start()
    {
        //取得
        button = GameObject.Find("PickUp").GetComponent<PickUp>();

        //------------
        manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
    }

    //更新
    void Update()
    {
    }

    //ボタンをクリックされた
    public void OnClick()
    {
        //------------
        manager.SelectMenu();

        //フラグ変更
        button.flg = false;
    }
}
