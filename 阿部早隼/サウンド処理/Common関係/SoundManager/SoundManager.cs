using System.IO;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
	//再生用(BGM)
	public static AudioSource AudioSourceBGM = null;

	//再生用(SE)
	public static AudioSource AudioSourceSE = null;

	//オーディオミキサー
	private static AudioMixer AudioMixer = null;

	//オーディオミキサーインスタンス
	private static AudioMixer InstanceAudioMixer = null;

	//オーディオグループ
	public static AudioMixerGroup AudioMixerGroup = null;

	//BGM用
	private static AudioClip bgm = null;

	//SE用
	private static AudioClip se = null;

	//-------
	//SE数
	public static int seNum = 18;

	//格納用
	public static AudioSource[] AudioList = new AudioSource[seNum];

	//対応したオーディオソース番号
	public static int currentNum = 0;

	//音量(BGM)
	public static float bgmVol = 0;

	//音量(SE)
	public static float seVol = 0;

	//書き込む先のパス
	public static string soundPath = "Sound/CSV_Sound.csv";

	//読み込み用
	public static string str = null;

	//格納用
	public static string[] splitStr = null;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
	}

	//サウンドマネージャー生成
	public static void SoundManagerGeneration()
	{
		//サウンドマネージャーを生成指定なかったら生成
		if (AudioSourceBGM == null)
		{
			//BGM用オーディオソース生成
			AudioSourceBGM = new GameObject("SoundManager").AddComponent<AudioSource>();

			//SE用オーディオソース生成
			for (int i = 0; i < seNum; i++)
			{
				AudioList[i] = GameObject.Find("SoundManager").AddComponent<AudioSource>();
			}

			//オーディオミキサー取得
			AudioMixer = Resources.Load<AudioMixer>("AudioMixer");

			//インスタンス生成
			InstanceAudioMixer = Instantiate(AudioMixer);

			//BGM用オーディオグループをセット
			foreach (var g in InstanceAudioMixer.FindMatchingGroups("BGM"))
			{
				AudioMixerGroup = g;
			}

			//オーディオソースにオーディオグループをセット
			AudioSourceBGM.outputAudioMixerGroup = AudioMixerGroup;

			//SE用オーディオグループをセット
			foreach (var g in InstanceAudioMixer.FindMatchingGroups("SE"))
			{
				AudioMixerGroup = g;
			}

			//オーディオソースにオーディオグループをセット
			for (int i = 0; i < seNum; i++)
			{
				AudioList[i].outputAudioMixerGroup = AudioMixerGroup;
			}

			//ファイルから音量読み込み
			string path = Path.Combine(Application.streamingAssetsPath, soundPath);
			str = File.ReadAllText(path);

			//分割した文字列を格納
			splitStr = str.Split(',');

			//代入
			bgmVol = int.Parse(splitStr[0]);
			seVol = int.Parse(splitStr[1]);
		}
	}

	//再生したいBGMを指定
	//引数：Resources未満のパス(拡張子は除く)
	public static void PlayBGM(string path)
	{
		//ファイルを設定
		bgm = Resources.Load<AudioClip>(path);

		//BGMにセット
		AudioSourceBGM.clip = bgm;

		//ループ設定
		AudioSourceBGM.loop = true;

		//再生
		AudioSourceBGM.Play();
	}

	//再生されているBGMを停止
	public static void StopBGM()
	{
		//停止
		AudioSourceBGM.Stop();
	}

	//再生したいSEを指定
	//引数：Resources未満のパス(拡張子は除く)
	public static void PlaySE(string path)
	{
		//ファイルを設定
		se = Resources.Load<AudioClip>(path);

		//-------
		//対応する番号を探す
		switch (path)
		{
			case "PushZ":
				currentNum = 0;
				break;
			case "Choice":
				currentNum = 1;
				break;
			case "Select":
				currentNum = 2;
				break;
			case "Stairs":
				currentNum = 3;
				break;
			case "LevelUp":
				currentNum = 4;
				break;
			case "SwordAttack":
				currentNum = 5;
				break;
			case "AxeAttack":
				currentNum = 6;
				break;
			case "BowAttack":
				currentNum = 7;
				break;
			case "EnemySword":
				currentNum = 8;
				break;
			case "EnemyAxe":
				currentNum = 9;
				break;
			case "EnemyBow":
				currentNum = 10;
				break;
			case "Heal":
				currentNum = 11;
				break;
			case "Archive":
				currentNum = 12;
				break;
			case "Poison":
				currentNum = 13;
				break;
			case "Slow":
				currentNum = 14;
				break;
			case "Ice":
				currentNum = 15;
				break;
			case "Magma":
				currentNum = 16;
				break;
			case "Get":
				currentNum = 17;
				break;
			default:
				currentNum = 0;
				break;
		}

		//再生中だったら
		if (AudioList[currentNum].isPlaying)
		{
			//停止
			AudioList[currentNum].Stop();
		}

		//再生
		AudioList[currentNum].PlayOneShot(se);
	}

	//書き込みチェック(タイトルシーンのUpdateのみ記述)
	public static void CheckVolume()
	{
		//音量が変更されていたら
		if (AudioSourceBGM.volume != (bgmVol / 100) || AudioList[0].volume != (seVol / 100))
		{
			//代入
			AudioSourceBGM.volume = bgmVol / 100;

			//代入
			for (int i = 0; i < seNum; i++)
			{
				AudioList[i].volume = seVol / 100;
			}

			//書き出す
			string path = Path.Combine(Application.streamingAssetsPath, soundPath);
			File.WriteAllText(path, bgmVol.ToString() + ',' + seVol.ToString());
		}
	}
}