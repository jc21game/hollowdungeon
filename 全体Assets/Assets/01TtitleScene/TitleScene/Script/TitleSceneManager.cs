using SceneManager;
using UnityEngine;
using UnityEngine.UI;

//タイトルシーン
public class TitleSceneManager : MonoBehaviour
{
	//シーンマネージャー
	private Scene Scene = null;

	//武器選択シーンへ移動する時間
	public float invokeTime = 0;

	//キャンバス
	public Canvas canvas = null;
	public GameObject scroll = null;
	public GameObject arasuji = null;

	//BGMバー
	private BGMBar bgmBar = null;

	//SEバー
	private SEBar seBar = null;

	//ボタン
	private GameObject[] buttons = null;
	private GameObject[] objects = null;

	//あらすじ表示
	bool DispraySynopsis = false;

	//スクロール再生
	public bool IsScroll = false;

	//初期化
	public void Start()
	{
		//フレームレート設定
		Application.targetFrameRate = 60;

		//シーン登録
		Scene = new Scene();
		Scene.SetNowScene(SCENES.TITLE_SCENE);

		//獲得フラグ初期化
		LevelOfAchievement.AchievementList.FlgReset();

		//フェード画像初期化
		FadeScript.Darkening();
		FadeScript.FadeIn();

		//生成
		Instantiate(canvas);
		Instantiate(scroll);

		//レイヤー順番設定
		arasuji.GetComponent<SpriteRenderer>().sortingOrder = 3;

		//サウンドマネージャー生成
		SoundManager.SoundManagerGeneration();

		//取得
		bgmBar = GameObject.Find("BGMBar").GetComponent<BGMBar>();
		seBar = GameObject.Find("SEBar").GetComponent<SEBar>();
		buttons = new GameObject[4];
		buttons[0] = GameObject.Find("ConfigButton");
		buttons[1] = GameObject.Find("GaidoButton");
		buttons[2] = GameObject.Find("AchievementButton");
		buttons[3] = GameObject.Find("GameEndButton");
		objects = new GameObject[4];
		objects[0] = GameObject.Find("Config");
		objects[1] = GameObject.Find("Gaido");
		objects[2] = GameObject.Find("Achievement");
		objects[3] = GameObject.Find("GameEnd");

		//各スライダーの初期設定
		bgmBar.Initialize();
		seBar.Initialize();

		//再生
		SoundManager.PlayBGM("Title");

		//初期化
		ScoreManager.Score.Initialize();
	}

	//更新
	public void Update()
	{
		//ボタンの開閉
		bool b = false;
		Color color = new Color(1, 1, 1, 1);
		for (int i = 0; i < 4; i++)
		{
			b |= objects[i].activeSelf;
		}
		if (b)
		{
			color.a = 0;
		}
		for (int i = 0; i < 4; i++)
		{
			buttons[i].GetComponent<Image>().color = color;
		}


		//進める
		if (!b || DispraySynopsis)
		{
			IsScroll = true;
			//Zキーを押したらフェードアウトして武器選択シーンに移動する
			if (Input.GetKeyDown(KeyCode.Z))
			{
				if (DispraySynopsis)
				{
					//フェードアウト
					FadeScript.FadeOut();

					//4秒後に武器選択シーンに移動する
					Invoke("ChangeWeapon", invokeTime);
				}
				else
				{
					//再生
					SoundManager.PlaySE("PushZ");

					//生成
					Instantiate(arasuji);
					GameObject.Find("Canvas(Clone)").SetActive(false);

					//あらすじ表示した
					DispraySynopsis = true;
				}
			}
		}
		else
		{
			IsScroll = false;
		}
	}

	//武器選択シーンに移動する関数
	public void ChangeWeapon()
	{
		//武器選択シーンに移動
		Scene.SetNextScene(SCENES.SELECT_SCENE);
	}

	//ページ切り替え
	public void ChangePage()
	{
		//SE
		SoundManager.PlaySE("Select");
	}

	//選択した
	public void SelectMenu()
	{
		//SE
		SoundManager.PlaySE("Choice");
	}
}
