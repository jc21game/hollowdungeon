using UnityEngine;

public class NoEndButton : MonoBehaviour
{
	//オブジェクト
	private GameEndButton GameEnd = null;

	//マネージャー
	private TitleSceneManager manager = null;

	//初期化
	void Start()
	{
		GameEnd = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	void Update()
	{

	}

	public void OnClick()
	{
		GameEnd.flg = false;
		manager.SelectMenu();
	}
}
