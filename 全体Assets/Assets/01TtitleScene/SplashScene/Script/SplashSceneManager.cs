using SceneManager;
using System.Threading;
using UnityEngine;

//スプラッシュ
public class SplashSceneManager : MonoBehaviour
{
	//フェード時間
	private float FadeInTime = 10;
	private float FadeOutTime = 5;
	private bool flgA = false;
	private bool flgB = false;

	//スリープ時間
	private int sleepTime = 1000;

	//シーンマネージャー
	private static Scene scene = null;

	//初期化
	public void Start()
	{
		//現在のシーンを登録
		scene = new Scene();
		scene.SetNowScene(SCENES.SPLASH_SCENE);

		//フレームレート設定
		Application.targetFrameRate = 60;

		//最初真っ暗な画面にする
		FadeScript.Darkening();

		//フェードインでスプラッシュシーンを表示する
		FadeScript.FadeIn();

		//アチーブメントの読み込み
		for (int i = 1; i < 17; i++)
		{
			AchievementSaveLoad.AchievementRoad(i);
		}

		//武器の読み込み
		ScoreManager.Score.ReadWeaponFlgs();
	}

	//更新
	public void Update()
	{

		//FadeOutTime秒後にフェードアウトする
		if (flgA == false && (FadeOutTime -= Time.deltaTime) <= 0)
		{
			FadeOutPlay();
			flgA = true;
		}

		//FadeInTime秒後にタイトルに移動する
		if (flgB == false && (FadeInTime -= Time.deltaTime) <= 0)
		{
			ChangeTitle();
			flgB = true;
		}
		//Zキー入力でスプラッシュを飛ばす
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//スリーブ
			Thread.Sleep(sleepTime);

			//タイトルシーンに移動
			scene.SetNextScene(SCENES.TITLE_SCENE);
		}
	}

	//タイトルシーンに移動する関数
	public void ChangeTitle()
	{
		//タイトルシーンに移動
		scene.SetNextScene(SCENES.TITLE_SCENE);
	}

	//フェードアウトする関数
	public void FadeOutPlay()
	{
		//フェードアウトする
		FadeScript.FadeOut();
	}
}
