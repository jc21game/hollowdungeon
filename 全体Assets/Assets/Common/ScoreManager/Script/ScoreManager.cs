using System.IO;
using UnityEngine;

namespace ScoreManager
{
	public class Score
	{
		//決定した武器
		static private IconBase.Icon MyWeapon = IconBase.Icon.SWORD;

		//決定したダンジョン
		static private IconBase.Icon MyDungeon = IconBase.Icon.DUNGEON_1;

		//プレイヤーのレベル
		static private int plLv = 1;

		//プレイヤーのHP
		static private int plMHp = 20;
		static private int plHp = 20;

		//プレイヤーの攻撃力
		static private int plAttack = 5;

		//プレイヤーの防御力
		static private int plDefence = 5;

		//使用スキルポイント
		static private int skillPt = 0;

		//階層
		static private int currentFloor = 1;
		static public int MaxFloor = 5;
		static public int ChangeFloor = 3;

		//総ターン数
		static private int progressTurn = 0;

		//総与攻撃回数
		static private int givingAttack = 0;

		//総与ダメージ量
		static private int givingDamage = 0;

		//総被攻撃回数
		static private int coveredAttack = 0;

		//総被ダメージ量
		static private int coveredDamage = 0;

		//総アイテム取得数
		static private int haveItem = 0;

		//総アイテム使用回数
		static private int useItem = 0;

		//総被回復量
		static private int coveredRecovery = 0;

		//緑色ポーションの所持数
		static private int haveGreenPotion = 0;

		//赤色ポーションの所持数
		static private int haveRedPotion = 0;

		//青色のポーションの所持数
		static private int haveBluePotion = 0;

		//黄色のポーションの所持数
		static private int haveYellowPotion = 0;

		//白色のポーションの所持数
		static private int haveWhitePotion = 0;

		//緑色ポーションの使用数
		static private int useGreenPotion = 0;

		//赤色ポーションの使用数
		static private int useRedPotion = 0;

		//青色のポーションの使用数
		static private int useBluePotion = 0;

		//黄色のポーションの使用数
		static private int useYellowPotion = 0;

		//白色のポーションの使用数
		static private int useWhitePotion = 0;

		//取得した経験値の設定
		static private int haveExp = 0;
		static private int haveAllExp = 0;

		//次のレベルまでの経験値
		static private int nextExp = 50;

		//倒した敵数
		static private int killNum = 0;

		//受けた状態異常の回数
		static private int coveredTrap = 0;

		//倒した敵の種類
		static private bool[] huntEnemys = new bool[6] { false, false, false, false, false, false };

		//個別の状態異常
		static private bool[] abnormalCondition = new bool[2] { false, false };

		//金剛
		static private bool vajra = false;

		//テスト
		static private bool initFloor = true;

		//今までにクリアした武器
		static private bool[] weaponFlgs = new bool[6] { false, false, false, false, false, false };
		static private string weaponSeet = "Weapon/Save.txt";

		//セッター
		static public void ReadWeaponFlgs()
		{
			string path = Path.Combine(Application.streamingAssetsPath, weaponSeet);
			string str = File.ReadAllText(path);
			string[] strs = str.Split(',');
			for (int i = 0; i < 6; i++)
			{
				weaponFlgs[i] = bool.Parse(strs[i]);
			}
		}

		//ゲッター
		static public bool GetD1WeaponFlg()
		{
			return weaponFlgs[0] && weaponFlgs[1] && weaponFlgs[2];
		}
		static public bool GetD2WeaponFlg()
		{
			return weaponFlgs[3] && weaponFlgs[4] && weaponFlgs[5];
		}
		static public void WriteWeaponFlgs()
		{
			//変更
			weaponFlgs[((int)MyWeapon - 1) + ((int)MyDungeon - 4) * 3] = true;

			//保存
			string path = Path.Combine(Application.streamingAssetsPath, weaponSeet);
			File.WriteAllText(path,
				weaponFlgs[0] + ","
				+ weaponFlgs[1] + ","
				+ weaponFlgs[2] + ","
				+ weaponFlgs[3] + ","
				+ weaponFlgs[4] + ","
				+ weaponFlgs[5]);
		}

		//決定した武器のゲッター
		static public IconBase.Icon GetMyWeapon()
		{
			return MyWeapon;
		}

		//決定した武器のセッター
		static public void SetMyWeapon(IconBase.Icon icon)
		{
			MyWeapon = icon;
		}

		//決定したダンジョンのゲッター
		static public IconBase.Icon GetMyDungeon()
		{
			return MyDungeon;
		}

		//決定したダンジョンのセッター
		static public void SetMyDungeon(IconBase.Icon icon)
		{
			MyDungeon = icon;
		}

		//プレイヤーレベルを取得
		static public int GetLv()
		{
			return plLv;
		}

		//プレイヤーレベルを設定
		static public void SetLv(int lv)
		{
			plLv = lv;
		}

		//プレイヤーのHPを取得
		static public int GetMHp()
		{
			return plMHp;
		}

		//プレイヤーのHPを設定
		static public void SetMHp(int hp)
		{
			plMHp = hp;
		}

		//プレイヤーのHPを取得
		static public int GetHp()
		{
			return plHp;
		}

		//プレイヤーのHPを設定
		static public void SetHp(int hp)
		{
			plHp = hp;
		}

		//プレイヤーの攻撃力を取得
		static public int GetAttack()
		{
			return plAttack;
		}

		//プレイヤーの攻撃力を設定
		static public void SetAttack(int attack)
		{
			plAttack = attack;
		}

		//プレイヤーの防御力を取得
		static public int GetDefence()
		{
			return plDefence;
		}

		//プレイヤーの防御力を設定
		static public void SetDefence(int defence)
		{
			plDefence = defence;
		}

		//使用スキルポイントを取得
		static public int GetSkillPt()
		{
			return skillPt;
		}

		//使用スキルポイントの設定
		static public void SetSkillPt(int pt)
		{
			skillPt = pt;
		}

		//階層を取得
		static public int GetFloor()
		{
			return currentFloor;
		}

		//階層の設定
		static public void SetFloor(int floor)
		{
			currentFloor = floor;
		}

		//テスト
		static public void SetFloor(int floor, int i)
		{
			if (i == 0 && initFloor)
			{
				currentFloor = floor;
				initFloor = false;
			}
		}

		//総ターン数を取得
		static public int GetTurn()
		{
			return progressTurn;
		}

		//総ターン数を設定
		static public void SetTurn(int turn)
		{
			progressTurn = turn;
		}

		//総与攻撃回数の取得
		static public int GetGivingAttack()
		{
			return givingAttack;
		}

		//総与攻撃回数の設定
		static public void SetGivingAttack(int attack)
		{
			givingAttack = attack;
		}

		//総与ダメージ量の取得
		static public int GetGivingDamage()
		{
			return givingDamage;
		}

		//総与ダメージ量の設定
		static public void SetGivingDamage(int damage)
		{
			givingDamage += damage;
		}

		//総被攻撃回数の取得
		static public int GetCoveredAttack()
		{
			return coveredAttack;
		}

		//総被攻撃回数の設定
		static public void SetCoveredAttack(int attack)
		{
			coveredAttack = attack;
		}

		//総被ダメージ量の取得
		static public int GetCoveredDamage()
		{
			return coveredDamage;
		}

		//総被ダメージ量の取得
		static public void SetCoveredDamage(int damage)
		{
			coveredDamage += damage;
		}

		//総アイテム取得数の取得
		static public int GetHaveItem()
		{
			return haveItem;
		}

		//総アイテム取得数の設定
		static public void SetHaveItem(int item)
		{
			haveItem = item;
		}

		//総アイテム使用数の取得
		static public int GetUseItem()
		{
			return useItem;
		}

		//総アイテム使用数の設定
		static public void SetUseItem(int item)
		{
			useItem = item;
		}

		//総被回復量の取得
		static public int GetCoveredRecovery()
		{
			return coveredRecovery;
		}

		//総被回復量の設定
		static public void SetCoveredRecovery(int recovery)
		{
			coveredRecovery = recovery;
		}

		//緑色ポーションの所持数の設定
		static public void SetGreenPotion(int num)
		{
			haveGreenPotion = num;
		}

		//緑色ポーションの所持数の取得
		static public int GetGreenPotion()
		{
			return haveGreenPotion;
		}

		//緑色ポーションの使用数の設定
		static public void SetUseGreenPotion(int num)
		{
			useGreenPotion = num;
		}
		static public void AddUseGreenPotion()
		{
			useGreenPotion++;
		}

		//緑色ポーションの使用数の取得
		static public int GetUseGreenPotion()
		{
			return useGreenPotion;
		}

		//赤色ポーションの所持数の設定
		static public void SetRedPotion(int num)
		{
			haveRedPotion = num;
		}

		//赤色ポーションの所持数の取得
		static public int GetRedPotion()
		{
			return haveRedPotion;
		}

		//赤色ポーションの使用数の設定
		static public void SetUseRedPotion(int num)
		{
			useRedPotion = num;
		}
		static public void AddUseRedPotion()
		{
			useRedPotion++;
		}

		//赤色ポーションの使用数の取得
		static public int GetUseRedPotion()
		{
			return useRedPotion;
		}

		//青色ポーションの所持数の設定
		static public void SetBluePotion(int num)
		{
			haveBluePotion = num;
		}

		//青色ポーションの所持数の取得
		static public int GetBluePotion()
		{
			return haveBluePotion;
		}

		//青色ポーションの使用数の設定
		static public void SetUseBluePotion(int num)
		{
			useBluePotion = num;
		}
		static public void AddUseBluePotion()
		{
			useBluePotion++;
		}

		//青色ポーションの使用数の取得
		static public int GetUseBluePotion()
		{
			return useBluePotion;
		}

		//黄色ポーションの所持数の設定
		static public void SetYellowPotion(int num)
		{
			haveYellowPotion = num;
		}

		//黄色ポーションの所持数の取得
		static public int GetYellowPotion()
		{
			return haveYellowPotion;
		}

		//黄色ポーションの使用数の設定
		static public void SetUseYellowPotion(int num)
		{
			useYellowPotion = num;
		}
		static public void AddUseYellowPotion()
		{
			useYellowPotion++;
		}

		//黄色ポーションの使用数の取得
		static public int GetUseYellowPotion()
		{
			return useYellowPotion;
		}

		//白色ポーションの所持数の設定
		static public void SetWhitePotion(int num)
		{
			haveWhitePotion = num;
		}

		//白色ポーションの所持数の取得
		static public int GetWhitePotion()
		{
			return haveWhitePotion;
		}

		//白色ポーションの使用数の設定
		static public void SetUseWhitePotion(int num)
		{
			useWhitePotion = num;
		}
		static public void AddUseWhitePotion()
		{
			useWhitePotion++;
		}

		//白色ポーションの使用数の取得
		static public int GetUseWhitePotion()
		{
			return useWhitePotion;
		}

		//取得した経験値の設定
		static public void SetHaveExp(int num)
		{
			haveExp = num;
		}
		static public void SetHaveAllExp(int num)
		{
			haveAllExp += num;
		}

		//取得した経験値の取得
		static public int GetHaveExp()
		{
			return haveExp;
		}
		static public int GetHaveAllExp()
		{
			return haveAllExp;
		}

		//次のレベルまでの経験値を設定
		static public void SetNextExp(int num)
		{
			nextExp = num;
		}

		//次のレベルまでの経験値を取得
		static public int GetNextExp()
		{
			return nextExp;
		}

		//倒した敵数の設定
		static public void SetKillNum(int num)
		{
			killNum += num;
		}

		//倒した敵数の取得
		static public int GetKillNum()
		{
			return killNum;
		}

		//受けた状態異常の回数の設定
		static public void SetCovredTrap(int num)
		{
			coveredTrap += num;
		}

		//受けた状態異常の回数の取得
		static public int GetCoveredTrap()
		{
			return coveredTrap;
		}

		//初期化
		static public void Initialize()
		{
			//決定した武器
			MyWeapon = IconBase.Icon.SWORD;

			//決定したダンジョン
			MyDungeon = IconBase.Icon.DUNGEON_1;

			//プレイヤーのレベル
			plLv = 1;

			//プレイヤーのHP
			plMHp = 20;
			plHp = 20;

			//プレイヤーの攻撃力
			plAttack = 5;

			//プレイヤーの防御力
			plDefence = 5;

			//使用スキルポイント
			skillPt = 0;

			//階層
			currentFloor = 1;

			//総ターン数
			progressTurn = 0;

			//総与攻撃回数
			givingAttack = 0;

			//総与ダメージ量
			givingDamage = 0;

			//総被攻撃回数
			coveredAttack = 0;

			//総被ダメージ量
			coveredDamage = 0;

			//総アイテム取得数
			haveItem = 0;

			//総アイテム使用回数
			useItem = 0;

			//総被回復量
			coveredRecovery = 0;

			//緑色ポーションの所持数
			haveGreenPotion = 0;
			useGreenPotion = 0;

			//赤色ポーションの所持数
			haveRedPotion = 0;
			useRedPotion = 0;

			//青色のポーションの所持数
			haveBluePotion = 0;
			useBluePotion = 0;

			//黄色のポーションの所持数
			haveYellowPotion = 0;
			useYellowPotion = 0;

			//白色のポーションの所持数
			haveWhitePotion = 0;
			useWhitePotion = 0;

			//取得した経験値の設定
			haveExp = 0;

			//次のレベルまでの経験値
			nextExp = 50;

			//時間
			playTime = 0;

			//ステータス
			addHP = 0;
			addATC = 0;
			addDEF = 0;

			//倒した敵
			huntEnemys = new bool[6] { false, false, false, false, false, false };

			//状態異常
			abnormalCondition = new bool[2] { false, false };
		}

		//プレイ時間(フレーム)
		static private float playTime = 0;
		static public void AddTime()
		{
			//経過
			if ((playTime += Time.deltaTime) >= 6000)
			{
				playTime = 5999;
			}
		}
		static public int GetMinutes()
		{
			return (int)playTime / 60;
		}
		static public int GetSeconds()
		{
			return (int)playTime % 60;
		}
		static public string GetTime()
		{
			int minutes = GetMinutes();
			int seconds = GetSeconds();
			string minute = minutes.ToString();
			string second = seconds.ToString();
			if (minutes < 10)
			{
				minute = "0" + minute;
			}
			if (seconds < 10)
			{
				second = "0" + second;
			}
			return minute + ":" + second;
		}

		//振った値
		static private int addHP = 0;
		static private int addATC = 0;
		static private int addDEF = 0;
		static public void SetAddHP(int i)
		{
			addHP = i;
		}
		static public void SetAddATC(int i)
		{
			addATC = i;
		}
		static public void SetAddDEF(int i)
		{
			addDEF = i;
		}
		static public int GetAddHP()
		{
			return addHP;
		}
		static public int GetAddATC()
		{
			return addATC;
		}
		static public int GetAddDEF()
		{
			return addDEF;
		}

		//敵を倒した
		static public void HuntEnemy(string name)
		{
			switch (name)
			{
				case "天使": huntEnemys[0] = true; break;
				case "妖精": huntEnemys[1] = true; break;
				case "ナイト": huntEnemys[2] = true; break;
				case "ミノタウロス": huntEnemys[3] = true; break;
				case "スライム": huntEnemys[4] = true; break;
				case "トレント": huntEnemys[5] = true; break;
			}
		}
		static public bool AllHunt()
		{
			return huntEnemys[0] && huntEnemys[1] && huntEnemys[2] && huntEnemys[3] && huntEnemys[4] && huntEnemys[5];
		}

		//状態異常を踏む
		static public void OnTrap(int num)
		{
			abnormalCondition[num] = true;
		}
		static public bool AllTrap()
		{
			return abnormalCondition[0] && abnormalCondition[1];
		}

		//金剛
		static public void Vajra(int num)
		{
			if (num >= 5)
			{
				vajra = true;
			}
		}
		static public bool IsVajra()
		{
			return vajra;
		}
	}
}
