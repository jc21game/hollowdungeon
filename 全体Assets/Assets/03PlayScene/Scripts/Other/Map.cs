using ScoreManager;
using UnityEngine;

//マップ
public class Map : MonoBehaviour
{
	//生成マップ
	private MakeMap makeMap = null;

	//マップ情報
	public MAP mapInfomation = null;

	//壁
	private GameObject wall = null;

	//床
	private GameObject room = null;

	//ギミックの設置位置管理(0:なし、1:ダメージ(毒/マグマ)、2:沼、3:氷)
	public int[,] settingTrap = null;

	//ギミック
	private const float gMin = 8;
	private const float gMax = 15;
	private Vector3 gSize;

	//ダメージマス
	private GameObject[] damage = null;
	public Vector2 damagePosition;
	public GameObject[] damages = null;
	public int damageNum = 0;

	//スローマス
	private GameObject slow = null;
	public Vector2 slowPosition;
	public GameObject[] slows = null;
	public int slowNum = 0;

	//スライドマス
	private GameObject ice = null;
	public Vector2 icePosition;
	public GameObject[] ices = null;
	public int iceNum = 0;

	//階段
	private GameObject stairs = null;
	public Vector2 stairsPosition;

	//マップ情報
	private int[,] map;

	//マネージャー
	private PlaySceneManager manager = null;

	//アイテム
	public GameObject[] potions = null;
	public int maxPotions = 0;

	//初期化
	void Start()
	{
		//生成マップ
		makeMap = new MakeMap();
		if (Score.GetFloor() != Score.MaxFloor)
		{
			//通常マップ
			makeMap.Create();
		}
		else
		{
			//ボスマップ
			makeMap.CreateCSV();
		}

		//マップ情報
		mapInfomation = new MAP();

		//壁プレハブを取得
		wall = (GameObject)Resources.Load("Map/Wall");

		//床プレハブを取得
		room = (GameObject)Resources.Load("Map/Room");

		//生成
		settingTrap = new int[makeMap.GetMapSize().x, makeMap.GetMapSize().y];
		for (int x = 0; x < makeMap.GetMapSize().x; x++)
		{
			for (int y = 0; y < makeMap.GetMapSize().y; y++)
			{
				//ギミックなし
				settingTrap[x, y] = 0;
			}
		}

		//大きさ
		gSize = new Vector3(1.5f, 1.5f, 1);

		//ダメージプレハブを取得(毒、マグマ)
		damage = new GameObject[2];
		damage[0] = (GameObject)Resources.Load("Map/Poison");
		damage[1] = (GameObject)Resources.Load("Map/Magma");
		damagePosition = new Vector2(0, 0);

		//生成するダメージマスの個数をランダムで決める
		damageNum = (int)Random.Range(gMin, gMax);

		//ダンジョン2の1Fと2Fと5F(最終層)には生成しない(ダメージマス)
		if ((Score.GetMyDungeon() == IconBase.Icon.DUNGEON_2 && Score.GetFloor() < Score.ChangeFloor) || Score.GetFloor() == Score.MaxFloor)
		{
			damages = new GameObject[0];
			damageNum = 0;
		}
		else
		{
			damages = new GameObject[damageNum];

			//個数分(ダメージマス)
			for (int i = 0; i < damageNum; i++)
			{
				//プレハブセット
				damages[i] = damage[(int)Score.GetMyDungeon() - 4];
			}
		}

		//スロープレハブを取得
		slow = (GameObject)Resources.Load("Map/Slow");
		slowPosition = new Vector2(0, 0);

		//生成するスローマスの個数をランダムで決める
		slowNum = (int)Random.Range(gMin, gMax);

		//ダンジョン2と5F(最終層)には生成しない(スローマス)
		if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_2 || Score.GetFloor() == Score.MaxFloor)
		{
			slows = new GameObject[0];
			slowNum = 0;
		}
		else
		{
			slows = new GameObject[slowNum];

			//個数分(スローマス)
			for (int i = 0; i < slowNum; i++)
			{
				//プレハブセット
				slows[i] = slow;
			}
		}

		//スライドプレハブを取得
		ice = (GameObject)Resources.Load("Map/Ice");
		icePosition = new Vector2(0, 0);

		//生成するスライドマスの個数をランダムで決める
		iceNum = (int)Random.Range(gMin, gMax);

		//ダンジョン1とダンジョン2の3Fと4Fと5Fには生成しない(スライドマス)
		if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1 || (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_2 && Score.GetFloor() >= Score.ChangeFloor) ||
		Score.GetFloor() == Score.MaxFloor)
		{
			ices = new GameObject[0];
			iceNum = 0;
		}
		else
		{
			ices = new GameObject[iceNum];

			//個数分(スライドマス)
			for (int i = 0; i < iceNum; i++)
			{
				//プレハブセット
				ices[i] = ice;
			}
		}

		//階段プレハブを取得
		stairs = (GameObject)Resources.Load("Map/Stairs");
		stairsPosition = new Vector2(0, 0);
		stairs.GetComponent<SpriteRenderer>().sortingOrder = 1;

		//マネージャー
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();

		//マップ生成
		MakeMap();

		//アイテム生成
		ItemCreate();
	}

	//更新
	void Update()
	{

	}

	//マップ生成
	private void MakeMap()
	{
		//容量づくり
		map = new int[makeMap.GetMapSize().x, makeMap.GetMapSize().y];

		//書き込み
		for (int x = 0; x < makeMap.GetMapSize().x; x++)
		{
			for (int y = 0; y < makeMap.GetMapSize().y; y++)
			{
				map[x, y] = makeMap.GetMap((x, y));
			}
		}

		//マップ生成
		for (int y = 0; y < map.GetLength(1); y++)
		{
			for (int x = 0; x < map.GetLength(0); x++)
			{
				//壁生成
				if (map[x, y] == mapInfomation.WALL)
				{
					Instantiate(wall, new Vector3(x, -y, 0), Quaternion.identity).transform.SetParent(gameObject.transform, false);
				}

				//床生成
				else
				{
					Instantiate(room, new Vector3(x, -y, 0), Quaternion.identity).transform.SetParent(gameObject.transform, false);
				}
			}
		}

		//位置
		while (true)
		{
			//位置決定
			stairsPosition.x = Random.Range(1, makeMap.GetMapSize().x - 1);
			stairsPosition.y = Random.Range(-makeMap.GetMapSize().y + 1, -1);
			if (Score.GetFloor() == Score.MaxFloor)
			{
				stairsPosition.x = 0;
				stairsPosition.y = 0;
				break;
			}

			//部屋以外の場合
			if (map[(int)stairsPosition.x, -(int)stairsPosition.y] < mapInfomation.ROOM)
			{
				//NO
				continue;
			}

			//OK
			break;
		}

		//決められた個数分(ダメージマス)
		for (int i = 0; i < damageNum; i++)
		{
			//位置(ダメージマス)
			while (true)
			{
				//位置決定
				damagePosition.x = Random.Range(1, makeMap.GetMapSize().x - 1);
				damagePosition.y = Random.Range(-makeMap.GetMapSize().y + 1, -1);

				//部屋以外の場合 または ギミックが設置されていたら または 階段が設置されていたら
				if (map[(int)damagePosition.x, -(int)damagePosition.y] < mapInfomation.ROOM || settingTrap[(int)damagePosition.x, -(int)damagePosition.y] > 0 || (damagePosition == stairsPosition))
				{
					//NO
					continue;
				}

				//生成
				damages[i] = Instantiate(damage[(int)Score.GetMyDungeon() - 4], new Vector3(damagePosition.x, damagePosition.y, -1), Quaternion.identity);
				damages[i].transform.SetParent(manager.objects.transform, false);
				damages[i].transform.localScale = gSize;

				//書き込み(ダメージギミック)
				settingTrap[(int)damagePosition.x, -(int)damagePosition.y] = 1;

				//OK
				break;
			}
		}

		//決められた個数分(スローマス)
		for (int i = 0; i < slowNum; i++)
		{
			//位置(スローマス)
			while (true)
			{
				//位置決定
				slowPosition.x = Random.Range(1, makeMap.GetMapSize().x - 1);
				slowPosition.y = Random.Range(-makeMap.GetMapSize().y + 1, -1);

				//部屋以外の場合 または ギミックが設置されていたら または 階段が設置されていたら
				if (map[(int)slowPosition.x, -(int)slowPosition.y] < mapInfomation.ROOM || settingTrap[(int)slowPosition.x, -(int)slowPosition.y] > 0 || (slowPosition == stairsPosition))
				{
					//NO
					continue;
				}

				//生成
				slows[i] = Instantiate(slow, new Vector3(slowPosition.x, slowPosition.y, -1), Quaternion.identity);
				slows[i].transform.SetParent(manager.objects.transform, false);
				slows[i].transform.localScale = gSize;

				//書き込み(スローギミック)
				settingTrap[(int)slowPosition.x, -(int)slowPosition.y] = 2;

				//OK
				break;
			}
		}

		//決められた個数分(スライドマス)
		for (int i = 0; i < iceNum; i++)
		{
			//位置(スライドマス)
			while (true)
			{
				//位置決定
				icePosition.x = Random.Range(1, makeMap.GetMapSize().x - 1);
				icePosition.y = Random.Range(-makeMap.GetMapSize().y + 1, -1);

				//部屋以外の場合 または ギミックが設置されていたら または 階段が設置されていたら
				if (map[(int)icePosition.x, -(int)icePosition.y] < mapInfomation.ROOM || settingTrap[(int)icePosition.x, -(int)icePosition.y] > 0 || (icePosition == stairsPosition))
				{
					//NO
					continue;
				}

				//生成
				ices[i] = Instantiate(ice, new Vector3(icePosition.x, icePosition.y, -1), Quaternion.identity);
				ices[i].transform.SetParent(manager.objects.transform, false);
				ices[i].transform.localScale = gSize;

				//書き込み(スライドギミック)
				settingTrap[(int)icePosition.x, -(int)icePosition.y] = 3;

				//OK
				break;
			}
		}

		//生成
		if (Score.GetFloor() != Score.MaxFloor)
		{
			GameObject game = Instantiate(stairs, new Vector3(stairsPosition.x, stairsPosition.y, 0), Quaternion.identity);
			game.transform.SetParent(manager.objects.transform, false);
			game.transform.localScale = new Vector3(1.5f, 1.5f, 1);
		}
	}

	//アイテム生成
	private void ItemCreate()
	{
		//個数
		maxPotions = Random.Range(0, (int)gMax);
		if (Score.GetFloor() == Score.MaxFloor)
		{
			maxPotions = 0;
		}
		potions = new GameObject[maxPotions];

		//座標
		Vector2 position;

		//生成
		for (int i = 0; i < maxPotions; i++)
		{
			//位置
			position = Spawn(-1);

			//アイテムの種類
			potions[i] = Instantiate(manager.potions[Random.Range(0, manager.potions.Length)]);

			//親
			potions[i].transform.SetParent(manager.objects.transform, false);

			//座標
			potions[i].transform.position = position;

			//オーダー
			potions[i].GetComponent<SpriteRenderer>().sortingOrder = 1;
		}
	}

	//生成
	public Vector2 Spawn(int num)
	{
		//生成位置
		Vector2 position = new Vector2(0, 0);

		//キャラと重なっていないか
		bool check = true;

		//位置
		while (true)
		{
			//位置決定
			position.x = Random.Range(1, makeMap.GetMapSize().x - 1);
			position.y = Random.Range(-makeMap.GetMapSize().y + 1, -1);

			//部屋以外の場合
			if (map[(int)position.x, -(int)position.y] < mapInfomation.ROOM)
			{
				//NO
				continue;
			}

			//階段の場合
			if (position.x == stairs.transform.position.x && position.y == stairs.transform.position.y)
			{
				//NO
				continue;
			}

			//アイテム
			if (num == -1)
			{
				//OK
				break;
			}

			//キャラクターが存在する場合
			for (int number = 0; number < manager.GetMaxChar(); number++)
			{
				//キャラクターが存在する場合
				if (position == manager.GetPosition(number))
				{
					//フラグ変更
					check = false;
					break;
				}
			}

			//キャラと重なっている
			if (check == false)
			{
				//フラグ変更
				check = true;
				continue;
			}

			//OK
			break;
		}

		//生成位置を返す
		return position;
	}

	//移動可能か
	public bool IsMove(Vector2 position)
	{
		//壁の場合
		if (map[(int)position.x, -(int)position.y] == mapInfomation.WALL)
		{
			//NO
			return false;
		}

		//キャラクターが存在する場合
		for (int number = 0; number < manager.GetMaxChar(); number++)
		{
			//キャラクターが存在する場合
			if (position == manager.GetPosition(number))
			{
				//NO
				return false;
			}
		}

		//OK
		return true;
	}

	//マップ情報取得
	public int GetMap(Vector2 vector)
	{
		//マップ情報
		return GetMap((int)vector.x, -(int)vector.y);
	}
	public int GetMap(int x, int y)
	{
		//マップ情報
		return map[x, y];
	}
	public int GetTrap(int x, int y)
	{
		//ギミック情報
		return settingTrap[x, y];
	}
	public int GetWidth()
	{
		//幅
		return makeMap.GetMapSize().x;
	}
	public int GetHeight()
	{
		//高さ
		return makeMap.GetMapSize().y;
	}

	//階段の位置
	public Vector2 GetStairsPosition()
	{
		//階段の位置
		return stairsPosition;
	}

	//マップ情報
	public class MAP
	{
		//壁
		public int WALL = 0;

		//通路
		public int AISLE = 1;

		//部屋
		public int ROOM = 2;
	}

	//画面に映る範囲か
	public bool IsExist(Vector2 vector)
	{
		//w:9 h:5以内の場合
		if (Mathf.Abs(manager.GetPosition(0).x - vector.x) <= 9 && Mathf.Abs(manager.GetPosition(0).y - vector.y) <= 5)
		{
			//映る
			return true;
		}

		//映っていない
		return false;
	}

	//ポーションの削除
	public void Remove(int number)
	{
		Destroy(potions[number]);

		//移動
		for (; number < maxPotions - 1; number++)
		{
			//移動
			potions[number] = potions[number + 1];
		}

		maxPotions--;
	}
}
