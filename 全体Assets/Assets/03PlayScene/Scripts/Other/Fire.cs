using UnityEngine;
using UnityEngine.UI;

//火の粉
public class Fire : MonoBehaviour
{
	//位置
	private RectTransform fire = null;

	//落下速度
	public float speedX = 0;
	public float speedY = 0;
	public float valX = 0;
	public float valY = 0;
	public float alpha = 0;
	public float size = 0;

	//色
	public Color[] colors = null;

	//生存位置
	private (float min, float max) x = (0, 0);
	private (float min, float max) y = (0, 0);

	//方向
	private int dir = 1;

	//スタート
	public void Start()
	{
		//生存位置
		x = (-560, 650);
		y = (350, -300);

		//アタッチ
		fire = gameObject.GetComponent<RectTransform>();

		//初期位置
		fire.anchoredPosition = new Vector3(Random.Range(x.min, x.max), y.max, 0);

		//サイズ
		float size = Random.Range(0, 0.1f);
		speedY -= (fire.sizeDelta.y - size) / 2f;
		fire.sizeDelta = new Vector2(fire.sizeDelta.x - size, fire.sizeDelta.y - size);

		//落下方向
		speedX -= Random.Range(-valX, valX);
		if (Random.Range(0, 2) == 0)
		{
			dir = -1;
		}

		//色
		gameObject.GetComponent<Image>().color = colors[Random.Range(0, colors.Length)];
	}

	//更新
	public void Update()
	{
		//落下
		Vector3 vector = fire.anchoredPosition;
		vector.x -= speedX * dir/* - Random.Range(0, valX)*/;
		vector.y += speedY/* - Random.Range(0, valY)*/;
		fire.anchoredPosition = vector;

		//色
		Color color = gameObject.GetComponent<Image>().color;
		color.a -= alpha;
		gameObject.GetComponent<Image>().color = color;

		//サイズ
		Vector2 localSize = fire.sizeDelta;
		localSize.x = localSize.y -= alpha / size;
		fire.sizeDelta = localSize;

		//生存限界
		if (color.a <= 0)
		{
			Destroy(gameObject);
		}
	}
}
