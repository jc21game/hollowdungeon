using UnityEngine;
using UnityEngine.UI;

//ステータス
public class Status : MonoBehaviour
{
	//プレイヤー
	private Player player = null;

	//自身
	private Text text = null;

	//初期化
	public void Start()
	{
		//プレイヤー
		player = GameObject.Find("Player(Clone)").GetComponent<Player>();

		//自身
		text = gameObject.GetComponent<Text>();
	}

	//更新
	public void Update()
	{
		//情報
		text.text = "LV:" + player.LV.ToString() + "\nHP:" + player.HP.ToString() + "/" + player.MAX_HP.ToString() + "\n攻撃力:" + player.ATC.ToString() + "\n防御力:" + player.DEF + "\nEXP:" + player.EXP.ToString() + "\n次のレベルまで:" + (player.nextEXP - player.EXP).ToString() + "\nスキルポイント:" + player.skillPoint.ToString();
	}
}
