using UnityEngine;

//マップ生成ロジック
public class MakeMap
{
	//マップ内情報
	private class MapData
	{
		//壁
		public const int WALL = 0;

		//通路(仮)
		public const int AISLE_TENT = -1;

		//通路
		public const int AISLE = 1;

		//部屋
		public const int ROOM = 2;
	}

	//マップサイズ
	private int[,] map;
	private int width, height;

	//マップと同じサイズの踏破したかを記憶
	private bool[,] breakThrough;

	//マップのバックアップ
	private int[,] backup;

	//マップサイズ
	private int mapSizeMin = 3;
	private int mapSizeMax = 5;

	//単位
	private const int UNIT = 16;

	//上下左右
	private const int DIRECTIONS = 4;

	//マップ生成
	public void Create()
	{
		//マップ生成
		do
		{
			//マップ生成
			CreateNomalMap();
		} while (!AllGreen());
	}

	//マップ生成通常
	private void CreateNomalMap()
	{
		//マップサイズの作成(3 〜 4)
		width = Random.Range(mapSizeMin, mapSizeMax) * UNIT;
		height = Random.Range(mapSizeMin, mapSizeMax) * UNIT;
		map = new int[width, height];

		//踏破用マップとバックアップ用マップも生成
		breakThrough = new bool[width, height];
		backup = new int[width, height];

		//部屋数(5 〜 9)
		int rooms = (width + height) / UNIT - 1;

		//通路の方向(true:水平 / false:垂直)
		bool aisleDir = true;

		//幅のほうが広い場合
		if (width > height)
		{
			//最初の通路の方向を垂直にする
			aisleDir = false;
		}

		//通路作成位置
		int aislePos = 0;

		//通路同士の間(10 〜 15マス空く)
		int betweenAisleMin = 11;
		int betweenAisleMax = 15;

		//部屋に対する通路の位置
		int aisleDistance = 3;

		//区画
		Rect rect = new Rect(0, 0, width - 1, height - 1);

		//部屋用区画
		Rect miniRect = new Rect();

		//最後に作ったライン
		Rect finalLine = new Rect();

		//部屋
		Rect room = new Rect();

		//通路から部屋までの最短距離
		int distance = 2;

		//最低部屋の広さ(6 * 6)
		int roomSizeMin = 6;

		//部屋の番号記憶
		int roomNumber = MapData.ROOM;

		//部屋管理
		Rect[] roomArray = new Rect[rooms];

		//通路を設置するか
		bool[] isSet = new bool[DIRECTIONS] { false, false, false, false };

		//通路の最長
		int lengthMax = 20;

		//部屋の作成
		for (int i = 0; i < rooms; i++)
		{
			//部屋作成
			roomArray[i] = new Rect();
		}

		//初期化
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				map[x, y] = MapData.WALL;
			}
		}

		//通路(仮)と部屋作成
		{
			//通路作成(部屋の数 - 1ライン通路を作成)
			for (int i = 0; i < rooms - 1; i++)
			{
				//区画をコピー
				miniRect.CopyFrom(rect);

				//生成方向が水平
				if (aisleDir)
				{
					//位置設定
					int min, max;
					min = rect.GetRect(Rect.RECT.TOP);
					max = rect.GetRect(Rect.RECT.BOTTOM);
					aislePos = Random.Range(min, max);

					//狭いほうに近づける
					//上の方に近い場合
					if (aislePos - rect.GetRect(Rect.RECT.TOP) < rect.GetRect(Rect.RECT.BOTTOM) - aislePos)
					{
						//最低幅より近くにある場合
						if (aislePos - rect.GetRect(Rect.RECT.TOP) < betweenAisleMin)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.TOP) + betweenAisleMin;
						}

						//最高幅より遠くにある場合
						if (aislePos - rect.GetRect(Rect.RECT.TOP) > betweenAisleMax)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.TOP) + betweenAisleMax;
						}

						//rectのTOPをaislePosに更新
						rect.SetRect(Rect.RECT.TOP, aislePos);

						//miniRectのBOTTOMをrectのTOPに更新
						miniRect.SetRect(Rect.RECT.BOTTOM, rect.GetRect(Rect.RECT.TOP));
					}

					//下の方に近い場合
					else
					{
						//最低幅より近くにある場合
						if (rect.GetRect(Rect.RECT.BOTTOM) - aislePos < betweenAisleMax)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.BOTTOM) - betweenAisleMax;
						}

						//最高幅より遠くにある場合
						if (rect.GetRect(Rect.RECT.BOTTOM) - aislePos > betweenAisleMax)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.BOTTOM) - betweenAisleMax;
						}

						//rectのBottomをaislePosに更新
						rect.SetRect(Rect.RECT.BOTTOM, aislePos);

						//miniRectのTOPをrectのBOTTOMに更新
						miniRect.SetRect(Rect.RECT.TOP, rect.GetRect(Rect.RECT.BOTTOM));
					}

					//通路作成
					for (int x = rect.GetRect(Rect.RECT.LEFT); x < rect.GetRect(Rect.RECT.RIGHT); x++)
					{
						//壁を通路に置き換え
						map[x, aislePos] = MapData.AISLE_TENT;
					}

					//最後に作ったライン更新
					finalLine.Reset(rect.GetRect(Rect.RECT.LEFT), aislePos, rect.GetRect(Rect.RECT.RIGHT), aislePos);
				}

				//生成方向が垂直
				else
				{
					//位置設定
					int min, max;
					min = rect.GetRect(Rect.RECT.LEFT);
					max = rect.GetRect(Rect.RECT.RIGHT);
					aislePos = Random.Range(min, max);

					//狭いほうに近づける
					//左の方に近い場合
					if (aislePos - rect.GetRect(Rect.RECT.LEFT) < rect.GetRect(Rect.RECT.RIGHT) - aislePos)
					{
						//最低幅より近くにある場合
						if (aislePos - rect.GetRect(Rect.RECT.LEFT) < betweenAisleMin)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.LEFT) + betweenAisleMin;
						}

						//最高幅より遠くにある場合
						if (aislePos - rect.GetRect(Rect.RECT.LEFT) > betweenAisleMax)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.LEFT) + betweenAisleMax;
						}

						//rectのLEFTをaislePosに更新
						rect.SetRect(Rect.RECT.LEFT, aislePos);

						//miniRectのRIGHTをrectのLEFTに更新
						miniRect.SetRect(Rect.RECT.RIGHT, rect.GetRect(Rect.RECT.LEFT));
					}

					//右の方に近い場合
					else
					{
						//最低幅より近くにある場合
						if (rect.GetRect(Rect.RECT.RIGHT) - aislePos < betweenAisleMin)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.RIGHT) - betweenAisleMin;
						}

						//最高幅より遠くにある場合
						if (rect.GetRect(Rect.RECT.RIGHT) - aislePos > betweenAisleMax)
						{
							//最低幅に設定
							aislePos = rect.GetRect(Rect.RECT.RIGHT) - betweenAisleMax;
						}

						//rectのRIGHTをaislePosに更新
						rect.SetRect(Rect.RECT.RIGHT, aislePos);

						//miniRectのLEFTをrectのRIGHTに更新
						miniRect.SetRect(Rect.RECT.LEFT, rect.GetRect(Rect.RECT.RIGHT));
					}

					//通路作成
					for (int y = rect.GetRect(Rect.RECT.TOP); y < rect.GetRect(Rect.RECT.BOTTOM); y++)
					{
						//壁を通路に置き換え
						map[aislePos, y] = MapData.AISLE_TENT;
					}

					//最後に作ったライン更新
					finalLine.Reset(aislePos, rect.GetRect(Rect.RECT.TOP), aislePos, rect.GetRect(Rect.RECT.BOTTOM));
				}

				//通路の方向(true:水平 / false:垂直)
				aisleDir = true;

				//幅のほうが広い場合
				if (rect.GetWidth() > rect.GetHeight())
				{
					//最初の通路の方向を垂直にする
					aisleDir = false;
				}

				//部屋の位置最大を設定
				room.SetRect(Rect.RECT.LEFT, miniRect.GetRect(Rect.RECT.LEFT) + distance);
				room.SetRect(Rect.RECT.TOP, miniRect.GetRect(Rect.RECT.TOP) + distance);
				room.SetRect(Rect.RECT.RIGHT, miniRect.GetRect(Rect.RECT.RIGHT) - distance);
				room.SetRect(Rect.RECT.BOTTOM, miniRect.GetRect(Rect.RECT.BOTTOM) - distance);

				//部屋の完全な位置設定
				room.SetRect(Rect.RECT.LEFT, Random.Range(room.GetRect(Rect.RECT.LEFT), room.GetRect(Rect.RECT.RIGHT) - roomSizeMin));
				room.SetRect(Rect.RECT.TOP, Random.Range(room.GetRect(Rect.RECT.TOP), room.GetRect(Rect.RECT.BOTTOM) - roomSizeMin));
				room.SetRect(Rect.RECT.RIGHT, Random.Range(room.GetRect(Rect.RECT.LEFT) + roomSizeMin + 1, room.GetRect(Rect.RECT.RIGHT)));
				room.SetRect(Rect.RECT.BOTTOM, Random.Range(room.GetRect(Rect.RECT.TOP) + roomSizeMin + 1, room.GetRect(Rect.RECT.BOTTOM)));

				//壁を部屋に置き換え
				for (int x = room.GetRect(Rect.RECT.LEFT) + 1; x < room.GetRect(Rect.RECT.LEFT) + room.GetWidth(); x++)
				{
					for (int y = room.GetRect(Rect.RECT.TOP) + 1; y < room.GetRect(Rect.RECT.TOP) + room.GetHeight(); y++)
					{
						//壁を部屋に置き換え
						map[x, y] = roomNumber;
					}
				}

				//部屋記憶
				roomArray[roomNumber - MapData.ROOM].CopyFrom(room);

				//部屋番号更新
				roomNumber++;
			}

			//四辺を壁に置き換える
			for (int x = 0; x < width; x++)
			{
				//壁(仮)を壁に置き換え
				map[x, 0] = MapData.WALL;
				map[x, height - 1] = MapData.WALL;
			}
			for (int y = 0; y < height; y++)
			{
				//壁(仮)を壁に置き換え
				map[0, y] = MapData.WALL;
				map[width - 1, y] = MapData.WALL;
			}

			//最後の一部屋の位置設定
			room.SetRect(Rect.RECT.LEFT, rect.GetRect(Rect.RECT.LEFT) + distance);
			room.SetRect(Rect.RECT.TOP, rect.GetRect(Rect.RECT.TOP) + distance);
			room.SetRect(Rect.RECT.RIGHT, rect.GetRect(Rect.RECT.RIGHT) - distance);
			room.SetRect(Rect.RECT.BOTTOM, rect.GetRect(Rect.RECT.BOTTOM) - distance);

			//部屋の完全な位置設定
			room.SetRect(Rect.RECT.LEFT, Random.Range(room.GetRect(Rect.RECT.LEFT), room.GetRect(Rect.RECT.RIGHT) - roomSizeMin));
			room.SetRect(Rect.RECT.TOP, Random.Range(room.GetRect(Rect.RECT.TOP), room.GetRect(Rect.RECT.BOTTOM) - roomSizeMin));
			room.SetRect(Rect.RECT.RIGHT, Random.Range(room.GetRect(Rect.RECT.LEFT) + roomSizeMin + 1, room.GetRect(Rect.RECT.RIGHT)));
			room.SetRect(Rect.RECT.BOTTOM, Random.Range(room.GetRect(Rect.RECT.TOP) + roomSizeMin + 1, room.GetRect(Rect.RECT.BOTTOM)));

			//壁を部屋に置き換え
			for (int x = room.GetRect(Rect.RECT.LEFT) + 1; x < room.GetRect(Rect.RECT.LEFT) + room.GetWidth(); x++)
			{
				for (int y = room.GetRect(Rect.RECT.TOP) + 1; y < room.GetRect(Rect.RECT.TOP) + room.GetHeight(); y++)
				{
					//壁を部屋に置き換え
					map[x, y] = roomNumber;
				}
			}

			//部屋記憶
			roomArray[roomNumber - MapData.ROOM].CopyFrom(room);

			//部屋番号更新
			roomNumber++;

			//最後に作ったラインを消去
			//垂直
			if (finalLine.GetRect(Rect.RECT.LEFT) == finalLine.GetRect(Rect.RECT.RIGHT))
			{
				for (int y = finalLine.GetRect(Rect.RECT.TOP) + 1; y < finalLine.GetRect(Rect.RECT.BOTTOM); y++)
				{
					//通路(仮)を壁に置き換え
					map[finalLine.GetRect(Rect.RECT.LEFT), y] = MapData.WALL;
				}
			}

			//水平
			if (finalLine.GetRect(Rect.RECT.TOP) == finalLine.GetRect(Rect.RECT.BOTTOM))
			{
				for (int x = finalLine.GetRect(Rect.RECT.LEFT) + 1; x < finalLine.GetRect(Rect.RECT.RIGHT); x++)
				{
					//通路(仮)を壁に置き換え
					map[x, finalLine.GetRect(Rect.RECT.TOP)] = MapData.WALL;
				}
			}
		}

		//部屋から通路までの通路作成
		{
			//部屋から通路までの通路作成
			for (int i = 0; i < rooms; i++)
			{
				//左の通路の位置設定
				aislePos = Random.Range(roomArray[i].GetRect(Rect.RECT.TOP) + aisleDistance, roomArray[i].GetRect(Rect.RECT.BOTTOM) - aisleDistance);

				//左
				for (int x = roomArray[i].GetRect(Rect.RECT.LEFT); x > 0; x--)
				{
					//部屋チェック
					if (map[x, aislePos - 1] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[x, aislePos] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[x, aislePos + 1] >= MapData.ROOM)
					{
						//処理なし
						break;
					}

					//通路チェック
					if (map[x, aislePos] == MapData.AISLE_TENT || map[x, aislePos] == MapData.AISLE)
					{
						//通路を作成
						for (x = roomArray[i].GetRect(Rect.RECT.LEFT); map[x, aislePos] == MapData.WALL; x--)
						{
							//壁を通路に置き換え
							map[x, aislePos] = MapData.AISLE;
						}
						map[x, aislePos] = MapData.AISLE;

						//処理終了
						break;
					}
				}

				//上の通路の位置設定
				aislePos = Random.Range(roomArray[i].GetRect(Rect.RECT.LEFT) + aisleDistance, roomArray[i].GetRect(Rect.RECT.RIGHT) - aisleDistance);

				//上
				for (int y = roomArray[i].GetRect(Rect.RECT.TOP); y > 0; y--)
				{
					//部屋チェック
					if (map[aislePos - 1, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[aislePos, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[aislePos + 1, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}

					//通路チェック
					if (map[aislePos, y] == MapData.AISLE_TENT || map[aislePos, y] == MapData.AISLE)
					{
						//通路を作成
						for (y = roomArray[i].GetRect(Rect.RECT.TOP); map[aislePos, y] == MapData.WALL; y--)
						{
							//壁を通路に置き換え
							map[aislePos, y] = MapData.AISLE;
						}
						map[aislePos, y] = MapData.AISLE;

						//処理終了
						break;
					}
				}

				//右の通路の位置設定
				aislePos = Random.Range(roomArray[i].GetRect(Rect.RECT.TOP) + aisleDistance, roomArray[i].GetRect(Rect.RECT.BOTTOM) - aisleDistance);

				//右
				for (int x = roomArray[i].GetRect(Rect.RECT.RIGHT); x < width; x++)
				{
					//部屋チェック
					if (map[x, aislePos - 1] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[x, aislePos] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[x, aislePos + 1] >= MapData.ROOM)
					{
						//処理なし
						break;
					}

					//通路チェック
					if (map[x, aislePos] == MapData.AISLE_TENT || map[x, aislePos] == MapData.AISLE)
					{
						//通路を作成
						for (x = roomArray[i].GetRect(Rect.RECT.RIGHT); map[x, aislePos] == MapData.WALL; x++)
						{
							//壁を通路に置き換え
							map[x, aislePos] = MapData.AISLE;
						}
						map[x, aislePos] = MapData.AISLE;

						//処理終了
						break;
					}
				}

				//下の通路の位置設定
				aislePos = Random.Range(roomArray[i].GetRect(Rect.RECT.LEFT) + aisleDistance, roomArray[i].GetRect(Rect.RECT.RIGHT) - aisleDistance);

				//下
				for (int y = roomArray[i].GetRect(Rect.RECT.BOTTOM); y < height; y++)
				{
					//部屋チェック
					if (map[aislePos - 1, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[aislePos, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}
					if (map[aislePos + 1, y] >= MapData.ROOM)
					{
						//処理なし
						break;
					}

					//通路チェック
					if (map[aislePos, y] == MapData.AISLE_TENT || map[aislePos, y] == MapData.AISLE)
					{
						//通路を作成
						for (y = roomArray[i].GetRect(Rect.RECT.BOTTOM); map[aislePos, y] == MapData.WALL; y++)
						{
							//壁を通路に置き換え
							map[aislePos, y] = MapData.AISLE;
						}
						map[aislePos, y] = MapData.AISLE;

						//処理終了
						break;
					}
				}
			}

			//通路を確定
			for (int x = 1; x < width - 1; x++)
			{
				for (int y = 1; y < height - 1; y++)
				{
					//自身のマスがMAP_DATA.AISLEの場合
					if (map[x, y] == MapData.AISLE)
					{
						//左
						for (int i = x - 1; i > 0; i--)
						{
							//自身のマスがMapData.AISLE_TENTの場合
							if (map[i, y] == MapData.AISLE_TENT)
							{
								//処理なし
								continue;
							}

							//自身のマスがMapData.AISLEの場合
							if (map[i, y] == MapData.AISLE)
							{
								//そのマスまで通路確定
								for (; i < x; i++)
								{
									//壁(仮)を通路に置き換え
									map[i, y] = MapData.AISLE;
								}
							}

							//処理終了またはそれ以外
							break;
						}

						//上
						for (int j = y - 1; j > 0; j--)
						{
							//自身のマスがMapData.AISLE_TENTの場合
							if (map[x, j] == MapData.AISLE_TENT)
							{
								//処理なし
								continue;
							}

							//自身のマスがMapData.AISLEの場合
							if (map[x, j] == MapData.AISLE)
							{
								//そのマスまで通路確定
								for (; j < y; j++)
								{
									//壁(仮)を通路に置き換え
									map[x, j] = MapData.AISLE;
								}
							}

							//処理終了またはそれ以外
							break;
						}

						//右
						for (int i = x + 1; i < width - 1; i++)
						{
							//自身のマスがMapData.AISLE_TENTの場合
							if (map[i, y] == MapData.AISLE_TENT)
							{
								//処理なし
								continue;
							}

							//自身のマスがMapData.AISLEの場合
							if (map[i, y] == MapData.AISLE)
							{
								//そのマスまで通路確定
								for (; i > x; i--)
								{
									//壁(仮)を通路に置き換え
									map[i, y] = MapData.AISLE;
								}
							}

							//処理終了またはそれ以外
							break;
						}

						//下
						for (int j = y + 1; j < height - 1; j++)
						{
							//自身のマスがMapData.AISLE_TENTの場合
							if (map[x, j] == MapData.AISLE_TENT)
							{
								//処理なし
								continue;
							}

							//自身のマスがMapData.AISLEの場合
							if (map[x, j] == MapData.AISLE)
							{
								//そのマスまで通路確定
								for (; j > y; j--)
								{
									//壁(仮)を通路に置き換え
									map[x, j] = MapData.AISLE;
								}
							}

							//処理終了またはそれ以外
							break;
						}
					}
				}
			}

			//通路(仮)を削除
			for (int x = 1; x < width - 1; x++)
			{
				for (int y = 1; y < height - 1; y++)
				{
					//自身のマスがMapData.AISLE_TENTの場合
					if (map[x, y] == MapData.AISLE_TENT)
					{
						//壁(仮)を壁に置き換え
						map[x, y] = MapData.WALL;
					}
				}
			}
		}

		//通路整理
		{
			//部屋に対する通路の個数チェック
			foreach (Rect r in roomArray)
			{
				//通路の位置
				Rect[] posAisle = new Rect[5];

				//2回実行
				for (int n = 1; n < 3; n++)
				{
					//通路の個数
					int roomAisle = 0;

					//通路の位置の作成
					for (int i = 0; i < 5; i++)
					{
						//通路の位置作成
						posAisle[i] = new Rect();
					}

					//通路の個数計上
					for (int x = r.GetRect(Rect.RECT.LEFT); x <= r.GetRect(Rect.RECT.RIGHT); x++)
					{
						for (int y = r.GetRect(Rect.RECT.TOP); y <= r.GetRect(Rect.RECT.BOTTOM); y++)
						{
							//通路の場合
							if (map[x, y] == MapData.AISLE)
							{
								//座標記憶
								posAisle[roomAisle].Reset(x, y, 0, 0);

								//通路の個数を増やす
								roomAisle++;
							}
						}
					}

					//通路の個数が3個以上の場合かつ70%の確率
					if (roomAisle >= 3 && Random.Range(1, 101) < 100)
					{
						//対象通路
						int select = Random.Range(0, roomAisle);

						//通路を消していく
						while (true)
						{
							//通路の個数を初期化
							roomAisle = 0;

							//上下左右で通路の個数を計上
							for (int i = 0; i < DIRECTIONS; i++)
							{
								//通路の場合
								if (map[GetPos(posAisle[select].GetRect(Rect.RECT.LEFT), posAisle[select].GetRect(Rect.RECT.TOP), i).x,
									GetPos(posAisle[select].GetRect(Rect.RECT.LEFT), posAisle[select].GetRect(Rect.RECT.TOP), i).y]
									== MapData.AISLE)
								{
									//座標記憶
									posAisle[DIRECTIONS].Reset(GetPos(posAisle[select].GetRect(Rect.RECT.LEFT), posAisle[select].GetRect(Rect.RECT.TOP), i).x,
										GetPos(posAisle[select].GetRect(Rect.RECT.LEFT), posAisle[select].GetRect(Rect.RECT.TOP), i).y, 0, 0);

									//通路の個数を増やす
									roomAisle++;
								}
							}

							//通路の個数が1個の場合
							if (roomAisle == 1)
							{
								//自身のマスを壁に置き換え
								map[posAisle[select].GetRect(Rect.RECT.LEFT), posAisle[select].GetRect(Rect.RECT.TOP)] = MapData.WALL;

								//selectの座標変更
								posAisle[select].Reset(posAisle[4].GetRect(Rect.RECT.LEFT), posAisle[4].GetRect(Rect.RECT.TOP), 0, 0);

								//処理継続
								continue;
							}

							//処理終了
							break;
						}
					}
				}
			}

			//部屋ミニ通路廃止
			foreach (Rect r in roomArray)
			{
				//通路本数
				int aisleNum = 0;

				//各方向の通路の位置
				int[,] dirPos = new int[DIRECTIONS, 2] { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };

				//通路の長さが短いか
				bool[] isShort = new bool[DIRECTIONS] { false, false, false, false };

				//通路左右
				for (int y = r.GetRect(Rect.RECT.TOP) + aisleDistance; y <= r.GetRect(Rect.RECT.BOTTOM) - aisleDistance; y++)
				{
					//通路の場合(左)
					if (map[r.GetRect(Rect.RECT.LEFT), y] == MapData.AISLE)
					{
						//位置記憶
						dirPos[0, 0] = r.GetRect(Rect.RECT.LEFT);
						dirPos[0, 1] = y;

						//本数計上
						aisleNum++;
					}

					//通路の場合(右)
					if (map[r.GetRect(Rect.RECT.RIGHT), y] == MapData.AISLE)
					{
						//位置記憶
						dirPos[2, 0] = r.GetRect(Rect.RECT.RIGHT);
						dirPos[2, 1] = y;

						//本数計上
						aisleNum++;
					}
				}

				//通路上下
				for (int x = r.GetRect(Rect.RECT.LEFT) + aisleDistance; x <= r.GetRect(Rect.RECT.RIGHT) - aisleDistance; x++)
				{
					//通路の場合(上)
					if (map[x, r.GetRect(Rect.RECT.TOP)] == MapData.AISLE)
					{
						//位置記憶
						dirPos[1, 0] = x;
						dirPos[1, 1] = r.GetRect(Rect.RECT.TOP);

						//本数計上
						aisleNum++;
					}

					//通路の場合(下)
					if (map[x, r.GetRect(Rect.RECT.BOTTOM)] == MapData.AISLE)
					{
						//位置記憶
						dirPos[3, 0] = x;
						dirPos[3, 1] = r.GetRect(Rect.RECT.BOTTOM);

						//本数計上
						aisleNum++;
					}
				}

				//通路が1本以下の場合
				if (aisleNum < 2)
				{
					//処理なし
					continue;
				}

				//本数初期化
				aisleNum = 0;

				//左
				if (dirPos[0, 0] > 0 &&
				map[dirPos[0, 0] - 2, dirPos[0, 1] + 1] == MapData.AISLE &&
				map[dirPos[0, 0] - 2, dirPos[0, 1]] == MapData.AISLE &&
				map[dirPos[0, 0] - 2, dirPos[0, 1] - 1] == MapData.AISLE)
				{
					//短い通路として扱う
					isShort[0] = true;

					//本数計上
					aisleNum++;
				}

				//上
				if (dirPos[1, 0] > 0 &&
				map[dirPos[1, 0] + 1, dirPos[1, 1] - 2] == MapData.AISLE &&
				map[dirPos[1, 0], dirPos[1, 1] - 2] == MapData.AISLE &&
				map[dirPos[1, 0] - 1, dirPos[1, 1] - 2] == MapData.AISLE)
				{
					//短い通路として扱う
					isShort[1] = true;

					//本数計上
					aisleNum++;
				}

				//右
				if (dirPos[2, 0] > 0 &&
				map[dirPos[2, 0] + 2, dirPos[2, 1] + 1] == MapData.AISLE &&
				map[dirPos[2, 0] + 2, dirPos[2, 1]] == MapData.AISLE &&
				map[dirPos[2, 0] + 2, dirPos[2, 1] - 1] == MapData.AISLE)
				{
					//短い通路として扱う
					isShort[2] = true;

					//本数計上
					aisleNum++;
				}

				//下
				if (dirPos[3, 0] > 0 &&
				map[dirPos[3, 0] + 1, dirPos[3, 1] + 2] == MapData.AISLE &&
				map[dirPos[3, 0], dirPos[3, 1] + 2] == MapData.AISLE &&
				map[dirPos[3, 0] - 1, dirPos[3, 1] + 2] == MapData.AISLE)
				{
					//短い通路として扱う
					isShort[3] = true;

					//本数計上
					aisleNum++;
				}

				//短い通路がない場合
				if (aisleNum == 0)
				{
					//処理なし
					continue;
				}

				//通路を一本選ぶ
				do
				{
					//選ぶ
					aislePos = Random.Range(0, DIRECTIONS);
				} while (!isShort[aislePos]);

				//バックアップを作成
				MakeBackup();

				//通路を削除
				EraseAisle((dirPos[aislePos, 0], dirPos[aislePos, 1]), true);

				//生成に失敗した場合
				if (AllGreen())
				{
					//バックアップから復元
					RestoreMap();
				}
			}

			//太陽型通路の複製
			//各部屋に対する通路の位置をチェック
			foreach (Rect r in roomArray)
			{
				//通路本数
				int aisleNum = 0;

				//各方向の通路の位置
				int[,] dirPos = new int[DIRECTIONS, 2] { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };

				//サイドの位置
				int[] sidePos = new int[2] { 0, 0 };

				//強制終了
				bool fin = false;

				//通路左右
				for (int y = r.GetRect(Rect.RECT.TOP) + aisleDistance; y <= r.GetRect(Rect.RECT.BOTTOM) - aisleDistance; y++)
				{
					//通路の場合(左)
					if (map[r.GetRect(Rect.RECT.LEFT), y] == MapData.AISLE)
					{
						//位置記憶
						dirPos[0, 0] = r.GetRect(Rect.RECT.LEFT);
						dirPos[0, 1] = y;

						//本数計上
						aisleNum++;
					}

					//通路の場合(右)
					if (map[r.GetRect(Rect.RECT.RIGHT), y] == MapData.AISLE)
					{
						//位置記憶
						dirPos[2, 0] = r.GetRect(Rect.RECT.RIGHT);
						dirPos[2, 1] = y;

						//本数計上
						aisleNum++;
					}
				}

				//通路上下
				for (int x = r.GetRect(Rect.RECT.LEFT) + aisleDistance; x <= r.GetRect(Rect.RECT.RIGHT) - aisleDistance; x++)
				{
					//通路の場合(上)
					if (map[x, r.GetRect(Rect.RECT.TOP)] == MapData.AISLE)
					{
						//位置記憶
						dirPos[1, 0] = x;
						dirPos[1, 1] = r.GetRect(Rect.RECT.TOP);

						//本数計上
						aisleNum++;
					}

					//通路の場合(下)
					if (map[x, r.GetRect(Rect.RECT.BOTTOM)] == MapData.AISLE)
					{
						//位置記憶
						dirPos[3, 0] = x;
						dirPos[3, 1] = r.GetRect(Rect.RECT.BOTTOM);

						//本数計上
						aisleNum++;
					}
				}

				//通路が2本以外の場合
				if (aisleNum != 2)
				{
					//処理なし
					continue;
				}

				//通路の方向
				//左上
				else if (dirPos[0, 0] > 0 && dirPos[1, 0] > 0)
				{
					//左通路の長さ
					for (; map[dirPos[0, 0], dirPos[0, 1] - 1] != MapData.AISLE; dirPos[0, 0]--)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//上通路の長さ
					for (; map[dirPos[1, 0] - 1, dirPos[1, 1]] != MapData.AISLE; dirPos[1, 1]--)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//サイド位置を設定
					sidePos[0] = r.GetRect(Rect.RECT.LEFT);
					sidePos[1] = r.GetRect(Rect.RECT.TOP);

					//左通路が上通路と合流するまで
					for (dirPos[0, 1] -= 1; dirPos[0, 1] > dirPos[1, 1]; dirPos[0, 1]--)
					{
						//自身の左のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[0, 0] - 1, dirPos[0, 1]] == MapData.AISLE && sidePos[0] == r.GetRect(Rect.RECT.LEFT))
						{
							//サイド位置を記憶
							sidePos[1] = dirPos[0, 1];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//上通路が左通路と合流するまで
					for (dirPos[1, 0] -= 1; dirPos[1, 0] > dirPos[0, 0]; dirPos[1, 0]--)
					{
						//自身の上のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[1, 0], dirPos[1, 1] - 1] == MapData.AISLE && sidePos[1] == r.GetRect(Rect.RECT.TOP))
						{
							//サイド位置を記憶
							sidePos[0] = dirPos[1, 0];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//狭いほうの通路
					//横
					if (r.GetRect(Rect.RECT.LEFT) - dirPos[0, 0] <= r.GetRect(Rect.RECT.TOP) - dirPos[0, 1])
					{
						//通路として成立する場合
						if (sidePos[1] <= r.GetRect(Rect.RECT.TOP) - aisleDistance && sidePos[1] - dirPos[0, 1] >= aisleDistance)
						{
							//通路を複製
							for (int x = dirPos[0, 0] + 1; map[x, sidePos[1]] != MapData.AISLE; x++)
							{
								//壁を通路に置き換え
								map[x, sidePos[1]] = MapData.AISLE;
							}
						}
					}

					//縦
					else
					{
						//通路として成立する場合
						if (sidePos[0] <= r.GetRect(Rect.RECT.LEFT) - aisleDistance && sidePos[0] - dirPos[0, 0] >= aisleDistance)
						{
							//通路を複製
							for (int y = dirPos[0, 1] + 1; map[sidePos[0], y] != MapData.AISLE; y++)
							{
								//壁を通路に置き換え
								map[sidePos[0], y] = MapData.AISLE;
							}
						}
					}
				}

				//右上
				else if (dirPos[2, 0] > 0 && dirPos[1, 0] > 0)
				{
					//右通路の長さ
					for (; map[dirPos[2, 0], dirPos[2, 1] - 1] != MapData.AISLE; dirPos[2, 0]++)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//上通路の長さ
					for (; map[dirPos[1, 0] + 1, dirPos[1, 1]] != MapData.AISLE; dirPos[1, 1]--)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//サイド位置を設定
					sidePos[0] = r.GetRect(Rect.RECT.RIGHT);
					sidePos[1] = r.GetRect(Rect.RECT.TOP);

					//右通路が上通路と合流するまで
					for (dirPos[2, 1] -= 1; dirPos[2, 1] > dirPos[1, 1]; dirPos[2, 1]--)
					{
						//自身の右のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[2, 0] + 1, dirPos[2, 1]] == MapData.AISLE && sidePos[0] == r.GetRect(Rect.RECT.RIGHT))
						{
							//サイド位置を記憶
							sidePos[1] = dirPos[2, 1];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//上通路が右通路と合流するまで
					for (dirPos[1, 0] += 1; dirPos[1, 0] < dirPos[2, 0]; dirPos[1, 0]++)
					{
						//自身の上のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[1, 0], dirPos[1, 1] - 1] == MapData.AISLE && sidePos[1] == r.GetRect(Rect.RECT.TOP))
						{
							//サイド位置を記憶
							sidePos[0] = dirPos[1, 0];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//狭いほうの通路
					//横
					if (dirPos[1, 0] - r.GetRect(Rect.RECT.RIGHT) <= r.GetRect(Rect.RECT.TOP) - dirPos[1, 1])
					{
						//通路として成立する場合
						if (sidePos[1] <= r.GetRect(Rect.RECT.TOP) - aisleDistance && sidePos[1] - dirPos[1, 1] >= aisleDistance)
						{
							//通路を複製
							for (int x = dirPos[1, 0] - 1; map[x, sidePos[1]] != MapData.AISLE; x--)
							{
								//壁を通路に置き換え
								map[x, sidePos[1]] = MapData.AISLE;
							}
						}
					}

					//縦
					else
					{
						//通路として成立する場合
						if (sidePos[0] >= r.GetRect(Rect.RECT.RIGHT) + aisleDistance && dirPos[0, 0] - sidePos[0] >= aisleDistance)
						{
							//通路を複製
							for (int y = dirPos[1, 1] + 1; map[sidePos[0], y] != MapData.AISLE; y++)
							{
								//壁を通路に置き換え
								map[sidePos[0], y] = MapData.AISLE;
							}
						}
					}
				}

				//右下
				else if (dirPos[2, 0] > 0 && dirPos[3, 0] > 0)
				{
					//右通路の長さ
					for (; map[dirPos[2, 0], dirPos[2, 1] + 1] != MapData.AISLE; dirPos[2, 0]++)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//下通路の長さ
					for (; map[dirPos[3, 0] + 1, dirPos[3, 1]] != MapData.AISLE; dirPos[3, 1]++)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//サイド位置を設定
					sidePos[0] = r.GetRect(Rect.RECT.RIGHT);
					sidePos[1] = r.GetRect(Rect.RECT.BOTTOM);

					//右通路が下通路と合流するまで
					for (dirPos[2, 1] += 1; dirPos[2, 1] < dirPos[3, 1]; dirPos[2, 1]++)
					{
						//自身の右のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[2, 0] + 1, dirPos[2, 1]] == MapData.AISLE && sidePos[0] == r.GetRect(Rect.RECT.RIGHT))
						{
							//サイド位置を記憶
							sidePos[1] = dirPos[2, 1];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//下通路が右通路と合流するまで
					for (dirPos[3, 0] += 1; dirPos[3, 0] < dirPos[2, 0]; dirPos[3, 0]++)
					{
						//自身の下のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[3, 0], dirPos[3, 1] + 1] == MapData.AISLE && sidePos[1] == r.GetRect(Rect.RECT.BOTTOM))
						{
							//サイド位置を記憶
							sidePos[0] = dirPos[3, 0];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//狭いほうの通路
					//横
					if (dirPos[2, 0] - r.GetRect(Rect.RECT.RIGHT) <= dirPos[2, 1] - r.GetRect(Rect.RECT.BOTTOM))
					{
						//通路として成立する場合
						if (sidePos[1] >= r.GetRect(Rect.RECT.BOTTOM) + aisleDistance && dirPos[2, 1] - sidePos[1] >= aisleDistance)
						{
							//通路を複製
							for (int x = dirPos[2, 0] - 1; map[x, sidePos[1]] != MapData.AISLE; x--)
							{
								//壁を通路に置き換え
								map[x, sidePos[1]] = MapData.AISLE;
							}
						}
					}

					//縦
					else
					{
						//通路として成立する場合
						if (sidePos[0] >= r.GetRect(Rect.RECT.RIGHT) + aisleDistance && dirPos[2, 0] - sidePos[0] >= aisleDistance)
						{
							//通路を複製
							for (int y = dirPos[2, 1] - 1; map[sidePos[0], y] != MapData.AISLE; y--)
							{
								//壁を通路に置き換え
								map[sidePos[0], y] = MapData.AISLE;
							}
						}
					}
				}

				//左下
				else if (dirPos[0, 0] > 0 && dirPos[3, 0] > 0)
				{
					//左通路の長さ
					for (; map[dirPos[0, 0], dirPos[0, 1] + 1] != MapData.AISLE; dirPos[0, 0]--)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//下通路の長さ
					for (; map[dirPos[3, 0] - 1, dirPos[3, 1]] != MapData.AISLE; dirPos[3, 1]++)
					{
						//自身のマスが通路ではない場合
						if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//サイド位置を設定
					sidePos[0] = r.GetRect(Rect.RECT.LEFT);
					sidePos[1] = r.GetRect(Rect.RECT.BOTTOM);

					//左通路が下通路と合流するまで
					for (dirPos[0, 1] += 1; dirPos[0, 1] < dirPos[3, 1]; dirPos[0, 1]++)
					{
						//自身の左のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[0, 0] - 1, dirPos[0, 1]] == MapData.AISLE && sidePos[0] == r.GetRect(Rect.RECT.LEFT))
						{
							//サイド位置を記憶
							sidePos[1] = dirPos[0, 1];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//下通路が左通路と合流するまで
					for (dirPos[3, 0] -= 1; dirPos[3, 0] > dirPos[0, 0]; dirPos[3, 0]--)
					{
						//自身の下のマスが通路の場合かつサイドの位置が初期値の場合
						if (map[dirPos[3, 0], dirPos[3, 1] + 1] == MapData.AISLE && sidePos[1] == r.GetRect(Rect.RECT.BOTTOM))
						{
							//サイド位置を記憶
							sidePos[0] = dirPos[3, 0];
						}

						//自身のマスが通路ではない場合
						if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE)
						{
							//強制終了
							fin = true;

							//処理なし
							break;
						}
					}

					//強制終了
					if (fin)
					{
						//処理終了
						continue;
					}

					//狭いほうの通路
					//横
					if (r.GetRect(Rect.RECT.LEFT) - dirPos[3, 0] <= dirPos[3, 1] - r.GetRect(Rect.RECT.BOTTOM))
					{
						//通路として成立する場合
						if (sidePos[1] >= r.GetRect(Rect.RECT.BOTTOM) + aisleDistance && dirPos[3, 1] - sidePos[1] >= aisleDistance)
						{
							//通路を複製
							for (int x = dirPos[3, 0] + 1; map[x, sidePos[1]] != MapData.AISLE; x++)
							{
								//壁を通路に置き換え
								map[x, sidePos[1]] = MapData.AISLE;
							}
						}
					}

					//縦
					else
					{
						//通路として成立する場合
						if (sidePos[0] <= r.GetRect(Rect.RECT.LEFT) - aisleDistance && sidePos[0] - dirPos[3, 0] >= aisleDistance)
						{
							//通路を複製
							for (int y = dirPos[3, 1] - 1; map[sidePos[0], y] != MapData.AISLE; y--)
							{
								//壁を通路に置き換え
								map[sidePos[0], y] = MapData.AISLE;
							}
						}
					}
				}
			}

			//卍型通路の個性化
			//各部屋に対する通路の位置をチェック
			foreach (Rect r in roomArray)
			{
				//通路の個数計上
				for (int x = r.GetRect(Rect.RECT.LEFT); x <= r.GetRect(Rect.RECT.RIGHT); x++)
				{
					for (int y = r.GetRect(Rect.RECT.TOP); y <= r.GetRect(Rect.RECT.BOTTOM); y++)
					{
						//通路の場合
						if (map[x, y] == MapData.AISLE)
						{
							//変更位置
							Rect changePos = new Rect();

							//変更あるか
							bool wasChanged = false;

							//置き換え用座標
							int l = 0;

							//左
							if (x == r.GetRect(Rect.RECT.LEFT))
							{
								//通路が続く限り
								for (l = x - 1; map[l, y] == MapData.AISLE; l--) ;

								//通路じゃないそのマスの2マス上が通路の場合
								if (map[l, y - 2] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l + 1, y - 3, l + 1, y + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス上が通路の場合
								else if (map[l, y - 1] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l + 1, y - 2, l + 1, y + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス下が通路の場合
								else if (map[l, y + 1] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l + 1, y - 1, l + 1, y + 2);
									wasChanged = true;
								}

								//通路じゃないそのマスの2マス下が通路の場合
								else if (map[l, y + 2] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l + 1, y - 1, l + 1, y + 3);
									wasChanged = true;
								}

								//変更がある場合
								if (wasChanged)
								{
									//初期化
									wasChanged = false;

									//対象座標のMAP_DATAを記憶
									int a = map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)];
									int b = map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)];

									//壁化
									map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = a;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP) - 1));
									}

									//壁化
									map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = b;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM) + 1));
									}
								}
							}

							//上
							if (y == r.GetRect(Rect.RECT.TOP))
							{
								//通路が続く限り
								for (l = y - 1; map[x, l] == MapData.AISLE; l--) ;

								//通路じゃないそのマスの2マス左が通路の場合
								if (map[x - 2, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 3, l + 1, x + 1, l + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス左が通路の場合
								else if (map[x - 1, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 2, l + 1, x + 1, l + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス右が通路の場合
								else if (map[x + 1, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 1, l + 1, x + 2, l + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの2マス右が通路の場合
								else if (map[x + 2, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 1, l + 1, x + 3, l + 1);
									wasChanged = true;
								}

								//変更がある場合
								if (wasChanged)
								{
									//初期化
									wasChanged = false;

									//対象座標のMAP_DATAを記憶
									int a = map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)];
									int b = map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)];

									//壁化
									map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = a;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.LEFT) - 1, changePos.GetRect(Rect.RECT.TOP)));
									}

									//壁化
									map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = b;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.RIGHT) + 1, changePos.GetRect(Rect.RECT.BOTTOM)));
									}
								}
							}

							//右
							if (x == r.GetRect(Rect.RECT.RIGHT))
							{
								//通路が続く限り
								for (l = x + 1; map[l, y] == MapData.AISLE; l++) ;

								//通路じゃないそのマスの2マス上が通路の場合
								if (map[l, y - 2] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l - 1, y - 3, l - 1, y + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス上が通路の場合
								else if (map[l, y - 1] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l - 1, y - 2, l - 1, y + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス下が通路の場合
								else if (map[l, y + 1] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l - 1, y - 1, l - 1, y + 2);
									wasChanged = true;
								}

								//通路じゃないそのマスの2マス下が通路の場合
								else if (map[l, y + 2] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(l - 1, y - 1, l - 1, y + 3);
									wasChanged = true;
								}

								//変更がある場合
								if (wasChanged)
								{
									//初期化
									wasChanged = false;

									//対象座標のMAP_DATAを記憶
									int a = map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)];
									int b = map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)];

									//壁化
									map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = a;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP) - 1));
									}

									//壁化
									map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = b;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM) + 1));
									}
								}
							}

							//下
							if (y == r.GetRect(Rect.RECT.BOTTOM))
							{
								//通路が続く限り
								for (l = y + 1; map[x, l] == MapData.AISLE; l++) ;

								//通路じゃないそのマスの2マス左が通路の場合
								if (map[x - 2, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 3, l + 1, x + 1, l + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス左が通路の場合
								else if (map[x - 1, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 2, l + 1, x + 1, l + 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの1マス右が通路の場合
								else if (map[x + 1, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 1, l - 1, x + 2, l - 1);
									wasChanged = true;
								}

								//通路じゃないそのマスの2マス右が通路の場合
								else if (map[x + 2, l] == MapData.AISLE)
								{
									//変更あり
									changePos.Reset(x - 1, l - 1, x + 3, l - 1);
									wasChanged = true;
								}

								//変更がある場合
								if (wasChanged)
								{
									//初期化
									wasChanged = false;

									//対象座標のMAP_DATAを記憶
									int a = map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)];
									int m = map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)];

									//壁化
									map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.LEFT), changePos.GetRect(Rect.RECT.TOP)] = a;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.LEFT) - 1, changePos.GetRect(Rect.RECT.TOP)));
									}

									//壁化
									map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = MapData.WALL;

									//孤立した場合
									if (!AllGreen())
									{
										//戻す
										map[changePos.GetRect(Rect.RECT.RIGHT), changePos.GetRect(Rect.RECT.BOTTOM)] = m;
									}

									//しない場合
									else
									{
										//通路に対する通路が1つしかない場合通路を潰す
										EraseAisle((changePos.GetRect(Rect.RECT.RIGHT) + 1, changePos.GetRect(Rect.RECT.BOTTOM)));
									}
								}
							}
						}
					}
				}
			}

			//3回実行
			for (int n = 0; n < 3; n++)
			{
				//口型を含む通路を取り除く
				//各部屋に対する通路の位置をチェック
				foreach (Rect r in roomArray)
				{
					//通路本数
					int aisleNum = 0;

					//各方向の通路の位置
					int[,] dirPos = new int[DIRECTIONS, 2] { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };

					//強制終了
					bool fin = false;

					//通路左右
					for (int y = r.GetRect(Rect.RECT.TOP) + aisleDistance; y <= r.GetRect(Rect.RECT.BOTTOM) - aisleDistance; y++)
					{
						//通路の場合(左)
						if (map[r.GetRect(Rect.RECT.LEFT), y] == MapData.AISLE)
						{
							//位置記憶
							dirPos[0, 0] = r.GetRect(Rect.RECT.LEFT);
							dirPos[0, 1] = y;

							//本数計上
							aisleNum++;
						}

						//通路の場合(右)
						if (map[r.GetRect(Rect.RECT.RIGHT), y] == MapData.AISLE)
						{
							//位置記憶
							dirPos[2, 0] = r.GetRect(Rect.RECT.RIGHT);
							dirPos[2, 1] = y;

							//本数計上
							aisleNum++;
						}
					}

					//通路上下
					for (int x = r.GetRect(Rect.RECT.LEFT) + aisleDistance; x <= r.GetRect(Rect.RECT.RIGHT) - aisleDistance; x++)
					{
						//通路の場合(上)
						if (map[x, r.GetRect(Rect.RECT.TOP)] == MapData.AISLE)
						{
							//位置記憶
							dirPos[1, 0] = x;
							dirPos[1, 1] = r.GetRect(Rect.RECT.TOP);

							//本数計上
							aisleNum++;
						}

						//通路の場合(下)
						if (map[x, r.GetRect(Rect.RECT.BOTTOM)] == MapData.AISLE)
						{
							//位置記憶
							dirPos[3, 0] = x;
							dirPos[3, 1] = r.GetRect(Rect.RECT.BOTTOM);

							//本数計上
							aisleNum++;
						}
					}

					//通路が2本以外の場合
					if (aisleNum != 2)
					{
						//処理なし
						continue;
					}

					//通路の方向
					//左上
					else if (dirPos[0, 0] > 0 && dirPos[1, 0] > 0)
					{
						//左通路の長さ
						for (; map[dirPos[0, 0], dirPos[0, 1] - 1] != MapData.AISLE; dirPos[0, 0]--)
						{
							//自身のマスが通路ではない場合または自身の下のマスが通路の場合
							if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE || map[dirPos[0, 0], dirPos[0, 1] + 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//上通路の長さ
						for (; map[dirPos[1, 0] - 1, dirPos[1, 1]] != MapData.AISLE; dirPos[1, 1]--)
						{
							//自身のマスが通路ではない場合または自身の右のマスが通路の場合
							if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE || map[dirPos[1, 0] + 1, dirPos[1, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//左通路が上通路と合流するまで
						for (dirPos[0, 1] -= 1; dirPos[0, 1] > dirPos[1, 1]; dirPos[0, 1]--)
						{
							//自身のマスが通路ではない場合または自身の左のマスが通路の場合
							if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE || map[dirPos[0, 0] - 1, dirPos[0, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//上通路が左通路と合流するまで
						for (dirPos[1, 0] -= 1; dirPos[1, 0] > dirPos[0, 0]; dirPos[1, 0]--)
						{
							//自身のマスが通路ではない場合または自身の上のマスが通路の場合
							if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE || map[dirPos[1, 0], dirPos[1, 1] - 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//削除する方向
						//横
						if (Random.Range(0, 2) == 0)
						{
							//通路を削除
							map[dirPos[0, 0] + 1, dirPos[0, 1]] = 0;
							EraseAisle((dirPos[0, 0] + 2, dirPos[0, 1]));
							EraseAisle((dirPos[0, 0], dirPos[0, 1]));
						}

						//縦
						else
						{
							//通路を削除
							map[dirPos[0, 0], dirPos[0, 1] + 1] = 0;
							EraseAisle((dirPos[0, 0], dirPos[0, 1] + 2));
							EraseAisle((dirPos[0, 0], dirPos[0, 1]));
						}
					}

					//右上
					else if (dirPos[2, 0] > 0 && dirPos[1, 0] > 0)
					{
						//右通路の長さ
						for (; map[dirPos[2, 0], dirPos[2, 1] - 1] != MapData.AISLE; dirPos[2, 0]++)
						{
							//自身のマスが通路ではない場合または自身の下のマスが通路の場合
							if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE || map[dirPos[2, 0], dirPos[2, 1] + 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//上通路の長さ
						for (; map[dirPos[1, 0] + 1, dirPos[1, 1]] != MapData.AISLE; dirPos[1, 1]--)
						{
							//自身のマスが通路ではない場合または自身の左のマスが通路の場合
							if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE || map[dirPos[1, 0] - 1, dirPos[1, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//右通路が上通路と合流するまで
						for (dirPos[2, 1] -= 1; dirPos[2, 1] > dirPos[1, 1]; dirPos[2, 1]--)
						{
							//自身のマスが通路ではない場合または自身の右のマスが通路の場合
							if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE || map[dirPos[2, 0] + 1, dirPos[2, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//上通路が右通路と合流するまで
						for (dirPos[1, 0] += 1; dirPos[1, 0] < dirPos[2, 0]; dirPos[1, 0]++)
						{
							//自身のマスが通路ではない場合または自身の上のマスが通路の場合
							if (map[dirPos[1, 0], dirPos[1, 1]] != MapData.AISLE || map[dirPos[1, 0], dirPos[1, 1] - 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//削除する方向
						//横
						if (Random.Range(0, 2) == 0)
						{
							//通路を削除
							map[dirPos[1, 0] - 1, dirPos[1, 1]] = 0;
							EraseAisle((dirPos[1, 0] - 2, dirPos[1, 1]));
							EraseAisle((dirPos[1, 0], dirPos[1, 1]));
						}

						//縦
						else
						{
							//通路を削除
							map[dirPos[1, 0], dirPos[1, 1] + 1] = 0;
							EraseAisle((dirPos[1, 0], dirPos[1, 1] + 2));
							EraseAisle((dirPos[1, 0], dirPos[1, 1]));
						}
					}

					//右下
					else if (dirPos[2, 0] > 0 && dirPos[3, 0] > 0)
					{
						//右通路の長さ
						for (; map[dirPos[2, 0], dirPos[2, 1] + 1] != MapData.AISLE; dirPos[2, 0]++)
						{
							//自身のマスが通路ではない場合または自身の上のマスが通路の場合
							if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE || map[dirPos[2, 0], dirPos[2, 1] - 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//下通路の長さ
						for (; map[dirPos[3, 0] + 1, dirPos[3, 1]] != MapData.AISLE; dirPos[3, 1]++)
						{
							//自身のマスが通路ではない場合または自身の左のマスが通路の場合
							if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE || map[dirPos[3, 0] - 1, dirPos[3, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//右通路が下通路と合流するまで
						for (dirPos[2, 1] += 1; dirPos[2, 1] < dirPos[3, 1]; dirPos[2, 1]++)
						{
							//自身のマスが通路ではない場合または自身の右のマスが通路の場合
							if (map[dirPos[2, 0], dirPos[2, 1]] != MapData.AISLE || map[dirPos[2, 0] + 1, dirPos[2, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//下通路が右通路と合流するまで
						for (dirPos[3, 0] += 1; dirPos[3, 0] < dirPos[2, 0]; dirPos[3, 0]++)
						{
							//自身のマスが通路ではない場合または自身の下のマスが通路の場合
							if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE || map[dirPos[3, 0], dirPos[3, 1] + 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//削除する方向
						//横
						if (Random.Range(0, 2) == 0)
						{
							//通路を削除
							map[dirPos[2, 0] - 1, dirPos[2, 1]] = 0;
							EraseAisle((dirPos[2, 0] - 2, dirPos[2, 1]));
							EraseAisle((dirPos[2, 0], dirPos[2, 1]));
						}

						//縦
						else
						{
							//通路を削除
							map[dirPos[2, 0], dirPos[2, 1] - 1] = 0;
							EraseAisle((dirPos[2, 0], dirPos[2, 1] - 2));
							EraseAisle((dirPos[2, 0], dirPos[2, 1]));
						}
					}

					//左下
					else if (dirPos[0, 0] > 0 && dirPos[3, 0] > 0)
					{
						//左通路の長さ
						for (; map[dirPos[0, 0], dirPos[0, 1] + 1] != MapData.AISLE; dirPos[0, 0]--)
						{
							//自身のマスが通路ではない場合または自身の上のマスが通路の場合
							if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE || map[dirPos[0, 0], dirPos[0, 1] - 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//下通路の長さ
						for (; map[dirPos[3, 0] - 1, dirPos[3, 1]] != MapData.AISLE; dirPos[3, 1]++)
						{
							//自身のマスが通路ではない場合または自身の右のマスが通路の場合
							if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE || map[dirPos[3, 0] + 1, dirPos[3, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//左通路が下通路と合流するまで
						for (dirPos[0, 1] += 1; dirPos[0, 1] < dirPos[3, 1]; dirPos[0, 1]++)
						{
							//自身のマスが通路ではない場合または自身の左のマスが通路の場合
							if (map[dirPos[0, 0], dirPos[0, 1]] != MapData.AISLE || map[dirPos[0, 0] - 1, dirPos[0, 1]] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//下通路が左通路と合流するまで
						for (dirPos[3, 0] -= 1; dirPos[3, 0] > dirPos[0, 0]; dirPos[3, 0]--)
						{
							//自身のマスが通路ではない場合または自身の下のマスが通路の場合
							if (map[dirPos[3, 0], dirPos[3, 1]] != MapData.AISLE || map[dirPos[3, 0], dirPos[3, 1] + 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//削除する方向
						//横
						if (Random.Range(0, 2) == 0)
						{
							//通路を削除
							map[dirPos[3, 0] + 1, dirPos[3, 1]] = 0;
							EraseAisle((dirPos[3, 0] + 2, dirPos[3, 1]));
							EraseAisle((dirPos[3, 0], dirPos[3, 1]));
						}

						//縦
						else
						{
							//通路を削除
							map[dirPos[3, 0], dirPos[3, 1] - 1] = 0;
							EraseAisle((dirPos[3, 0], dirPos[3, 1] - 2));
							EraseAisle((dirPos[3, 0], dirPos[3, 1]));
						}
					}
				}
			}

			//口型通路を取り除く
			for (int x = 1; x < width - 1; x++)
			{
				for (int y = 1; y < height - 1; y++)
				{
					//自身のマスが通路で自身の右のマスが通路の場合かつ自身の一マス下が通路の場合
					if (map[x, y] == MapData.AISLE && map[x + 1, y] == MapData.AISLE && map[x, y + 1] == MapData.AISLE)
					{
						//範囲
						Rect aria = new Rect(x, y, x + 1, y + 1);

						//強制終了
						bool fin = false;

						//右のマスが通路になるまで
						for (; map[x + 1, aria.GetRect(Rect.RECT.BOTTOM)] != MapData.AISLE; aria.SetRect(Rect.RECT.BOTTOM, aria.GetRect(Rect.RECT.BOTTOM) + 1))
						{
							//自身のマスが通路以外または自身の左のマスが通路の場合
							if (map[x, aria.GetRect(Rect.RECT.BOTTOM)] != MapData.AISLE ||
								map[x - 1, aria.GetRect(Rect.RECT.BOTTOM)] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//横の長さを調べる
						for (; ; aria.SetRect(Rect.RECT.RIGHT, aria.GetRect(Rect.RECT.RIGHT) + 1))
						{
							//通路以外
							if (map[aria.GetRect(Rect.RECT.RIGHT), y] != MapData.AISLE ||
								map[aria.GetRect(Rect.RECT.RIGHT), aria.GetRect(Rect.RECT.BOTTOM)] != MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}

							//上と下の内側が通路である場合
							else if (map[aria.GetRect(Rect.RECT.RIGHT), y + 1] == MapData.AISLE &&
								map[aria.GetRect(Rect.RECT.RIGHT), aria.GetRect(Rect.RECT.BOTTOM) - 1] == MapData.AISLE)
							{
								//処理終了
								break;
							}

							//上と下で内側が同じ結果ではない場合または外側に通路がある場合
							else if (map[aria.GetRect(Rect.RECT.RIGHT), y + 1] != map[aria.GetRect(Rect.RECT.RIGHT), aria.GetRect(Rect.RECT.BOTTOM) - 1] ||
								map[aria.GetRect(Rect.RECT.RIGHT), y - 1] == MapData.AISLE || map[aria.GetRect(Rect.RECT.RIGHT), aria.GetRect(Rect.RECT.BOTTOM) + 1] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//上と下を繋ぐ通路の右側に通路があるか調べる
						for (int l = aria.GetRect(Rect.RECT.BOTTOM) - 1; l > y; l--)
						{
							//自身のマスが通路以外または自身の右のマスが通路の場合
							if (map[aria.GetRect(Rect.RECT.RIGHT), l] != MapData.AISLE || map[aria.GetRect(Rect.RECT.RIGHT) + 1, l] == MapData.AISLE)
							{
								//強制終了
								fin = true;

								//処理なし
								break;
							}
						}

						//強制終了
						if (fin)
						{
							//処理終了
							continue;
						}

						//ランダムな位置を選定(0:LEFT / 1:TOP / 2:RIGHT / 3:BOTTOM)
						switch (Random.Range(0, DIRECTIONS))
						{
							case 0:
								EraseAisle((x, y + 1), true);
								break;
							case 1:
								EraseAisle((x + 1, y), true);
								break;
							case 2:
								EraseAisle((aria.GetRect(Rect.RECT.RIGHT), y + 1), true);
								break;
							case 3:
								EraseAisle((x + 1, aria.GetRect(Rect.RECT.BOTTOM)), true);
								break;
						}
					}
				}
			}

			//一定以上の長さの通路摘発
			//横
			for (int y = 1; y < height - 1; y++)
			{
				for (int x = 1; x < width - 1; x++)
				{
					//自身のマスが通路の場合
					if (map[x, y] == MapData.AISLE)
					{
						//長さ
						int length = 1;

						//通路の個数確認
						for (int i = x + 1; length < lengthMax; i++, length++)
						{
							//自身のマスが通路でも通路(仮)でもない場合または自身のマスの上下が壁以外の場合
							if (!(map[i, y] == MapData.AISLE || map[i, y] == MapData.AISLE_TENT) ||
								!(map[i, y - 1] == MapData.WALL) || !(map[i, y + 1] == MapData.WALL))
							{
								//処理終了
								break;
							}
						}

						//通路の長さが基準以上の場合
						if (length >= lengthMax)
						{
							//通路を削除
							EraseAisle((x + 1, y), true);
						}
					}
				}
			}

			//縦
			for (int x = 1; x < width - 1; x++)
			{
				for (int y = 1; y < height - 1; y++)
				{
					//自身のマスが通路の場合
					if (map[x, y] == MapData.AISLE)
					{
						//長さ
						int length = 1;

						//通路の個数確認
						for (int j = y + 1; length < lengthMax; j++, length++)
						{
							//自身のマスが通路でも通路(仮)でもない場合または自身のマスの左右が壁以外の場合
							if (!(map[x, j] == MapData.AISLE || map[x, j] == MapData.AISLE_TENT) ||
								!(map[x - 1, j] == MapData.WALL) || !(map[x + 1, j] == MapData.WALL))
							{
								//処理終了
								break;
							}
						}

						//通路の長さが基準以上の場合
						if (length >= lengthMax)
						{
							//通路を削除
							EraseAisle((x, y), true);
						}
					}
				}
			}

			//四通路の削除
			for (int x = 1; x < width - 2; x++)
			{
				for (int y = 1; y < height - 2; y++)
				{
					//通路の個数
					int aisleNum = 0;

					//前後左右
					for (int i = 0; i < DIRECTIONS; i++)
					{
						//通路の場合
						if (map[GetPos(x, y, i).x, GetPos(x, y, i).y] == MapData.AISLE)
						{
							//通路の個数計上
							aisleNum++;
						}
					}

					//四通路の場合
					if (aisleNum == DIRECTIONS)
					{
						//削除可能通路
						bool[] eraseAisle = new bool[DIRECTIONS] { true, true, true, true };

						//削除がうまくいくまたはうまくいかない間
						while (aisleNum > 0)
						{
							//バックアップ
							MakeBackup();

							//削除方向選定
							do
							{
								//削除方向
								aislePos = Random.Range(0, DIRECTIONS);

								//削除可能な場合
								if (eraseAisle[aislePos])
								{
									//通路を削除
									EraseAisle(GetPos(x, y, aislePos), true);
								}
							} while (!eraseAisle[aislePos]);

							//正常な場合
							if (AllGreen())
							{
								//処理終了
								break;
							}

							//正常ではない場合
							else
							{
								//復元
								RestoreMap();

								//削除可能通路の更新
								eraseAisle[aislePos] = false;

								//削除可能マスを減らす
								aisleNum--;
							}
						}
					}
				}
			}

			//I字通路を取り除く(三重チェック)
			for (int n = 0; n < 3; n++)
			{
				for (int x = 1; x < width - 2; x++)
				{
					for (int y = 2; y < height - 2; y++)
					{
						//縦型I字の探査
						//AAA
						// A
						//の探査(A:AISLE / W:WALL)
						if (map[x - 1, y] == MapData.AISLE && map[x, y] == MapData.AISLE && map[x + 1, y] == MapData.AISLE && map[x, y + 1] == MapData.AISLE)
						{
							//I字チェック
							for (int j = y + 1; j < height - 2; j++)
							{
								//WAW
								//以外の探査(A:AISLE / W:WALL)
								if (!(map[x - 1, j] == MapData.WALL && map[x, j] == MapData.AISLE && map[x + 1, j] == MapData.WALL))
								{
									//AAA
									// W
									//の探査(A:AISLE / W:WALL)
									if (map[x - 1, j] == MapData.AISLE && map[x, j] == MapData.AISLE && map[x + 1, j] == MapData.AISLE && map[x, j + 1] == MapData.WALL)
									{
										//I字の部分
										for (int l = y + 1; l < j; l++)
										{
											//通路を壁に置き換え
											map[x, l] = MapData.WALL;
										}
									}
									break;
								}
							}
						}

						//横型I字の探査
						//AW
						//AA
						//AW
						//の探査(A:AISLE / W:WALL)
						if (map[x, y - 1] == MapData.AISLE && map[x, y] == MapData.AISLE && map[x, y + 1] == MapData.AISLE && map[x + 1, y] == MapData.AISLE)
						{
							//I字チェック
							for (int i = x + 1; i < width - 2; i++)
							{
								//W
								//A
								//W
								//以外の探査(A:AISLE / W:WALL)
								if (!(map[i, y - 1] == MapData.WALL && map[i, y] == MapData.AISLE && map[i, y + 1] == MapData.WALL))
								{
									//A
									//AW
									//A
									//の探査(A:AISLE / W:WALL)
									if (map[i, y - 1] == MapData.AISLE && map[i, y] == MapData.AISLE && map[i, y + 1] == MapData.AISLE && map[i + 1, y] == MapData.WALL)
									{
										//I字の部分
										for (int k = x + 1; k < i; k++)
										{
											//通路を壁に置き換え
											map[k, y] = MapData.WALL;
										}
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	//通路が単体の場合潰す
	private void EraseAisle((int x, int y) pos, bool isEraseMe = false)
	{
		//自身も潰す場合
		if (isEraseMe)
		{
			//自身のマスを壁に置き換え
			map[pos.x, pos.y] = MapData.WALL;

			//上下左右に飛ぶ
			for (int i = 0; i < DIRECTIONS; i++)
			{
				//飛ぶ
				EraseAisle(GetPos(pos.x, pos.y, i));
			}
		}

		//自身のマスが通路の場合
		else if (map[pos.x, pos.y] == MapData.AISLE)
		{
			//通路の個数
			int aisleNum = 0;

			//自身のマスの上下左右のマスに通路の場合
			for (int i = 0; i < DIRECTIONS; i++)
			{
				//通路の場合
				if (map[GetPos(pos.x, pos.y, i).x, GetPos(pos.x, pos.y, i).y] == MapData.AISLE)
				{
					//通路の個数追加
					aisleNum++;
				}
			}

			//通路の個数が1つ以下しかない場合
			if (aisleNum <= 1)
			{
				//自身のマスを壁に置き換える
				EraseAisle(pos, true);
			}
		}
	}

	//座標(ダブル型)を返す
	private (int x, int y) GetPos(int x, int y, int position)
	{
		//範囲外用
		if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
		{
			//エラー
			return (x, y);
		}

		//位置
		switch (position)
		{
			//左
			case 0: return (x - 1, y);

			//上
			case 1: return (x, y - 1);

			//右
			case 2: return (x + 1, y);

			//下
			case 3: return (x, y + 1);
		}

		//エラー
		return (x, y);
	}

	//全マス踏破可能か
	private bool AllGreen()
	{
		//初期化兼エラーマップの検挙
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				//自身が通路の場合
				if (map[x, y] == MapData.AISLE)
				{
					//通路の個数
					int aisleNum = 0;

					//部屋と接している
					bool isRoom = false;

					//前後左右
					for (int i = 0; i < DIRECTIONS; i++)
					{
						//通路の場合
						if (map[GetPos(x, y, i).x, GetPos(x, y, i).y] == MapData.AISLE)
						{
							//通路の個数計上
							aisleNum++;
						}

						//部屋の場合
						if (map[GetPos(x, y, i).x, GetPos(x, y, i).y] >= MapData.ROOM)
						{
							//部屋と接している
							isRoom = true;
						}
					}

					//通路と2つ以上かつ部屋と接している場合
					if (aisleNum > 1 && isRoom)
					{
						//生成失敗
						return false;
					}
				}

				//マップ上で壁の場所は踏破済みに設定
				if (map[x, y] == MapData.WALL)
				{
					//踏破済み
					breakThrough[x, y] = true;
				}

				//それ以外は未踏破に設定
				else
				{
					//踏破済み
					breakThrough[x, y] = false;
				}
			}
		}

		//壁以外が選択されるまで
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				//自身のマスが未踏破の場合
				if (!breakThrough[x, y])
				{
					//マス踏破
					BreakThrough(x, y);

					//踏破済みじゃないマスの探査
					for (int i = 0; i < width; i++)
					{
						for (int j = 0; j < height; j++)
						{
							//踏破済みじゃない場合
							if (!breakThrough[i, j])
							{
								//未踏破
								return false;
							}
						}
					}

					//全マス踏破
					return true;
				}
			}
		}

		//全マス踏破
		return true;
	}

	//マス踏破
	private void BreakThrough(int x, int y)
	{
		//自身のマスを踏破に置き換え
		breakThrough[x, y] = true;

		//自身の上下左右のマスが未踏破の場合
		for (int i = 0; i < DIRECTIONS; i++)
		{
			//未踏破だった場合
			if (!breakThrough[GetPos(x, y, i).x, GetPos(x, y, i).y])
			{
				//マス踏破
				BreakThrough(GetPos(x, y, i).x, GetPos(x, y, i).y);
			}
		}
	}

	//マップのバックアップ作成
	private void MakeBackup()
	{
		//バックアップ作成
		backup = map;
	}

	//マップをバックアップから復元
	private void RestoreMap()
	{
		//復元
		map = backup;
	}

	//マップ内の区画管理
	public class Rect
	{
		//区画
		private int[,] rect;

		//部位
		public enum RECT
		{
			LEFT,
			TOP,
			RIGHT,
			BOTTOM
		}

		//コンストラクタ
		public Rect()
		{
			//生成
			rect = new int[2, 2];
		}
		public Rect(int left, int top, int right, int bottom)
		{
			//生成
			rect = new int[,] { { left, top }, { right, bottom } };
		}

		//区画変更
		public void Reset(int left, int top, int right, int bottom)
		{
			//生成
			SetRect(RECT.LEFT, left);
			SetRect(RECT.TOP, top);
			SetRect(RECT.RIGHT, right);
			SetRect(RECT.BOTTOM, bottom);
		}

		//区画コピー
		public void CopyFrom(Rect rect)
		{
			//コピー
			SetRect(RECT.LEFT, rect.GetRect(RECT.LEFT));
			SetRect(RECT.TOP, rect.GetRect(RECT.TOP));
			SetRect(RECT.RIGHT, rect.GetRect(RECT.RIGHT));
			SetRect(RECT.BOTTOM, rect.GetRect(RECT.BOTTOM));
		}
		public void CopyFrom(Rect rect, RECT r)
		{
			//値の設定
			SetRect(r, rect.GetRect(r));
		}
		public void CopyFrom(Rect rect, RECT copyTo, RECT copyFrom)
		{
			//値の設定
			SetRect(copyTo, rect.GetRect(copyFrom));
		}

		//値の取得
		public int GetRect(RECT r)
		{
			//対象
			switch (r)
			{
				case RECT.LEFT: return rect[0, 0];
				case RECT.TOP: return rect[0, 1];
				case RECT.RIGHT: return rect[1, 0];
				case RECT.BOTTOM: return rect[1, 1];
				default: return 0;
			}
		}
		public int GetRect(int r)
		{
			//対象
			switch (r)
			{
				case (int)RECT.LEFT: return rect[0, 0];
				case (int)RECT.TOP: return rect[0, 1];
				case (int)RECT.RIGHT: return rect[1, 0];
				case (int)RECT.BOTTOM: return rect[1, 1];
				default: return 0;
			}
		}

		//値の設定
		public void SetRect(RECT r, int n)
		{
			//対象
			switch (r)
			{
				case RECT.LEFT: rect[0, 0] = n; break;
				case RECT.TOP: rect[0, 1] = n; break;
				case RECT.RIGHT: rect[1, 0] = n; break;
				case RECT.BOTTOM: rect[1, 1] = n; break;
			}
		}
		public void SetRect(int r, int n)
		{
			//対象
			switch (r)
			{
				case (int)RECT.LEFT: rect[0, 0] = n; break;
				case (int)RECT.TOP: rect[0, 1] = n; break;
				case (int)RECT.RIGHT: rect[1, 0] = n; break;
				case (int)RECT.BOTTOM: rect[1, 1] = n; break;
			}
		}

		//幅取得
		public int GetWidth()
		{
			//width
			return GetRect(RECT.RIGHT) - GetRect(RECT.LEFT);
		}
		public int GetHeight()
		{
			//height
			return GetRect(RECT.BOTTOM) - GetRect(RECT.TOP);
		}
	}

	//CSV
	public void CreateCSV()
	{
		//マップ情報の置き場所
		CSVReader Csv = new CSVReader();

		//マップ情報読み込み
		Csv.LoadCsv("CSV_MAP");

		//マップ
		width = Csv.GetCsvWidth();
		height = Csv.GetCsvHeight();
		map = new int[width, height];
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				map[x, y] = Csv.GetDataInt(x, y);
			}
		}
	}

	//マップ取得
	public int GetMap((int x, int y) pos)
	{
		//マップ情報
		return map[pos.x, pos.y];
	}
	public int GetMap(int x, int y)
	{
		//マップ情報
		return map[x, y];
	}

	//マップサイズ
	public (int x, int y) GetMapSize()
	{
		//サイズ
		return (width, height);
	}
}