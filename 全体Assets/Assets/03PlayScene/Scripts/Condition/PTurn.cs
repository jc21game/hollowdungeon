using UnityEngine;
using UnityEngine.UI;

public class PTurn : MonoBehaviour
{
	//プレイヤー
	private Player player = null;
	//自身のテキスト
	private Text text = null;

	//初期化
	public void Start()
	{
		//プレイヤー取得
		player = GameObject.Find("Player(Clone)").GetComponent<Player>();

		//自身
		text = gameObject.GetComponent<Text>();
	}

	//更新
	public void Update()
	{
		//ターン
		text.text = player.state.trap[0].NowTurn().ToString();
	}
}
