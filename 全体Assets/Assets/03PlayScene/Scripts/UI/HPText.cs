using UnityEngine;
using UnityEngine.UI;

public class HPText : MonoBehaviour
{
	//コンポーネント取得用
	private Text text = null;

	//最大HP管理用
	private int maxHp = 0;

	//HP管理用
	private int hp = 0;

	//表示テキスト管理用
	private string str = "";

	//初期化
	public void Start()
	{
		//コンポーネント取得
		text = this.GetComponent<Text>();

		//初期化
		maxHp = ScoreManager.Score.GetMHp();
		hp = ScoreManager.Score.GetHp();
		str = hp + "/" + maxHp;
	}

	//更新
	public void Update()
	{
		//値の更新
		maxHp = ScoreManager.Score.GetMHp();
		hp = ScoreManager.Score.GetHp();

		//テキスト更新
		str = hp + "/" + maxHp;

		//テキスト表示
		text.text = str;
	}
}
