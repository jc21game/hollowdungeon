using UnityEngine;

//アイテムベース
public class ItemBase : MonoBehaviour
{
	//効果
	public enum EFFECT
	{
		HEAL,
		ATTACK,
		DEFFENCE,
		SKILL
	}
	public EFFECT effect = EFFECT.HEAL;

	//効能
	public int efficacy = 0;

	//ターン
	public int Max = 10;

	//初期化
	public virtual void Start()
	{
	}

	//更新
	public virtual void Update()
	{
	}

	//アタッチ
	protected void Attach(EFFECT effect)
	{
		//アタッチ
		this.effect = effect;
	}

	//効果
	public virtual void Execute()
	{
	}
}
