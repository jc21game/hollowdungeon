using UnityEngine;

public class Fairy : CharacterBase
{
	//移動方向
	enum Way
	{
		UP,
		RIGHT,
		DOWN,
		LEFT,
		RAND,
		NONE,
	}

	//方向(とりあえずは未定)
	Way way = Way.NONE;
	//プレイヤーの位置
	//主に１を見て行動を決める
	//２はプレイヤーが斜めの位置にいる場合に使用
	Way player1 = Way.NONE;
	Way player2 = Way.NONE;

	//関数呼び出し順
	private int sort = 0;

	//左右、上下に移動可能の場合
	private int LR = 0;
	private int UD = 0;

	//ターゲット
	private int target = 0;

	//初期化
	public override void Start()
	{
		base.Start();
	}

	//更新
	public override void Update()
	{
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//外部から呼び出された場合
		if (isColl)
		{
			//行動なし
			isColl = false;
			manager.Move();
			return true;
		}

		//回復
		if (Recovery())
		{
			//処理終了
			return true;
		}

		//移動
		if (Move())
		{
			//処理終了
			return true;
		}

		//処理なし
		return true;
	}

	//攻撃
	protected override bool Attack()
	{
		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//移動先
		if (IsPlayer())
		{
			//対象
			target = manager.GetCharacter(0).GetComponent<Player>().GetAroundCharacter();

			//プレイヤーの周りには誰もいない場合
			if (target == 0)
			{
				//差
				Vector2 dif = (Vector2)transform.position - manager.GetPosition(target);

				//プレイヤーが左にいる
				if (dif.x > 0 && dif.y == 0)
				{
					player1 = Way.LEFT;
					player2 = Way.LEFT;
				}
				//プレイヤーが右にいる
				else if (dif.x < 0 && dif.y == 0)
				{
					player1 = Way.RIGHT;
					player2 = Way.RIGHT;
				}
				//プレイヤーが下にいる
				else if (dif.x == 0 && dif.y > 0)
				{
					player1 = Way.DOWN;
					player2 = Way.DOWN;
				}
				//プレイヤーが上にいる
				else if (dif.x == 0 && dif.y < 0)
				{
					player1 = Way.UP;
					player1 = Way.UP;
				}
				//プレイヤーが左下にいる
				else if (dif.x > 0 && dif.y > 0)
				{
					player1 = Way.LEFT;
					player2 = Way.DOWN;
				}
				//プレイヤーが左上にいる
				else if (dif.x > 0 && dif.y < 0)
				{
					player1 = Way.LEFT;
					player2 = Way.UP;
				}
				//プレイヤーが右下にいる
				else if (dif.x < 0 && dif.y > 0)
				{
					player1 = Way.RIGHT;
					player2 = Way.DOWN;
				}
				//プレイヤーが右上にいる
				else if (dif.x < 0 && dif.y < 0)
				{
					player1 = Way.RIGHT;
					player2 = Way.UP;
				}

				//行動したか
				isMove = false;

				//現在位置が通路の場合
				if (map.GetMap(transform.position) == 1)
				{
					//四方のどこかが壁の場合未定に
					if (map.GetMap(GetFront(DIR.Dir.TOP)) == 0 || map.GetMap(GetFront(DIR.Dir.RIGHT)) == 0 || map.GetMap(GetFront(DIR.Dir.BOTTOM)) == 0 || map.GetMap(GetFront(DIR.Dir.LEFT)) == 0)
					{
						way = Way.NONE;
					}

					//呼び出す関数の順番を変える
					switch (sort)
					{
						case 0: Move_Up(); Move_Right(); Move_Down(); Move_Left(); sort++; break;
						case 1: Move_Right(); Move_Down(); Move_Left(); Move_Up(); sort++; break;
						case 2: Move_Down(); Move_Left(); Move_Up(); Move_Right(); sort++; break;
						case 3: Move_Left(); Move_Up(); Move_Right(); Move_Down(); sort = 0; break;
					}

				}
				//現在位置が部屋なら
				else if (map.GetMap(transform.position) > 1)
				{
					//左にいる
					if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x > 0 && manager.GetCharacter((GetFront(DIR.Dir.RIGHT))) == null)
					{
						//通路を優先的に移動
						//上に移動
						if (map.GetMap(GetFront(DIR.Dir.TOP)) == 1)
						{
							way = Way.UP;
						}
						//下に移動
						else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) == 1)
						{
							way = Way.DOWN;
						}
						else if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0)
						{
							//右に移動
							way = Way.RIGHT;
						}
						else
						{
							//プレイヤーとYが同じ
							if (dif.y == 0)
							{
								//上下に逃げ道がある場合ランダム
								if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0 && map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
								{
									//上下に逃げ道がある場合ランダム
									switch (UD)
									{
										case 0: way = Way.UP; UD = 1; break;
										case 1: way = Way.DOWN; UD = 0; break;
									}
								}
								//上だけ
								else if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0)
								{
									way = Way.UP;
								}
								//下だけ
								else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
								{
									way = Way.DOWN;
								}
							}
							else
							{
								//上に壁がないかつプレイヤーよりもYが高い
								if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0 && dif.y > 0)
								{
									way = Way.UP;
								}
								//下に壁がないかつプレイヤーよりもYが低い
								else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0 && dif.y < 0)
								{
									way = Way.DOWN;
								}
							}
						}
					}

					//右にいる
					else if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x < 0 && manager.GetCharacter((GetFront(DIR.Dir.LEFT))) == null)
					{
						//通路を優先的に移動
						//上に移動
						if (map.GetMap(GetFront(DIR.Dir.TOP)) == 1)
						{
							way = Way.UP;
						}
						//下に移動
						else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) == 1)
						{
							way = Way.DOWN;
						}
						else if (map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
						{
							//左に移動
							way = Way.LEFT;
						}
						else
						{
							//プレイヤーとYが同じ
							if (dif.y == 0)
							{
								//上下に逃げ道がある場合ランダム
								if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0 && map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
								{
									switch (UD)
									{
										case 0: way = Way.UP; UD = 1; break;
										case 1: way = Way.DOWN; UD = 0; break;
									}
								}
								//上だけ
								else if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0)
								{
									way = Way.UP;
								}
								//下だけ
								else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
								{
									way = Way.DOWN;
								}
							}
							else
							{
								//上に壁がないかつプレイヤーよりもYが高い
								if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0 && dif.y > 0)
								{
									way = Way.UP;
								}
								//下に壁がないかつプレイヤーよりもYが低い
								else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0 && dif.y < 0)
								{
									way = Way.DOWN;
								}
							}
						}
					}

					//下にいる
					else if (!isMove && dif.y > 0 && manager.GetCharacter((GetFront(DIR.Dir.TOP))) == null)
					{
						//通路を優先的に移動
						//右に移動
						if (map.GetMap(GetFront(DIR.Dir.RIGHT)) == 1)
						{
							way = Way.RIGHT;
						}
						//左に移動
						else if (map.GetMap(GetFront(DIR.Dir.LEFT)) == 1)
						{
							way = Way.LEFT;
						}
						else if (map.GetMap(GetFront(DIR.Dir.TOP)) != 0)
						{
							//上に移動
							way = Way.UP;
						}
						else
						{
							//プレイヤーとXが同じ
							if (dif.x == 0)
							{
								//左右に逃げ道がある場合ランダム
								if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0 && map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
								{
									switch (LR)
									{
										//右だけ
										case 0: way = Way.RIGHT; LR = 1; break;
										//左だけ
										case 1: way = Way.LEFT; LR = 0; break;
									}
								}
								//右に移動
								else if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0)
								{
									way = Way.RIGHT;
								}
								//左に移動
								else if (map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
								{
									way = Way.LEFT;
								}
							}
							else
							{
								//右に壁がないかつプレイヤーよりもXが高い
								if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0 && dif.x > 0)
								{
									way = Way.RIGHT;
								}
								//左に壁がないかつプレイヤーよりもXが低い
								else if (map.GetMap(GetFront(DIR.Dir.LEFT)) != 0 && dif.x < 0)
								{
									way = Way.LEFT;
								}
							}
						}
					}

					//上にいる
					else if (!isMove && dif.y < 0 && manager.GetCharacter((GetFront(DIR.Dir.BOTTOM))) == null)
					{
						//通路を優先的に移動
						//右に移動
						if (map.GetMap(GetFront(DIR.Dir.RIGHT)) == 1)
						{
							way = Way.RIGHT;
						}
						//左に移動
						else if (map.GetMap(GetFront(DIR.Dir.LEFT)) == 1)
						{
							way = Way.LEFT;
						}
						else if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
						{
							//下に移動
							way = Way.DOWN;
						}
						else
						{
							//プレイヤーとXが同じ
							if (dif.x == 0)
							{
								//左右に逃げ道がある場合ランダム
								if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0 && map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
								{
									switch (Random.Range(0, 1))
									{
										//右だけ
										case 0: way = Way.RIGHT; LR = 1; break;
										//左だけ
										case 1: way = Way.LEFT; LR = 0; break;
									}
								}
								//右に移動
								else if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0)
								{
									way = Way.RIGHT;
								}
								//左に移動
								else if (map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
								{
									way = Way.LEFT;
								}
							}
							else
							{
								//右に壁がないかつプレイヤーよりもXが高い
								if (map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0 && dif.x > 0)
								{
									way = Way.RIGHT;
								}
								//左に壁がないかつプレイヤーよりもXが低い
								else if (map.GetMap(GetFront(DIR.Dir.LEFT)) != 0 && dif.x < 0)
								{
									way = Way.LEFT;
								}
							}
						}
					}
				}
			}

			else
			{
				//差
				Vector2 dif = (Vector2)transform.position - manager.GetPosition(target);

				//行動したか
				isMove = false;

				//右にいる
				if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x > 0 && manager.GetCharacter((GetFront(DIR.Dir.LEFT))) == null && map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
				{
					//左に移動
					way = Way.LEFT;
				}

				//左にいる
				else if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x < 0 && manager.GetCharacter((GetFront(DIR.Dir.RIGHT))) == null && map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0)
				{
					//右に移動
					way = Way.RIGHT;
				}

				//上にいる
				else if (!isMove && dif.y > 0 && manager.GetCharacter((GetFront(DIR.Dir.BOTTOM))) == null && map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
				{
					//下に移動
					way = Way.DOWN;
				}

				//下にいる
				else if (!isMove && dif.y < 0 && manager.GetCharacter((GetFront(DIR.Dir.TOP))) == null && map.GetMap(GetFront(DIR.Dir.TOP)) != 0)
				{
					//上に移動
					way = Way.UP;
				}

				else
				{
					way = Way.RAND;
				}
			}
		}
		else
		{
			way = Way.RAND;
		}

		//wayを元に行動を決める
		switch (way)
		{
			case Way.UP: dir.dir = DIR.Dir.TOP; break;
			case Way.RIGHT: dir.dir = DIR.Dir.RIGHT; break;
			case Way.DOWN: dir.dir = DIR.Dir.BOTTOM; break;
			case Way.LEFT: dir.dir = DIR.Dir.LEFT; break;
			case Way.RAND: dir.dir = dir.ConvertDir(Random.Range(0, 4)); break;
			case Way.NONE: break;
		}

		isMove = true;

		if (map.IsMove(GetFront(dir.dir)) && isMove)
		{
			//カウント開始
			count = maxCount;

			//移動
			dir.DirChange(dir.dir);
			dir.AddFoot();
			transform.position = GetFront(dir.dir);
			manager.Move();

			//向き
			sprite.sprite = sprites[dir.GetNumber()];

			//処理終了
			return true;
		}

		//処理なし
		return base.Move();
	}

	//死亡
	public override void Death()
	{
		base.Death();
	}

	//被弾
	public override void Hit(int damage, int rank, string name, string text = "")
	{
		base.Hit(damage, rank, name);
	}

	//回復
	protected bool Recovery()
	{
		//対象
		target = manager.GetCharacter(0).GetComponent<Player>().GetAroundCharacter();

		//PL
		if (target == 0)
		{
			return false;
		}

		//八方
		Vector2[] vector = new Vector2[8] { transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position };
		vector[0].y++; vector[1].x++; vector[2].x--; vector[3].y--;
		vector[4].y++; vector[4].x++;
		vector[5].y++; vector[5].x--;
		vector[6].y--; vector[6].x++;
		vector[7].y--; vector[7].x--;

		//周り
		for (int i = 0; i < 8; i++)
		{
			//オブジェクト
			for (int j = 0; j < manager.GetMaxChar(); j++)
			{
				//いる場合
				if (vector[i] == manager.GetPosition(j))
				{
					if (manager.GetCharacter(vector[i]).GetNumber() == target)
					{
						//回復
						manager.GetCharacter(target).Heal(3 * ScoreManager.Score.GetFloor());
						return true;
					}
				}
			}
		}

		return false;
	}

	//上に移動
	private void Move_Up()
	{
		if (map.GetMap(GetFront(DIR.Dir.TOP)) >= 1 && player1 != Way.UP && player2 != Way.UP)
		{
			way = Way.UP;
		}
	}

	//右に移動
	private void Move_Right()
	{
		if (map.GetMap(GetFront(DIR.Dir.RIGHT)) >= 1 && player1 != Way.RIGHT && player2 != Way.RIGHT)
		{
			way = Way.RIGHT;
		}
	}

	//下に移動
	private void Move_Down()
	{
		if (map.GetMap(GetFront(DIR.Dir.BOTTOM)) >= 1 && player1 != Way.DOWN && player2 != Way.DOWN)
		{
			way = Way.DOWN;
		}
	}

	//左に移動
	private void Move_Left()
	{
		if (map.GetMap(GetFront(DIR.Dir.LEFT)) >= 1 && player1 != Way.LEFT && player2 != Way.LEFT)
		{
			way = Way.LEFT;
		}
	}
}
