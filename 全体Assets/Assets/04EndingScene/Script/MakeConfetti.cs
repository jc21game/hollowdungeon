using UnityEngine;

//紙吹雪生成
public class MakeConfetti : MonoBehaviour
{
	//プレハブ
	public GameObject confettiRight = null;
	public GameObject confettiLeft = null;
	public int probability = 0;

	//最大値
	private int maxvalue = 100;

	//スタート
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//生成
		if (Random.Range(0, maxvalue) >= maxvalue - probability)
		{
			//紙吹雪
			GameObject game = Instantiate(confettiRight);
			game.transform.SetParent(transform);
		}
		if (Random.Range(0, maxvalue) >= maxvalue - probability)
		{
			//紙吹雪
			GameObject game = Instantiate(confettiLeft);
			game.transform.SetParent(transform);
		}
	}
}
