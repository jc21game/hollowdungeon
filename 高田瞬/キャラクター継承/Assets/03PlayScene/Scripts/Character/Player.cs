using System.Threading;
using UnityEngine;
using UnityEngine.UI;

//プレイヤー
public class Player : CharacterBase
{
	//ステータス
	public int LV = 0;
	public int nextEXP = 0;
	public int autoHEAL = 0;
	public int autoHEALTurn = 0;
	private int turn = 1;
	public int skillPoint = 0;

	//前回の行動
	private BeforeAct act = null;
	private int action = 0;

	//ダッシュ
	private bool isDush = false;

	//カメラ
	private Camera mainCamera = null;
	private int camPos = -10;

	//時間
	private int sleepTimeDash = 100;

	//アイテム
	public Item item = null;

	//ステータス上昇値
	public int UP_HP = 0;
	public int UP_AT = 0;
	public int UP_DF = 0;
	public float UP_NE = 0;

	//ミニマップのみ表示
	private Canvas black = null;
	private Image blackImage = null;
	private bool onlyMinimap = false;

	//初期化
	public override void Start()
	{
		//親
		base.Start();

		//アイテム
		item = new Item(manager);

		//武器の設定
		SetWeapon();

		//前回の行動
		act = new BeforeAct();

		//カメラ
		mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		CameraMove();

		//ミニマップのみ表示用
		black = new GameObject("OnlyMiniMapCanvas").AddComponent<Canvas>();
		blackImage = new GameObject("OnlyMiniMapImage").AddComponent<Image>();
		black.transform.position = new Vector3(map.Csv.GetCsvWidth() / 2, -map.Csv.GetCsvHeight() / 2, 1);
		black.sortingOrder = 6499;
		blackImage.color = new Color(0, 0, 0, 255);
		blackImage.gameObject.transform.SetParent(black.transform);
		blackImage.gameObject.SetActive(false);
	}

	//更新
	public override void Update()
	{
		//--------------------------------------------------------------------------------------
		//スコアマネージャーにデータを渡す(基礎ステータス)
		SetBaseStatus();
		//--------------------------------------------------------------------------------------

		//親
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//メニューを開いていないかつダッシュ中でもない
		if (action != act.MENU && !isDush)
		{
			//ミニマップのみ表示
			OnlyMinimap();
		}

		//ミニマップのみ表示ではない場合
		if (!onlyMinimap)
		{
			//メニューを開いていない
			if (action != act.MENU)
			{
				//攻撃
				if (Attack())
				{
					//攻撃中
					if (isAttack)
					{
						//行動中
						return false;
					}

					//行動終了
					return true;
				}

				//移動
				if (Move())
				{
					//行動終了
					return true;
				}

				//待機
				if (Input.GetKey(KeyCode.C))
				{
					//行動終了
					return true;
				}
			}

			//メニュー
			if (Menu())
			{
				//行動終了
				return true;
			}
		}

		//処理なし
		return base.Execute();
	}

	//メニュー
	private bool Menu()
	{
		//Xキー入力かメニュー
		if (Input.GetKeyDown(KeyCode.X) || action == act.MENU)
		{
			//メニュー
			if (GameObject.Find("Menu(Clone)").GetComponent<Menu>().Execute())
			{
				//スリーブ
				Thread.Sleep(sleepTimeDash);

				//行動終了
				return true;
			}

			//行動
			action = GameObject.Find("Menu(Clone)").GetComponent<Menu>().GetState();
		}

		//処理なし
		return false;
	}

	//攻撃
	protected override bool Attack()
	{
		//Z入力または攻撃中
		if (Input.GetKeyDown(KeyCode.Z) || isAttack)
		{
			//行動変更
			action = act.ATTACK;

			//攻撃
			return base.Attack();
		}

		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//座標を記憶
		Vector3 pos = transform.position;

		//向き変えをするか
		bool isChange = Input.GetKey(KeyCode.LeftShift);

		//前行動が移動じゃない場合
		if (action != act.MOVE)
		{
			//足をリセット
			dir.ResetFoot();
		}

		//ダッシュ中ではなく複数キー入力している場合じゃない場合
		if (!isDush && (
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.LeftArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)))
			)
		{
			//処理なし
			isDush = false;
			sprite.sprite = sprites[dir.GetNumber()];
			return base.Move();
		}

		//上
		if ((Input.GetKey(KeyCode.UpArrow) && !isDush && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.TOP))
		{
			//上
			pos.y++;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.TOP);
		}

		//右
		if ((Input.GetKey(KeyCode.RightArrow) && !isDush && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.RIGHT))
		{
			//右
			pos.x++;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.RIGHT);
		}

		//左
		if ((Input.GetKey(KeyCode.LeftArrow) && !isDush && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.LEFT))
		{
			//左
			pos.x--;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.LEFT);
		}

		//下
		if ((Input.GetKey(KeyCode.DownArrow) && !isDush && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.BOTTOM))
		{
			//下
			pos.y--;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.BOTTOM);
		}

		//移動可能だった場合(向きのみ変更ではない場合)
		if (map.IsMove((Vector2)pos) && !isChange)
		{
			//移動
			transform.position = pos;
			dir.AddFoot();
			sprite.sprite = sprites[dir.GetNumber()];

			//表示
			sprite.sortingOrder = (int)(transform.position.x - transform.position.y * 64);

			//カメラ追従
			CameraMove();

			//カウント開始
			count = maxCount;

			//行動
			action = act.MOVE;

			//ダッシュ中
			if (isDush)
			{
				//スリーブ
				Thread.Sleep(sleepTimeDash);
			}

			//ダッシュ中ではない
			else
			{
				//スリーブ
				Thread.Sleep(sleepTimeNonDash);
			}

			//ダッシュに切り替え
			if (Input.GetKey(KeyCode.LeftControl))
			{
				//ダッシュ
				isDush = true;
			}

			//ダッシュ中
			if (isDush)
			{
				//八方
				Vector2[] vector = new Vector2[8] { transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position };
				vector[0].y++; vector[1].x++; vector[2].x--; vector[3].y--;
				vector[4].y++; vector[4].x++;
				vector[5].y++; vector[5].x--;
				vector[6].y--; vector[6].x++;
				vector[7].y--; vector[7].x--;

				//八方にオブジェクトが存在するか
				for (int i = 0; i < 8; i++)
				{
					//オブジェクト
					for (int j = 0; j < manager.GetMaxChar(); j++)
					{
						//いる場合
						if (vector[i] == manager.GetPosition(j) || vector[i] == map.GetStairsPosition() || (Vector2)transform.position == map.GetStairsPosition())
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}

					//アイテム
					for (int j = 0; j < map.maxPotions; j++)
					{
						//いる場合
						if (vector[i] == (Vector2)map.potions[j].transform.position || (Vector2)transform.position == (Vector2)map.potions[j].transform.position)
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}
				}

				//通路の本数
				int aisleNum = 0;

				//自身が通路で後ろも通路、前が壁の場合
				if (map.GetMap(transform.position) == map.mapInfomation.AISLE &&
					map.GetMap(GetFront(true)) == map.mapInfomation.AISLE &&
					map.GetMap(GetFront()) == map.mapInfomation.WALL)
				{
					//四方
					for (int i = 0; i < DIR.DIRCTIONS; i++)
					{
						//後ろの場合
						if (vector[i] == GetFront(true))
						{
							//処理なし
							continue;
						}

						//通路の場合
						if (map.GetMap(vector[i]) == map.mapInfomation.AISLE)
						{
							//通路の本数を増やす
							aisleNum++;
						}
					}

					//通路の本数が1本の場合
					if (aisleNum == 1)
					{
						//四方
						for (int i = 0; i < DIR.DIRCTIONS; i++)
						{
							//後ろの場合
							if (vector[i] == GetFront(true))
							{
								//処理なし
								continue;
							}

							//通路の場合
							if (map.GetMap(vector[i]) == map.mapInfomation.AISLE)
							{
								//方向
								switch (i)
								{
									case 0: dir.dir = CharacterBase.DIR.Dir.TOP; break;
									case 1: dir.dir = CharacterBase.DIR.Dir.RIGHT; break;
									case 2: dir.dir = CharacterBase.DIR.Dir.LEFT; break;
									case 3: dir.dir = CharacterBase.DIR.Dir.BOTTOM; break;
								}
								break;
							}
						}
					}
				}

				//方向を変えていない場合
				if (aisleNum != 1)
				{
					//移動可能か
					for (int i = 0; i < DIR.DIRCTIONS; i++)
					{
						//自身の左右が通路の場合
						//または自身と自身の目の前が一緒ではなく自身の前が壁ではない場合
						//または自身と自身の後ろが一緒ではなく自身の後ろが通路で壁ではない場合
						if ((
							(dir.dir == DIR.Dir.TOP || dir.dir == DIR.Dir.BOTTOM) &&
							(map.GetMap(GetFront(DIR.Dir.LEFT)) == map.mapInfomation.AISLE || map.GetMap(GetFront(DIR.Dir.RIGHT)) == map.mapInfomation.AISLE)
							) ||
							(
							(dir.dir == DIR.Dir.LEFT || dir.dir == DIR.Dir.RIGHT) &&
							(map.GetMap(GetFront(DIR.Dir.TOP)) == map.mapInfomation.AISLE || map.GetMap(GetFront(DIR.Dir.BOTTOM)) == map.mapInfomation.AISLE)
							) ||
							(
							((map.GetMap(transform.position) != map.GetMap(GetFront()) && map.GetMap(GetFront()) != map.mapInfomation.WALL) ||
							(map.GetMap(transform.position) != map.GetMap(GetFront(true)) && map.GetMap(GetFront(true)) == map.mapInfomation.AISLE)
							)
							))
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}
				}
			}

			//行動終了
			return true;
		}

		//処理なし
		isDush = false;
		sprite.sprite = sprites[dir.GetNumber()];
		return base.Move();
	}

	//攻撃を受ける
	public override void Hit(int damage)
	{
		//ダッシュが切れる
		isDush = false;

		//攻撃を受ける
		base.Hit(damage);
	}

	//死亡
	public override void Death()
	{
	}

	//経験値の取得
	public void GetEXP(int EXP)
	{
		//メッセージ送信
		manager.SetMessage("プレイヤーは" + EXP.ToString() + "の経験値を手に入れた。");

		//経験値取得
		if ((this.EXP += EXP) >= nextEXP)
		{
			//経験値分
			while (this.EXP >= nextEXP)
			{
				//上昇
				LV++;
				MAX_HP += UP_HP;
				HP += UP_HP;
				ATC += UP_AT;
				DEF += UP_DF;
				this.EXP -= nextEXP;
				nextEXP = (int)(nextEXP * UP_NE);

				//サウンド
				SoundManager.SoundManagerGeneration();

				//再生
				SoundManager.PlaySE("Sounds/SE/レベルアップ");

			}

			//メッセージ送信
			manager.SetMessage("プレイヤーはレベル" + LV + "に上がった!");
		}
	}

	//自動回復
	public void AutoHeal()
	{
		//ターン経過
		if ((++turn) == autoHEALTurn)
		{
			//初期化
			turn = 1;

			//回復
			Heal(autoHEAL);
		}
	}

	//武器の設定
	private void SetWeapon()
	{
		//武器パス
		string path = "Player/IMG_0";
		int baseNumber = 101;
		int imageNumber = 12;

		//基礎ステータス取得
		GetBaseStatus();

		//画像のアタッチ
		for (int i = 0; i < imageNumber; i++)
		{
			//アタッチ
			sprites[i] = Resources.Load<Sprite>(path + (baseNumber + i + (weapon - 1) * imageNumber).ToString());
		}
	}

	//カメラ追従
	private void CameraMove()
	{
		//カメラの位置
		Vector3 pos = transform.position;
		pos.z = camPos;

		//カメラの移動
		mainCamera.transform.position = pos;
	}

	//ミニマップのみ表示
	private void OnlyMinimap()
	{
		//スペースキーの入力
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//切り替え
			onlyMinimap = !onlyMinimap;
		}

		//表示
		blackImage.gameObject.SetActive(onlyMinimap);
	}

	//前回の行動
	private class BeforeAct
	{
		//なし
		public int NONE = 0;

		//移動
		public int MOVE = 1;

		//攻撃
		public int ATTACK = 2;

		//メニュー
		public int MENU = 3;
	}

	//アイテム
	public class Item
	{
		private PlaySceneManager manager = null;

		//回復
		public int GREEN = 0;
		public string GREEN_POTION = "緑色のポーション";

		//攻撃
		public int RED = 0;
		public string RED_POTION = "赤色のポーション";

		//防御
		public int BLUE = 0;
		public string BLUE_POTION = "青色のポーション";

		//経験値
		public int YELLOW = 0;
		public string YELLOW_POTION = "黄色のポーション";

		//スキルポイント
		public int WHITE = 0;
		public string WHITE_POTION = "白色のポーション";

		//コンストラクタ
		public Item(PlaySceneManager manager)
		{
			this.manager = manager;
		}

		//アイテム名取得
		public string GetItemName(int number)
		{
			//アイテム
			switch (number)
			{
				case 0:
					return GREEN_POTION;
				case 1:
					return RED_POTION;
				case 2:
					return BLUE_POTION;
				case 3:
					return YELLOW_POTION;
				case 4:
					return WHITE_POTION;
				default:
					return null;
			}
		}

		//アイテム取得
		public void GetItem(int number)
		{
			//アイテム
			switch (number)
			{
				case 0:
					GREEN++;
					break;
				case 1:
					RED++;
					break;
				case 2:
					BLUE++;
					break;
				case 3:
					YELLOW++;
					break;
				case 4:
					WHITE++;
					break;
			}
		}

		//アイテム使用
		public void UseItem(int number)
		{
			//アイテム
			switch (number)
			{
				case 0:
					GREEN--;
					manager.SetMessage("プレイヤーは" + GREEN_POTION + "を使用した。");
					break;
				case 1:
					RED--;
					manager.SetMessage("プレイヤーは" + RED_POTION + "を使用した。");
					break;
				case 2:
					BLUE--;
					manager.SetMessage("プレイヤーは" + BLUE_POTION + "を使用した。");
					break;
				case 3:
					YELLOW--;
					manager.SetMessage("プレイヤーは" + YELLOW_POTION + "を使用した。");
					break;
				case 4:
					WHITE--;
					manager.SetMessage("プレイヤーは" + WHITE_POTION + "を使用した。");
					break;
			}

			//効能実行
			GameObject potion = Instantiate(manager.potions[number]);
			potion.GetComponent<ItemBase>().Execute();
			Destroy(potion);
		}
	}

	//基礎ステータス渡し
	public void SetBaseStatus()
	{
		//レベル
		ScoreManager.Score.SetLv(LV);

		//HP
		ScoreManager.Score.SetMHp(MAX_HP);
		ScoreManager.Score.SetHp(HP);

		//攻撃力
		ScoreManager.Score.SetAttack(ATC);

		//防御力
		ScoreManager.Score.SetDefence(DEF);

		//スキルポイント
		ScoreManager.Score.SetSkillPt(skillPoint);

		//緑色ポーションの所持数
		ScoreManager.Score.SetGreenPotion(item.GREEN);

		//赤色ポーションの所持数
		ScoreManager.Score.SetRedPotion(item.RED);

		//青色ポーションの所持数
		ScoreManager.Score.SetBluePotion(item.BLUE);

		//黄色ポーションの所持数
		ScoreManager.Score.SetYellowPotion(item.YELLOW);

		//白色ポーションの所持数
		ScoreManager.Score.SetWhitePotion(item.WHITE);

		//取得した経験値
		ScoreManager.Score.SetHaveExp(EXP);

		//次のレベルまでの経験値
		ScoreManager.Score.SetNextExp(nextEXP);
	}

	//基礎ステータス取得
	public void GetBaseStatus()
	{
		//レベル
		LV = ScoreManager.Score.GetLv();

		//HP
		MAX_HP = ScoreManager.Score.GetMHp();
		HP = ScoreManager.Score.GetHp();

		//攻撃力
		ATC = ScoreManager.Score.GetAttack();

		//防御力
		DEF = ScoreManager.Score.GetDefence();

		//スキルポイント
		skillPoint = ScoreManager.Score.GetSkillPt();

		//緑色ポーションの所持数
		item.GREEN = ScoreManager.Score.GetGreenPotion();

		//赤色ポーションの所持数
		item.RED = ScoreManager.Score.GetRedPotion();

		//青色ポーションの所持数
		item.BLUE = ScoreManager.Score.GetBluePotion();

		//黄色ポーションの所持数
		item.YELLOW = ScoreManager.Score.GetYellowPotion();

		//白色ポーションの所持数
		item.WHITE = ScoreManager.Score.GetWhitePotion();

		//取得した経験値
		EXP = ScoreManager.Score.GetHaveExp();

		//次のレベルまでの経験値
		nextEXP = ScoreManager.Score.GetNextExp();

		//武器
		weapon = (int)ScoreManager.Score.GetMyWeapon();
	}
}
