using UnityEngine;
public class Dummy : CharacterBase
{
	private Trent trent = null;


	//初期化
	public override void Start()
	{
		//トレントを探してくる
		trent = GameObject.Find("Trent").GetComponent<Trent>();
		base.Start();
	}

	//更新
	public override void Update()
	{
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//処理なし
		return true;
	}

	//攻撃
	protected override bool Attack()
	{
		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//処理なし
		return base.Move();
	}
}