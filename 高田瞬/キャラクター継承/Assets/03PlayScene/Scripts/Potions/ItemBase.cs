using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//アイテムベース
public class ItemBase : MonoBehaviour
{
	//効果
	public enum EFFECT
	{
		HEAL,
		ATTACK,
		DEFFENCE,
		EXPERIENCE,
		SKILL
	}
	public EFFECT effect = EFFECT.HEAL;

	//効能
	public int efficacy = 0;

	//初期化
	public virtual void Start()
	{
	}

	//更新
	public virtual void Update()
	{
	}

	//アタッチ
	protected void Attach(EFFECT effect)
	{
		//アタッチ
		this.effect = effect;
	}

	//効果
	public virtual void Execute()
	{
	}
}
