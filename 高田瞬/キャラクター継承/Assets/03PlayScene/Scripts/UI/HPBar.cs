using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
	//コンポーネント取得用
	private Slider hpBar = null;

	//初期化
	public void Start()
	{
		//コンポーネント取得
		hpBar = this.GetComponent<Slider>();

		//初期化
		hpBar.maxValue = ScoreManager.Score.GetMHp();
		hpBar.value = ScoreManager.Score.GetHp();
	}

	//更新
	public void Update()
	{
		//最大HPを設定
		hpBar.maxValue = ScoreManager.Score.GetMHp();

		//HPを設定
		hpBar.value = ScoreManager.Score.GetHp();
	}
}
