using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	//インスペクターウィンドウからゲームオブジェクトを設定する
	public GameObject MenuPanel = null;
	public GameObject ItemPanel = null;
	public GameObject StatusPanel = null;
	//選択時の背景色の変更するためのオブジェクト
	public GameObject Item = null;
	public GameObject Status = null;
	public GameObject End = null;
	//大体のメニューに表示するステータス
	public GameObject MainStatusText = null;
	//選択時に背景色の変更するためのオブジェクト
	public GameObject Green = null;
	public GameObject Red = null;
	public GameObject Blue = null;
	public GameObject Yellow = null;
	public GameObject White = null;
	//各アイテムのテキスト変更
	public GameObject GreenText = null;
	public GameObject RedText = null;
	public GameObject BlueText = null;
	public GameObject YellowText = null;
	public GameObject WhiteText = null;
	//選択時の背景色の変更するためのオブジェクト
	public GameObject HP = null;
	public GameObject ATTACK = null;
	public GameObject DEFENSE = null;
	//ポイントを振った時の値を表示
	public GameObject HPText = null;
	public GameObject ATTACKText = null;
	public GameObject DEFENSEText = null;
	//ステータスメニューに表示するステータス
	public GameObject SubStatusText = null;
	//各メニューフラグ
	public static bool menu = false;
	public static bool item = false;
	public static bool status = false;
	public static bool end = false;

	//選択時の背景色を変化させる(非選択時の場合は背景色を背景と同化させる)
	Color Select = new Color(255f, 255f, 0f, 0.5f);
	Color NotSelect = new Color(82f, 125f, 220f, 0f);
	//メニュー表示時各項目の非選択時の背景色(白)
	Color Back = new Color(255f, 255f, 255f, 1f);

	//各アイテムごとのテキスト書き換え
	private Text GText = null;
	private Text RText = null;
	private Text BText = null;
	private Text YText = null;
	private Text WText = null;

	//選択している項目番号
	private int Number = 0;
	private Text HText = null;
	private Text AText = null;
	private Text DText = null;
	private Text MainState = null;
	private Text SubState = null;

	//各ステータスの加算値と初期のステータス
	private int H = 0;
	private int MH = 0;
	private int A = 0;
	private int D = 0;
	private int LH = 0;
	private int LMH = 0;
	private int LA = 0;
	private int LD = 0;

	//プレイヤー
	private GameObject player = null;
	private Player Player = null;

	//ターン終了
	private bool fin = false;

	//メッセージ
	private bool doOnce = false;
	private string[] potion = null;

	//初期化
	private void Start()
	{
		//テキストを書き込むための設定
		GText = GreenText.GetComponent<Text>();
		RText = RedText.GetComponent<Text>();
		BText = BlueText.GetComponent<Text>();
		YText = YellowText.GetComponent<Text>();
		WText = WhiteText.GetComponent<Text>();
		HText = HPText.GetComponent<Text>();
		AText = ATTACKText.GetComponent<Text>();
		DText = DEFENSEText.GetComponent<Text>();
		MainState = MainStatusText.GetComponent<Text>();
		SubState = SubStatusText.GetComponent<Text>();

		//各ステータスを取得
		player = GameObject.Find("Player(Clone)");
		Player = player.GetComponent<Player>();
		LH = H = Player.HP;
		LMH = MH = Player.MAX_HP;
		LA = A = Player.ATC;
		LD = D = Player.DEF;

		//ステータス状態(初期)
		HText.text = (H - Player.HP).ToString();
		AText.text = (A - Player.ATC).ToString();
		DText.text = (D - Player.DEF).ToString();

		//ステータス
		MainState.text = "LV:" + Player.LV.ToString() + "\nHP:" + Player.HP.ToString() + "/" + Player.MAX_HP.ToString() + "\n満腹度:100%\n攻撃力:" + Player.ATC.ToString() + "\n防御力:" + Player.DEF + "\nEXP:" + Player.EXP.ToString() + "\n次のレベルまで:10\nスキルポイント:0\n状態:健康";
		SubState.text = "LV:" + Player.LV.ToString() + "\nHP:" + Player.HP.ToString() + "/" + Player.MAX_HP.ToString() + "\n満腹度:100%\n攻撃力:" + Player.ATC.ToString() + "\n防御力:" + Player.DEF + "\nEXP:" + Player.EXP.ToString() + "\n次のレベルまで:10\nスキルポイント:0\n状態:健康";

		//オーダー
		gameObject.GetComponent<Canvas>().sortingOrder = 10;

		//テキスト
		potion = new string[5] 
		{ 
			"HPを3ポイント回復。",
			"攻撃力を3ポイント上昇。",
			"防御力を3ポイント上昇。",
			"経験値を3ポイント取得。",
			"スキルポイントを3ポイント取得。",
		};
	}

	//更新
	private void Update()
	{
		//trueなら表示,falseなら非表示(２つ以上trueになることはない)
		MenuPanel.SetActive(menu);
		ItemPanel.SetActive(item);
		StatusPanel.SetActive(status);
	}

	//行動
	public bool Execute()
	{
		//Xが押された時の各挙動
		if (Input.GetKeyDown(KeyCode.X))
		{
			//何も開いていない
			if (!menu && !item && !status && !end)
			{
				menu = true;
				MessageWindowManager.MessageImageHide();
				Number = 1;
			}
			//メニューのみ
			else if (menu && !item && !status && !end)
			{
				menu = false;
				Number = 1;
			}
			//アイテムのみ
			else if (!menu && item && !status && !end)
			{
				menu = true;
				item = false;
				Number = 1;
			}
			//ステータスのみ
			else if (!menu && !item && status && !end)
			{
				menu = true;
				status = false;
				Number = 2;
			}
		}

		//メニューが開いていたら
		if (menu && !end)
		{
			//カーソル移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				if (Number >= 3)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}

			//カーソル移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (Number <= 1)
				{
					Number = 3;
				}
				else
				{
					Number--;
				}
			}

			//決定
			if (Input.GetKeyDown(KeyCode.Z))
			{
				if (Number != 3)
				{
					menu = false;
				}
				else
				{
					doOnce = true;
				}
				SelectMenu(Number);
				return false;
			}

			//カーソルの色変更
			switch (Number)
			{
				case 1:
					Item.GetComponent<Image>().color = Select;
					Status.GetComponent<Image>().color = Back;
					End.GetComponent<Image>().color = Back;
					break;
				case 2:
					Item.GetComponent<Image>().color = Back;
					Status.GetComponent<Image>().color = Select;
					End.GetComponent<Image>().color = Back;
					break;
				case 3:
					Item.GetComponent<Image>().color = Back;
					Status.GetComponent<Image>().color = Back;
					End.GetComponent<Image>().color = Select;
					break;
			}
		}

		//アイテムメニューが開いていたら
		if (item)
		{
			//アイテムメニューテキストの表示
			GText.text = Player.item.GREEN_POTION + "　　" + Player.item.GREEN + "個";
			RText.text = Player.item.RED_POTION + "　　" + Player.item.RED + "個";
			BText.text = Player.item.BLUE_POTION + "　　" + Player.item.BLUE + "個";
			YText.text = Player.item.YELLOW_POTION + "　　" + Player.item.YELLOW + "個";
			WText.text = Player.item.WHITE_POTION + "　　" + Player.item.WHITE + "個";

			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				if (Number >= 5)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (Number <= 1)
				{
					Number = 5;
				}
				else
				{
					Number--;
				}
			}
			//カーソルの色変更
			switch (Number)
			{
				case 1:
					Green.GetComponent<Image>().color = Select;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 2:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = Select;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 3:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = Select;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 4:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = Select;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 5:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = Select;
					break;
			}

			//テキスト
			MessageWindowManager.MessageImageHide();
			MessageWindowManager.MessageTextChange(potion[Number - 1]);

			//アイテムの使用
			if (Input.GetKeyDown(KeyCode.Z))
			{
				//使用
				bool use = false;
				switch (Number)
				{
					case 1: if (Player.item.GREEN > 0) use = true; break;
					case 2: if (Player.item.RED > 0) use = true; break;
					case 3: if (Player.item.BLUE > 0) use = true; break;
					case 4: if (Player.item.YELLOW > 0) use = true; break;
					case 5: if (Player.item.WHITE > 0) use = true; break;
				}

				//使用
				if (use)
				{
					//ターン経過
					fin = true;

					//使用
					Player.item.UseItem(Number - 1);
					MessageWindowManager.MessageImageHide();
				}
			}
		}

		//ステータスメニューが開いていたら
		if (status)
		{
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				if (Number >= 3)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (Number <= 1)
				{
					Number = 3;
				}
				else
				{
					Number--;
				}
			}
			//カーソルの色変更
			switch (Number)
			{
				case 1:
					HP.GetComponent<Image>().color = Select;
					ATTACK.GetComponent<Image>().color = NotSelect;
					DEFENSE.GetComponent<Image>().color = NotSelect;
					break;
				case 2:
					HP.GetComponent<Image>().color = NotSelect;
					ATTACK.GetComponent<Image>().color = Select;
					DEFENSE.GetComponent<Image>().color = NotSelect;
					break;
				case 3:
					HP.GetComponent<Image>().color = NotSelect;
					ATTACK.GetComponent<Image>().color = NotSelect;
					DEFENSE.GetComponent<Image>().color = Select;
					break;

			}

			//ステータスパラメータの変更
			switch (Number)
			{
				case 1:
					if (Input.GetKeyDown(KeyCode.LeftArrow) && H - LH > 0)
					{
						H--;
						MH--;
					}
					if (Input.GetKeyDown(KeyCode.RightArrow))
					{
						H++;
						MH++;
					}
					break;
				case 2:
					if (Input.GetKeyDown(KeyCode.LeftArrow) && A - LA > 0)
					{
						A--;
					}
					if (Input.GetKeyDown(KeyCode.RightArrow))
					{
						A++;
					}
					break;
				case 3:
					if (Input.GetKeyDown(KeyCode.LeftArrow) && D - LD > 0)
					{
						D--;
					}
					if (Input.GetKeyDown(KeyCode.RightArrow))
					{
						D++;
					}
					break;

			}
			//ポイント振り分け
			HText.text = (H - LH).ToString();
			AText.text = (A - LA).ToString();
			DText.text = (D - LD).ToString();
			//振り分けた各ステータスをセット
			Player.HP = H;
			Player.MAX_HP = MH;
			Player.ATC = A;
			Player.DEF = D;
			//振り分けたステータスを反映
			SubState.text = "LV:" + Player.LV.ToString() + "\nHP:" + Player.HP.ToString() + "/" + Player.MAX_HP.ToString() + "\n満腹度:100%\n攻撃力:" + Player.ATC.ToString() + "\n防御力:" + Player.DEF + "\nEXP:" + Player.EXP.ToString() + "\n次のレベルまで:" + Player.nextEXP.ToString() + "\nスキルポイント:0\n状態:健康";
		}

		//end
		if (end)
		{
			if(doOnce)
			{
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().ResetMessage();
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().SetMessage("ゲームを終了しますか？");
			}
			GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMax();

			//クエスチョンボックスを表示
			YesNoQuestion.QuestionDisplay();

			//「はい」「いいえ」切り替え
			if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
			{
				YesNoQuestion.ArrowChange();
			}

			//選択決定
			else if (Input.GetKeyDown(KeyCode.Z))
			{
				//Yes
				if (YesNoQuestion.GetArrow())
				{
					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GameFinStart();
					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().gameFin = true;
				}

				//No
				else
				{
					end = false;
					Number = 3;
					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMin();
				}

				//クエスチョンボックスを非表示
				YesNoQuestion.QuestionHide();
			}

			else if (Input.GetKeyDown(KeyCode.X))
			{
				//クエスチョンボックスを非表示
				YesNoQuestion.QuestionHide();
				end = false;
				Number = 3;
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMin();
			}
		}

		//ステータス反映
		MainState.text = "LV:" + Player.LV.ToString() + "\nHP:" + Player.HP.ToString() + "/" + Player.MAX_HP.ToString() + "\n満腹度:100%\n攻撃力:" + Player.ATC.ToString() + "\n防御力:" + Player.DEF + "\nEXP:" + Player.EXP.ToString() + "\n次のレベルまで:" + Player.nextEXP.ToString() + "\nスキルポイント:0\n状態:健康";

		//ターン終了時
		if (fin)
		{
			menu = item = status = end = false;
			fin = false;
			return true;
		}

		//ターン終了していない
		return false;
	}

	//メニュー切り替え
	private void SelectMenu(int Val)
	{
		Number = 1;
		switch (Val)
		{
			case 1: item = true; break;
			case 2: status = true; break;
			case 3: end = true; break;
		}
	}

	//ステート
	public int GetState()
	{
		//状態
		if (menu || item || status || end)
			return 3;
		return 0;
	}
}
