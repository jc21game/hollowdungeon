using ScoreManager;
using UnityEngine;
using UnityEngine.UI;

//エンディングシーン
public class EndingSceneManager : MonoBehaviour
{
	//テキスト
	public Text[] texts = null;
	private int countFirst = 3;
	private int countSecond = 8;


	//カウント
	private int Count = 0;

	//初期化
	void Start()
	{
		//フェードイン
		FadeScript.FadeIn();

		//再生
		SoundManager.SoundManagerGeneration();
		SoundManager.PlayBGM("Sounds/BGM/Ending");

		//スコアを表示するための準備
		Resources.Load("Canvas");
		texts[0].text = "プレイ時間:" + Score.GetplayTime().ToString();
		texts[1].text = "倒した数:" + Score.Getdefeat().ToString();
		texts[2].text = "総被ダメージ:" + Score.GetCoveredDamage().ToString();
		texts[3].text = "総与ダメージ:" + Score.GetGivingDamage().ToString();
		texts[4].text = "HP:" + Score.GetHp().ToString();
		texts[5].text = "Lv:" + Score.GetLv().ToString();
		texts[6].text = "攻撃力:" + Score.GetAttack().ToString();
		texts[7].text = "防御力:" + Score.GetDefence().ToString();
		texts[8].text = "緑色ポーション:" + Score.GetUseGreenPotion().ToString();
		texts[9].text = "青色ポーション:" + Score.GetUseBluePotion().ToString();
		texts[10].text = "赤色ポーション:" + Score.GetUseRedPotion().ToString();
		texts[11].text = "白色ポーション:" + Score.GetUseWhitePotion().ToString();
		texts[12].text = "受けた状態異常の回数:" + Score.GetstateAnomaly().ToString();
		texts[13].text = "手に入れた経験値:" + Score.GetHaveExp().ToString();
		texts[14].text = "手に入れたスキルポイント:" + Score.GetSkillPt().ToString();


		//生成
		MessageWindowManager.ImageGeneration();
		MessageWindowManager.MessageImageDisplay();
	}

	//更新
	void Update()
	{
		//カウントを増やす
		if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
		{
			Count++;
		}

		//消す
		for (int i = 0; i < texts.Length; i++)
		{
			texts[i].gameObject.SetActive(false);
		}

		//状態
		switch (Count)
		{
			//0-3
			case 0:
			{
				//表示
				for (int i = 0; i <= countFirst; i++)
				{
					texts[i].gameObject.SetActive(true);
				}

				//終了
				break;
			}

			//4-8
			case 1:
			{
				//表示
				for (int i = countFirst + 1; i <= countSecond; i++)
				{
					texts[i].gameObject.SetActive(true);
				}

				//終了
				break;
			}

			//9-12
			case 2:
			{
				//表示
				for (int i = countSecond + 1; i < texts.Length; i++)
				{
					texts[i].gameObject.SetActive(true);
				}

				//終了
				break;
			}

			//終了
			default:
			{
				//表示
				for (int i = countSecond + 1; i < texts.Length; i++)
				{
					texts[i].gameObject.SetActive(true);
				}

				//フェードアウト
				FadeScript.FadeOut();

				//完全にフェードアウトが終了した場合
				if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
				{
					//タイトルシーンに移動
					UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
				}

				//終了
				break;
			}
		}
	}
}