using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ScoreManager
{
	public class Score
	{
		//決定した武器
		static private IconBase.Icon MyWeapon = IconBase.Icon.SWORD;

		//決定したダンジョン
		static private IconBase.Icon MyDungeon = IconBase.Icon.NONE;

		//プレイヤーのレベル
		static private int plLv = 1;

		//プレイヤーのHP
		static private int plMHp = 15;
		static private int plHp = 15;

		//プレイヤーの攻撃力
		static private int plAttack = 3;

		//プレイヤーの防御力
		static private int plDefence = 2;

		//使用スキルポイント
		static private int skillPt = 0;

		//プレイ時間
		static private int playTime = 0;

		//階層
		static private int currentFloor = 1;

		//総ターン数
		static private int progressTurn = 0;

		//倒した数
		static private int defeat = 0;

		//総与攻撃回数
		static private int givingAttack = 0;

		//総与ダメージ量
		static private int givingDamage = 0;

		//総被攻撃回数
		static private int coveredAttack = 0;

		//総被ダメージ量
		static private int coveredDamage = 0;

		//総アイテム取得数
		static private int haveItem = 0;

		//総アイテム使用回数
		static private int useItem = 0;

		//総被回復量
		static private int coveredRecovery = 0;

		//移動したマス
		static private int moveTrout = 0;

		//踏んだギミックマス
		static private int stepGimmickTrout = 0;

		//受けた状態異常の回数
		static private int stateAnomaly = 0;

		//-------------------------------------------------------------------------------------------

		//緑色ポーションの所持数
		static private int haveGreenPotion = 0;

		//赤色ポーションの所持数
		static private int haveRedPotion = 0;

		//青色のポーションの所持数
		static private int haveBluePotion = 0;

		//黄色のポーションの所持数
		static private int haveYellowPotion = 0;

		//白色のポーションの所持数
		static private int haveWhitePotion = 0;

		//緑色ポーションの使用数
		static private int useGreenPotion = 0;

		//赤色ポーションの使用数
		static private int useRedPotion = 0;

		//青色のポーションの使用数
		static private int useBluePotion = 0;

		//黄色のポーションの使用数
		static private int useYellowPotion = 0;

		//白色のポーションの使用数
		static private int useWhitePotion = 0;

		//取得した経験値の設定
		static private int haveExp = 0;

		//次のレベルまでの経験値
		static private int nextExp = 15;

		//-------------------------------------------------------------------------------------------

		//決定した武器のゲッター
		static public IconBase.Icon GetMyWeapon()
		{
			return MyWeapon;
		}

		//決定した武器のセッター
		static public void SetMyWeapon(IconBase.Icon icon)
		{
			MyWeapon = icon;
		}

		//決定したダンジョンのゲッター
		static public IconBase.Icon GetMyDungeon()
		{
			return MyDungeon;
		}

		//決定したダンジョンのセッター
		static public void SetMyDungeon(IconBase.Icon icon)
		{
			MyDungeon = icon;
		}






		//プレイヤーレベルを取得
		static public int GetLv()
		{
			return plLv;
		}

		//プレイヤーレベルを設定
		static public void SetLv(int lv)
		{
			plLv = lv;
		}

		//プレイヤーのHPを取得
		static public int GetMHp()
		{
			return plMHp;
		}

		//プレイヤーのHPを設定
		static public void SetMHp(int hp)
		{
			plMHp = hp;
		}

		//プレイヤーのHPを取得
		static public int GetHp()
		{
			return plHp;
		}

		//プレイヤーのHPを設定
		static public void SetHp(int hp)
		{
			plHp = hp;
		}

		//プレイヤーの攻撃力を取得
		static public int GetAttack()
		{
			return plAttack;
		}

		//プレイヤーの攻撃力を設定
		static public void SetAttack(int attack)
		{
			plAttack = attack;
		}

		//プレイヤーの防御力を取得
		static public int GetDefence()
		{
			return plDefence;
		}

		//プレイヤーの防御力を設定
		static public void SetDefence(int defence)
		{
			plDefence = defence;
		}

		//使用スキルポイントを取得
		static public int GetSkillPt()
		{
			return skillPt;
		}

		//使用スキルポイントの設定
		static public void SetSkillPt(int pt)
		{
			skillPt = pt;
		}

		//階層を取得
		static public int GetFloor()
		{
			return currentFloor;
		}

		//階層の設定
		static public void SetFloor(int floor)
		{
			currentFloor = floor;
		}

		//プレイ時間を取得
		static public int GetplayTime()
		{
			return playTime;
		}

		//プレイ時間の設定
		static public void SetplayTime(int pt)
		{
			playTime = pt;
		}

		//倒した数を取得
		static public int Getdefeat()
		{
			return defeat;
		}

		//倒した数の設定
		static public void Setdefeat(int de)
		{
			defeat = de;
		}


		//総ターン数を取得
		static public int GetTurn()
		{
			return progressTurn;
		}

		//総ターン数を設定
		static public void SetTurn(int turn)
		{
			progressTurn = turn;
		}

		//総与攻撃回数の取得
		static public int GetGivingAttack()
		{
			return givingAttack;
		}

		//総与攻撃回数の設定
		static public void SetGivingAttack(int attack)
		{
			givingAttack = attack;
		}

		//総与ダメージ量の取得
		static public int GetGivingDamage()
		{
			return givingDamage;
		}

		//総与ダメージ量の設定
		static public void SetGivingDamage(int damage)
		{
			givingDamage = damage;
		}

		//総被攻撃回数の取得
		static public int GetCoveredAttack()
		{
			return coveredAttack;
		}

		//総被攻撃回数の設定
		static public void SetCoveredAttack(int attack)
		{
			coveredAttack = attack;
		}

		//総被ダメージ量の取得
		static public int GetCoveredDamage()
		{
			return coveredDamage;
		}

		//総被ダメージ量の取得
		static public void SetCoveredDamage(int damage)
		{
			coveredDamage = damage;
		}

		//総アイテム取得数の取得
		static public int GetHaveItem()
		{
			return haveItem;
		}

		//総アイテム取得数の設定
		static public void SetHaveItem(int item)
		{
			haveItem = item;
		}

		//総アイテム使用数の取得
		static public int GetUseItem()
		{
			return useItem;
		}

		//総アイテム使用数の設定
		static public void SetUseItem(int item)
		{
			useItem = item;
		}

		//総被回復量の取得
		static public int GetCoveredRecovery()
		{
			return coveredRecovery;
		}

		//総被回復量の設定
		static public void SetCoveredRecovery(int recovery)
		{
			coveredRecovery = recovery;
		}

		//移動したマスの取得
		static private int GetmoveTrout()
		{
			return moveTrout;
		}

		//移動したマスの設定
		static public void SetmoveTrout(int move)
		{
			moveTrout =　move;
		}

		//踏んだギミックマスの取得
		static private int GetstepGimmickTrout()
		{
			return stepGimmickTrout;
		}

		//踏んだギミックマスの設定
		static public void SetstepGimmickTrout(int gimmick)
		{
			stepGimmickTrout = gimmick;
		}

		//受けた状態異常の回数の取得
		static public int GetstateAnomaly()
		{
			return stateAnomaly;
		}

		//受けた状態異常の回数の設定
		static public void SetstateAnomaly(int sa)
		{
			stateAnomaly = sa;
		}

		//-------------------------------------------------------------------------------------------

		//緑色ポーションの所持数の設定
		static public void SetGreenPotion(int num)
		{
			haveGreenPotion = num;
		}

		//緑色ポーションの所持数の取得
		static public int GetGreenPotion()
		{
			return haveGreenPotion;
		}

		//赤色ポーションの所持数の設定
		static public void SetRedPotion(int num)
		{
			haveRedPotion = num;
		}

		//赤色ポーションの所持数の取得
		static public int GetRedPotion()
		{
			return haveRedPotion;
		}

		//青色ポーションの所持数の設定
		static public void SetBluePotion(int num)
		{
			haveBluePotion = num;
		}

		//青色ポーションの所持数の取得
		static public int GetBluePotion()
		{
			return haveBluePotion;
		}

		//黄色ポーションの所持数の設定
		static public void SetYellowPotion(int num)
		{
			haveYellowPotion = num;
		}

		//黄色ポーションの所持数の取得
		static public int GetYellowPotion()
		{
			return haveYellowPotion;
		}

		//白色ポーションの所持数の設定
		static public void SetWhitePotion(int num)
		{
			haveWhitePotion = num;
		}

		//白色ポーションの所持数の取得
		static public int GetWhitePotion()
		{
			return haveWhitePotion;
		}

		//緑色ポーションの所持数の設定
		static public void SetUseGreenPotion(int num)
		{
			useGreenPotion = num;
		}

		//緑色ポーションの所持数の取得
		static public int GetUseGreenPotion()
		{
			return useGreenPotion;
		}

		//赤色ポーションの所持数の設定
		static public void SetUseRedPotion(int num)
		{
			useRedPotion = num;
		}

		//赤色ポーションの所持数の取得
		static public int GetUseRedPotion()
		{
			return useRedPotion;
		}

		//青色ポーションの所持数の設定
		static public void SetUseBluePotion(int num)
		{
			useBluePotion = num;
		}

		//青色ポーションの所持数の取得
		static public int GetUseBluePotion()
		{
			return useBluePotion;
		}

		//黄色ポーションの所持数の設定
		static public void SetUseYellowPotion(int num)
		{
			useYellowPotion = num;
		}

		//黄色ポーションの所持数の取得
		static public int GetUseYellowPotion()
		{
			return useYellowPotion;
		}

		//白色ポーションの所持数の設定
		static public void SetUseWhitePotion(int num)
		{
			useWhitePotion = num;
		}

		//白色ポーションの所持数の取得
		static public int GetUseWhitePotion()
		{
			return useWhitePotion;
		}

		//取得した経験値の設定
		static public void SetHaveExp(int num)
		{
			haveExp = num;
		}

		//取得した経験値の取得
		static public int GetHaveExp()
		{
			return haveExp;
		}

		//次のレベルまでの経験値を設定
		static public void SetNextExp(int num)
		{
			nextExp = num;
		}

		//次のレベルまでの経験値を取得
		static public int GetNextExp()
		{
			return nextExp;
		}

		//初期化
		static public void Initialize()
		{
			//決定した武器
			MyWeapon = IconBase.Icon.SWORD;

			//決定したダンジョン
			MyDungeon = IconBase.Icon.NONE;

			//プレイヤーのレベル
			plLv = 1;

			//プレイヤーのHP
			plMHp = 15;
			plHp = 15;

			//プレイヤーの攻撃力
			plAttack = 3;

			//プレイヤーの防御力
			plDefence = 2;

			//使用スキルポイント
			skillPt = 0;

			//プレイ時間
			playTime = 0;

			//倒した数
			defeat = 0;

			//階層
			currentFloor = 1;

			//総ターン数
			progressTurn = 0;

			//総与攻撃回数
			givingAttack = 0;

			//総与ダメージ量
			givingDamage = 0;

			//総被攻撃回数
			coveredAttack = 0;

			//総被ダメージ量
			coveredDamage = 0;

			//総アイテム取得数
			haveItem = 0;

			//総アイテム使用回数
			useItem = 0;

			//総被回復量
			coveredRecovery = 0;

			//移動したマス
			moveTrout = 0;

			//踏んだギミックマス
			stepGimmickTrout = 0;

			//受けた状態異常の回数
			stateAnomaly = 0;

			//緑色ポーションの所持数
			haveGreenPotion = 0;

			//赤色ポーションの所持数
			haveRedPotion = 0;

			//青色のポーションの所持数
			haveBluePotion = 0;

			//黄色のポーションの所持数
			haveYellowPotion = 0;

			//白色のポーションの所持数
			haveWhitePotion = 0;

			//緑色ポーションの使用数
			useGreenPotion = 0;

			//赤色ポーションの使用数
			useRedPotion = 0;

			//青色のポーションの使用数
			useBluePotion = 0;

			//黄色のポーションの使用数
			useYellowPotion = 0;

			//白色のポーションの使用数
			useWhitePotion = 0;

			//取得した経験値の設定
			haveExp = 0;

			//次のレベルまでの経験値
			nextExp = 15;
		}
		//-------------------------------------------------------------------------------------------
	}
}
