using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.IO;

public class SoundManager : MonoBehaviour
{
	//再生用(BGM)
	public static AudioSource AudioSourceBGM = null;

	//再生用(SE)
	public static AudioSource AudioSourceSE = null;

	//オーディオミキサー
	private static AudioMixer AudioMixer = null;

	//オーディオミキサーインスタンス
	private static AudioMixer InstanceAudioMixer = null;

	//オーディオグループ
	public static AudioMixerGroup AudioMixerGroup = null;

	//BGM用
	private static AudioClip bgm = null;

	//SE用
	private static AudioClip se = null;

	//音量(BGM)
	public static float bgmVol = 0;

	//音量(SE)
	public static float seVol = 0;

	//書き込む先のパス
	public static string path = "Assets/Common/SoundManager/Resources/CSV_Sound.csv";

	//読み込み用
	public static string str = null;

	//格納用
	public static string[] splitStr = null;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
	}

	//サウンドマネージャー生成
	public static void SoundManagerGeneration()
	{
		//サウンドマネージャーを生成指定なかったら生成
		if(AudioSourceBGM == null)
		{
			//BGM用オーディオソース生成
			AudioSourceBGM = new GameObject("SoundManager").AddComponent<AudioSource>();

			//SE用オーディオソース生成
			AudioSourceSE = GameObject.Find("SoundManager").AddComponent<AudioSource>();

			//オーディオミキサー取得
			AudioMixer = Resources.Load<AudioMixer>("AudioMixer");

			//インスタンス生成
			InstanceAudioMixer = Instantiate(AudioMixer);

			//BGM用オーディオグループをセット
			foreach (var g in InstanceAudioMixer.FindMatchingGroups("BGM"))
			{
				AudioMixerGroup = g;
			}

			//オーディオソースにオーディオグループをセット
			AudioSourceBGM.outputAudioMixerGroup = AudioMixerGroup;

			//SE用オーディオグループをセット
			foreach (var g in InstanceAudioMixer.FindMatchingGroups("SE"))
			{
				AudioMixerGroup = g;
			}

			//オーディオソースにオーディオグループをセット
			AudioSourceSE.outputAudioMixerGroup = AudioMixerGroup;

			//ファイルから音量読み込み
			str = File.ReadAllText(path);

			//分割した文字列を格納
			splitStr = str.Split(',');

			//代入
			bgmVol = int.Parse(splitStr[0]);
			seVol = int.Parse(splitStr[1]);
		}
	}

	//再生したいBGMを指定
	//引数：Resources未満のパス(拡張子は除く)
	public static void PlayBGM(string path)
	{
		//ファイルを設定
		bgm = Resources.Load<AudioClip>(path);

		//BGMにセット
		AudioSourceBGM.clip = bgm;

		//ループ設定
		AudioSourceBGM.loop = true;

		//再生
		AudioSourceBGM.Play();
	}

	//再生されているBGMを停止
	public static void StopBGM()
	{
		//停止
		AudioSourceBGM.Stop();
	}

	//再生したいSEを指定
	//引数：Resources未満のパス(拡張子は除く)
	public static void PlaySE(string path)
	{
		//ファイルを設定
		se = Resources.Load<AudioClip>(path);

		//再生
		AudioSourceSE.PlayOneShot(se);
	}

	//書き込みチェック(タイトルシーンのUpdateのみ記述)
	public static void CheckVolume()
	{
		//音量が変更されていたら
		if (AudioSourceBGM.volume != (bgmVol / 100) || AudioSourceSE.volume != (seVol / 100))
		{
			//代入
			AudioSourceBGM.volume = bgmVol / 100;
			AudioSourceSE.volume = seVol / 100;

			//書き出す
			File.WriteAllText(path, bgmVol.ToString() + ',' + seVol.ToString());
		}
	}
}