using UnityEngine;
using System.IO;

public class AchievementSaveLoad : MonoBehaviour
{
	//読み込み用
	public static string str1 = null;
	public static string str2 = null;
	public static string str3 = null;
	public static string str4 = null;
	public static string str5 = null;
	public static string str6 = null;
	public static string str7 = null;
	public static string str8 = null;
	public static string str9 = null;
	public static string str10 = null;
	public static string str11 = null;
	public static string str12 = null;
	public static string str13 = null;
	public static string str14 = null;
	public static string str15 = null;
	public static string str16 = null;
	

	//格納用
	public static string[] splitStr1 = null;
	public static string[] splitStr2 = null;
	public static string[] splitStr3 = null;
	public static string[] splitStr4 = null;
	public static string[] splitStr5 = null;
	public static string[] splitStr6 = null;
	public static string[] splitStr7 = null;
	public static string[] splitStr8 = null;
	public static string[] splitStr9 = null;
	public static string[] splitStr10 = null;
	public static string[] splitStr11 = null;
	public static string[] splitStr12 = null;
	public static string[] splitStr13 = null;
	public static string[] splitStr14 = null;
	public static string[] splitStr15 = null;
	public static string[] splitStr16 = null;

	//所持してる個数
	public static float count1 = 0;
	public static float count2 = 0;
	public static float count3 = 0;
	public static float count4 = 0;
	public static float count5 = 0;
	public static float count6 = 0;
	public static float count7 = 0;
	public static float count8 = 0;
	public static float count9 = 0;
	public static float count10 = 0;
	public static float count11 = 0;
	public static float count12 = 0;
	public static float count13 = 0;
	public static float count14 = 0;
	public static float count15 = 0;
	public static float count16 = 0;

	//獲得した月
	public static float Month1 = 0;
	public static float Month2 = 0;
	public static float Month3 = 0;
	public static float Month4 = 0;
	public static float Month5 = 0;
	public static float Month6 = 0;
	public static float Month7 = 0;
	public static float Month8 = 0;
	public static float Month9 = 0;
	public static float Month10 = 0;
	public static float Month11 = 0;
	public static float Month12 = 0;
	public static float Month13 = 0;
	public static float Month14 = 0;
	public static float Month15 = 0;
	public static float Month16 = 0;

	//獲得した日
	public static float Day1 = 1;
	public static float Day2 = 0;
	public static float Day3 = 0;
	public static float Day4 = 0;
	public static float Day5 = 0;
	public static float Day6 = 0;
	public static float Day7 = 0;
	public static float Day8 = 0;
	public static float Day9 = 0;
	public static float Day10 = 0;
	public static float Day11 = 0;
	public static float Day12 = 0;
	public static float Day13 = 0;
	public static float Day14 = 0;
	public static float Day15 = 0;
	public static float Day16 = 0;


	public static string Save1 = "Assets/Common/Achievement/素材/Save/1.txt";
	public static string Save2 = "Assets/Common/Achievement/素材/Save/2.txt";
	public static string Save3 = "Assets/Common/Achievement/素材/Save/3.txt";
	public static string Save4 = "Assets/Common/Achievement/素材/Save/4.txt";
	public static string Save5 = "Assets/Common/Achievement/素材/Save/5.txt";
	public static string Save6 = "Assets/Common/Achievement/素材/Save/6.txt";
	public static string Save7 = "Assets/Common/Achievement/素材/Save/7.txt";
	public static string Save8 = "Assets/Common/Achievement/素材/Save/8.txt";
	public static string Save9 = "Assets/Common/Achievement/素材/Save/9.txt";
	public static string Save10 = "Assets/Common/Achievement/素材/Save/10.txt";
	public static string Save11 = "Assets/Common/Achievement/素材/Save/11.txt";
	public static string Save12 = "Assets/Common/Achievement/素材/Save/12.txt";
	public static string Save13 = "Assets/Common/Achievement/素材/Save/13.txt";
	public static string Save14 = "Assets/Common/Achievement/素材/Save/14.txt";
	public static string Save15 = "Assets/Common/Achievement/素材/Save/15.txt";
	public static string Save16 = "Assets/Common/Achievement/素材/Save/16.txt";


	// Start is called before the first frame update
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{

	}

	public static void AchievementWrite1()
	{
		//書き出す
		File.WriteAllText(Save1, count1.ToString() + ',' + Month1.ToString() + ',' + Day1.ToString());
	}

	public static void AchievementRoad1()
	{
		//読み込み
		str1 = File.ReadAllText(Save1);
		splitStr1 = str1.Split(',');
		count1 = int.Parse(splitStr1[0]);
		Month1 = int.Parse(splitStr1[1]);
		Day1 = int.Parse(splitStr1[2]);
		Debug.Log(count1);
		Debug.Log(Month1);
		Debug.Log(Day1);

	}

	public static void AchievementWrite2()
	{
		//書き出す
		File.WriteAllText(Save1, count2.ToString() + ',' + Month2.ToString() + ',' + Day2.ToString());
	}

	public static void AchievementRoad2()
	{
		//読み込み
		str2 = File.ReadAllText(Save2);
		splitStr1 = str2.Split(',');
		count2 = int.Parse(splitStr2[0]);
		Month2 = int.Parse(splitStr2[1]);
		Day2 = int.Parse(splitStr2[2]);
	}

	public  void AchievementWrite3()
	{
		//書き出す
		File.WriteAllText(Save3, count3.ToString() + ',' + Month3.ToString() + ',' + Day3.ToString());
	}

	public static void AchievementRoad3()
	{
		//読み込み
		str3 = File.ReadAllText(Save3);
		splitStr3 = str3.Split(',');
		count3 = int.Parse(splitStr3[0]);
		Month3 = int.Parse(splitStr3[1]);
		Day3 = int.Parse(splitStr3[2]);
	}

	public  void AchievementWrite4()
	{
		//書き出す
		File.WriteAllText(Save4, count4.ToString() + ',' + Month4.ToString() + ',' + Day4.ToString());
	}

	public static void AchievementRoad4()
	{
		//読み込み
		str4 = File.ReadAllText(Save4);
		splitStr4 = str4.Split(',');
		count4 = int.Parse(splitStr4[0]);
		Month4 = int.Parse(splitStr4[1]);
		Day4 = int.Parse(splitStr4[2]);
	}

	public  void AchievementWrite5()
	{
		//書き出す
		File.WriteAllText(Save5, count5.ToString() + ',' + Month5.ToString() + ',' + Day5.ToString());
	}

	public static void AchievementRoad5()
	{
		//読み込み
		str5 = File.ReadAllText(Save5);
		splitStr5 = str5.Split(',');
		count5 = int.Parse(splitStr5[0]);
		Month5 = int.Parse(splitStr5[1]);
		Day5 = int.Parse(splitStr5[2]);
	}

	public static void AchievementWrite6()
	{
		//書き出す
		File.WriteAllText(Save6, count6.ToString() + ',' + Month6.ToString() + ',' + Day6.ToString());
	}

	public static void AchievementRoad6()
	{
		//読み込み
		str6 = File.ReadAllText(Save6);
		splitStr6 = str6.Split(',');
		count6 = int.Parse(splitStr6[0]);
		Month6 = int.Parse(splitStr6[1]);
		Day6 = int.Parse(splitStr6[2]);
	}

	public static void AchievementWrite7()
	{
		//書き出す
		File.WriteAllText(Save7, count7.ToString() + ',' + Month7.ToString() + ',' + Day7.ToString());
	}

	public static void AchievementRoad7()
	{
		//読み込み
		str7 = File.ReadAllText(Save7);
		splitStr7 = str7.Split(',');
		count7 = int.Parse(splitStr7[0]);
		Month7 = int.Parse(splitStr7[1]);
		Day7 = int.Parse(splitStr7[2]);
	}

	public static void AchievementWrite8()
	{
		//書き出す
		File.WriteAllText(Save8, count8.ToString() + ',' + Month8.ToString() + ',' + Day8.ToString());
	}

	public static void AchievementRoad8()
	{
		//読み込み
		str8 = File.ReadAllText(Save8);
		splitStr8 = str8.Split(',');
		count8 = int.Parse(splitStr8[0]);
		Month8 = int.Parse(splitStr8[1]);
		Day8 = int.Parse(splitStr8[2]);
	}

	public static void AchievementWrite9()
	{
		//書き出す
		File.WriteAllText(Save9, count9.ToString() + ',' + Month9.ToString() + ',' + Day9.ToString());
	}

	public static void AchievementRoad9()
	{
		//読み込み
		str9 = File.ReadAllText(Save9);
		splitStr9 = str9.Split(',');
		count9 = int.Parse(splitStr9[0]);
		Month9 = int.Parse(splitStr9[1]);
		Day9 = int.Parse(splitStr9[2]);
	}

	public static void AchievementWrite10()
	{
		//書き出す
		File.WriteAllText(Save10, count10.ToString() + ',' + Month10.ToString() + ',' + Day10.ToString());
	}

	public static void AchievementRoad10()
	{
		//読み込み
		str10 = File.ReadAllText(Save10);
		splitStr10 = str10.Split(',');
		count10 = int.Parse(splitStr10[0]);
		Month10 = int.Parse(splitStr10[1]);
		Day10 = int.Parse(splitStr10[2]);
	}

	public static void AchievementWrite11()
	{
		//書き出す
		File.WriteAllText(Save11, count11.ToString() + ',' + Month11.ToString() + ',' + Day11.ToString());
	}

	public static void AchievementRoad11()
	{
		//読み込み
		str11 = File.ReadAllText(Save11);
		splitStr11 = str11.Split(',');
		count11 = int.Parse(splitStr11[0]);
		Month11 = int.Parse(splitStr11[1]);
		Day11 = int.Parse(splitStr11[2]);
	}

	public static void AchievementWrite12()
	{
		//書き出す
		File.WriteAllText(Save12, count12.ToString() + ',' + Month12.ToString() + ',' + Day12.ToString());
	}


	public static void AchievementRoad12()
	{
		//読み込み
		str12 = File.ReadAllText(Save12);
		splitStr12 = str12.Split(',');
		count12 = int.Parse(splitStr12[0]);
		Month12 = int.Parse(splitStr12[1]);
		Day12 = int.Parse(splitStr12[2]);
	}

	public static void AchievementWrite13()
	{
		//書き出す
		File.WriteAllText(Save13, count13.ToString() + ',' + Month13.ToString() + ',' + Day13.ToString());
	}

	public static void AchievementRoad13()
	{
		//読み込み
		str13 = File.ReadAllText(Save13);
		splitStr13 = str13.Split(',');
		count13 = int.Parse(splitStr13[0]);
		Month13 = int.Parse(splitStr13[1]);
		Day13 = int.Parse(splitStr13[2]);
	}

	public static void AchievementWrite14()
	{
		//書き出す
		File.WriteAllText(Save14, count14.ToString() + ',' + Month14.ToString() + ',' + Day14.ToString());
	}

	public static void AchievementRoad14()
	{
		//読み込み
		str14 = File.ReadAllText(Save14);
		splitStr14 = str14.Split(',');
		count14 = int.Parse(splitStr14[0]);
		Month14 = int.Parse(splitStr14[1]);
		Day14 = int.Parse(splitStr14[2]);
	}

	public static void AchievementWrite15()
	{
		//書き出す
		File.WriteAllText(Save15, count15.ToString() + ',' + Month15.ToString() + ',' + Day15.ToString());
	}

	public static void AchievementRoad15()
	{
		//読み込み
		str15 = File.ReadAllText(Save15);
		splitStr15 = str15.Split(',');
		count15 = int.Parse(splitStr15[0]);
		Month15 = int.Parse(splitStr15[1]);
		Day15 = int.Parse(splitStr15[2]);
	}

	public static void AchievementWrite16()
	{
		//書き出す
		File.WriteAllText(Save16, count16.ToString() + ',' + Month16.ToString() + ',' + Day16.ToString());
	}

	public static void AchievementRoad16()
	{
		//読み込み
		str16 = File.ReadAllText(Save16);
		splitStr16 = str16.Split(',');
		count16 = int.Parse(splitStr16[0]);
		Month16 = int.Parse(splitStr16[1]);
		Day16 = int.Parse(splitStr16[2]);
	}
}