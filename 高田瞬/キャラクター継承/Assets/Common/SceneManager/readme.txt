namespace SceneManager 使用方法

...SceneManager.cs

using SceneManager;

public class ...SceneManager : MonoBehaviour
{
	Scene Scene;

	void Start()
	{
		Scene = new Scene();
		Scene.SetNowScene(SCENE. .._SCENE);
	}
}


①最初にusingで呼び出し
②初期化処理の最初に現在のシーン番号を登録

これだけ守ってください
どっかしらんシーンから始めてデバックすることもあると思うので