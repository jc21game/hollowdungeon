using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconDungeon1 : IconBase
{
	//自身の移動状況
	private static new MOVE_STATE MoveState = MOVE_STATE.NONE;

	//移動方向&速度
	private static new MOVE_DIR_SPEED MoveSpeed = MOVE_DIR_SPEED.NONE;

	//自身のオブジェクトを作成
	private static GameObject BaseObject = null;

	//キャンバスオブジェクト
	private static GameObject Dungeon1CanvasObject = null;

	//キャンバス
	private static Canvas Dungeon1Canvas = null;

	//キャンバスソリューション
	private static CanvasScaler CanvasScaler = null;

	//自身のイメージを作成
	private static Image BaseImage = null;

	//自身の番号
	private static new readonly Icon IconNumber = Icon.DUNGEON_1;

	//自身の拡縮状況
	private static SCALE_STATE IconState = SCALE_STATE.NONE;

	//初期化
	void Start()
	{
		//画像セット
		BaseImage.sprite = Resources.Load<Sprite>("ダンジョン1の代用2");

		//サイズ調整
		SetScaleState(SCALE_STATE.SCALE_USU);

		//拡縮
		Scale(BaseImage, GetScaleState());

		//移動状況を待機に
		MoveState = MOVE_STATE.STAY;

		//初期座標に移動
		BaseImage.rectTransform.anchoredPosition = new Vector2(CenterXPosition, IconYPosition);

	}

	//更新
	void Update()
	{
		//移動中ではない
		if (MoveState == MOVE_STATE.STAY)
		{
			//自身が選択されている
			if (WeaponSelectSceneManager.GetNowIcon() == IconNumber)
			{
				//自身をセンターに移動
				if (BaseImage.rectTransform.anchoredPosition.x != CenterXPosition)
				{
					MoveState = MOVE_STATE.MOVE_LEFT_CENTER;
				}

				//直前まで「均す」だったら変更
				if (GetScaleState() == SCALE_STATE.SCALE_USU)
				{
					//拡縮状況を「大きく」に変更
					SetScaleState(SCALE_STATE.SCALE_BIG);

					//描画優先度を変更

				}

				//サイズが規定値を超えたら拡縮状況を「小さく」に変更
				if (BaseImage.rectTransform.sizeDelta.x > IconMaxSize)
				{
					SetScaleState(SCALE_STATE.SCALE_SMALE);
				}

				//サイズが規定値より下回ったら拡縮状況を「大きく」に変更
				if (BaseImage.rectTransform.sizeDelta.x < IconMinSize)
				{
					SetScaleState(SCALE_STATE.SCALE_BIG);
				}
			}
			//選択されていない
			else
			{
				//現在選択されている武器に合わせて位置を変更
				switch (WeaponSelectSceneManager.GetNowIcon())
				{
					case Icon.SWORD:
						//関係なし
						break;
					case Icon.AXE:
						//関係なし
						break;
					case Icon.BOW:
						//関係なし
						break;
					case Icon.DUNGEON_1:
						//自身なのでなし
						break;
					case Icon.DUNGEON_2:
						//左に設置
						if (MoveState != MOVE_STATE.MOVE_LEFT && BaseImage.rectTransform.anchoredPosition.x != LeftXPosition)
						{
							MoveState = MOVE_STATE.MOVE_LEFT;
						}

						break;
					case Icon.NONE:
						//なし
						break;
				}

				//拡縮状況を「均す」に変更
				SetScaleState(SCALE_STATE.SCALE_USU);

				//拡縮
				Scale(BaseImage, GetScaleState());
			}
		}

		//「大きく」か「小さく」だったら拡縮自動起動
		if (GetScaleState() == SCALE_STATE.SCALE_BIG || GetScaleState() == SCALE_STATE.SCALE_SMALE)
		{
			//拡縮
			Scale(BaseImage, GetScaleState());
		}

		//アイコン移動
		switch (MoveState)
		{
			//右へ
			case MOVE_STATE.MOVE_RIGHT:
				break;
			//中央へ
			case MOVE_STATE.MOVE_CENTER:
				//現在位置に合わせてフラグ変更
				if (BaseImage.rectTransform.anchoredPosition.x > CenterXPosition)
				{
					MoveState = MOVE_STATE.MOVE_RIGHT_CENTER;
					MoveSpeed = MOVE_DIR_SPEED.W_LEFT;
				}
				if (BaseImage.rectTransform.anchoredPosition.x < CenterXPosition)
				{
					MoveState = MOVE_STATE.MOVE_LEFT_CENTER;
					MoveSpeed = MOVE_DIR_SPEED.W_RIGHT;
				}
				break;
			//右から中央
			case MOVE_STATE.MOVE_RIGHT_CENTER:
				break;
			//左から中央
			case MOVE_STATE.MOVE_LEFT_CENTER:
				//移動速度
				MoveSpeed = MOVE_DIR_SPEED.W_RIGHT;

				//移動
				IconMove(BaseImage, MoveSpeed);

				//規定値になったら
				if (BaseImage.rectTransform.anchoredPosition.x >= CenterXPosition)
				{
					//整理
					BaseImage.rectTransform.anchoredPosition = new Vector2(CenterXPosition, IconYPosition);

					//移動終了
					MoveState = MOVE_STATE.STAY;

					//速度リセット
					MoveSpeed = MOVE_DIR_SPEED.NONE;
				}
				break;
			//左へ
			case MOVE_STATE.MOVE_LEFT:
				//歩き
				MoveSpeed = MOVE_DIR_SPEED.W_LEFT;

				//移動
				IconMove(BaseImage, MoveSpeed);

				//規定値になったら
				if (BaseImage.rectTransform.anchoredPosition.x <= LeftXPosition)
				{
					//整理
					BaseImage.rectTransform.anchoredPosition = new Vector2(LeftXPosition, IconYPosition);

					//移動終了
					MoveState = MOVE_STATE.STAY;

					//速度リセット
					MoveSpeed = MOVE_DIR_SPEED.NONE;
				}
				break;
		}
	}

	//自身の拡縮状態のセッター
	private static void SetScaleState(SCALE_STATE state)
	{
		IconState = state;
	}

	//自身の拡縮状況のゲッター
	private static SCALE_STATE GetScaleState()
	{
		return IconState;
	}

	//拡縮
	protected override void Scale(Image image, SCALE_STATE state)
	{
		base.Scale(image, state);
	}

	//キャンバス生成
	private static void Dungeon1CanvasGeneration()
	{
		//-----------------Canvas生成部-----------------//

		//Canvas名設定
		Dungeon1CanvasObject = new GameObject("SwordCanvas");

		//Canvas生成
		Dungeon1Canvas = Dungeon1CanvasObject.AddComponent<Canvas>();

		//親をシーンオブジェクトに設定
		Dungeon1Canvas.transform.SetParent(Dungeon1CanvasObject.transform, false);

		//CanvasScalerを獲得、生成
		CanvasScaler = Dungeon1CanvasObject.AddComponent<CanvasScaler>();

		//描画優先度が有効なモードに変更
		Dungeon1Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//ReferenceResolutionの設定
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//画面サイズに合わせて自動でサイズ調整するモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
	}

	//剣アイコン生成
	public static void Dungeon1Generation()
	{
		//オブジェクトが未生成だったら
		if (Dungeon1CanvasObject == null)
		{
			Dungeon1CanvasGeneration();
		}

		//オブジェクトを作成
		BaseObject = new GameObject("Dungeon1Icon");

		//コンポーネント追加
		BaseObject.AddComponent<IconDungeon1>();

		//コンポーネント追加
		BaseImage = BaseObject.AddComponent<Image>();

		//親を設定
		BaseImage.rectTransform.SetParent(Dungeon1Canvas.transform, false);
	}

	//非表示
	public static void Dungeon1Hide()
	{
		BaseImage.enabled = false;
	}

	//表示
	public static void Dungeon1Display()
	{
		BaseImage.enabled = true;
	}

	//アイコンの移動状況がSTAY
	public static bool GetIconMoveState()
	{
		if(MoveState == MOVE_STATE.STAY)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
