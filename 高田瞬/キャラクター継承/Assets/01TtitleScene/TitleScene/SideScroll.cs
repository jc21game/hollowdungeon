	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

//横スクロール
public class SideScroll : MonoBehaviour
{
	//再生成位置
	public float reSpawnPos = 0;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//移動
		transform.position -= new Vector3(Time.deltaTime * 5, 0);
	}

	//カメラに映らなくなったら
	private void OnBecameInvisible()
	{
		//再生成
		transform.position = new Vector2(reSpawnPos, 0);
	}
}
