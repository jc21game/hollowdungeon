using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//タイトルシーン
public class TitleSceneManager : MonoBehaviour
{
	//武器選択シーンへ移動する時間
	public float invokeTime = 0;

	//キャンバス
	public Canvas canvas = null;
	public GameObject game = null;

	//BGMバー
	private BGMBar bgmBar = null;

	//SEバー
	private SEBar seBar = null;

	//初期化
	public void Start()
	{
		//フェード画像初期化
		FadeScript.Darkening();
		FadeScript.FadeIn();

		//生成
		Instantiate(canvas);
		Instantiate(game);

		//サウンドマネージャー生成
		SoundManager.SoundManagerGeneration();

		//コンポーネント取得
		bgmBar = GameObject.Find("BGMBar").GetComponent<BGMBar>();

		//コンポーネント取得
		seBar = GameObject.Find("SEBar").GetComponent<SEBar>();

		//各スライダーの初期設定
		bgmBar.Initialize();
		seBar.Initialize();

		//再生
		SoundManager.PlayBGM("Sounds/Title");

		//初期化
		ScoreManager.Score.Initialize();
	}

	//更新
	public void Update()
	{
		//Zキーを押したらフェードアウトして武器選択シーンに移動する
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//フェードアウト
			FadeScript.FadeOut();

			AchievementSaveLoad.AchievementRoad1();
			AchievementSaveLoad.count1 += 1;
			if (AchievementSaveLoad.count1 == 1)
			{
				DateTime today = DateTime.Now;
				AchievementSaveLoad.Month1 = today.Month;
				AchievementSaveLoad.Day1 = today.Day;
			}
			AchievementSaveLoad.AchievementWrite1();

			//4秒後に武器選択シーンに移動する
			Invoke("ChangeWeapon", invokeTime);
		}

		//書き込みチェック
		SoundManager.CheckVolume();
	}

	//武器選択シーンに移動する関数
	public void ChangeWeapon()
	{
		//武器選択シーンに移動
		UnityEngine.SceneManagement.SceneManager.LoadScene("WeaponSelectScene");
	}



}
