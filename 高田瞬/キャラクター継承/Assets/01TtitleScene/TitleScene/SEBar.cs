using UnityEngine;
using UnityEngine.UI;

public class SEBar : MonoBehaviour
{
	//スライダー
	private Slider slider = null;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//スライダーの値が変更されたら
		if (slider.value != SoundManager.seVol)
		{
			SoundManager.seVol = (int)slider.value;
		}
	}

	//スライダーの初期設定
	public void Initialize()
	{
		//取得
		slider = GameObject.Find("SEBar").GetComponent<Slider>();

		//初期設定
		slider.maxValue = 100;
		slider.value = SoundManager.seVol;
	}
}
