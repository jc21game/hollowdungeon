using UnityEngine;
using UnityEngine.UI;

public class BGMBar : MonoBehaviour
{
	//スライダー
	private Slider slider = null;

	//初期化
	public void Start()
	{

	}

	//更新
	public void Update()
	{
		//スライダーの値が変更されたら
		if (slider.value != SoundManager.bgmVol)
		{
			SoundManager.bgmVol = (int)slider.value;
		}
	}

	//スライダーの初期設定
	public void Initialize()
	{
		//取得
		slider = GameObject.Find("BGMBar").GetComponent<Slider>();

		//初期設定
		slider.maxValue = 100;
		slider.value = SoundManager.bgmVol;
	}
}
