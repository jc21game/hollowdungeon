using UnityEngine;

public class TrapBase : MonoBehaviour
{
	protected PlaySceneManager manager = null;
	protected CharacterBase.State state = null;
	protected int status = 0;

	public virtual void Start()
	{
		state = new CharacterBase.State();
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
	}
	public virtual void Update()
	{

	}
	protected virtual void Execute(CharacterBase character)
	{

	}


}