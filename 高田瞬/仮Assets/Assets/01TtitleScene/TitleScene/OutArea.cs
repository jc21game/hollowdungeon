using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutArea : MonoBehaviour
{
    //ボタン
    private ConfigButton button = null;

    //初期化
    void Start()
    {
        //取得
        button = GameObject.Find("ConfigButton").GetComponent<ConfigButton>();
    }

    //更新
    void Update()
    {
    }

    //ボタンをクリックされた
    public void OnClick()
    {
        Debug.Log("領域外クリック");

        //フラグ変更
        button.flg = false;
    }
}
