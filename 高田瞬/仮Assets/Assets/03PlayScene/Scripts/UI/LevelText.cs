using UnityEngine;
using UnityEngine.UI;

public class LevelText : MonoBehaviour
{
	//コンポーネント取得
	private Text text = null;

	//表示テキスト管理用
	private string str = "";

	//初期化
	private void Start()
	{
		text = this.GetComponent<Text>();

		//初期化
		str = "LV.";
	}

	//更新
	private void Update()
	{
		//テキスト表示
		text.text = str + ScoreManager.Score.GetLv().ToString();
	}
}
