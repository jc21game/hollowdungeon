//TrapBaseクラスを親クラスに設定
public class ConfusionTrap : TrapBase
{
	public override void Start()
	{
		base.Start();
		status = state.confusion;
	}
	public override void Update()
	{

	}
	protected override void Execute(CharacterBase character)
	{
		character.SetState(status);
	}
}
