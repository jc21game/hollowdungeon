using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//キャラクターベース
public class CharacterBase : MonoBehaviour
{
	//オブジェクト
	protected PlaySceneManager manager = null;
	protected Map map = null;
	protected GameObject objects = null;

	//方向と足
	protected DIR dir = null;
	protected int sleepTimeNonDash = 150;
	protected float count = 0;
	protected float maxCount = 1;

	//スプライト
	public Sprite[] sprites;
	protected SpriteRenderer sprite = null;

	public Vector3 size;

	//ステータス
	public string NAME = "";
	public int MAX_HP = 0;
	public int HP = 0;
	public int ATC = 0;
	public int DEF = 0;
	public int EXP = 0;
	public bool LIFE = false;

	//状態異常
	protected State state = null;

	//現在の状態
	protected int nowState = 0;

	//攻撃中か
	public int weapon = 0;		//(NONE, 剣, 斧, 弓)
	protected bool isAttack = false;
	private bool isArrowAttack = true;
	protected float attackTime = 0;
	protected float approach = 0.25f;
	protected float attackPos = 0.25f;
	protected Vector3 basePosition;
	private int powerSward = 3;
	private int powerAxe = 2;
	private int powerArrow = 2;
	private GameObject arrow = null;
	private Vector3 arrowFinPos;
	private float arrowMaxDistance = 5;
	private float arrowSpeed = 0.5f;
	private float finPosDistance = 0.5f;

	//初期化
	public virtual void Start()
	{
		//オブジェクト
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
		map = GameObject.Find("Map").GetComponent<Map>();
		objects = GameObject.Find("Object");
		transform.SetParent(objects.transform, false);

		//生成
		manager.AddChar(gameObject.GetComponent<CharacterBase>());

		//生命
		LIFE = true;

		//方向と足
		dir = new DIR();

		//キャラクター設定
		sprite = gameObject.GetComponent<SpriteRenderer>();
		sprite.sprite = sprites[dir.GetNumber()];

		//初期設定
		transform.localScale = size;

		//表示
		sprite.sortingOrder = (int)(transform.position.x - transform.position.y * map.GetWidth());

		//初期位置
		basePosition = transform.position;
	}

	//更新
	public virtual void Update()
	{
		//時間測定
		if ((count -= Time.deltaTime) < 0)
		{
			//足をSTOPに設定
			dir.ResetFoot();
			sprite.sprite = sprites[dir.GetNumber()];
			count = 0;
		}

		//見た目
		sprite.sprite = sprites[dir.GetNumber()];
	}

	//実行
	public virtual bool Execute()
	{
		//処理なし
		return false;
	}

	//弓の攻撃範囲にいるか
	protected bool IsArrowRange(DIR.Dir dir)
	{
		//座標
		arrowFinPos = GetFront(dir);

		//矢を生成
		arrow = new GameObject();
		arrow.transform.position = transform.position;

		//壁かプレイヤーに当たるまでまたは5マス
		while (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= arrowMaxDistance)
		{
			//壁の場合
			if (map.GetMap(arrowFinPos) == map.mapInfomation.WALL)
			{
				//処理終了
				break;
			}

			//キャラクターかつプレイヤーの場合
			CharacterBase character = manager.GetCharacter(arrowFinPos);
			if (character != null && character.NAME == "Player")
			{
				//処理終了
				Destroy(arrow);
				return true;
			}

			//移動
			switch (dir)
			{
				case DIR.Dir.TOP: arrowFinPos.y++; break;
				case DIR.Dir.RIGHT: arrowFinPos.x++; break;
				case DIR.Dir.LEFT: arrowFinPos.x--; break;
				case DIR.Dir.BOTTOM: arrowFinPos.y--; break;
			}
		}

		//いない
		Destroy(arrow);
		return false;
	}

	//攻撃
	protected virtual bool Attack()
	{
		//攻撃中ではない
		if (!isAttack)
		{
			//足をリセット
			dir.ResetFoot();

			//攻撃範囲および攻撃
			switch (weapon)
			{
				case 1: AttackSword(); break;
				case 2: AttackAxe(); break;
				case 3: AttackArrow(); break;
			}

			//攻撃
			isAttack = true;
			basePosition = transform.position;
		}

		//攻撃中
		if (isAttack)
		{
			//攻撃モーション
			AttackMotion();

			//攻撃中
			return true;
		}

		//処理なし
		return false;
	}

	//攻撃(剣)
	private void AttackSword()
	{
		//座標
		Vector2 vector = GetFront();

		//敵
		CharacterBase character;

		//左正面に敵がいる場合
		switch (dir.dir)
		{
			case DIR.Dir.TOP: vector.x--; break;
			case DIR.Dir.RIGHT: vector.y++; break;
			case DIR.Dir.LEFT: vector.y--; break;
			case DIR.Dir.BOTTOM: vector.x++; break;
		}
		character = manager.GetCharacter(vector);
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward);
		}

		//正面に敵がいる場合
		character = manager.GetCharacter(GetFront());
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward);
		}

		//右正面に敵がいる場合
		vector = GetFront();
		switch (dir.dir)
		{
			case DIR.Dir.TOP: vector.x++; break;
			case DIR.Dir.RIGHT: vector.y--; break;
			case DIR.Dir.LEFT: vector.y++; break;
			case DIR.Dir.BOTTOM: vector.x--; break;
		}
		character = manager.GetCharacter(vector);
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward);
		}
	}

	//攻撃(斧)
	private void AttackAxe()
	{
		//正面に敵がいる場合
		CharacterBase character = manager.GetCharacter(GetFront());
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerAxe);
		}
	}

	//攻撃(弓)
	private void AttackArrow()
	{
		//座標
		arrowFinPos = GetFront();

		//矢を生成
		arrow = Instantiate((GameObject)Resources.Load("Prefab/Arrow"));
		arrow.transform.position = transform.position;
		arrow.GetComponent<SpriteRenderer>().sortingOrder = gameObject.GetComponent<SpriteRenderer>().sortingOrder;
		isArrowAttack = true;

		//回転
		switch (dir.dir)
		{
			case DIR.Dir.TOP: arrow.transform.Rotate(new Vector3(0, 0, 90)); break;
			case DIR.Dir.RIGHT: arrow.transform.Rotate(new Vector3(0, 0, 0)); break;
			case DIR.Dir.LEFT: arrow.transform.Rotate(new Vector3(0, 0, 180)); break;
			case DIR.Dir.BOTTOM: arrow.transform.Rotate(new Vector3(0, 0, 270)); break;
		}

		//壁かキャラクターに当たるまでまたは5マス
		while (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= arrowMaxDistance)
		{
			//壁の場合
			if (map.GetMap(arrowFinPos) == map.mapInfomation.WALL)
			{
				//処理終了
				break;
			}

			//キャラクターの場合
			CharacterBase character = manager.GetCharacter(arrowFinPos);
			if (character != null)
			{
				//攻撃
				character.Hit(ATC + powerArrow);

				//処理終了
				break;
			}

			//移動
			switch (dir.dir)
			{
				case DIR.Dir.TOP: arrowFinPos.y++; break;
				case DIR.Dir.RIGHT: arrowFinPos.x++; break;
				case DIR.Dir.LEFT: arrowFinPos.x--; break;
				case DIR.Dir.BOTTOM: arrowFinPos.y--; break;
			}
		}
	}

	//攻撃モーション
	private void AttackMotion()
	{
		//迫る
		if ((attackTime += Time.deltaTime) <= approach)
		{
			//座標
			Vector3 vector = basePosition;

			//方向
			switch (dir.dir)
			{
				//上
				case DIR.Dir.TOP:
					vector.y += attackPos;
					break;

				//右
				case DIR.Dir.RIGHT:
					vector.x += attackPos;
					break;

				//左
				case DIR.Dir.LEFT:
					vector.x -= attackPos;
					break;

				//下
				case DIR.Dir.BOTTOM:
					vector.y -= attackPos;
					break;
			}

			//近づく
			transform.position = vector;
		}

		//終了
		else
		{
			//元の位置
			transform.position = basePosition;

			//武器が弓以外
			if (weapon != 3)
			{
				//初期化
				attackTime = 0;
				isAttack = false;
			}

			//弓
			else if(!isArrowAttack)
			{
				//初期化
				attackTime = 0;
				isAttack = false;
			}
		}

		//矢
		if (weapon == 3 && isArrowAttack)
		{
			//位置
			Vector3 vector = arrow.transform.position;

			//移動
			switch (dir.dir)
			{
				case DIR.Dir.TOP: vector.y += arrowSpeed; break;
				case DIR.Dir.RIGHT: vector.x += arrowSpeed; break;
				case DIR.Dir.LEFT: vector.x -= arrowSpeed; break;
				case DIR.Dir.BOTTOM: vector.y -= arrowSpeed; break;
			}
			arrow.transform.position = vector;

			//到着
			if (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= finPosDistance)
			{
				//矢の処理終了
				isArrowAttack = false;
				Destroy(arrow);
			}
		}
	}

	//移動
	protected virtual bool Move()
	{
		//処理なし
		return false;
	}

	//攻撃を受ける
	public virtual void Hit(int damage)
	{
		//プレイヤー
		if(NAME == "Player")
		{
			//メッセージ送信
			manager.SetMessage("プレイヤーは" + damage.ToString() + "ポイントのダメージを受けた。");
		}

		//敵
		else
		{
			//メッセージ送信
			manager.SetMessage("プレイヤーは" + NAME  + "に" +  damage.ToString() + "ポイントのダメージを与えた。");
		}

		//ダメージを受ける
		if ((HP -= damage) <= 0)
		{
			//死亡
			LIFE = false;
		}
	}

	//死亡
	public virtual void Death()
	{
		//経験値取得
		manager.GetCharacter(0).GetComponent<Player>().GetEXP(EXP);

		//死亡
		Destroy(gameObject);
	}

	//回復
	public virtual void Heal(int heal)
	{
		//回復
		if ((HP += heal) > MAX_HP)
		{
			//最大
			HP = MAX_HP;
		}
	}

	//目の前の座標
	protected Vector2 GetFront(bool isReverse = false)
	{
		//座標
		return GetFront(dir.dir, isReverse);
	}
	protected Vector2 GetFront(DIR.Dir dir, bool isReverse = false)
	{
		//座標
		Vector2 vector = transform.position;

		//方向
		int dirction = 1;

		//逆方向
		if(isReverse)
		{
			//逆方向
			dirction = -1;
		}

		//方向
		switch(dir)
		{
			//上
			case DIR.Dir.TOP:
				vector.y += dirction;
				break;

			//右
			case DIR.Dir.RIGHT:
				vector.x += dirction;
				break;
			
			//左
			case DIR.Dir.LEFT:
				vector.x -= dirction;
				break;

			//下
			case DIR.Dir.BOTTOM:
				vector.y -= dirction;
				break;
		}

		//座標
		return vector;
	}

	//方向と足
	protected class DIR
	{
		//方向
		public enum Dir
		{
			TOP = 0,
			RIGHT = 3,
			LEFT = 6,
			BOTTOM = 9
		}
		public Dir dir = Dir.TOP;
		public const int DIRCTIONS = 4;

		//足
		private enum Foot
		{
			STOP = 0,
			RIGHT,
			LEFT,
			MAX
		}
		private Foot foot = Foot.STOP;

		//方向チェンジ
		public void DirChange(Dir dir)
		{
			//方向が切り替わる場合
			if(this.dir != dir)
			{
				//足リセット
				ResetFoot();
			}

			//方向チェンジ
			this.dir = dir;
		}

		//足を進める
		public void AddFoot()
		{
			//足変更
			if (++foot == Foot.MAX)
			{
				//右に設定
				foot = Foot.RIGHT;
			}
		}

		//足リセット
		public void ResetFoot()
		{
			//停止に設定
			foot = Foot.STOP;
		}

		//番号取得
		public int GetNumber()
		{
			//方向 + 足
			return (int)dir + (int)foot;
		}

		//0〜3を方向に変換
		public Dir ConvertDir(int i)
		{
			//番号
			switch(i)
			{
				case 0:i = 0;break;
				case 1:i = 3;break;
				case 2:i = 6;break;
				case 3:i = 9;break;
			}

			//終了
			return (Dir)i;
		}
	}

	//混乱時の移動
	protected bool ConfMove()
	{
		//移動先
		dir.dir = dir.ConvertDir(Random.Range(0, 4));

		if (map.IsMove(GetFront(dir.dir)))
		{
			//カウント開始
			count = maxCount;

			//移動
			dir.DirChange(dir.dir);
			dir.AddFoot();
			transform.position = GetFront(dir.dir);
			manager.Move();

			//向き
			sprite.sprite = sprites[dir.GetNumber()];

			//移動先座標が画面に映る場合
			if (map.IsExist(transform.position))
			{
				//スリープ
				//Thread.Sleep(sleepTimeNonDash);
			}

			//処理終了
			return true;
		}

		//処理なし
		return /*base.*/Move();
	}

	//方向をランダムに指定する
	protected void RandamDir()
	{
		//0〜3をランダムで指定して、結果を方向に変換して代入
		dir.ConvertDir(Random.Range(0, 3));
	}

	//状態異常付与
	public void SetState(int state)
	{
		nowState = state;
	}

	//状態異常管理
	public class State
	{
		//健康
		public int health = 0;

		//混乱
		public int confusion = 1;

		//状態異常2
		public int c = 2;

		//状態異常3
		public int d = 3;

		//状態異常4
		public int e = 4;
	}
}