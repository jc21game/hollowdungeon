using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//経験値ポーション
public class EXPPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.EXPERIENCE);
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//経験値
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).GetComponent<Player>().GetEXP(efficacy);
	}
}
