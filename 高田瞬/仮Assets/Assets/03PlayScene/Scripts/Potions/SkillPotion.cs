using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//スキルポーション
public class SkillPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.SKILL);
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//スキルポイント
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).GetComponent<Player>().skillPoint += efficacy;
	}
}
