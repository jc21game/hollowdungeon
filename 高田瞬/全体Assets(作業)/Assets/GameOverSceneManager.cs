using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ScoreManager;

public class GameOverSceneManager : MonoBehaviour
{

    //オブジェクト管理用
    private GameObject SoundManager = null;

    //コンポーネント格納用
    private SoundManager sound = null;

    private
    // Start is called before the first frame update
    void Start()
    {
        //0〜2までからランダムに数字を取得する
        int SerectBGM = Random.Range(0, 3);

        //オブジェクト取得
        SoundManager = GameObject.Find("SoundManager");

        //コンポーネント取得
        sound = SoundManager.GetComponent<SoundManager>();

        //Gameoverの時のBGMをランダムに再生する
        switch (SerectBGM)
        {
            case 0:
                //再生
                sound.playBGM("Assets/Sounds/Gameover1.mp3"); break;
            case 1:
                //再生
                sound.playBGM("Assets/Sounds/Gameover2.mp3"); break;
            default:
                //再生
                sound.playBGM("Assets/Sounds/Gameover3.mp3"); break;
        }

        //メッセージウィンドウの生成
        MessageWindowManager.ImageGeneration();

        Score.GetFloor();

        // コンソールへ表示
        Debug.Log(Score.GetFloor());

    }

    // Update is called once per frame
    void Update()
    {
        //テキストセット
        MessageWindowManager.MessageTextChange(Score.GetFloor().ToString());
    }

    //タイトルシーンに移動するための関数
    public void ChangeTitle()
	{
        //タイトルシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
    }
}
