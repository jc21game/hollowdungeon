using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleSceneManager : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {

        FadeScript.Darkening();
        FadeScript.FadeIn();

    }

    // Update is called once per frame
    void Update()
    {
        //Zキーを押したらフェードアウトして武器選択シーンに移動する
        if (Input.GetKeyDown(KeyCode.Z))
		{
            FadeScript.FadeOut();

            //4秒後に武器選択シーンに移動する
            Invoke("ChangeWeapon", 4.0f);
        }

    }

    //武器選択シーンに移動する関数
    public void ChangeWeapon()
    {
        //武器選択シーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("WeaponSelectScene");
    }

}
