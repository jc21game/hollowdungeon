using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Threading;

public class SplashSceneManager : MonoBehaviour
{
    int time = 0;

    // Start is called before the first frame update
    void Start()
    {
        //最初真っ暗な画面にする
        FadeScript.Darkening();

        //フェードインでスプラッシュシーンを表示する
        FadeScript.FadeIn();

        //5秒後にフェードアウトする
        Invoke("FadeOutPlay", 5.0f);

        //10秒後にタイトルに移動する
        Invoke("ChangeTitle", 10.0f);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            //スリーブ
            Thread.Sleep(1000);

            //タイトルシーンに移動
            UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
        }
    }

    //タイトルシーンに移動する関数
    public void ChangeTitle()
    {
        //タイトルシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
    }

    //フェードアウトする関数
    public void FadeOutPlay()
    {
        //フェードアウトする
        FadeScript.FadeOut();
    }
}
