using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScoreManager;
using UnityEngine.SceneManagement;


public class EndingSceneManager : MonoBehaviour
{

    //オブジェクト管理用
    private GameObject SoundManager = null;

    //コンポーネント格納用
    private SoundManager sound = null;

    enum TEXT_TYPE
	{
        NONE,
        STATUS,
        BATTLE,
        OTHER
	}
    private static  TEXT_TYPE type = TEXT_TYPE.NONE;

    private static void SetType(TEXT_TYPE texttype)
	{
        type = texttype;
	}

    private static TEXT_TYPE GetTextType()
	{
        return type;
	}

    private string GetF;


    // Start is called before the first frame update
    void Start()
    {

        

        //オブジェクト取得
        SoundManager = GameObject.Find("SoundManager");

        //コンポーネント取得
        sound = SoundManager.GetComponent<SoundManager>();

        //再生
        sound.playBGM("Assets/Sounds/Ending.mp3");


        //生成
        MessageWindowManager.ImageGeneration();

        //スコアを表示するための準備
        Score.GetHp();                // HP
        Score.GetAttack();            // 攻撃力
        Score.GetDefence();           // 防御力
        Score.GetLv();                // Lv
        Score.GetFloor();             // 階層
        Score.GetTurn();              // 総ターン数
        Score.GetCoveredRecovery();   // 総回復量
        Score.GetCoveredDamage();     // 総破ダメージ
        Score.GetGivingDamage();      // 総与ダメージ
        Score.GetCoveredAttack();     // 総破ダメージ回数
        Score.GetGivingAttack();      // 総与攻撃回数
        Score.GetHaveItem();          // 総アイテム取得数
        Score.GetUseItem();           // 総アイテム使用回数
        
        // コンソールへ表示
        Debug.Log(Score.GetFloor());

        

    }

    // Update is called once per frame
    void Update()
    {

		if (GetTextType() == TEXT_TYPE.NONE)
		{
            //テキストセット
            MessageWindowManager.MessageTextChange("                                                                     今回の記録");

            //テキストセット
            MessageWindowManager.MessageTextChange("                                   HP:" + Score.GetHp().ToString() + 
                "                                     LV:" + Score.GetLv().ToString() + "                                     満腹度:0");

            // テキストセット
            MessageWindowManager.MessageTextChange("                                                                     ");

            //テキストセット
            MessageWindowManager.MessageTextChange("                                             攻撃力:" + Score.GetAttack().ToString() +
                "                                                防御力:" + Score.GetDefence().ToString());
           

            // テキストセット
            MessageWindowManager.MessageTextChange("                                                                                  ▼");

            //セッター
            SetType(TEXT_TYPE.STATUS);
        }

		if (Input.GetKeyDown(KeyCode.Z ) || Input.GetKeyDown(KeyCode.X))
		{
            if (GetTextType() == TEXT_TYPE.STATUS)
            {
                //表示
                //テキスト消去
                MessageWindowManager.TextReset();

                //テキストセット
                MessageWindowManager.MessageTextChange("                                                                     今回の記録");

                //テキストセット
                MessageWindowManager.MessageTextChange("                            階層:" + Score.GetFloor().ToString() +
                    "                                     総ターン数:" + Score.GetTurn().ToString() + "                                    総回復量:" + Score.GetCoveredRecovery().ToString());

                // テキストセット
                MessageWindowManager.MessageTextChange("                                                                     ");

                //テキストセット
                MessageWindowManager.MessageTextChange("                                             総与ダメージ:" + Score.GetGivingDamage().ToString() +
                    "                                                総破ダメージ:" + Score.GetCoveredDamage().ToString());


                // テキストセット
                MessageWindowManager.MessageTextChange("                                                                                  ▼");

                SetType(TEXT_TYPE.BATTLE);

                return;
            }

            if (GetTextType() == TEXT_TYPE.BATTLE)
            {
                //表示
                //テキスト消去
                MessageWindowManager.TextReset();

                //テキストセット
                MessageWindowManager.MessageTextChange("                                                                     今回の記録");


                //テキストセット
                MessageWindowManager.MessageTextChange("                         　総与攻撃回数:" + Score.GetGivingAttack().ToString() +
                    "                             総破ダメージ回数:" + Score.GetCoveredAttack().ToString());

                // テキストセット
                MessageWindowManager.MessageTextChange("                                                                     ");

                //テキストセット
                MessageWindowManager.MessageTextChange("                                     総アイテム取得数:" + Score.GetHaveItem().ToString() +
                    "                   総アイテム使用回数:" + Score.GetUseItem().ToString());


                // テキストセット
                MessageWindowManager.MessageTextChange("                                                                                  ▼");


                SetType(TEXT_TYPE.OTHER);

                return;
            }
            if (GetTextType() == TEXT_TYPE.OTHER)
            {

                FadeScript.FadeOut();

                //4秒後にタイトルシーンに移動する
                Invoke("ChangeTitle", 4.0f);
                
            }
        }
        

    }

    //タイトルシーンに移動する関数
    public void ChangeTitle()
    {
        //タイトルシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
    }


}

