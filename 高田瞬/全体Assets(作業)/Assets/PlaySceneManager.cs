using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ScoreManager;



public class PlaySceneManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Score.SetAttack(100);
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    //エンディングシーンに移動する関数
    public void ChangeEnding()
	{
        //エンディングシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("EndingScene");
	}

    //ゲームオーバーシーンに移動する関数
    public void ChangeGameOver()
    {
        //ゲームオーバーシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("GameOverScene");
    }
}
