using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageWindowManager : MonoBehaviour
{
    //数字
    private static int Zero = 0;
    private static int One = 1;

    //オブジェクト
    private static GameObject MessageWindow;

    //メッセージボックス用のCanvas:Image:Text
    //サイズ調整用のCanvasScaler
    private static Canvas Canvas;
    private static CanvasScaler CanvasScaler;
    private static Image Image;
    private static Text Text;

    //描画優先度
    private static int Priority = 10;

    //メッセージボックスの縦横比
    private static float MessageBoxWidth = Screen.width / 5 * 4;
    private static float MessageBoxHeight = Screen.height / 7 * 2;

    //メッセージボックスの位置
    private static float MessageBoxXPosition = Zero;
    private static float MessageBoxYPosition = -(Screen.height / 2 - Screen.height / 6);

    //メッセージボックスの透明度
    private static float Alpha = 0.7f;

	//画像
	private static Sprite Sprite;

    //Textの位置調整
    private static float TextYPosition = 8.8f;

    //TextとImageの間隔
    private static float Difference = 27.0f;

    //Fontサイズ
    private static int FontSize = 15;

    //テキストがセットされた回数
    private static int SetTextNumber = 0;

    //表示するテキストの置き場所
    private static string MessageText = "";

    //メッセージボックスにセットするテキスト群
    private static string[] MessageList = { "", "", "", "", "" };

    //Canvas生成
    private static void CanvasGeneration()
    {
        //----------------Canvas生成部----------------//

        //Canvas名設定
        MessageWindow = new GameObject("MessageCanvas");

        //Canvasを獲得、生成
        Canvas = MessageWindow.AddComponent<Canvas>();

        //CanvasScalerを獲得、生成
        CanvasScaler = MessageWindow.AddComponent<CanvasScaler>();

        //描画優先度が有効なモードに変更
        Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        //ReferenceResolutionの設定
        //これを設定しないと、CanvasのデフォルトサイズがScreenサイズなのに
        //実際の画面構成はCanvasScalerのデフォルト値が反映されややこしくなる
        CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        //画面サイズの変更に合わせて自動でサイズ変更するモードに変更
        CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

        //MessageCanvasにこのソースをアタッチ
        MessageWindow.AddComponent<MessageWindowManager>();

        //描画優先度設定
        Canvas.sortingOrder = Priority;

    }

    //Image生成
    public static void ImageGeneration()
    {
        //MessageWindowがnullだったら
        if (MessageWindow == null)
        {
            //生成する
            CanvasGeneration();
        }

        //----------------Image生成部----------------//

        //Image名設定
        Image = new GameObject("MessageImage").AddComponent<Image>();

        //Imageの親にCanvasを設定
        Image.transform.SetParent(Canvas.transform, false);

        //Imageの位置を設定
        Image.rectTransform.anchoredPosition = new Vector2(MessageBoxXPosition, MessageBoxYPosition);

        //サイズの調整
        Image.rectTransform.sizeDelta = new Vector2(MessageBoxWidth, MessageBoxHeight);

        //Imageの透明度設定
        Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, Alpha);

        //画像をImageにアタッチ
        Sprite = Resources.Load<Sprite>("MessageBoxImage");
        Image.sprite = Sprite;

        //-------------------------------------------//

        //Textがいなければ生成
        if (Text == null)
        {
            //生成
            TextGeneration();
        }

    }

    //Text内容リセット
    public static void TextReset()
    {
        //テキストセット回数リセット
        SetTextNumber = 0;

        //セットしているテキスト内容をリセットする
        for (int i = Zero; i < MessageList.Length; i++)
        {
            MessageList[i] = "";
        }

        //テキスト反映
        TextOrganize();
    }

    //Text内容整理
    private static void TextOrganize()
    {
        //テキスト内容をリセット
        MessageText = "";

        //テキスト内容を構築
        for (int i = Zero; i < MessageList.Length; i++)
        {
            //2行目以降の処理
            if (i > Zero)
            {
                MessageText += "\n" + MessageList[i];
            }

            //1行目のみ前に改行ナシ
            else
            {
                MessageText += MessageList[i];
            }
        }

        //テキスト内容更新
        Text.text = MessageText;
    }

    //Text生成
    private static void TextGeneration()
    {
        //----------------Text生成部----------------//

        //Text名設定
        Text = new GameObject("MessageText").AddComponent<Text>();

        //Imageを親に設定
        Text.transform.SetParent(Image.transform, false);

        //Textの位置調整
        Text.rectTransform.anchoredPosition = new Vector2(Zero, -TextYPosition);

        //Text領域を変更
        Text.rectTransform.sizeDelta = new Vector2(MessageBoxWidth - Difference, MessageBoxHeight);

        //フォントを変更
        Text.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        //フォントサイズを変更
        Text.fontSize = FontSize;

        //フォントカラーを変更
        Text.color = Color.white;

        //Text整理
        TextOrganize();

    }

    //テキスト内容変更
    public static void MessageTextChange(string message)
    {
        //新しいテキストをセット
        //セット回数が5行以下
        if (SetTextNumber < MessageList.Length)
        {
            //セット回数行にテキストセット
            MessageList[SetTextNumber] = message;
        }
        //5行以上
        else if (SetTextNumber > MessageList.Length - One)
        {
            //行送り
            MessageLineSpacing();

            //5行目にテキストセット
            MessageList[MessageList.Length - One] = message;
        }

        //Text整理
        TextOrganize();

        //テキストセット回数増加
        SetTextNumber++;
    }

    //Image非表示
    public static void MessageImageHide()
    {
        //Imageを非表示
        Image.enabled = false;

        //テキストリセット
        TextReset();
    }

    //Image表示
    public static void MessageImageDisplay()
    {
        Image.enabled = true;
    }

    //行送り
    private static void MessageLineSpacing()
    {
        //テキストを並び替え
        for (int i = Zero; i < MessageList.Length - One; i++)
        {
            //テキスト内容を1行繰り上げ
            MessageList[i] = MessageList[i + One];
        }
    }

    //行流し
    public static void MessageFlowing()
	{
        //行送り
        MessageLineSpacing();

        //規定値より大きければ減少
        if(SetTextNumber > Zero)
		{
            //セット回数減少
            SetTextNumber--;
        }
        
        //Text整理
        TextOrganize();
	}

}
