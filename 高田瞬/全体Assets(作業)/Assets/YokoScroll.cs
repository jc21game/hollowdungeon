using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YokoScroll : MonoBehaviour
{
    [SerializeField] float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(Time.deltaTime * speed,0);

        //Xが-23.0まで来れば、45.0まで移動する
        if (transform.position.x <= -23.0f)
        {


            transform.position = new Vector2(45.0f, 0);


        }
    }
}
