using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ScoreManager;

public class GameOverSceneManager : MonoBehaviour
{
    int Count = 0;
    enum TEXT_TYPE
    {
        NONE,
        STATUS,
        BATTLE,
        OTHER
    }
    private static TEXT_TYPE type = TEXT_TYPE.NONE;

    private static void SetType(TEXT_TYPE texttype)
    {
        type = texttype;
    }

    private static TEXT_TYPE GetTextType()
    {
        return type;
    }

    private string GetF;

    //オブジェクト管理用
    private GameObject SoundManager = null;

    //コンポーネント格納用
    private SoundManager sound = null;

    private
    // Start is called before the first frame update
    void Start()
    {
        FadeScript.FadeIn();

        //0〜2までからランダムに数字を取得する
        int SerectBGM = Random.Range(0, 3);

        //オブジェクト取得
        SoundManager = GameObject.Find("SoundManager");

        //コンポーネント取得
        sound = SoundManager.GetComponent<SoundManager>();

        //Gameoverの時のBGMをランダムに再生する
        switch (SerectBGM)
        {
            case 0:
                //再生
                sound.PlayBGM("Assets/05GameOverScene/Sounds/BGM/Gameover1.mp3"); break;
            case 1:
                //再生
                sound.PlayBGM("Assets/05GameOverScene/Sounds/BGM/Gameover2.mp3"); break;
            default:
                //再生
                sound.PlayBGM("Assets/05GameOverScene/Sounds/BGM/Gameover3.mp3"); break;
        }

        //メッセージウィンドウの生成
        MessageWindowManager.ImageGeneration();
       

        //スコアを表示するための準備
        Score.GetHp();                // HP
        Score.GetAttack();            // 攻撃力
        Score.GetDefence();           // 防御力
        Score.GetLv();                // Lv
        Score.GetFloor();             // 階層
        Score.GetTurn();              // 総ターン数
        Score.GetCoveredRecovery();   // 総回復量
        Score.GetCoveredDamage();     // 総破ダメージ
        Score.GetGivingDamage();      // 総与ダメージ
        Score.GetCoveredAttack();     // 総破ダメージ回数
        Score.GetGivingAttack();      // 総与攻撃回数
        Score.GetHaveItem();          // 総アイテム取得数
        Score.GetUseItem();           // 総アイテム使用回数

        // コンソールへ表示
        Debug.Log(Score.GetFloor());

    }

    // Update is called once per frame
    void Update()
    {
        //生成
        MessageWindowManager.ImageGeneration();

        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
        {
            Count++;
        }

        if (Count == 3)
        {

            FadeScript.FadeOut();

            //4秒後にタイトルシーンに移動する
            Invoke("ChangeTitle", 4.0f);


        }


    }

    //タイトルシーンに移動するための関数
    public void ChangeTitle()
	{
        //タイトルシーンに移動
        UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
    }
}
