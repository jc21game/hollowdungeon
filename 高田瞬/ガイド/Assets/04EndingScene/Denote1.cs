using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ScoreManager;

public class Denote1 : MonoBehaviour
{
    [SerializeField] GameObject text;
    int Count = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
        {
            Count++;
        }
        if (Count == 0)
        {
            //ゲームオブジェクト非表示→表示
            text.SetActive(true);
        }
        else
        {
            text.gameObject.SetActive(false);

        }




    }
}
