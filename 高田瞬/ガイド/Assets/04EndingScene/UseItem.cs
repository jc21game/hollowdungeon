using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ScoreManager;

public class UseItem : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        Score.GetUseItem();           // 総アイテム使用回数
        gameObject.GetComponent<UnityEngine.UI.Text>().text = "総アイテム使用回数:" + Score.GetUseItem().ToString();
    }

    // Update is called once per frame
    void Update()
    {
       

    }
}
