using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//タイトルシーン
public class TitleSceneManager : MonoBehaviour
{
	//武器選択シーンへ移動する時間
	public float invokeTime = 0;

	//サウンドオブジェクト
	private GameObject SoundObject = null;

	//インスタンス
	private GameObject SoundInstance = null;

	//サウンドマネージャー
	private SoundManager SoundManager = null;

	//初期化
	public void Start()
	{
		//フェード画像初期化
		FadeScript.Darkening();
		FadeScript.FadeIn();

		//サウンドプレハブを取得
		SoundObject = (GameObject)Resources.Load("SoundManager");

		//インスタンス生成
		SoundInstance = Instantiate(SoundObject);

		//コンポーネント取得
		SoundManager = SoundInstance.GetComponent<SoundManager>();

		//初期音量設定
		//SoundManager.ChangeVolume();

		//再生
		SoundManager.PlayBGM("Assets/03PlayScene/Sounds/BGM/BGM_1.mp3");
	}

	//更新
	public void Update()
	{
		//Zキーを押したらフェードアウトして武器選択シーンに移動する
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//フェードアウト
			FadeScript.FadeOut();

			//4秒後に武器選択シーンに移動する
			Invoke("ChangeWeapon", invokeTime);
		}

		//音量更新
		//SoundManager.ChangeVolume();

		if(Input.GetKeyDown("k"))
		{
			SoundManager.PlaySE("Assets/03PlayScene/Sounds/SE/SE_1.mp3");
		}
	}

	//武器選択シーンに移動する関数
	public void ChangeWeapon()
	{
		//武器選択シーンに移動
		UnityEngine.SceneManagement.SceneManager.LoadScene("WeaponSelectScene");
	}

}
