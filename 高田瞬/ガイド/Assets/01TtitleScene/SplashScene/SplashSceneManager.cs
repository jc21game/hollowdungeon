using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

//スプラッシュ
public class SplashSceneManager : MonoBehaviour
{
	//フェード時間
	public float FadeInTime = 0;
	public float FadeOutTime = 0;

	//スリープ時間
	public int sleepTime = 0;

	//初期化
	public void Start()
	{
		//フレームレート設定
		Application.targetFrameRate = 60;

		//最初真っ暗な画面にする
		FadeScript.Darkening();

		//フェードインでスプラッシュシーンを表示する
		FadeScript.FadeIn();

		//FadeOutTime秒後にフェードアウトする
		Invoke("FadeOutPlay", FadeOutTime);

		//FadeInTime秒後にタイトルに移動する
		Invoke("ChangeTitle", FadeInTime);

	}

	//更新
	public void Update()
	{
		//Zキー入力でスプラッシュを飛ばす
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//スリーブ
			Thread.Sleep(sleepTime);

			//タイトルシーンに移動
			UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
		}
	}

	//タイトルシーンに移動する関数
	public void ChangeTitle()
	{
		//タイトルシーンに移動
		UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
	}

	//フェードアウトする関数
	public void FadeOutPlay()
	{
		//フェードアウトする
		FadeScript.FadeOut();
	}
}
