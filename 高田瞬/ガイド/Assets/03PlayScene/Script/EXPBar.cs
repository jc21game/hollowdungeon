using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EXPBar : MonoBehaviour
{
    //コンポーネント取得用
    private Slider expBar = null;

    private HPBar hpBar = null;

    private LevelText lvText = null;

    //経験値管理用
    private int exp = 0;

    //次のレベルまでの経験値管理用
    private int nextExp = 0;

    //初期化
    public void Start()
    {
        //コンポーネント取得
        expBar = this.GetComponent<Slider>();
        hpBar = GameObject.Find("HPBar").GetComponent<HPBar>();
        lvText = GameObject.Find("LevelText").GetComponent<LevelText>();

        //初期化
        exp = 0;
        nextExp = 100;
    }

    // 更新
    public void Update()
    {
        //4を押したら
        if(Input.GetKeyDown("3"))
		{
            //経験値加算
            AddEXP(30);
		}

        //経験値を設定
        expBar.value = exp;

        //次のレベルまでの経験値を設定
        expBar.maxValue = nextExp;

    }

    //経験値加算
    public void AddEXP(int add)
	{
        //経験値加算
        exp += add;

        //経験値上限を超えたら
        if(exp >= nextExp)
		{
            //レベル更新
            lvText.LvUp();

            //最大HP増加
            hpBar.AddMaxHP(3);

            //HP回復
            hpBar.RecoveryHP(3);

            //ゲージをリセット
            exp -= nextExp;

            //上限更新
            UpdateNextEXP(25);
		}
	}

    //経験値の上限更新
    public void UpdateNextEXP(int update)
	{
        nextExp += update;
	}

    //経験値を取得
    public int GetExp() { return exp; }

    //次のレベルまでの経験値取得
    public int GetNextExp() { return nextExp; }
}
