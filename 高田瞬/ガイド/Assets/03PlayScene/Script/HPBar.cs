using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    //コンポーネント取得用
    private Slider hpBar = null;

    //最大HP管理用
    private int maxHp = 0;

    //HP管理用
    private int hp = 0;

    //初期化
    public void Start()
    {
        //コンポーネント取得
        hpBar = this.GetComponent<Slider>();

        //初期化
        maxHp = 10;
        hp = 10;
    }

    //更新
    public void Update()
    {
        //1を押したら
        if(Input.GetKeyDown("1"))
		{
            //HP回復
            RecoveryHP(3);
		}

        //2を押したら
        if(Input.GetKeyDown("2"))
		{
            //HP減少
            SubtractHP(3);
        }

        //最大HPを設定
        hpBar.maxValue = maxHp;

        //HPを設定
        hpBar.value = hp;
    }

    //HP回復
    public void RecoveryHP(int recovery)
	{
        //HP加算
        hp += recovery;

        //最大HP以上に回復したら
        if(hp >= maxHp)
		{
            hp = maxHp;
		}
	}

    //HP減少
    public void SubtractHP(int subtract)
	{
        //HP減少
        hp -= subtract;

        //最小HP以上に減少したら
        if(hp <= 0)
		{
            hp = 0;
		}
	}

    //最大HP増加
    public void AddMaxHP(int add)
	{
        //最大HP増加
        maxHp += add;
	}

    //HP取得
    public int GetHp() { return hp; }

    //最大HP取得
    public int GetMaxHp() { return maxHp; }
}
