using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelText : MonoBehaviour
{
    //コンポーネント取得用
    private Text text = null;

    //レベル管理用
    private int lv = 0;

    //表示テキスト管理用
    private string str = "";

    //初期化
    public void Start()
    {
        //コンポーネント取得
        text = this.GetComponent<Text>();

        //初期化
        lv = 1;
        str = "Lv.";
    }

    //更新
    public void Update()
    {
        //テキスト表示
        text.text = str + lv.ToString();
    }

    //レベル更新
    public void LvUp()
	{
        //レベル更新
        lv++;
    }
}
