using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public class Enemy : CharacterBase
{
    //エネミーのプレハブ格納
    public GameObject EnemyObject = null;
    //エネミーオブジェクト
    private GameObject Enem = null;
    //位置
    public static int ex;
    public static int ey;
    //行動時間(仮)
    float second = 0.5f;

    //記録配列
    private int[,] memory = new int[64, 64];

    private void Awake()
    {
        //敵位置決め
        while (true)
        {
            ex = Random.Range(0, 64);
            ey = Random.Range(0, 64);
           
            if (Map.CsvMap[ex, ey] >= 2)
            {
                break;
            }
            continue;
            
        }
    }
    //初期化
    public override void Start()
    {
        Enem = Instantiate(EnemyObject) as GameObject;
        Enem.transform.position = new Vector3(ey, -ex, 0);

        //仮
        SetHp(5);
    }

    //更新
    public override void Update()
    {
        //レイヤー切り替え
        ChngLayer();

        //flgがfalseなら非表示
        Enem.SetActive(PlaySceneManager.flg);

    }



    public override bool Execute()
    {
        //行動終了
        //if (Input.GetKeyDown(KeyCode.Return))
        //{
        //敵の思考時間(仮)
        return true;
        if (0.0f > (second -= Time.deltaTime))
        {
            second = 0.5f;
        }
              

            
        //}

        //死亡
        if(Input.GetKeyDown(KeyCode.A))
        {
            OnDamage(1);

            //テスト用
            //Debug.Log(GetCharacterName() + "のHP:" + GetHp());
            if (GetHp() == 0)
            {
                Debug.Log(GetCharacterName() + "の削除 + ターン終了");
            }
        }

        return false;

    }

    //行動
    private void Move(int x, int y)
    {
        //移動可能だったら移動
        if (Map.CsvMap[x, y] != 0)
        {
            if ((x != Player.px || y != Player.py) && (x != Boss.bx || y != Boss.by))
            {
                memory[ex, ey] = 0;
                ex = x;
                ey = y;
                memory[ey, ex] = 3;
                Enem.transform.position = new Vector3(ey, -ex, 0);
            }
        }
    }

    //レイヤーを切り替える
    private void ChngLayer()
    {
        if (Player.px >= ex)
        {
            Enem.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        else if (Player.px < ex)
        {
            Enem.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
    }

    //死亡
    public override void Deth()
    {
        //フラグ変更
        SetIsDeth(true);
    }
}
