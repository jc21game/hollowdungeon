using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    //各地形のプレハブを格納
    public GameObject WallPrefab = null;
    public GameObject RoomPrefab = null;
    public GameObject AislPrefab = null;

    //大きさ
    public float scale = 0;

    //表示位置の微調整
    private int w = 0;
    private int h = 0;

    //地形情報を記録
    private GameObject[,] m = new GameObject[64, 64];
    //各地形を格納
    private GameObject obj = null;

    //マップ情報
    public static int[,] CsvMap;

    private void Awake()
    {
        w = 5;
        h = 3;

        //CSVを読み込んでマップ情報を取得
        CSVReader.LoadCsv("CSV_MAP");
        CsvMap = new int[CSVReader.GetCsvHeight(), CSVReader.GetCsvWidth()];

        for (int Vertical = 0; Vertical < CSVReader.GetCsvHeight(); Vertical++)
        {
            for (int Horizontal = 0; Horizontal < CSVReader.GetCsvWidth(); Horizontal++)
            {
                CsvMap[Vertical, Horizontal] = CSVReader.GetDataInt(Horizontal, Vertical);
            }
        }
    }

    private void Start()
    {
        //CSVからマップの生成
        for (int Vertical = 0; Vertical < CsvMap.GetLength(0); Vertical++)
        {
            for (int Horizontal = 0; Horizontal < CsvMap.GetLength(1); Horizontal++)
            {
                //壁
                if (CsvMap[Vertical, Horizontal] == 0)
                {
                    obj = Instantiate(WallPrefab) as GameObject;
                    m[Vertical, Horizontal] = obj;
                }

                //通路
                if (CsvMap[Vertical, Horizontal] == 1)
                {
                    obj = Instantiate(AislPrefab) as GameObject;
                    m[Vertical, Horizontal] = obj;
                }

                //部屋
                if (CsvMap[Vertical, Horizontal] >= 2)
                {
                    obj = Instantiate(RoomPrefab) as GameObject;
                    m[Vertical, Horizontal] = obj;
                }

                //表示
                obj.transform.position = new Vector3(Horizontal * scale, (-Vertical) * scale, 0);
                
            }
        }
    }

    //更新
    private void Update()
    {
        //flgがfalseなら非表示
        for(int i = 0;i < CsvMap.GetLength(0);i++)
        {
            for(int j = 0;j < CsvMap.GetLength(1);j++)
            {
                m[i, j].SetActive(PlaySceneManager.flg);
            }
        }
    }
}
