using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public class Boss : CharacterBase
{

    public GameObject BossObject = null;

    private GameObject boss = null;

    public static int bx;
    public static int by;

    float second = 4.0f;

    private int[,] memory = new int[64, 64];


    private void Awake()
    {
        while (true)
        {
            bx = Random.Range(0, 64);
            by = Random.Range(0, 64);

            if (Map.CsvMap[bx, by] >= 2)
            {
                break;
            }
            continue;

        }
    }
    // Start is called before the first frame update
    public override void Start()
    {
        boss = Instantiate(BossObject) as GameObject;
        boss.transform.position = new Vector3(by, -bx, 0);

        //仮
        SetHp(5);
    }

    // Update is called once per frame
    public override void Update()
    {
        ChngLayer();

        boss.SetActive(PlaySceneManager.flg);
    }


    public override bool Execute()
    {
        //行動終了
        //if (Input.GetKeyDown(KeyCode.Return))
        //{        
        //敵の思考時間(仮)
        if (0.0f > (second -= Time.deltaTime))
        {
            second = 4.0f;
            return true;
        }
        //}

        //死亡
        if (Input.GetKeyDown(KeyCode.A))
        {
            OnDamage(1);

            //テスト用
            Debug.Log(GetCharacterName() + "のHP:" + GetHp());
            if (GetHp() == 0)
            {
                Debug.Log(GetCharacterName() + "の削除 + ターン終了");
            }
        }

        return false;

    }

    //移動
    private void Move(int x, int y)
    {
        //移動可能だったら移動
        if (Map.CsvMap[x, y] != 0)
        {
            if ((x != Enemy.ex || y != Enemy.ey) && (x != Player.px || y != Player.py))
            {
                memory[bx, by] = 0;
                bx = x;
                by = y;
                memory[by, bx] = 3;
                boss.transform.position = new Vector3(by, -bx, 0);
            }
        }
    }

    //レイヤー切り替え
    private void ChngLayer()
    {
        if (Player.px >= bx)
        {
            boss.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        else if (Player.px < bx)
        {
            boss.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
    }

    //死亡
    public override void Deth()
    {
        //フラグ変更
        SetIsDeth(true);
    }
}
