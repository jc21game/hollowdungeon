using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.UI;

public class Player : CharacterBase
{
    //コンポーネント取得(キャラクター)
    //private Character chr = null;

    //プレイヤーの移動(4方向)*移動動作(2)
    //上
    public GameObject PlayerObject = null;
    public GameObject PlayerObject1 = null;
    //右
    public GameObject PlayerObject2 = null;
    public GameObject PlayerObject3 = null;
    //左
    public GameObject PlayerObject4 = null;
    public GameObject PlayerObject5 = null;
    //下
    public GameObject PlayerObject6 = null;
    public GameObject PlayerObject7 = null;

    //待機モーション
    public GameObject Up = null;
    public GameObject Right = null;
    public GameObject Down = null;
    public GameObject Left = null;


    //プレイヤーの詳細(大きさや位置など)
    private GameObject Hero = null ;

    //カメラ(プレイヤーを中心に追従)
    public GameObject Camera = null;

    //向き
    private int HeroDir = 1;

    //プレイヤーの位置 px:X座標　py:Y座標
    public static int px;
    public static int py;

    private float time = 0.5f;
    private bool check = true;

    //プレイヤーがいる位置を記録
    private int[,] memory = new int[64, 64];

    //選択武器
    private int selWeapon = 0;

    //自然回復量
    private int selfRecovery = 0;

    //必要自然回復ターン数
    private int necessarySelfRecoveryTurn = 0;

    //スキルポイント
    private int skillPt = 0;

    //レベルアップまでに必要な総経験値
    private int nextExp = 0;

    //必要経験値の増える量
    private int addNecessaryExp = 0;

    //経過必要自然回復ターン数
    private int progressNecessarySelfRecoveryTurn = 0;

    //LShiftキー確認
    private bool isShift = false;

    //移動したかどうか
    private bool isWalk = false;

    //移動できるマス数(最大)
    private int checkUp = 0;
    private int checkDown = 0;
    private int checkRight = 0;
    private int checkLeft = 0;

    //移動したマス数(移動量)
    private int moveUp = 0;
    private int moveDown = 0;
    private int moveRight = 0;
    private int moveLeft = 0;

    //移動前の位置と移動後の差
    private int diffUp = 0;
    private int diffDown = 0;
    private int diffRight = 0;
    private int diffLeft = 0;

    //各項目の取得
    public int GetSelWeapon() { return selWeapon; }
    public int GetSelfRecovery() { return selfRecovery; }
    public int GetNecessarySelfRecoveryTurn() { return necessarySelfRecoveryTurn; }
    public int GetSkillPt() { return skillPt; }
    public int GetNextExp() { return nextExp; }
    public int GetAddNecessaryExp() { return addNecessaryExp; }

    //各項目の設定
    public void SetSelWeapon(int weapon) { selWeapon = weapon; }
    public void SetSelfRecovery(int value) { selfRecovery = value; }
    public void SetNecessarySelfRecoveryTurn(int turn) { necessarySelfRecoveryTurn = turn; }
    public void SetSkillPt(int value) { skillPt = value; }
    public void SetNextExp(int value) { nextExp = value; }
    public void SetAddNecessaryExp(int value) { addNecessaryExp = value; }

    private bool Walk = true;

    private bool IsItem = false;

    bool c = true;

    private void Awake()
    {
        //プレイヤーの位置決め
        while (true)
        {
            px = Random.Range(0, 64);
            py = Random.Range(0, 64);

            if (Map.CsvMap[px, py] >= 2)
            {
                break;
            }
            continue;
        }
    }


    //初期化
    public override void Start()
    {
        //初期化
        selWeapon = 0;
        selfRecovery = 0;
        necessarySelfRecoveryTurn = 0;
        skillPt = 0;
        nextExp = 0;
        addNecessaryExp = 0;
        progressNecessarySelfRecoveryTurn = 0;
        isShift = false;
        isWalk = false;

        //記憶配列の初期化
        for (int Vertical = 0; Vertical < Map.CsvMap.GetLength(0); Vertical++)
        {
            for (int Horizontal = 0; Horizontal < Map.CsvMap.GetLength(1); Horizontal++)
            {
                memory[Vertical, Horizontal] = 0;
            }
        }

        //プレイヤー座標を3
        memory[py, px] = 3;
        //プレイヤーの表示位置
        Hero = Instantiate(Up) as GameObject;
        Hero.transform.position = new Vector3(py, -px, 0);
        //カメラの位置
        Camera.transform.position = new Vector3(py, -px, -10);




        //仮のステータスセット
        SetHp(10);
        SetMaxHp(10);
        SetExp(0);
        SetNextExp(100);
        SetLv(1);

        //コンポーネント取得
        //chr = this.GetComponent<Character>();

        //checkUp = chr.GetCheckUp();
        //checkDown = chr.GetCheckDown();
        //checkRight = chr.GetCheckRight();
        //checkLeft = chr.GetCheckLeft();

        moveUp = 0;
        moveDown = 0;
        moveRight = 0;
        moveLeft = 0;

        diffUp = 0;
        diffDown = 0;
        diffRight = 0;
        diffLeft = 0;


    }

    //更新
    public override void Update()
    {
        ////行動処理
        //Execute();

        //待機モーション
        if (c || !check)
        {
            Destroy(Hero);
            switch (HeroDir)
            {
                case 1: Hero = Instantiate(Up) as GameObject; break;
                case 2: Hero = Instantiate(Right) as GameObject; break;
                case 3: Hero = Instantiate(Left) as GameObject; break;
                case 4: Hero = Instantiate(Down) as GameObject; break;
            }
           
            
            Hero.transform.position = new Vector3(py, -px, 0);
        }

        Hero.SetActive(PlaySceneManager.flg);
    }

    //行動
    public override bool Execute()
    {
        //プレイヤーが移動していたら
        if (isWalk)
        {
            ////プレイヤーの位置から移動できる各方向のマス数を調べる
            //chr.CheckMap(chr.GetMap());

            ////求めたマス数をセット
            //checkUp = chr.GetCheckUp();
            //checkDown = chr.GetCheckDown();
            //checkRight = chr.GetCheckRight();
            //checkLeft = chr.GetCheckLeft();

            //フラグ変更
            isWalk = false;
        }

        //何も押していなくすでに待機モーションじゃない場合
        if (!Input.anyKey && check)
        {
            //0.5秒たったら待機モーションに切り替える
            if (0.0f > (time -= Time.deltaTime))
            {
                time = 0.5f;
                Debug.Log("停止中");
                check = false;
            }
        }

        //LShiftキーの状態
        //if (Input.GetKeyDown("left shift"))
        //{
        //    isShift = true;
        //}
        //if (Input.GetKeyUp("left shift"))
        //{
        //    isShift = false;
        //}

        //メニューを開いていない場合
        if (!Menu.menu && !Menu.item && !Menu.status && !Menu.end)
        {
            //上移動
            if (Input.GetKey(KeyCode.UpArrow))
            {
                HeroDir = 1;
                changwalk();
                Move(px - 1, py);
                check = true;
                return true;
            }

            //下移動
            if (Input.GetKey(KeyCode.DownArrow))
            {
                HeroDir = 4;
                changwalk();
                Move(px + 1, py);
                check = true;
                return true;
            }

            //左移動
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                HeroDir = 3;
                changwalk();
                Move(px, py - 1);
                check = true;
                return true;
            }

            //右移動
            if (Input.GetKey(KeyCode.RightArrow))
            {
                HeroDir = 2;
                changwalk();
                Move(px, py + 1);
                check = true;
                return true;
            }
        }

        //アイテム削除
        KillItem();

        if(IsItem)
        {
            //アイテム取得
            ItemBase.ObtainItem(0);
            IsItem = false;
        }


        //移動処理
        //上方向
        //if (Input.GetKeyDown("up"))
        //{
        //          //移動
        //          //MoveUp(isShift);
        //}
        //      //下方向
        //if (Input.GetKeyDown("down"))
        //{
        //          //移動
        //          //MoveDown(isShift);
        //      }
        //      //右方向
        //if (Input.GetKeyDown("right"))
        //{
        //          //移動
        //          //MoveRight(isShift);
        //      }
        //      //左方向
        //if (Input.GetKeyDown("left"))
        //{
        //          //移動
        //          //MoveLeft(isShift);
        //      }

        //攻撃処理
        if (Input.GetKeyDown("z"))
        {
            //攻撃処理
            Attack();

            Debug.Log("攻撃");

            //必要回復ターンを減らす
            DecreaseNecessarySelfRecoveryTurn();
        }

        //メニューを開く処理
        if (Input.GetKeyDown("x"))
        {
            //メニューを開く

            Debug.Log("メニューを開く");
        }

        //足踏み処理
        if (Input.GetKeyDown("c"))
        {
            //ターン経過

            Debug.Log("足踏み");

            //必要回復ターンを減らす
            DecreaseNecessarySelfRecoveryTurn();
        }

        //仮の処理
        //      if(Input.GetKeyDown("s"))
        //{
        //          //ダメージを受ける
        //          OnDamage(1);
        //          Debug.Log(GetCharacterName() + "のHP:" + GetHp());
        //      }


        if(Input.GetKeyDown(KeyCode.Q))
		{
            //タイトルシーンに移動
            UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
        }

        if(Input.GetKeyDown(KeyCode.E))
		{
            //タイトルシーンに移動
            UnityEngine.SceneManagement.SceneManager.LoadScene("EndingScene");
        }

        if(Input.GetKeyDown(KeyCode.R))
		{
            //タイトルシーンに移動
            UnityEngine.SceneManagement.SceneManager.LoadScene("GameOverScene");
        }

        return false;
    }

    //移動
    private void Move(int x, int y)
    {
        //移動可能だったら移動
        if (Map.CsvMap[x, y] != 0)
        {
            if((x != Enemy.ex || y != Enemy.ey) && (x != Boss.bx || y != Boss.by))
            {
            memory[px, py] = 0;
            px = x;
            py = y;
            memory[py, px] = 3;
            Hero.transform.position = new Vector3(py, -px, 0);
            Camera.transform.position = new Vector3(py, -px, -10);
            }
            c = false;
            Thread.Sleep(100);
        }
		else
		{
            c = true;
        }
    }

    //歩きモーション
    private void changwalk()
    {
        Destroy(Hero);
        if (Walk)
        {
            switch (HeroDir)
            {
                case 1:
                    Hero = Instantiate(PlayerObject1) as GameObject;
                    break;
                case 2:                    
                    Hero = Instantiate(PlayerObject3) as GameObject;
                    break;
                case 3:
                    Hero = Instantiate(PlayerObject5) as GameObject;
                    break;
                case 4:
                    Hero = Instantiate(PlayerObject7) as GameObject;
                    break;

            }
            Hero.transform.position = new Vector3(py, -px, 0);
            Walk = false;

        }
        else
        {
            switch (HeroDir)
            {
                case 1:
                    Hero = Instantiate(PlayerObject) as GameObject;
                    break;
                case 2:
                    Hero = Instantiate(PlayerObject2) as GameObject;
                    break;
                case 3:
                    Hero = Instantiate(PlayerObject4) as GameObject;
                    break;
                case 4:
                    Hero = Instantiate(PlayerObject6) as GameObject;
                    break;
            }

            Hero.transform.position = new Vector3(py, -px, 0);
            Walk = true;
        }
    }

    //アイテムの削除
    private void KillItem()
    {
        //同じ位置にアイテムがあったら
        if (px == Item.ix && py == Item.iy)
        {
            ItemBase.Itemflg = false;
            Item.ix = 0;
            Item.iy = 0;
            IsItem = true;


        }
    }


    //死亡
    public override void Deth()
    {
        //フラグ変更
        //SetIsDeth(true);
    }

    //必要自然回復ターンを減らす
    public void DecreaseNecessarySelfRecoveryTurn()
    {
        //経過ターン数を加算
        progressNecessarySelfRecoveryTurn++;

        //必要ターン数経過したら
        if (necessarySelfRecoveryTurn <= progressNecessarySelfRecoveryTurn)
        {
            //HPを回復
            OnRecovery(selfRecovery);

            //経過ターン数を初期化
            progressNecessarySelfRecoveryTurn = 0;
        }
    }

    //経験値を加算&レベルアップ判定
    public void AddExp(int value)
    {
        //経験値を加算
        exp += value;

        //レベルアップするか
        if (exp >= nextExp)
        {
            //上限を超えた(あふれた)経験値を設定
            exp -= nextExp;

            //レベルアップまでに必要な総経験値を設定
            SetNextExp(nextExp + 50);

            //レベルアップ
            SetLv(++lv);

            //HP回復
            SetHp(hp + 3);

            //最大HP加算
            SetMaxHp(maxHp + 3);
        }
    }

    //上方向に移動
    //引数：LShiftキーが押されているか
    //   public void MoveUp(bool shift)
    //{
    //       //移動量のリセット
    //       moveUp = 0;
    //       //差のリセット
    //       diffUp = 0;
    //       //フラグ変更
    //       isWalk = true;

    //       //移動量を求める
    //       if (shift)
    //	{
    //           //移動できるだけ移動
    //           moveUp = chr.px - checkUp;
    //       }
    //       else
    //	{
    //           //1マス移動
    //           moveUp = chr.px - 1;
    //       }

    //       //プレイヤーの現在の位置と移動後の位置の差を求める
    //       diffUp = chr.px - moveUp;

    //       //差の分移動する
    //       for (int i = 1; i <= diffUp; i++)
    //       {
    //           //移動
    //           chr.Move(chr.px - 1, chr.py);

    //           //必要回復ターンを減らす
    //           DecreaseNecessarySelfRecoveryTurn();

    //           //止まるかどうか
    //           if (chr.CheckStopUp(chr.GetMap()))
    //		{
    //               //移動したマス数を記憶
    //               moveUp = i;

    //               //止まる処理(仮)
    //               i = 100;
    //		}
    //           else
    //		{
    //               //移動したマス数を加算
    //               moveUp = i;
    //		}
    //       }
    //   }

    //下方向に移動
    //引数：LShiftキーが押されているか
    //   public void MoveDown(bool shift)
    //{
    //       //移動量のリセット
    //       moveDown = 0;
    //       //差のリセット
    //       diffDown = 0;
    //       //フラグ変更
    //       isWalk = true;

    //       //移動量を求める
    //       if (shift)
    //       {
    //           //移動できるだけ移動
    //           moveDown = chr.px + checkDown;
    //       }
    //       else
    //       {
    //           //1マス移動
    //           moveDown = chr.px + 1;
    //       }

    //       //プレイヤーの現在の位置と移動後の位置の差を求める
    //       diffDown = moveDown - chr.px;

    //       //差の分移動する
    //       for (int i = 1; i <= diffDown; i++)
    //       {
    //           //移動
    //           chr.Move(chr.px + 1, chr.py);

    //           //必要回復ターンを減らす
    //           DecreaseNecessarySelfRecoveryTurn();

    //           //止まるかどうか
    //           if (chr.CheckStopDown(chr.GetMap()))
    //           {
    //               //移動したマス数を記憶
    //               moveDown = i;

    //               //止まる処理(仮)
    //               i = 100;
    //           }
    //           else
    //           {
    //               //移動したマス数を加算
    //               moveDown = i;
    //           }
    //       }
    //   }

    //右方向に移動
    //引数：LShiftキーが押されているか
    //   public void MoveRight(bool shift)
    //{
    //       //移動量のリセット
    //       moveRight = 0;
    //       //差のリセット
    //       diffRight = 0;
    //       //フラグ変更
    //       isWalk = true;

    //       //移動量を求める
    //       if (shift)
    //       {
    //           //移動できるだけ移動
    //           moveRight = chr.py + checkRight;
    //       }
    //       else
    //       {
    //           //1マス移動
    //           moveRight = chr.py + 1;
    //       }

    //       //プレイヤーの現在の位置と移動後の位置の差を求める
    //       diffRight = moveRight - chr.py;

    //       //差の分移動する
    //       for (int i = 1; i <= diffRight; i++)
    //       {
    //           //移動
    //           chr.Move(chr.px, chr.py + 1);

    //           //必要回復ターンを減らす
    //           DecreaseNecessarySelfRecoveryTurn();

    //           //止まるかどうか
    //           if (chr.CheckStopRight(chr.GetMap()))
    //           {
    //               //移動したマス数を記憶
    //               moveRight = i;

    //               //止まる処理(仮)
    //               i = 100;
    //           }
    //           else
    //           {
    //               //移動したマス数を加算
    //               moveRight = i;
    //           }
    //       }
    //   }

    //左方向に移動
    //   public void MoveLeft(bool shift)
    //{
    //       //移動量のリセット
    //       moveLeft = 0;
    //       //差のリセット
    //       diffLeft = 0;
    //       //フラグ変更
    //       isWalk = true;

    //       //移動量を求める
    //       if (shift)
    //       {
    //           //移動できるだけ移動
    //           moveLeft = chr.py - checkLeft;
    //       }
    //       else
    //       {
    //           //1マス移動
    //           moveLeft = chr.py - 1;
    //       }

    //       //プレイヤーの現在の位置と移動後の位置の差を求める
    //       diffLeft = chr.py - moveLeft;

    //       //差の分移動する
    //       for (int i = 1; i <= diffLeft; i++)
    //       {
    //           //移動
    //           chr.Move(chr.px, chr.py - 1);

    //           //必要回復ターンを減らす
    //           DecreaseNecessarySelfRecoveryTurn();

    //           //止まるかどうか
    //           if (chr.CheckStopLeft(chr.GetMap()))
    //           {
    //               //移動したマス数を記憶
    //               moveLeft = i;

    //               //止まる処理(仮)
    //               i = 100;
    //           }
    //           else
    //           {
    //               //移動したマス数を加算
    //               moveLeft = i;
    //           }
    //       }
    //   }

    //上方向の移動量を取得
    public int GetMoveUp() { return moveUp; }
    //下方向の移動量を取得
    public int GetMoveDown() { return moveDown; }
    //右方向の移動量を取得
    public int GetMoveRight() { return moveRight; }
    //左方向の移動量を取得
    public int GetMoveLeft() { return moveLeft; }
}
