using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float speed = 1;
    Vector2 flg = new Vector2(0, 0);

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))flg.y = -1;
        if (Input.GetKeyDown(KeyCode.DownArrow)) flg.y = 1;
        if (Input.GetKeyDown(KeyCode.RightArrow)) flg.x = -1;
        if (Input.GetKeyDown(KeyCode.LeftArrow)) flg.x = 1;

        transform.position += new Vector3(Time.deltaTime * speed * flg.x, Time.deltaTime * speed * flg.y);
    }
}
