using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemBase : MonoBehaviour
{
    //アイテム名
    enum アイテム名
    {
        緑色のポーション,
        赤色のポーション,
        青色のポーション,
        黄色のポーション,
        白色のポーション,

    };

    //アイテム名を格納する配列
    private static アイテム名[] ItemName = new アイテム名[5] { アイテム名.緑色のポーション, アイテム名.赤色のポーション, アイテム名.青色のポーション, アイテム名.黄色のポーション, アイテム名.白色のポーション };
    //アイテムの取得数
    private static int[] ItemStock = new int[5] { 0, 0, 0, 0, 0 };
    //アイテムを取得したか
    public static bool Itemflg = true;

    //各種ステータスを取得
    //アイテム名を取得
    public static string GetItemName(int Val) { return ItemName[Val].ToString(); }
    //アイテム所持数取得
    public static int GetItem(int Val) { return ItemStock[Val]; }
    //アイテム取得
    public static void ObtainItem(int Val) { ItemStock[Val] += 1; }
    //アイテム使用
    public static void UseItem(int Val) { ItemStock[Val] -= 1; }

    //初期化
    private void Start()
    {

    }

    //更新
    private void Update()
    {

    }
}
