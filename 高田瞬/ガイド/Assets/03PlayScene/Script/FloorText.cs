using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorText : MonoBehaviour
{
    //コンポーネント取得用
    private Text text = null;

    //フロア管理用
    private int floor = 0;

    //表示テキスト管理用
    private string str = "";

    //初期化
    public void Start()
    {
        //コンポーネント取得
        text = this.GetComponent<Text>();

        //初期化
        floor = 1;
        str = "/5";
    }

    //更新
    public void Update()
    {

		//4を押したら
		if (Input.GetKeyDown("4"))
		{
			//フロア上限(5)まで
			if (floor < 5)
			{
				//フロア更新
				FloorUp();
			}
		}

		//テキスト表示
		text.text = floor.ToString() + str;
    }

    //フロア更新
    public void FloorUp()
	{
        //フロア更新
        floor++;
    }
}
