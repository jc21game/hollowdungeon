using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CharacterBase : MonoBehaviour
{
    //マネージャー(プレイヤー)
    public PlaySceneManager playScene = null;

    //ステータス(初期値)
    public string characterName = "";
    public int initLv = 0;
    public int initHp = 0;
    public int initAtc = 0;
    public int initDef = 0;
    public int initExp = 0;
    public int initAtcType = 0;

    //ステータス現在
    protected static int lv = 0;
    protected static int hp = 0;
    protected static int atc = 0;
    protected static int def = 0;
    protected static int exp = 0;

    //最大HP
    protected static int maxHp = 0;

    //攻撃タイプ
    protected static int atcType = 0;

    //状態異常

    //死亡しているかフラグ(仮)
    private static bool isDeth = false;

    //自身の配列番号
    private static int number = 0;

    //各種ステータスを取得
    public string GetCharacterName() { return characterName; }

    public static int GetLv() { return lv; }
    public static int GetHp() { return hp; }
    public static int GetAtc() { return atc; }
    public static int GetDef() { return def; }
    public static int GetExp() { return exp; }
    public static int GetMaxHp() { return maxHp; }
    public static int GetAtcType() { return atcType; }
    public bool GetIsDeth() { return isDeth; }
    public int GetNumber() { return number; }

    //各種ステータスを設定
    public void SetCharacterName(string name)
    {
        characterName = name;
    }

    public static void SetLv(int value)
    {
        lv = value;
    }

    public static void SetHp(int value)
    {
        hp = value;
    }
    public static void SetAtc(int value)
    {
        atc = value;
    }
    public static void SetDef(int value)
    {
        def = value;
    }
    public static void SetExp(int value)
    {
        exp = value;
    }
    public static void SetMaxHp(int value)
    {
        maxHp = value;
    }
    public static void SetAtcType(int type)
    {
        atcType = type;
    }

    public static void SetIsDeth(bool flg)
    {
        isDeth = flg;
    }

    public void SetNumber(int num)
    {
        number = num;
    }

    //初期化
    public virtual void Start()
    {
        //初期ステータス割り当て
        lv = initLv;
        hp = maxHp = initHp;
        atc = initAtc;
        def = initDef;
        exp = initExp;
        atcType = initAtcType;
        isDeth = false;
        playScene = null;
        number = 0;
    }

    //更新
    public virtual void Update()
    {
    }

    //行動
    public virtual bool Execute()
    {
        return true;
    }

    //移動
    public virtual void Move()
    {

    }

    //攻撃
    public void Attack()
    {

    }

    //ダメージを受ける
    public void OnDamage(int damage)
    {
        //ダメージ分HP減少
        hp -= damage;

        //HPが下回った場合
        if (hp <= 0)
        {
            //HPを0に設定
            hp = 0;

            //死亡
            Deth();

            //配列から削除
            playScene.Remove(number);
        }
    }

    //回復
    public void OnRecovery(int recovery)
    {
        //回復分HPを増やす
        hp += recovery;

        //HPが最大体力を超えた場合
        if (hp >= maxHp)
        {
            //HPを最大体力にする
            hp = maxHp;
        }
    }

    //死亡
    public virtual void Deth()
    {
    }
}
