using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPText : MonoBehaviour
{
    //コンポーネント取得用
    private Text text = null;

    private HPBar hpBar = null;

    //最大HP管理用
    private int maxHp = 0;

    //HP管理用
    private int hp = 0;

    //表示テキスト管理用
    private string str = "";

    //初期化
    public void Start()
    {
        //コンポーネント取得
        text = this.GetComponent<Text>();

        hpBar = GameObject.Find("HPBar").GetComponent<HPBar>();

        //初期化
        maxHp = hpBar.GetMaxHp();
        hp = hpBar.GetHp();
        str = hp + "/" + maxHp;
    }

    //更新
    public void Update()
    {
        //値の更新
        maxHp = hpBar.GetMaxHp();
        hp = hpBar.GetHp();

        //テキスト更新
        str = hp + "/" + maxHp;

        //テキスト表示
        text.text = str;
    }
}
