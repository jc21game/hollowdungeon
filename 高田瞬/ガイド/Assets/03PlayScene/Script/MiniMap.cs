using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;

public class MiniMap : MonoBehaviour
{
    //マップの各地形用画像のプレハブを格納
    public GameObject WallPrefab = null;
    public GameObject RoomPrefab = null;
    public GameObject AislePrefab = null;
    public GameObject PlayerPrefab = null;
    public GameObject EnemyPrefab = null;
    public GameObject ItemPrefab = null;
   
    //各地形のオブジェクト
    private GameObject PlayerP = null;
    private GameObject EnemyP = null;
    private GameObject BossP = null;
    private GameObject ItemP = null;

    //記録配列
    private int[,] memory = new int[64, 64];

    //表示大きさ
    public float scale_ = 0;
    //表示位置の微調整
    public int w = 0;
    public int h = 0;

    //各オブジェクトの座標
    private int px = 0;
    private int py = 0;
    private int ex = 0;
    private int ey = 0;
    private int bx = 0;
    private int by = 0;
    private int ix = 0;
    private int iy = 0;

    //アンカー
    private Vector3 anchor;

    //オープンするミニマップの探索
    private int[,] Search = new int[,] {
    {-1,0 },
    { -1,1},
    {0,1 },
    { 1,1},
    { 1,0},
    { 1,-1},
    { 0,-1},
    { -1,-1} };

    //オブジェクトを格納
    private GameObject[,] minmap = new GameObject[64, 64];
    
    //プレイヤーの点滅
    public int Mtime = 60;
    private int time = 60;
    private bool dir = true;

   

    //初期化
    private void Start()
    {
        //フレームレート設定
        Application.targetFrameRate = 60;
  
        //各オブジェクトの位置
        px = Player.px;
        py = Player.py;
        ex = Enemy.ex;
        ey = Enemy.ey;
        bx = Boss.bx;
        by = Boss.by;
        ix = Item.ix;
        iy = Item.iy;
        
  

        //アンカーの設定
        anchor = new Vector3(3 * py - 420, -3 * px + 180, 0);


        //ミニマップの生成
        for (int i = 0; i < Map.CsvMap.GetLength(0); i++)
        {
            for (int j = 0; j < Map.CsvMap.GetLength(1); j++)
            {
                GameObject obj = null;
               
                //壁
                if (Map.CsvMap[i, j] == 0)
                {
                    obj = Instantiate(WallPrefab) as GameObject;
                }

                //通路
                if (Map.CsvMap[i, j] == 1)
                {
                    obj = Instantiate(AislePrefab) as GameObject;
                }

                //部屋
                if (Map.CsvMap[i, j] >= 2)
                {
                    obj = Instantiate(RoomPrefab) as GameObject;
                }
 
                //親子関係の設定
                obj.transform.SetParent(GameObject.Find("Canvas").transform, false);
                //表示位置
                obj.transform.position = new Vector3((j * scale_) - w, -i * scale_ + h, 0);
                //プレイヤーが居るところを表示、それ以外は非表示
                obj.SetActive(false);
                if (i == px && j == py) obj.SetActive(true);
                minmap[i, j] = obj;

                memory[i, j] = 0;
                

            }
        }
        memory[px, py] = 3;
        //各オブジェクトを生成、プレイヤー以外初期状態は非表示
        PlayerP = Instantiate(PlayerPrefab) as GameObject;
        PlayerP.transform.SetParent(GameObject.Find("Canvas").transform, false);
        RectTransform recttrans = PlayerP.GetComponent<RectTransform>();
        PlayerP.transform.position = new Vector3((py * scale_) - w, -px * scale_ + h, 0);
        EnemyP = Instantiate(EnemyPrefab) as GameObject;
        EnemyP.transform.SetParent(GameObject.Find("Canvas").transform, false);
        EnemyP.transform.position = new Vector3((ey * scale_) - w, -ex * scale_ + h, 0);
        EnemyP.SetActive(false);
        BossP = Instantiate(EnemyPrefab) as GameObject;
        BossP.transform.SetParent(GameObject.Find("Canvas").transform, false);
        BossP.transform.position = new Vector3((by * scale_) - w, -bx * scale_ + h, 0);
        BossP.SetActive(false);
        ItemP = Instantiate(ItemPrefab) as GameObject;
        ItemP.transform.SetParent(GameObject.Find("Canvas").transform, false);
        ItemP.transform.position = new Vector3((iy * scale_) - w, -ix * scale_ + h, 0);
        ItemP.SetActive(false);


        //プレイヤーがいるフロアと通路がつながっていれば通路1マスを表示
        //アイテムや敵、階段などがあればそれも表示
        int f = Map.CsvMap[px, py];
            if (f != 1)
            {
                for (int i = 0; i < 64; i++)
                {
                    for (int j = 0; j < 64; j++)
                    {
                        if (Map.CsvMap[i, j] == f)
                        {
                            minmap[i, j].SetActive(true);
                            for (int k = 0; k < 8; k++)
                            {
                                if (Map.CsvMap[i + Search[k, 0], j + Search[k, 1]] == 1)
                                {
                                    minmap[i + Search[k, 0], j + Search[k, 1]].SetActive(true);
                                }
                            }

                        if (i == bx && j == by)
                        {
                            BossP.SetActive(true);
                        }
                        if (i == ix && j == iy)
                        {
                            ItemP.SetActive(true);
                        }
                        if (i == ex && j == ey)
                        {
                            EnemyP.SetActive(true);
                        }

                    }
                    }
                }
            }
       
    }

      

    //更新
    private void Update()
    {
        //Debug.Log(Player.px);

        //移動処理
        Move(Player.px, Player.py);
        
        //アイテムを消すかどうか
        if(!ItemBase.Itemflg)
        {
            ItemP.SetActive(false);
        }

        //プレイヤーの点滅
        Color pcol = PlayerP.GetComponent<SpriteRenderer>().color;

        if (dir)
        {
            if (time-- <= 0)
            {
                time = 0;
                dir = false;
                pcol.a = 0;
            }
        }
        else
        {
            if (time++ >= Mtime)
            {
                time = Mtime;
                dir = true;
                pcol.a = 1;
            }
        }
        PlayerP.GetComponent<SpriteRenderer>().color = pcol;    

    }

   
    //移動
    private void Move(int x, int y)
    {
            //移動可能だったら移動
            if (Map.CsvMap[x, y] != 0)
            {
            //アンカーの移動
                anchor = new Vector3(anchor.x + 3 * (y - py), anchor.y - 3 * (x - px), 0);
                memory[px, py] = 0;
                px = x;
                py = y;
                memory[px, py] = 3;

                minmap[x, y].SetActive(true);

            //プレイヤーの周囲を表示
            //アイテムや敵、階段などがあればそれも表示
            for (int i = -2; i < 3; i++)
            {
                for (int j = -2; j < 3; j++)
                {
                    if (px + i >= 0 && px + i <= 63 && py + j >= 0 && py + j <= 63)
                    {
                        minmap[px + i, py + j].SetActive(true);
                        if (px + i == bx && py + j == by)
                        {
                            BossP.SetActive(true);
                        }
                        if (Item.ItemP != null)
                        {
                            if (px + i == ix && py + j == iy)
                            {
                                ItemP.SetActive(true);
                            }
                        }
                        if (px + i == ex && py + j == ey)
                        {
                            EnemyP.SetActive(true);
                        }


                    }
                }
            }

            //プレイヤーがフロアに入ったらフロアと通路がつながっていれば通路1マスを表示

            int f = Map.CsvMap[px, py];
                if (f != 1)
                {
                    for (int i = 0; i < 64; i++)
                    {
                        for (int j = 0; j < 64; j++)
                        {
                            if (Map.CsvMap[i, j] == f)
                            {
                                minmap[i, j].SetActive(true);
                                for (int k = 0; k < 8; k++)
                                {
                                    if (Map.CsvMap[i + Search[k, 0], j + Search[k, 1]] == 1)
                                    {
                                        minmap[i + Search[k, 0], j + Search[k, 1]].SetActive(true);
                                    }
                                }
                                if (i == bx && j == by)
                                {
                                    BossP.SetActive(true);
                                }
                                if (Item.ItemP != null)
                                {
                                    if (i == ix && j == iy)
                                    {
                                        ItemP.SetActive(true);
                                    }
                                }
                                else
                                {
                                    Destroy(ItemP);
                                }
                                if (i == ex && j == ey)
                                {
                                    EnemyP.SetActive(true);
                                }

                            }
                        }
                    }
                }
                else
                {
                    minmap[px, py].SetActive(true);
                }

                PlayerP.GetComponent<RectTransform>().anchoredPosition = anchor;
            }
        }
        
        
}

   
