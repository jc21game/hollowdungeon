using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HungerBar : MonoBehaviour
{
    //コンポーネント取得用
    private Slider hungerBar = null;

    //空腹度
    private int hunger = 0;

    //初期化
    public void Start()
    {
        //コンポーネント取得
        hungerBar = this.GetComponent<Slider>();

        //初期化
        hunger = 100;
        hungerBar.maxValue = hunger;
    }

    //更新
    public void Update()
    {
        if(Input.GetKeyDown("5"))
        {
            AddHunger(20);
		}

        if(Input.GetKeyDown("6"))
        {
            SubtractHunger(30);
		}

        hungerBar.value = hunger;
    }

    //空腹度加算
    public void AddHunger(int value)
    {
        hunger += value;

        //上限を超えたら
        if(hunger >= 100)
        {
            hunger = 100;
		}
	}

    //空腹度減算
    public void SubtractHunger(int value)
    {
        hunger -= value;

        //下限を超えたら
        if(hunger <= 0)
        {
            hunger = 0;
		}
	}
}
