using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EXPText : MonoBehaviour
{
    //コンポーネント取得用
    private Text text = null;

    private EXPBar expBar = null;

    //経験値管理用
    private int exp = 0;

    //次のレベルまでの経験値管理用
    private int nextExp = 0;

    //表示テキスト管理用
    private string str = "";

    //初期化
    public void Start()
    {
        //コンポーネント取得用
        text = this.GetComponent<Text>();
        expBar = GameObject.Find("EXPBar").GetComponent<EXPBar>();

        //初期化
        exp = expBar.GetExp();
        nextExp = expBar.GetNextExp();
        str = exp + "/" + nextExp;
    }

    //更新
    public void Update()
    {
        //値を更新
        exp = expBar.GetExp();
        nextExp = expBar.GetNextExp();

        //テキスト更新
        str = exp + "/" + nextExp;

        //テキスト表示
        text.text = str;
    }
}
