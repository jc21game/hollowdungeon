using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//Csv読み取りクラス
//Csv（カンマ区切りのデータ）を読み込み、格納しておくクラス
//Csvのカンマのセル単位にデータへアクセスし、必要データを取得する

public class CSVReader : MonoBehaviour
{
    //読み込みデータ
    //リストのリスト
    //行単位　＝　リスト
    //1行（リスト）内の列単位　＝　リスト
    private static List<List<string>> datas_;

    //データ横幅（1行目の横幅）
    private static int width_;
    //データ縦幅
    private static int height_;

    private void Awake()
    {
        width_ = 0;
        height_ = 0;
    }

    //初期化
    void Start()
    {

    }

    //更新
    void Update()
    {

    }

    //（ビルド時に実行されるかは未確認）
    //Csvファイルのロード
    //Csvファイルを読み込み、データを格納する
    //引数：Csvファイルパス（Resource/　以下）
    public static void LoadCsv(string FILE_NAME)
    {
        //格納先の動的確保
        //行としてリストを確保できるように側の作成
        //リストのリストの動的確保
        //行内の列単位の要素を確保するためのリストを下記リストに追加していく
        datas_ = new List<List<string>>();


        //Csvファイルのロード
        //Resources以下のファイルをロード
        TextAsset csvFile = Resources.Load<TextAsset>(FILE_NAME);

        //文字列読み込みクラスの作成
        StringReader stringReader = new StringReader(csvFile.text);

        // , 区切りに一行ずつ読み込み
        //リストに要素を追加していく
        while (stringReader.Peek() != -1)
        {
            //一行ずつ読み込み
            string line = stringReader.ReadLine();

            //カンマ区切りに分けられた文字列配列の取得
            string[] split = line.Split(',');

            //格納リストの動的確保
            List<string> list = new List<string>();

            //文字列配列をひと要素ずつ、リストに格納する
            for (int i = 0; i < split.Length; i++)
            {
                list.Add(split[i]);
            }

            //リストをリストのリストへ追加
            datas_.Add(list);
        }

        //CsvファイルのWidthとHeightの格納
        if (datas_.Count > 0)
        {
            //1行目の列数を代表して、Csvファイルの列数とする
            //今回においては、Csvファイルの列数は共通であるとする
            width_ = datas_[0].Count;
            height_ = datas_.Count;
        }


    }

    //データの取得（String）
    public static string GetData(int x, int y)
    {
        //ｙ　＝　行番号
        //ｘ　＝　列番号
        return datas_[y][x];
    }

    //データの取得（int）
    public static int GetDataInt(int x, int y)
    {
        string data = GetData(x, y);

        //文字をint型へ変換し返す
        return int.Parse(data);
    }

    //CsvファイルのWidthの取得
    public static int GetCsvWidth()
    {
        return width_;
    }

    //CsvファイルのHeightの取得
    public static int GetCsvHeight()
    {
        return height_;
    }
}
