using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    //アイテムのプレハブ格納
    public GameObject ItemObject = null;
    //アイテムオブジェクト
    public static GameObject ItemP = null;

    //位置
    public static int ix = 0;
    public static int iy = 0;

    //初期化
    private　void Start()
    {
        while (true)
        {
            ix = Random.Range(0, 64);
            iy = Random.Range(0, 64);


            if (Map.CsvMap[ix, iy] >= 2)
            {
                break;
            }
            continue;

        }

        ItemP = Instantiate(ItemObject) as GameObject;
        ItemP.transform.position = new Vector3(iy, -ix, 0);

    }

    //更新
    private void Update()
    {
        //アイテムがあるか
        if(ItemBase.Itemflg)
        {
            //あった場合
            //flgによる表示・非表示
            ItemP.SetActive(PlaySceneManager.flg);
        }
        else
        {
            //ない場合非表示
            ItemP.SetActive(false);
        }


    }
}
