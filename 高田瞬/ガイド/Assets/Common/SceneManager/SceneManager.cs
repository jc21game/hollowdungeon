using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* シーン遷移用namespace　使用方法
 * 
 * シーン開始時(Start)必ず SetNowScene で
 * 現在のシーン番号をセットすること
 * 
 * シーン遷移するときは SetNextScene に
 * 次のシーン番号をセットすること
 */

//シーン遷移用namespace
namespace SceneManager
{
    //各シーン番号
    enum SCENES
    {
        NONE,                   //待機
        TITLE_SCENE,            //タイトルシーン
        SELECT_WEAPON_SCENE,    //武器選択シーン
        PLAY_SCENE,             //プレイシーン
        ENDING_SCENE,           //エンディングシーン
        OVER_SCENE              //ゲームオーバーシーン
    }

    //シーン遷移処理
    class Scene
    {
        //現在のシーン番号
        //デフォルトでタイトルに設定
        private static SCENES NowScene = SCENES.TITLE_SCENE;

        //次のシーン番号
        private static SCENES NextScene = SCENES.NONE;

        //現在のシーン番号のセッター
        //シーン開始時に必ずセットすること
        public void SetNowScene(SCENES scenes)
        {
            NowScene = scenes;
        }

        //現在のシーン番号のゲッター
        public SCENES GetNowScene()
        {
            return NowScene;
        }

        //次のシーン番号のセッター
        public void SetNextScene(SCENES scenes)
        {
            //次のシーン番号の登録
            NextScene = scenes;

            //次のシーン番号を設定している
            if (NextScene != SCENES.NONE)
            {
                //現在のシーン番号と次のシーン番号が違ったら
                if (NowScene != NextScene)
                {
                    //現在のシーン番号を更新
                    NowScene = NextScene;

                    //シーン遷移を行う
                    ChangeScene();
                }
            }
        }

        //次のシーン番号のゲッター
        public SCENES GetNextScene()
        {
            return NextScene;
        }

        //シーン切り替え関数
        private void ChangeScene()
        {
            //次のシーン番号
            switch (NextScene)
            {
                //タイトルシーンへ
                case SCENES.NONE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
                    break;
                //タイトルシーンへ
                case SCENES.TITLE_SCENE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
                    break;
                //武器選択シーンへ
                case SCENES.SELECT_WEAPON_SCENE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("WeaponSelectScene");
                    break;
                //プレイシーンへ
                case SCENES.PLAY_SCENE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("PlayScene");
                    break;
                //エンディングシーンへ
                case SCENES.ENDING_SCENE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("EndingScene");
                    break;
                //ゲームオーバーシーンへ
                case SCENES.OVER_SCENE:
                    UnityEngine.SceneManagement.SceneManager.LoadScene("GameOverScene");
                    break;
            }
        }
    }
}
