using UnityEngine;

public class TrapBase : MonoBehaviour
{
	protected PlaySceneManager manager = null;


	public virtual void Start()
	{
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
	}
	public virtual void Update()
	{

	}
	protected virtual void Execute()
	{

	}


}