using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class Trent : CharacterBase
{
	//初期化
	public override void Start()
	{
		base.Start();
	}

	//更新
	public override void Update()
	{
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//攻撃
		if (Attack())
		{
			//攻撃中
			if (isAttack)
			{
				//行動中
				return false;
			}

			//行動終了
			return true;
		}

		//処理なし
		return true;
	}

	//攻撃
	protected override bool Attack()
	{
		//攻撃中
		if (isAttack)
		{
			return base.Attack();
		}

		//四方
		for (int i = 0; i < DIR.DIRCTIONS; i++)
		{
			//プレイヤーがいる場合
			if (manager.GetPosition(0) == GetFront(dir.ConvertDir(i)))
			{
				//攻撃
				dir.dir = dir.ConvertDir(i);
				return base.Attack();
			}
		}

		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//処理なし
		return base.Move();
	}

	//死亡
	public override void Death()
	{
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GameFinStart();
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().gameClear = true;
		base.Death();
	}

	//被弾
	public override void Hit(int damage)
	{
		base.Hit(damage);
	}
}
