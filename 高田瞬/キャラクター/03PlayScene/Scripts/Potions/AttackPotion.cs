using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//攻撃ポーション
public class AttackPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.ATTACK);
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//攻撃
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).ATC += efficacy;
	}
}
