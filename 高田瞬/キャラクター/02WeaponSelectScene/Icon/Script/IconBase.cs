using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBase : MonoBehaviour
{
    //おまえは誰？
    public enum Icon
    {
        NONE,       //なし
        SWORD,      //剣
        AXE,        //斧
        BOW,        //弓
        DUNGEON_1,  //ダンジョン1
        DUNGEON_2   //ダンジョン2
    }

    //自身の番号
    protected Icon IconNumber = Icon.NONE;

    //拡縮状況
    public enum SCALE_STATE
    {
        NONE,           //なし
        SCALE_BIG,      //大きく
        SCALE_SMALE,    //小さく
        SCALE_USU       //均す
    }
    protected SCALE_STATE MyState = SCALE_STATE.NONE;

    //自身の移動状況
    public enum MOVE_STATE
    {
        NONE,               //なし
        MOVE_RIGHT,         //右に移動中
        MOVE_RIGHT_CENTER,  //右から中央に移動中
        MOVE_CENTER,        //中央へ
        MOVE_LEFT_CENTER,   //左から中央に移動中
        MOVE_LEFT,          //左に移動中
        STAY                //待機中
    }
    protected MOVE_STATE MoveState = MOVE_STATE.NONE;

    //移動方向&速度
    public enum MOVE_DIR_SPEED
    {
        NONE,       //なし
        W_RIGHT,    //歩き＋右
        W_LEFT,     //歩き＋左
        R_RIGHT,    //走り＋右
        R_LEFT      //走り＋左
    }
    protected MOVE_DIR_SPEED MoveSpeed = MOVE_DIR_SPEED.NONE;

    //描画優先度
    protected int MyPriority = 0;
    protected static readonly int Front = 30;
    protected static readonly int Back = 10;

    //移動速度
    protected static readonly float Walk = Screen.width / 240;
    protected static readonly float Run = Screen.width / 120;

    //アイコンのY座標
    protected static readonly float IconYPosition = Screen.height / 10;

    //センターのX座標
    protected static readonly float CenterXPosition = .0f;

    //ライトのX座標
    protected static readonly float RightXPosition = Screen.width / 4;

    //レフトのX座標
    protected static readonly float LeftXPosition = -Screen.width / 4;

    //アイコンの標準サイズ
    protected static readonly float IconWidthHeight = Screen.height / 3;

    //アイコンの上限サイズ
    protected static readonly float IconMaxSize = IconWidthHeight * 1.75f;

    //アイコンの下限サイズ
    protected static readonly float IconMinSize = IconWidthHeight * 1.25f;

    //拡縮率
    protected static readonly float ScaleRate = IconMinSize / 100;

    //描画優先度
    protected static readonly float IconMain = 20.0f;
    protected static readonly float IconSub = 10.0f;

    //拡縮
    protected virtual void Scale(Image image, SCALE_STATE state)
    {
        //拡縮状況が「均す」だったら
        if (state == SCALE_STATE.SCALE_USU)
        {
            //標準サイズに変更
            image.rectTransform.sizeDelta = new Vector2(IconWidthHeight, IconWidthHeight);
        }
        //拡縮状況が「大きく」だったら
        else if (state == SCALE_STATE.SCALE_BIG)
        {
            image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x + ScaleRate, image.rectTransform.sizeDelta.y + ScaleRate);
        }
        //拡縮状況が「小さく」だったら
        else if (state == SCALE_STATE.SCALE_SMALE)
        {
            image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x - ScaleRate, image.rectTransform.sizeDelta.y - ScaleRate);
        }
        //その他
        else
        {
        }
    }

    //移動
    protected virtual void IconMove(Image image, MOVE_DIR_SPEED speed)
	{
		//速度に合わせて移動
		switch (speed)
		{
            case MOVE_DIR_SPEED.W_RIGHT:
                //右へ歩いて移動
                image.rectTransform.anchoredPosition = new Vector2(image.rectTransform.anchoredPosition.x + Walk, image.rectTransform.anchoredPosition.y);
                break;
            case MOVE_DIR_SPEED.W_LEFT:
                //左へ歩いて移動
                image.rectTransform.anchoredPosition = new Vector2(image.rectTransform.anchoredPosition.x - Walk, image.rectTransform.anchoredPosition.y);
                break;
            case MOVE_DIR_SPEED.R_RIGHT:
                //右へ走って移動
                image.rectTransform.anchoredPosition = new Vector2(image.rectTransform.anchoredPosition.x + Run, image.rectTransform.anchoredPosition.y);
                break;
            case MOVE_DIR_SPEED.R_LEFT:
                //左へ走って移動
                image.rectTransform.anchoredPosition = new Vector2(image.rectTransform.anchoredPosition.x - Run, image.rectTransform.anchoredPosition.y);
                break;
		}

    }

    //アイコン非表示
    public static void WeaponIconHide()
    {
        IconSword.SwordHide();
        IconAxe.AxeHide();
        IconBow.BowHide();
    }

    public static void DungeonIconHide()
    {
        IconDungeon1.Dungeon1Hide();
        IconDungeon2.Dungeon2Hide();
    }

    //アイコン表示
    public static void WeaponIconDisplay()
    {
        IconSword.SwordDisplay();
        IconAxe.AxeDisplay();
        IconBow.BowDisplay();
    }

    public static void DungeonIconDisplay()
    {
        IconDungeon1.Dungeon1Display();
        IconDungeon2.Dungeon2Display();
    }

    public static bool GetMoveState()
	{
        //すべてのアイコンがSTAY
        if(IconSword.GetIconMoveState() && IconAxe.GetIconMoveState() && IconBow.GetIconMoveState() && IconDungeon1.GetIconMoveState() && IconDungeon2.GetIconMoveState())
		{
            return true;
		}
		else
		{
            return false;
		}
    }
}
