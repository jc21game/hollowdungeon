using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class SoundManager : MonoBehaviour
{
	//再生用(BGM)
	public AudioSource bgmSource = null;

	//再生用(SE)
	public AudioSource seSource = null;

	//BGM用
	//private AudioClip bgm = null;

	//SE用
	//private AudioClip se = null;

	//音量(BGM)
	public static float bgmVol = 0.5f;

	//音量(SE)
	public static float seVol = 0.5f;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
	}

	//再生したいBGMを指定
	//引数：Assetsからの絶対パス
	public void PlayBGM(string path)
	{
		////ファイルを設定
		//bgm = AssetDatabase.LoadAssetAtPath<AudioClip>(path);

		////再生する音を指定
		//bgmSource.clip = bgm;

		////ループ設定
		//bgmSource.loop = true;

		////再生
		//bgmSource.Play();
	}

	//再生されているBGMを停止
	public void StopBGM()
	{
		////停止
		//bgmSource.Stop();
	}

	//再生したいSEを指定
	//引数：Assetsからの絶対パス
	public void PlaySE(string path)
	{
		////ファイルを設定
		//se = AssetDatabase.LoadAssetAtPath<AudioClip>(path);

		////再生
		//seSource.PlayOneShot(se);
	}

	//音量変更(BGM)
	public void ChangeVolume()
	{
		//bgmSource.volume = bgmVol;
		//seSource.volume = seVol;
	}
}