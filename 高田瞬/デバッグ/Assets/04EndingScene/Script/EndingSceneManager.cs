using LevelOfAchievement;
using ScoreManager;
using UnityEngine;

//エンディングシーン
public class EndingSceneManager : MonoBehaviour
{
	//Zキーを押した回数
	private int cnt = 0;

	//Canvas(戦績)
	public Canvas record = null;

	//初期化
	void Start()
	{
		//生成
		Instantiate(record);

		//フェードイン
		FadeScript.FadeIn();

		//サウンドマネージャー
		SoundManager.SoundManagerGeneration();

		//BGM再生
		SoundManager.PlayBGM("GameClear");

		//テキストボックス生成
		MessageWindowManager.ImageGeneration();

		//ボックスの透明度変更
		MessageWindowManager.AlphaChenge(1.0f);

		//最初に表示されるメッセージ
		MessageWindowManager.MessageTextChange(
		"\n" +
		"		階層				：" + Score.GetFloor().ToString() + "					" +
		"プレイ時間			：" + Score.GetTime().ToString() + "\n" +
		"		与えたダメージ	：" + Score.GetGivingDamage() + "				" +
		"受けたダメージ		：" + Score.GetCoveredDamage() + "\n" +
		"		倒した敵			：" + Score.GetKillNum() + "					" +
		"受けた状態以上の回数	：" + Score.GetCoveredTrap()
		);

		//使用武器の保存
		Score.WriteWeaponFlgs();

		//アチーブ確認
		AchievementList.AchievementCheck();

		//生成
		AchievementPeaple.AppealGeneration();
	}

	//更新
	void Update()
	{
		//Zキーを押したら
		if (Input.GetKeyUp(KeyCode.Z))
		{
			//何回目か
			if (++cnt == 1)
			{
				//1つ目のメッセージ削除
				MessageWindowManager.MessageImageHide();

				MessageWindowManager.MessageTextChange(
									"\n" +
									"		Lv				：" + Score.GetLv() + "                  " +
									"				HP				：" + Score.GetHp() + "\n" +
									"		攻撃力			：" + Score.GetAttack() + "                  " +
									"				防御力		：" + Score.GetDefence() + "\n" +
									"		獲得スキルポイント	：" + Score.GetSkillPt() + "                 " +
									"				獲得経験値	：" + Score.GetHaveAllExp()
									);
			}
			if (cnt == 2)
			{
				//2つ目のメッセージ削除
				MessageWindowManager.MessageImageHide();

				//メッセージ切り替え
				MessageWindowManager.MessageTextChange(
				"\n" +
				"		緑色ポーションの使用回数：" + Score.GetUseGreenPotion() + "			" +
				"赤色ポーションの使用回数：" + Score.GetUseRedPotion() + "\n" +
				"\n" +
				"		青色ポーションの使用回数：" + Score.GetUseBluePotion() + "			" +
				"白色ポーションの使用回数：" + Score.GetUseWhitePotion()
				);
				if (!AchievementList.GetDungeonTraversal_Flg())
				{
					cnt++;
					if (!AchievementList.GetMonsterHunter_Flg())
					{
						cnt++;
						if (!AchievementList.GetDr_Potion_Flg())
						{
							cnt++;
							if (!AchievementList.GetClumsyPerson_Flg())
							{
								cnt++;
								if (!AchievementList.GetDiamond_Flg())
								{
									cnt++;
									if (!AchievementList.GetGameOver_Flg())
									{
										cnt++;
										if (!AchievementList.GetTalented_Flg())
										{
											cnt++;
											if (!AchievementList.GetGreatAdventurer_Flg())
											{
												cnt++;
											}
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 3)
			{
				if (AchievementList.GetDungeonTraversal_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("TreasureChest");
					if (!AchievementList.GetMonsterHunter_Flg())
					{
						cnt++;
						if (!AchievementList.GetDr_Potion_Flg())
						{
							cnt++;
							if (!AchievementList.GetClumsyPerson_Flg())
							{
								cnt++;
								if (!AchievementList.GetDiamond_Flg())
								{
									cnt++;
									if (!AchievementList.GetGameOver_Flg())
									{
										cnt++;
										if (!AchievementList.GetTalented_Flg())
										{
											cnt++;
											if (!AchievementList.GetGreatAdventurer_Flg())
											{
												cnt++;
											}
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 4)
			{
				if (AchievementList.GetMonsterHunter_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Trophy");
					if (!AchievementList.GetDr_Potion_Flg())
					{
						cnt++;
						if (!AchievementList.GetClumsyPerson_Flg())
						{
							cnt++;
							if (!AchievementList.GetDiamond_Flg())
							{
								cnt++;
								if (!AchievementList.GetGameOver_Flg())
								{
									cnt++;
									if (!AchievementList.GetTalented_Flg())
									{
										cnt++;
										if (!AchievementList.GetGreatAdventurer_Flg())
										{
											cnt++;
										}
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 5)
			{
				if (AchievementList.GetDr_Potion_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Apple");
					if (!AchievementList.GetClumsyPerson_Flg())
					{
						cnt++;
						if (!AchievementList.GetDiamond_Flg())
						{
							cnt++;
							if (!AchievementList.GetGameOver_Flg())
							{
								cnt++;
								if (!AchievementList.GetTalented_Flg())
								{
									cnt++;
									if (!AchievementList.GetGreatAdventurer_Flg())
									{
										cnt++;
									}
								}
							}
						}
					}
				}
			}

			else if (cnt == 6)
			{
				if (AchievementList.GetClumsyPerson_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Rhombus");
					if (!AchievementList.GetDiamond_Flg())
					{
						cnt++;
						if (!AchievementList.GetGameOver_Flg())
						{
							cnt++;
							if (!AchievementList.GetTalented_Flg())
							{
								cnt++;
								if (!AchievementList.GetGreatAdventurer_Flg())
								{
									cnt++;
								}
							}
						}
					}
				}
			}

			else if (cnt == 7)
			{
				if (AchievementList.GetDiamond_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Heart");
					if (!AchievementList.GetGameOver_Flg())
					{
						cnt++;
						if (!AchievementList.GetTalented_Flg())
						{
							cnt++;
							if (!AchievementList.GetGreatAdventurer_Flg())
							{
								cnt++;
							}
						}
					}
				}
			}

			else if (cnt == 8)
			{
				if (AchievementList.GetGameOver_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Weak");
					if (!AchievementList.GetTalented_Flg())
					{
						cnt++;
						if (!AchievementList.GetGreatAdventurer_Flg())
						{
							cnt++;
						}
					}
				}
			}

			else if (cnt == 9)
			{
				if (AchievementList.GetTalented_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Medal");
					if (!AchievementList.GetGreatAdventurer_Flg())
					{
						cnt++;
					}
				}
			}

			else if (cnt == 10)
			{
				if (AchievementList.GetGreatAdventurer_Flg())
				{
					SoundManager.PlaySE("Archive");
					AchievementPeaple.SetNowDuplicatePath("Crown");
				}

			}
		}

		//終了
		if (cnt > 10)
		{
			//フェードアウト
			FadeScript.FadeOut();

			//完全にフェードアウトが終了した場合
			if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
			{
				//タイトルシーンに移動
				UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
			}
		}
	}
}
