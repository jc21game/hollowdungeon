using UnityEngine;
using UnityEngine.UI;

//紙吹雪
public class FallL : MonoBehaviour
{
	//生存範囲
	public float lifeX, lifeY;

	//速度
	public float speed = 0;
	private float ReturnSpeed = 2.5f;
	private float YSpeed = 0;
	private float YReturnSpeed = 0;

	//自身
	private Image image = null;
	private RectTransform trans = null;

	//フラグ
	private bool flg = false;

	//移動ベクトル
	private Vector2 vector = new Vector2();

	//座標
	private int yshaft = -100;
	private int ZeroToTwentyfour = -600;
	private int TwentyfiveToFifty = -400;

	//割る
	private float divide = 0.75f;

	//高さ制限
	private int limitheight = 400;

	//回転
	private int rotex = 3;
	private int rotez = 5;

	//スタート
	public void Start()
	{
		//取得
		image = gameObject.GetComponent<Image>();
		trans = gameObject.GetComponent<RectTransform>();

		//色
		image.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), 1);

		//サイズ
		image.rectTransform.sizeDelta = new Vector2(Screen.height / divide, Screen.height / divide);

		//座標
		if (Random.Range(0, 50) > 25)
		{
			trans.anchoredPosition = new Vector2(ZeroToTwentyfour, yshaft);
		}
		else
		{
			trans.anchoredPosition = new Vector2(TwentyfiveToFifty, yshaft);
		}

		//フラグ初期化
		flg = false;

		//XY軸速度設定
		speed = Random.Range(1, 2);
		ReturnSpeed = Random.Range(1, 3);
		YSpeed = Random.Range(2, 8);
		YReturnSpeed = Random.Range(1, 6);

		//移動ベクトル
		vector = trans.anchoredPosition;

	}

	//更新
	public void Update()
	{
		//一定の高さに行くまでは上昇
		if (vector.y < limitheight && flg == false)
		{
			vector.x += speed;
			vector.y += YSpeed;
			transform.Rotate(new Vector3(rotex, 0, rotez));
		}
		else
		{
			flg = true;

			//下降
			vector.x += ReturnSpeed;
			vector.y -= YReturnSpeed;
			transform.Rotate(new Vector3(rotex, 0, rotez));
		}

		trans.anchoredPosition = vector;

		//死亡
		if (vector.x >= lifeX || vector.y <= lifeY)
		{
			Destroy(gameObject);
		}
	}
}
