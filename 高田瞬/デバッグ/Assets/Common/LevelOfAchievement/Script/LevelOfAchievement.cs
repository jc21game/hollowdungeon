using ScoreManager;
using System;

namespace LevelOfAchievement
{
	//アチーブメント管理
	public class AchievementList
	{
		//------------------アチーブメント------------------//
		//アチーブ名：ダンジョン踏破
		//条件：ダンジョンクリア
		//宝箱
		public static int DungeonTraversal_Poison = 0;
		public static int DungeonTraversal_FlameIce = 0;
		private static bool DungeonTraversal_Flg = false;

		//アチーブ名：モンスターハンター
		//条件：すべてのモンスター(ボスを含む)を倒す
		//トロフィー
		public static int MonsterHunter_Poison = 0;
		public static int MonsterHunter_FlameIce = 0;
		private static bool MonsterHunter_Flg = false;

		//アチーブ名：ポーション博士
		//条件：すべてのポーションを入手、使用
		//リンゴ
		public static int Dr_Potion_Poison = 0;
		public static int Dr_Potion_FlameIce = 0;
		private static bool Dr_Potion_Flg = false;

		//アチーブ名：ドジっ子
		//条件：すべてのダンジョンギミックを踏む
		//ラミエル
		public static int ClumsyPerson_Poison = 0;
		public static int ClumsyPerson_FlameIce = 0;
		private static bool ClumsyPerson_Flg = false;

		//アチーブ名：金剛
		//条件：モンスターからの攻撃を連続5ターン受ける
		//ハート
		public static int Diamond_Poison = 0;
		public static int Diamond_FlameIce = 0;
		private static bool Diamond_Flg = false;

		//アチーブ名：死んでしまった！！
		//条件：ゲームオーバーになる
		//スライム
		public static int GameOver_Poison = 0;
		public static int GameOver_FlameIce = 0;
		private static bool GameOver_Flg = false;

		//アチーブ名：才能豊か
		//条件：それぞれの武器でダンジョンをクリアする
		//さいころ
		public static int Talented_Poison = 0;
		public static int Talented_FlameIce = 0;
		private static bool Talented_Flg = false;

		//アチーブ名：一人前の冒険者!!
		//条件：すべてのアチーブメントを獲得する
		public static int GreatAdventurer_Poison = 0;
		public static int GreatAdventurer_FlameIce = 0;
		private static bool GreatAdventurer_Flg = false;

		//------------------アチーブのゲッター------------------//
		public static int GetDungeonTraversal_Poison()
		{
			return DungeonTraversal_Poison;
		}
		public static int GetDungeonTraversal_FlameIce()
		{
			return DungeonTraversal_FlameIce;
		}
		public static bool GetDungeonTraversal_Flg()
		{
			return DungeonTraversal_Flg;
		}
		public static int GetMonsterHunter_Poison()
		{
			return MonsterHunter_Poison;
		}
		public static int GetMonsterHunter_FlameIce()
		{
			return MonsterHunter_FlameIce;
		}
		public static bool GetMonsterHunter_Flg()
		{
			return MonsterHunter_Flg;
		}
		public static int GetDr_Potion_Poison()
		{
			return Dr_Potion_Poison;
		}
		public static int GetDr_Potion_FlameIce()
		{
			return Dr_Potion_FlameIce;
		}
		public static bool GetDr_Potion_Flg()
		{
			return Dr_Potion_Flg;
		}
		public static int GetClumsyPerson_Poison()
		{
			return ClumsyPerson_Poison;
		}
		public static int GetClumsyPerson_FlameIce()
		{
			return ClumsyPerson_FlameIce;
		}
		public static bool GetClumsyPerson_Flg()
		{
			return ClumsyPerson_Flg;
		}
		public static int GetDiamond_Poison()
		{
			return Diamond_Poison;
		}
		public static int GetDiamond_FlameIce()
		{
			return Diamond_FlameIce;
		}
		public static bool GetDiamond_Flg()
		{
			return Diamond_Flg;
		}
		public static int GetGameOver_Poison()
		{
			return GameOver_Poison;
		}
		public static int GetGameOver_FlameIce()
		{
			return GameOver_FlameIce;
		}
		public static bool GetGameOver_Flg()
		{
			return GameOver_Flg;
		}
		public static int GetTalented_Poison()
		{
			return Talented_Poison;
		}
		public static int GetTalented_FlameIce()
		{
			return Talented_FlameIce;
		}
		public static bool GetTalented_Flg()
		{
			return Talented_Flg;
		}
		public static int GetGreatAdventurer_Poison()
		{
			return GreatAdventurer_Poison;
		}
		public static int GetGreatAdventurer_FlameIce()
		{
			return GreatAdventurer_FlameIce;
		}
		public static bool GetGreatAdventurer_Flg()
		{
			return GreatAdventurer_Flg;
		}

		//フラグの初期化
		public static void FlgReset()
		{
			DungeonTraversal_Flg = false;
			MonsterHunter_Flg = false;
			Dr_Potion_Flg = false;
			ClumsyPerson_Flg = false;
			Diamond_Flg = false;
			GameOver_Flg = false;
			Talented_Flg = false;
			GreatAdventurer_Flg = false;
		}

		//------------------アチーブ増加------------------//

		//アチーブ獲得チェック
		public static void AchievementCheck()
		{
			//挑んでいるダンジョン
			if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1)
			{
				//ダンジョン踏破
				//生き残っているので
				if (Score.GetHp() > 0)
				{
					DungeonTraversal_Poison++;
					DungeonTraversal_Flg = true;
				}

				//モンスターハンター
				if (Score.AllHunt())
				{
					MonsterHunter_Poison++;
					MonsterHunter_Flg = true;
				}


				//ポーション博士
				if (Score.GetUseGreenPotion() > 0 && Score.GetUseRedPotion() > 0 &&
					Score.GetUseBluePotion() > 0 && /*Score.GetUseYellowPotion() > 0 &&*/
					Score.GetUseWhitePotion() > 0)
				{
					Dr_Potion_Poison++;
					Dr_Potion_Flg = true;
				}


				//ドジっ子
				if (Score.AllTrap())
				{
					ClumsyPerson_Poison++;
					ClumsyPerson_Flg = true;
				}


				//金剛
				if (Score.IsVajra())
				{
					Diamond_Poison++;
					Diamond_Flg = true;
				}

				//ゲームオーバー
				if (Score.GetHp() < 1)
				{
					GameOver_Poison++;
					GameOver_Flg = true;
				}

				//才能豊か
				if (Score.GetD1WeaponFlg() && GetTalented_Poison() == 0)
				{
					Talented_Poison++;
					Talented_Flg = true;
				}

				//一人前の冒険者!!
				if (GetDungeonTraversal_Poison() > 0 && GetMonsterHunter_Poison() > 0 && GetDr_Potion_Poison() > 0 &&
					GetClumsyPerson_Poison() > 0 && GetDiamond_Poison() > 0 &&
					GetTalented_Poison() > 0 && GetGameOver_Poison() > 0 && GetGreatAdventurer_Poison() == 0)
				{
					GreatAdventurer_Poison++;
					GreatAdventurer_Flg = true;
				}
			}
			else
			{
				//ダンジョン踏破
				//生き残っているので
				if (Score.GetHp() > 0)
				{
					DungeonTraversal_FlameIce++;
					DungeonTraversal_Flg = true;
				}

				//モンスターハンター
				if (Score.AllHunt())
				{
					MonsterHunter_FlameIce++;
					MonsterHunter_Flg = true;
				}


				//ポーション博士
				if (Score.GetUseGreenPotion() > 0 && Score.GetUseRedPotion() > 0 &&
					Score.GetUseBluePotion() > 0 && /*Score.GetUseYellowPotion() > 0 &&*/
					Score.GetUseWhitePotion() > 0)
				{
					Dr_Potion_FlameIce++;
					Dr_Potion_Flg = true;
				}


				//ドジっ子
				if (Score.AllTrap())
				{
					ClumsyPerson_FlameIce++;
					ClumsyPerson_Flg = true;
				}


				//金剛
				if (Score.IsVajra())
				{
					Diamond_FlameIce++;
					Diamond_Flg = true;
				}

				//ゲームオーバー
				if (Score.GetHp() < 1)
				{
					GameOver_FlameIce++;
					GameOver_Flg = true;
				}

				//才能豊か
				if (Score.GetD2WeaponFlg() && GetTalented_FlameIce() == 0)
				{
					Talented_FlameIce++;
					Talented_Flg = true;
				}

				//一人前の冒険者!!
				if (GetDungeonTraversal_FlameIce() > 0 && GetMonsterHunter_FlameIce() > 0 && GetDr_Potion_FlameIce() > 0 &&
					GetClumsyPerson_FlameIce() > 0 && GetDiamond_FlameIce() > 0 &&
					GetTalented_FlameIce() > 0 && GetGameOver_FlameIce() > 0 && GetGreatAdventurer_FlameIce() == 0)
				{
					GreatAdventurer_FlameIce++;
					GreatAdventurer_Flg = true;
				}
			}


			//アチーブメントの取得数・取得月・取得日の保存
			//ダンジョン1
			if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1)
			{
				//ダンジョン踏破
				if (DungeonTraversal_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad1();
					AchievementSaveLoad.count1 += 1;

					if (AchievementSaveLoad.count1 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month1 = today.Month;
						AchievementSaveLoad.Day1 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite1();

				}

				//モンスターハンター
				if (MonsterHunter_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad2();
					AchievementSaveLoad.count2 += 1;
					if (AchievementSaveLoad.count2 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month2 = today.Month;
						AchievementSaveLoad.Day2 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite2();

				}

				//ポーション博士
				if (Dr_Potion_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad3();
					AchievementSaveLoad.count3 += 1;
					if (AchievementSaveLoad.count3 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month3 = today.Month;
						AchievementSaveLoad.Day3 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite3();

				}

				//ドジっ子
				if (ClumsyPerson_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad4();
					AchievementSaveLoad.count4 += 1;
					if (AchievementSaveLoad.count4 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month4 = today.Month;
						AchievementSaveLoad.Day4 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite4();

				}

				//金剛
				if (Diamond_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad5();
					AchievementSaveLoad.count5 += 1;
					if (AchievementSaveLoad.count5 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month5 = today.Month;
						AchievementSaveLoad.Day5 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite5();

				}

				//ゲームオーバー
				if (GameOver_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad6();
					AchievementSaveLoad.count6 += 1;
					if (AchievementSaveLoad.count6 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month6 = today.Month;
						AchievementSaveLoad.Day6 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite6();

				}

				//才能豊か
				if (Talented_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad7();
					AchievementSaveLoad.count7 += 1;
					if (AchievementSaveLoad.count7 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month7 = today.Month;
						AchievementSaveLoad.Day7 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite7();

				}

				//一人前の冒険者!!
				if (GreatAdventurer_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad8();
					AchievementSaveLoad.count8 += 1;
					if (AchievementSaveLoad.count8 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month8 = today.Month;
						AchievementSaveLoad.Day8 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite8();

				}

			}

			//ダンジョン2
			else
			{
				//ダンジョン踏破
				if (DungeonTraversal_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad9();
					AchievementSaveLoad.count9 += 1;
					if (AchievementSaveLoad.count9 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month9 = today.Month;
						AchievementSaveLoad.Day9 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite9();

				}

				//モンスターハンター
				if (MonsterHunter_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad10();
					AchievementSaveLoad.count10 += 1;
					if (AchievementSaveLoad.count10 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month10 = today.Month;
						AchievementSaveLoad.Day10 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite10();

				}

				//ポーション博士
				if (Dr_Potion_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad11();
					AchievementSaveLoad.count11 += 1;
					if (AchievementSaveLoad.count11 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month11 = today.Month;
						AchievementSaveLoad.Day11 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite11();

				}

				//ドジっ子
				if (ClumsyPerson_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad12();
					AchievementSaveLoad.count12 += 1;
					if (AchievementSaveLoad.count12 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month12 = today.Month;
						AchievementSaveLoad.Day12 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite12();

				}

				//金剛
				if (Diamond_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad13();
					AchievementSaveLoad.count13 += 1;
					if (AchievementSaveLoad.count13 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month13 = today.Month;
						AchievementSaveLoad.Day13 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite13();

				}

				//ゲームオーバー
				if (GameOver_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad14();
					AchievementSaveLoad.count14 += 1;
					if (AchievementSaveLoad.count14 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month14 = today.Month;
						AchievementSaveLoad.Day14 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite14();

				}

				//才能豊か
				if (Talented_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad15();
					AchievementSaveLoad.count15 += 1;
					if (AchievementSaveLoad.count15 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month15 = today.Month;
						AchievementSaveLoad.Day15 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite15();

				}

				//一人前の冒険者!!
				if (GreatAdventurer_Flg == true)
				{
					AchievementSaveLoad.AchievementRoad16();
					AchievementSaveLoad.count16 += 1;
					if (AchievementSaveLoad.count16 == 1)
					{
						DateTime today = DateTime.Now;
						AchievementSaveLoad.Month16 = today.Month;
						AchievementSaveLoad.Day16 = today.Day;
					}
					AchievementSaveLoad.AchievementWrite16();

				}
			}


		}

		//アチーブ獲得状況のリセット
		static public void AchieveReset()
		{

		}

		//アチーブ詳細テキスト
		static public string DungeonTraversalText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ダンジョン踏破\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if(AchievementSaveLoad.Month1 > 0 && AchievementSaveLoad.Day1 > 0)
				{
					text += AchievementSaveLoad.Month1 + "月" + AchievementSaveLoad.Day1 + "日";
				}
				text += "\n獲得数：" + GetDungeonTraversal_Poison().ToString() + "個     \n";
				text += "- 詳細 -\nダンジョンをクリア";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month9 > 0 && AchievementSaveLoad.Day9 > 0)
				{
					text += AchievementSaveLoad.Month9 + "月" + AchievementSaveLoad.Day9 + "日";
				}
				text += "\n獲得数：" + GetDungeonTraversal_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\nダンジョンをクリア";
			}
			//アチーブ名：ダンジョン踏破
			return text;
		}
		static public string MonsterHunterText(IconBase.Icon icon)
		{
			//テキスト
			string text = "モンスターハンター\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month2 > 0 && AchievementSaveLoad.Day2 > 0)
				{
					text += AchievementSaveLoad.Month2 + "月" + AchievementSaveLoad.Day2 + "日";
				}
				text += "\n獲得数：" + GetMonsterHunter_Poison().ToString() + "個     \n";
				text += "- 詳細 -\n全モンスターの討伐";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month10 > 0 && AchievementSaveLoad.Day10 > 0)
				{
					text += AchievementSaveLoad.Month10 + "月" + AchievementSaveLoad.Day10 + "日";
				}
				text += "\n獲得数：" + GetMonsterHunter_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n全モンスターの討伐";
			}

			//アチーブ名：モンスターハンター
			return text;
		}
		static public string Dr_PotionText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ポーション博士\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month3 > 0 && AchievementSaveLoad.Day3 > 0)
				{
					text += AchievementSaveLoad.Month3 + "月" + AchievementSaveLoad.Day3 + "日";
				}
				text += "\n獲得数：" + GetDr_Potion_Poison().ToString() + "個\n";
				text += "- 詳細 -\n全ポーションの使用";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month11 > 0 && AchievementSaveLoad.Day11 > 0)
				{
					text += AchievementSaveLoad.Month11 + "月" + AchievementSaveLoad.Day11 + "日";
				}
				text += "\n獲得数：" + GetDr_Potion_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n全ポーションの使用";
			}

			//アチーブ名：ポーション博士
			return text;
		}
		static public string ClumsyPersonText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ドジっ子\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month4 > 0 && AchievementSaveLoad.Day4 > 0)
				{
					text += AchievementSaveLoad.Month4 + "月" + AchievementSaveLoad.Day4 + "日";
				}
				text += "\n獲得数：" + GetClumsyPerson_Poison().ToString() + "個     \n";
				text += "- 詳細 -\n2つの罠にかかる";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month12 > 0 && AchievementSaveLoad.Day12 > 0)
				{
					text += AchievementSaveLoad.Month12 + "月" + AchievementSaveLoad.Day12 + "日";
				}
				text += "\n獲得数：" + GetClumsyPerson_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n2つの罠にかかる";
			}

			//アチーブ名：ドジっ子
			return text;
		}
		static public string DiamondText(IconBase.Icon icon)
		{
			//テキスト
			string text = "金剛\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month5 > 0 && AchievementSaveLoad.Day5 > 0)
				{
					text += AchievementSaveLoad.Month5 + "月" + AchievementSaveLoad.Day5 + "日";
				}
				text += "\n獲得数：" + GetDiamond_Poison().ToString() + "個     \n";
				text += "- 詳細 -\n行動しないで5回被弾";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month13 > 0 && AchievementSaveLoad.Day13 > 0)
				{
					text += AchievementSaveLoad.Month13 + "月" + AchievementSaveLoad.Day13 + "日";
				}
				text += "\n獲得数：" + GetDiamond_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n行動しないで5回被弾";
			}

			//アチーブ名：金剛
			return text;
		}
		static public string GameOverText(IconBase.Icon icon)
		{
			//テキスト
			string text = "死んでしまった!!\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month6 > 0 && AchievementSaveLoad.Day6 > 0)
				{
					text += AchievementSaveLoad.Month6 + "月" + AchievementSaveLoad.Day6 + "日";
				}
				text += "\n獲得数：" + GetGameOver_Poison().ToString() + "個     \n";
				text += "- 詳細 -\nゲームオーバーになる";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month14 > 0 && AchievementSaveLoad.Day14 > 0)
				{
					text += AchievementSaveLoad.Month14 + "月" + AchievementSaveLoad.Day14 + "日";
				}
				text += "\n獲得数：" + GetGameOver_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\nゲームオーバーになる";
			}

			//アチーブ名：死んでしまった!!
			return text;
		}
		static public string TalentedText(IconBase.Icon icon)
		{
			//テキスト
			string text = "才能豊か\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month7 > 0 && AchievementSaveLoad.Day7 > 0)
				{
					text += AchievementSaveLoad.Month7 + "月" + AchievementSaveLoad.Day7 + "日";
				}
				text += "\n獲得数：" + GetTalented_Poison().ToString() + "個     \n";
				text += "- 詳細 -\n全武器でクリア";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month15 > 0 && AchievementSaveLoad.Day15 > 0)
				{
					text += AchievementSaveLoad.Month15 + "月" + AchievementSaveLoad.Day15 + "日";
				}
				text += "\n獲得数：" + GetTalented_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n全武器でクリア";
			}

			//アチーブ名：才能豊か
			return text;
		}
		static public string GreatAdventurerText(IconBase.Icon icon)
		{
			//テキスト
			string text = "一人前の冒険者!!\n" + "獲得日：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				if (AchievementSaveLoad.Month8 > 0 && AchievementSaveLoad.Day8 > 0)
				{
					text += AchievementSaveLoad.Month8 + "月" + AchievementSaveLoad.Day8 + "日";
				}
				text += "\n獲得数：" + GetGreatAdventurer_Poison().ToString() + "個     \n";
				text += "- 詳細 -\n7つアチーブメント取得";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				if (AchievementSaveLoad.Month16 > 0 && AchievementSaveLoad.Day16 > 0)
				{
					text += AchievementSaveLoad.Month16 + "月" + AchievementSaveLoad.Day16 + "日";
				}
				text += "\n獲得数：" + GetGreatAdventurer_FlameIce().ToString() + "個     \n";
				text += "- 詳細 -\n7つアチーブメント取得";
			}

			//アチーブ名：一人前の冒険者!!
			return text;
		}
	}
}