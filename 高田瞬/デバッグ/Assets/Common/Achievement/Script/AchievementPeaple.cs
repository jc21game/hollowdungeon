using UnityEngine;
using UnityEngine.UI;

public class AchievementPeaple : Achievement
{
	//アチーブのオブジェクト
	private static GameObject AppealObject = null;

	//背景イメージ
	private static Image Image = null;

	//キャンバス
	private Canvas Canvas = null;

	//前の画像のパス
	private static string DuplicatePath = "";

	//現在の画像のパス
	private static string NowDuplicatePath = "";

	//初期値
	private Vector2[] initVector;
	private Color initColor;

	//前のパスの設定
	public static void SetDuplicatePath(string path)
	{
		DuplicatePath = path;
	}

	private static string GetDuplicatePath()
	{
		return DuplicatePath;
	}

	//現在のパスの設定
	public static void SetNowDuplicatePath(string path)
	{
		NowDuplicatePath = path;
	}

	private static string GetNowDuplicatePath()
	{
		return NowDuplicatePath;
	}

	//初期化
	protected override void Start()
	{
		//キャンバス確保
		Canvas = GameObject.Find("Canvas(Clone)").GetComponent<Canvas>();

		//イメージ作成
		Image = new GameObject("BackImage").AddComponent<Image>();
		BaseImage = new GameObject("AppealImage").AddComponent<Image>();

		//親設定
		Image.transform.SetParent(Canvas.transform, false);
		BaseImage.transform.SetParent(Canvas.transform, false);

		//初期値
		initVector = new Vector2[3];
		initVector[0] = new Vector2(0, 90);
		initVector[1] = new Vector2(150, 150);
		initVector[2] = new Vector2(300, 350);
		initColor = new Color(0, 255, 230);

		//位置調整
		BaseImage.rectTransform.anchoredPosition = initVector[0];
		Image.rectTransform.anchoredPosition = new Vector2();

		//サイズ調整
		BaseImage.rectTransform.sizeDelta = initVector[1];
		Image.rectTransform.sizeDelta = initVector[2];

		//透明化
		BaseImage.color = new Color(BaseImage.color.r, BaseImage.color.g, BaseImage.color.b, 0);

		//色設定
		Image.color = initColor;
		Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 0);

		//いろいろ生成
		Initialize(BaseImage, GetNowDuplicatePath());
	}

	//更新
	protected override void Update()
	{
		//パスが変わった
		if (GetDuplicatePath() != GetNowDuplicatePath())
		{
			//前のパスを更新
			SetDuplicatePath(GetNowDuplicatePath());

			//透明度変更
			BaseImage.color = new Color(BaseImage.color.r, BaseImage.color.g, BaseImage.color.b, 100);
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 100);

			//画像群セット
			Initialize(BaseImage, GetNowDuplicatePath());
		}

		base.Update();
	}

	//アピールオブジェクト生成
	public static void AppealGeneration()
	{
		//アチーブの作成
		AppealObject = new GameObject("AppealObject");

		//コンポーネントを追加
		AppealObject.AddComponent<AchievementPeaple>();
	}

	//画像群セット
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	//いろいろ生成
	protected override void Initialize(Image image, string path)
	{
		base.Initialize(image, path);
	}

	//クリックされたら
	protected override void OnClick()
	{
		base.OnClick();
	}
}
