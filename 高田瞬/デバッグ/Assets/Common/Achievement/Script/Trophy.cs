using LevelOfAchievement;
using UnityEngine;
using UnityEngine.UI;

public class Trophy : Achievement
{
	//画像のパス
	private static readonly string TrophyPath = "Trophy";

	//初期化
	protected override void Start()
	{
		base.Start();

		//いろいろ生成
		Initialize(BaseImage, TrophyPath);
	}

	//いろいろ生成
	protected override void Initialize(Image image, string path)
	{
		base.Initialize(image, path);
	}

	//更新
	protected override void Update()
	{
		base.Update();

		//現在のページ
		if (Title.Icon == IconBase.Icon.DUNGEON_1)
		{
			//所持している
			//踏破
			if (AchievementList.GetMonsterHunter_Poison() > 0)
			{
				BaseImage.color = Color.white;
			}
			else
			{
				BaseImage.color = Color.black;
			}
		}
		else if (Title.Icon == IconBase.Icon.DUNGEON_2)
		{
			//所持している
			//踏破
			if (AchievementList.GetMonsterHunter_FlameIce() > 0)
			{
				BaseImage.color = Color.white;
			}
			else
			{
				BaseImage.color = Color.black;
			}
		}
	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	//クリックされたら
	protected override void OnClick()
	{
		base.OnClick();

		//画像セット
		Duplicate.SetNowDuplicatePath(TrophyPath);
	}
}
