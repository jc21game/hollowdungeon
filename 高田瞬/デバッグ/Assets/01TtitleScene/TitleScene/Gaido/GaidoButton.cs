using UnityEngine;
using SceneManager;

public class GaidoButton : MonoBehaviour
{
	private Scene Scene = null;

	private GameObject GaidoArea = null;

	//設定画面
	private GameObject Gaido = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//現在のシーンを登録
		Scene = new Scene();
		Scene.SetNowScene(SCENES.TITLE_SCENE);

		//取得
		GaidoArea = GameObject.Find("GaidoArea");
		Gaido = GameObject.Find("Gaido");

		//ひとつ前のシーンがスプラッシュシーンの場合
		if(Scene.GetBeforeScene() == SCENES.NONE)
		{
			flg = true;
		}

		GaidoArea.SetActive(flg);
		Gaido.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		GaidoArea.SetActive(flg);
		Gaido.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{

		//フラグ切り替え
		if (flg)
			flg = false;
		else
			flg = true;
	}
}
