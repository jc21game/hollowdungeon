using UnityEngine;

public class ConfigButton : MonoBehaviour
{
	//ボタン
	private GameObject outArea = null;

	//設定画面
	private GameObject config = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//取得
		outArea = GameObject.Find("OutArea");
		config = GameObject.Find("Config");

		//非表示
		outArea.SetActive(flg);
		config.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		outArea.SetActive(flg);
		config.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//フラグ切り替え
		if (flg)
			flg = false;
		else
			flg = true;
	}
}
