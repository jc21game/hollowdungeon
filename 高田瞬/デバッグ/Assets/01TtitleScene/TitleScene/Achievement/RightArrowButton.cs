using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightArrowButton : MonoBehaviour
{
    //右矢印
    private GameObject RightArrow = null;

    //オブジェクト
    private TitleAchievement TitleAchievement= null;

    //表示フラグ
    public bool flg = false;

    //初期化
    void Start()
    {
        //取得
        RightArrow = GameObject.Find("RightArrow");
        TitleAchievement = GameObject.Find("Achievement").GetComponent<TitleAchievement>();

        //アクティブ反映
        RightArrow.SetActive(flg);
    }

    //更新
    void Update()
    {
        if(TitleAchievement.Icon == IconBase.Icon.DUNGEON_1)
		{
            flg = true;
		}
        else if(TitleAchievement.Icon == IconBase.Icon.DUNGEON_2)
		{
            flg = false;
		}

        //表示切り替え
        RightArrow.SetActive(flg);
    }

    //ボタンをクリックされた
    public void OnClick()
    {
        TitleAchievement.Icon = IconBase.Icon.DUNGEON_2;
    }
}
