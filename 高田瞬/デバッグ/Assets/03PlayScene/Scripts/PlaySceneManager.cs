using SceneManager;
using ScoreManager;
using UnityEngine;
using UnityEngine.UI;

//プレイシーン
public class PlaySceneManager : MonoBehaviour
{
	//階層画像
	public Sprite[] floors = null;
	private Sprite sprite = null;
	private Image image = null;
	private Color color;
	private const float addAlpha = 0.008f;
	private const float incAlpha = 0.006f;
	public Vector2 fPos;

	//マップオブジェクト
	private GameObject map = null;
	private Map mapScript = null;
	private GameObject miniMap = null;
	private MiniMap minimapScript = null;
	public GameObject effect = null;
	public GameObject smog = null;

	//キャラクター
	public GameObject objects = null;
	public int characters = 25;
	private CharacterBase[] characterArray;
	public CharacterBase[] allCharacter;
	public CharacterBase trent;

	//キャラクター情報
	private int execute = 0;
	private int maxChar = 0;

	//キャラクター生成確立
	private const int generate = 2;

	//アイテム
	public GameObject[] potions = null;

	//シーンマネージャー
	private Scene scene = null;

	//UI
	private GameObject ui = null;

	//ゲーム途中終了
	public bool gameFin = false;
	public bool gameClear = false;

	//時間
	private float count = 0;
	private const float maxCount = 5;

	//ゲームスタート
	private bool initialize = false;
	private bool isStart = false;

	//フロア移動
	private bool select = false;
	private bool move = false;

	//行動時間
	public float actTime = 0;
	public float enemyActTime = 0;
	public float dushTime = 0;
	private float nowTime = 0;
	private bool nextAct = false;

	//煙の位置
	private const int smogPos = 270;

	//トレントの初期生成
	public Vector3[] trantInitGen;

	//テスト
	public int initFloor = 0;
	public bool useInitFloor = false;

	//初期化
	public void Start()
	{
		//フレームレート設定
		Application.targetFrameRate = 60;

		//画面真っ暗
		FadeScript.Darkening();

		//真っ暗な時間
		count = maxCount;

		//イメージ生成
		image = new GameObject("Floor").AddComponent<Image>();
		color = image.color;
		color.a = 0;
		image.color = color;
		Instantiate(effect);

		//サウンド
		SoundManager.SoundManagerGeneration();

		SoundManager.PlaySE("Stairs");
		SoundManager.PlayBGM("Play1");
		
		//テスト
		if (useInitFloor)
		{
			Score.SetFloor(initFloor, 0);
		}
	}

	//更新
	public void Update()
	{
		//フロア移動するか
		if (move)
		{
			IsMove();
			return;
		}

		//フロア移動
		if (select)
		{
			NextFloor();
			return;
		}

		//階層表示
		if (!isStart)
		{
			//階層の表示
			sprite = floors[Score.GetFloor() - 1];

			if (image.color.a < 1)
			{
				color.a += addAlpha;
				image.color = color;
			}

			image.rectTransform.sizeDelta = fPos;

			image.transform.SetParent(GameObject.Find("FadeCanvas").transform, false);
			image.sprite = sprite;

			//一定時間でメッセージウィンドウ初期化
			if ((count -= Time.deltaTime) < 0)
			{
				//ゲーム開始
				isStart = true;
			}
		}

		//ゲーム開始
		else if (isStart)
		{
			//初期化
			if (!initialize)
			{
				//フェードイン開始
				FadeScript.FadeIn();

				//シーン
				scene = new Scene();
				scene.SetNowScene(SCENES.PLAY_SCENE);

				//マップオブジェクト生成
				map = new GameObject("Map");

				//ソースをアタッチ
				mapScript = map.AddComponent<Map>();

				//ミニマップオブジェクトの生成
				miniMap = new GameObject("MiniMap");
				minimapScript = miniMap.AddComponent<MiniMap>();

				//毒霧
				if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1 && Score.GetFloor() < 5)
				{
					GameObject game = Instantiate(smog);
					game.transform.SetParent(miniMap.transform, true);
					game.transform.position = new Vector3(0, -smogPos, 0);
					game = Instantiate(smog);
					game.transform.SetParent(miniMap.transform, true);
					game.transform.position = new Vector3(0, 0, 0);
					game = Instantiate(smog);
					game.transform.SetParent(miniMap.transform, true);
					game.transform.position = new Vector3(0, smogPos, 0);
				}

				//UI
				ui = new GameObject("UI");
				ui.AddComponent<UI>();

				//テキストボックス生成
				MessageWindowManager.ImageGeneration();

				//クエスチョンボックスを生成
				YesNoQuestion.QuestionGeneration();

				//キャラクター配列
				characterArray = new CharacterBase[characters];

				//生成
				objects = new GameObject("Object");
				Instantiate(Resources.Load("Prefab/Player"));
				Instantiate(Resources.Load("Prefab/Menu"));

				//敵の生成
				if (Score.GetFloor() != Score.MaxFloor)
				{
					for (int i = Random.Range(0, 6); i > 0; i--)
					{
						//生成
						Instantiate(allCharacter[Random.Range(0, allCharacter.Length)]);
					}
				}
				else
				{
					Instantiate(trent);
					CharacterBase[] characters = new CharacterBase[trantInitGen.Length];
					for (int i = 0; i < trantInitGen.Length; i++)
					{
						characters[i] = CreateCharacter();
						characters[i].transform.position = trantInitGen[i];
					}
				}

				//初期化終了
				initialize = true;
			}

			//α値減少
			if (image.color.a > 0)
			{
				color = image.color;
				color.a -= incAlpha;
				image.color = color;
			}

			//一定時間でメッセージウィンドウ初期化
			if ((count -= Time.deltaTime) < 0)
			{
				//初期化
				MessageWindowManager.MessageImageHide();
			}

			//人数なし
			if (maxChar <= 0)
			{
				return;
			}

			//時間経過
			Score.AddTime();

			//ゲーム途中終了
			if (gameFin)
			{
				//シーン遷移
				GameFin((int)SCENES.TITLE_SCENE);

				//処理終了
				return;
			}

			//ゲームクリア
			if (gameClear)
			{
				//シーン遷移
				GameFin((int)SCENES.ENDING_SCENE);

				//処理終了
				return;
			}

			//プレイヤーの死亡
			if (!characterArray[0].LIFE)
			{
				//シーン遷移
				GameFin((int)SCENES.OVER_SCENE);

				//処理終了
				return;
			}

			//実行
			if (!nextAct)
			{
				//行動
				if (characterArray[execute].Execute())
				{
					//行動終了
					nextAct = true;

					//プレイヤーの行動
					if (execute == 0)
					{
						nowTime = actTime;
					}

					//敵の行動
					else
					{
						nowTime = enemyActTime;
					}

					//プレイヤーでダッシュ中だったら
					if (execute == 0 && GetCharacter(0).GetComponent<Player>().isDush)
					{
						nowTime = dushTime;
					}

					//画面内に映るか
					if (!mapScript.IsExist(GetPosition(execute)))
					{
						nowTime = 0;
					}
				}
			}

			//行動終了
			else if ((nowTime -= Time.deltaTime) <= 0)
			{
				//非実行
				nextAct = false;

				//プレイヤーの死亡
				if (!characterArray[0].LIFE)
				{
					//フェード
					GameFinStart();

					//処理終了
					return;
				}

				//死亡しているか
				for (int i = 1; i < maxChar; i++)
				{
					//死亡
					if (!characterArray[i].LIFE)
					{
						//削除
						Remove(i--);
					}
				}

				//プレイヤーだったら
				if (execute == 0)
				{
					//状態を調べる(0:毒、1:鈍足、2:炎、3:氷)
					for (int i = 0; i < 4; i++)
					{
						//罹患ターンを減少(経過)させる(かかっていたら)
						characterArray[execute].state.trap[i].Turn();

						//状態異常の処理(かかっていたら)
						if (characterArray[execute].state.trap[i].isState)
						{
							switch (i)
							{
								//毒
								case 0:
									//HP減少
									characterArray[execute].Hit(1, "", "プレイヤーは毒で1ポイントのダメージを受けた！！(残り" + characterArray[execute].state.trap[i].NowTurn().ToString() + "ターン)");
									break;

								//炎(やけど)
								case 2:
									//HP減少
									characterArray[execute].Hit(1, "", "プレイヤーはやけどで1ポイントのダメージを受けた！！(残り" + characterArray[execute].state.trap[i].NowTurn().ToString() + "ターン)");
									break;

								default:
									break;
							}
						}
					}

					//プレイヤーの死亡
					if (!characterArray[0].LIFE)
					{
						//フェード
						GameFinStart();

						//処理終了
						return;
					}

					//死亡しているか
					for (int i = 1; i < maxChar; i++)
					{
						//死亡
						if (!characterArray[i].LIFE)
						{
							//削除
							Remove(i--);
						}
					}
				}

				//次へ進む
				if (++execute == maxChar)
				{
					//初期化
					execute = 0;

					//ターン経過
					GetCharacter(0).GetComponent<Player>().AddTurn();

					//キャラクターの生成
					if (Score.GetFloor() != Score.MaxFloor)
					{
						if (Random.Range(0, 100) == generate)
						{
							//生成
							CreateCharacter();
						}
					}
				}
			}
		}
	}

	//最大人数
	public int GetMaxChar()
	{
		//最大人数
		return maxChar;
	}

	//生成
	public CharacterBase CreateCharacter(int num = 1)
	{
		//キャラクターの生成
		if (maxChar + num < characters)
		{
			//生成
			return Instantiate(allCharacter[Random.Range(0, allCharacter.Length)]);
		}

		//生成終了
		return null;
	}

	//追加
	public int AddChar(CharacterBase character, bool isOnlyAdd = false)
	{
		//追加
		characterArray[maxChar++] = character;

		//座標の追加
		if (!isOnlyAdd)
		{
			//座標が初期値以外
			if (character.transform.position == new Vector3(0, 0, 0))
			{
				//座標
				character.transform.position = mapScript.Spawn(maxChar - 1);
			}

			//ミニマップ
			minimapScript.GenerateObject(maxChar - 1, character.transform.position.x, -character.transform.position.y);
		}

		return maxChar - 1;
	}

	//ミニマップのみ追加
	public void AddChar(Vector2 vector)
	{
		//ミニマップ
		minimapScript.GenerateObject(maxChar - 1, vector.x, -vector.y);
	}

	//削除
	public void Remove(int number)
	{
		//死亡
		characterArray[number].Death();
		minimapScript.Death(number);

		//移動
		for (; number < maxChar - 1; number++)
		{
			//移動
			characterArray[number] = characterArray[number + 1];
		}

		//実行を戻すか
		if (number <= execute)
		{
			//戻す
			execute--;
		}

		//最大人数
		maxChar--;
	}

	//座標取得
	public Vector2 GetPosition(int number)
	{
		//座標
		return characterArray[number].transform.position;
	}

	//一致する座標に存在するキャラクターを返す	
	public CharacterBase GetCharacter(Vector2 vector)
	{
		//全キャラ回す
		for (int number = 0; number < maxChar; number++)
		{
			//座標が同じ場合
			if ((Vector2)characterArray[number].transform.position == vector)
			{
				//キャラクターを返す
				return characterArray[number];
			}
		}

		//いない場合
		return null;
	}

	//キャラクターを返す
	public CharacterBase GetCharacter(int number)
	{
		//キャラクターを返す
		return characterArray[number];
	}

	//キャラクターの移動
	public void Move()
	{
		//キャラクターの移動
		miniMap.GetComponent<MiniMap>().Move(execute, characterArray[execute].transform.position);
	}

	//ゲーム終了
	public void GameFinStart()
	{
		//フェード
		FadeScript.FadeOut();
	}
	public void GameFin(int nextScene)
	{
		//フェード終了
		if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
		{
			//シーン移動
			scene.SetNextScene((SCENES)nextScene);
		}
	}

	//メッセージ送信
	public void SetMessage(string message)
	{
		count = maxCount;
		MessageWindowManager.MessageTextChange(message);
	}

	//メッセージリセット
	public void ResetMessage()
	{
		MessageWindowManager.TextReset();
	}

	//メッセージ永続表示
	public void CountMax()
	{
		count = 1;
	}

	//メッセージ非表示
	public void CountMin()
	{
		count = 0;
	}

	//階層移動する
	public void MoveNextFloor()
	{
		//移動
		move = true;

		//テキスト
		ResetMessage();
		SetMessage("次のフロアへ移動しますか？");
	}

	//YesNoの選択
	private void IsMove()
	{
		//カウント最大
		CountMax();

		//クエスチョンボックスを表示
		YesNoQuestion.QuestionDisplay();

		//「はい」「いいえ」切り替え
		if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			//SE再生
			SoundManager.PlaySE("Choice");

			YesNoQuestion.ArrowChange();
		}

		//選択決定
		else if (Input.GetKeyDown(KeyCode.Z))
		{
			//SE再生
			SoundManager.PlaySE("Select");

			//Yes
			if (YesNoQuestion.GetArrow())
			{
				//移動する
				select = true;

				//フェードアウト
				GameFinStart();
			}

			//No
			else
			{
				//カウント最低
				CountMin();
			}

			//IsMoveは終了
			move = false;

			//クエスチョンボックスを非表示
			YesNoQuestion.QuestionHide();
		}

		else if (Input.GetKeyDown(KeyCode.X))
		{

			//IsMoveは終了
			move = false;

			//クエスチョンボックスを非表示
			YesNoQuestion.QuestionHide();
			CountMin();
		}
	}

	//階層移動
	public void NextFloor()
	{
		//フェード終了
		if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
		{
			//階層変更
			Score.SetFloor(Score.GetFloor() + 1);
		}

		//シーン遷移
		GameFin((int)SCENES.PLAY_SCENE);
	}

	//プレイヤーの移動反映
	public void MovePlayer()
	{
		miniMap.GetComponent<MiniMap>().PlayerMove();
	}

	//今誰
	public int GetExec()
	{
		return execute;
	}
}