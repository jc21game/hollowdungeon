using UnityEngine;

public class Minotaur : CharacterBase
{
	//初期化
	public override void Start()
	{
		base.Start();
	}

	//更新
	public override void Update()
	{
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//外部から呼び出された場合
		if (isColl)
		{
			//行動なし
			isColl = false;
			manager.Move();
			return true;
		}

		//攻撃
		if (Attack())
		{
			//攻撃中
			if (isAttack)
			{
				//行動中
				return false;
			}

			//行動終了
			return true;
		}

		//移動
		if (Move())
		{
			//処理終了
			return true;
		}

		//処理なし
		return true;
	}

	//攻撃
	protected override bool Attack()
	{
		//攻撃中
		if (isAttack)
		{
			return base.Attack();
		}

		//四方
		for (int i = 0; i < DIR.DIRCTIONS; i++)
		{
			//プレイヤーがいる場合
			if (manager.GetPosition(0) == GetFront(dir.ConvertDir(i)))
			{
				//攻撃
				dir.dir = dir.ConvertDir(i);
				return base.Attack();
			}
		}

		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//移動先
		if (IsPlayer())
		{
			//差
			Vector2 dif = (Vector2)transform.position - manager.GetPosition(0);

			//行動したか
			isMove = false;

			//右にいる
			if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x > 0 && manager.GetCharacter((GetFront(DIR.Dir.LEFT))) == null && map.GetMap(GetFront(DIR.Dir.LEFT)) != 0)
			{
				//左に移動
				dir.dir = DIR.Dir.LEFT;
				isMove = true;
			}

			//左にいる
			else if (Mathf.Abs(dif.x) > Mathf.Abs(dif.y) && dif.x < 0 && manager.GetCharacter((GetFront(DIR.Dir.RIGHT))) == null && map.GetMap(GetFront(DIR.Dir.RIGHT)) != 0)
			{
				//右に移動
				dir.dir = DIR.Dir.RIGHT;
				isMove = true;
			}

			//上にいる
			else if (!isMove && dif.y > 0 && manager.GetCharacter((GetFront(DIR.Dir.BOTTOM))) == null && map.GetMap(GetFront(DIR.Dir.BOTTOM)) != 0)
			{
				//下に移動
				dir.dir = DIR.Dir.BOTTOM;
				isMove = true;
			}

			//下にいる
			else if (!isMove && dif.y < 0 && manager.GetCharacter((GetFront(DIR.Dir.TOP))) == null && map.GetMap(GetFront(DIR.Dir.TOP)) != 0)
			{
				//上に移動
				dir.dir = DIR.Dir.TOP;
				isMove = true;
			}

			//最終
			else
			{
				dir.dir = dir.ConvertDir(Random.Range(0, DIR.DIRCTIONS));
				isMove = true;
			}
		}
		else
		{
			dir.dir = dir.ConvertDir(Random.Range(0, DIR.DIRCTIONS));
			isMove = true;
		}

		if (map.IsMove(GetFront(dir.dir)) && isMove)
		{
			//カウント開始
			count = maxCount;

			//移動
			dir.DirChange(dir.dir);
			dir.AddFoot();
			transform.position = GetFront(dir.dir);
			manager.Move();

			//向き
			sprite.sprite = sprites[dir.GetNumber()];

			//処理終了
			return true;
		}

		//処理なし
		return base.Move();
	}

	//死亡
	public override void Death()
	{
		base.Death();
	}

	//被弾
	public override void Hit(int damage, string name, string text = "")
	{
		base.Hit(damage, name);
	}
}
