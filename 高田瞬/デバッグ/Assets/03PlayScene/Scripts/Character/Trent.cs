using UnityEngine;

//トレント
public class Trent : CharacterBase
{
	//1度だけ行う行動
	private bool seriously = true;

	//初期座標と座標
	public Vector3 initPos;
	public Vector3 initScale;

	//回復量
	public int recovery = 0;

	//攻撃力
	public int longDm = 0;
	public int shortDm = 0;

	//行動範囲
	public int longAtackRange = 0;
	public int shortAtackRange = 0;
	public int collEnemyRange = 0;

	//敵生成数
	public int collEnemys = 0;

	//初期化
	public override void Start()
	{
		//オブジェクト
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
		map = GameObject.Find("Map").GetComponent<Map>();
		objects = GameObject.Find("Object");
		transform.SetParent(objects.transform, false);

		//生成
		number = manager.AddChar(gameObject.GetComponent<CharacterBase>(), true);

		//生命
		LIFE = true;

		//方向と足
		dir = new DIR();

		//キャラクター設定
		sprite = gameObject.GetComponent<SpriteRenderer>();
		sprite.sprite = sprites[dir.GetNumber()];

		//初期設定
		transform.position = initPos;
		transform.localScale = initScale;

		//初期位置
		basePosition = transform.position;

		//表示
		sprite.sortingOrder = (int)(transform.position.x - transform.position.y * unit);

		//ミニマップに座標追加
		manager.AddChar(transform.position);
	}

	//更新
	public override void Update()
	{
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//外部から呼び出された場合
		if (isColl)
		{
			//行動なし
			isColl = false;
			manager.Move();
			return true;
		}

		//本気行動
		if (HP <= MAX_HP / 2 && seriously)
		{
			//仲間呼び
			Coll();
			seriously = false;
			return true;
		}

		//隣接攻撃
		else if (Attack())
		{
			//攻撃中
			if (isAttack)
			{
				//行動中
				return false;
			}

			//行動終了
			return true;
		}

		//遠距離攻撃
		else if (AwayAttack())
		{
			//攻撃中
			if (isAttack)
			{
				//行動中
				return false;
			}

			//行動終了
			return true;
		}

		//仲間呼び
		else if (Coll())
		{
			//攻撃中
			if (isAttack)
			{
				//行動中
				return false;
			}

			//行動終了
			return true;
		}

		//回復
		else
		{
			//回復
			manager.SetMessage("トレントは回復している！！");
			Heal(recovery);

			//行動終了
			return true;
		}
	}

	//隣接攻撃
	protected override bool Attack()
	{
		//攻撃中
		if (isAttack)
		{
			return base.Attack();
		}

		//プレイヤーがいる場合
		for (int y = (int)-transform.position.y - shortAtackRange; y <= (int)-transform.position.y + shortAtackRange; y++)
		{
			for (int x = (int)transform.position.x - shortAtackRange; x <= (int)transform.position.x + shortAtackRange; x++)
			{
				if (manager.GetPosition(0).x == x && manager.GetPosition(0).y == -y)
				{
					manager.GetCharacter(0).Hit(shortDm, NAME);
					return base.Attack();
				}
			}
		}

		//処理なし
		return false;
	}

	//遠距離攻撃
	private bool AwayAttack()
	{
		//攻撃中
		if (isAttack)
		{
			return base.Attack();
		}

		//プレイヤーがいる場合
		for (int y = (int)-transform.position.y - longAtackRange; y <= (int)-transform.position.y + longAtackRange; y++)
		{
			for (int x = (int)transform.position.x - longAtackRange; x <= (int)transform.position.x + longAtackRange; x++)
			{
				if (manager.GetPosition(0).x == x && manager.GetPosition(0).y == -y)
				{
					manager.GetCharacter(0).Hit(longDm, NAME);
					return base.Attack();
				}
			}
		}

		//処理なし
		return false;
	}

	//仲間呼び
	private bool Coll()
	{
		//生成キャラクタ
		CharacterBase[] character = new CharacterBase[collEnemys];

		//攻撃中
		if (isAttack)
		{
			return base.Attack();
		}

		//プレイヤーがいる場合
		for (int y = (int)-transform.position.y - collEnemyRange; y <= (int)-transform.position.y + collEnemyRange; y++)
		{
			for (int x = (int)transform.position.x - collEnemyRange; x <= (int)transform.position.x + collEnemyRange; x++)
			{
				if (manager.GetPosition(0).x == x && manager.GetPosition(0).y == -y)
				{

					//仲間を呼ぶ
					for (int i = 0; i < collEnemys; i++)
					{
						//生成
						character[i] = manager.CreateCharacter(i + collEnemys);

						//いっぱい
						if (character[i] == null)
						{
							break;
						}

						//行動なし
						character[i].isColl = true;
					}

					//行動
					manager.SetMessage("トレントは仲間を呼び出した！！");
					return base.Attack();
				}
			}
		}

		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//処理なし
		return base.Move();
	}

	//死亡
	public override void Death()
	{
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GameFinStart();
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().gameClear = true;
		base.Death();
	}

	//被弾
	public override void Hit(int damage, string name, string text = "")
	{
		base.Hit(damage, name);
	}
}
