using UnityEngine;

//攻撃ポーション
public class AttackPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.ATTACK);
		efficacy = 5;
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//攻撃
		Player player = GameObject.Find("Player(Clone)").GetComponent<Player>();
		if (player.item.turnR == 0)
		{
			player.ATC += efficacy;
		}
		player.item.turnB = Max;
	}
}
