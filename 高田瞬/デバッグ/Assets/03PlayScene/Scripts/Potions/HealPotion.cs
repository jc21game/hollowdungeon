using UnityEngine;

//回復ポーション
public class HealPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.HEAL);
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//回復
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).Heal(efficacy);
	}
}
