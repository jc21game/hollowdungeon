using UnityEngine;

//防御ポーション
public class DeffensePotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.DEFFENCE);
		efficacy = 5;
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//防御
		Player player = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).GetComponent<Player>();
		if (player.item.turnB == 0)
		{
			player.DEF += efficacy;
		}
		player.item.turnB = Max;
	}
}
