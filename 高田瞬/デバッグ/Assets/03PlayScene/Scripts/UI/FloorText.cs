using UnityEngine;
using UnityEngine.UI;

public class FloorText : MonoBehaviour
{
	//コンポーネント取得用
	private Text text = null;

	//フロア管理用
	private int floor = 0;

	//表示テキスト管理用
	private string str = "";

	//初期化
	void Start()
	{
		//コンポーネント取得
		text = this.GetComponent<Text>();

		//初期化
		floor = ScoreManager.Score.GetFloor();
		str = "/5";
	}

	//更新
	void Update()
	{
		//テキスト更新
		text.text = floor.ToString() + str;
	}
}
