using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
	//Canvas
	private Canvas Canvas = null;
	private Canvas BCanvas = null;
	private CanvasScaler canvasScaler = null;

	//プレイヤー
	private GameObject player = null;
	private Player Player = null;

	//背景
	private Image BG = null;
	private Sprite BGSprite = null;

	//状態異常
	GameObject Poison;
	GameObject SlowFoot;
	GameObject Fire;

	//初期化
	public void Start()
	{
		//Canvas生成
		GenerateCanvas();

		//レベルUI
		GameObject LevelText = Instantiate((GameObject)Resources.Load("Prefab/LevelText"));
		LevelText.transform.SetParent(GameObject.Find("UI").transform, false);

		//階層UI
		GameObject FloorText = Instantiate((GameObject)Resources.Load("Prefab/FloorText"));
		FloorText.transform.SetParent(GameObject.Find("UI").transform, false);

		//HPバーUI
		GameObject HPBar = Instantiate((GameObject)Resources.Load("Prefab/HPBar"));
		HPBar.transform.SetParent(GameObject.Find("UI").transform, false);

		//経験値バーUI
		GameObject EXPBar = Instantiate((GameObject)Resources.Load("Prefab/EXPBar"));
		EXPBar.transform.SetParent(GameObject.Find("UI").transform, false);

		//HPテキスト
		GameObject HPText = Instantiate((GameObject)Resources.Load("Prefab/HPText"));
		HPText.transform.SetParent(GameObject.Find("UI").transform, false);

		//経験値テキスト
		GameObject EXPText = Instantiate((GameObject)Resources.Load("Prefab/EXPText"));
		EXPText.transform.SetParent(GameObject.Find("UI").transform, false);

		//毒
		Poison = Instantiate((GameObject)Resources.Load("Prefab/StatePoison"));
		Poison.SetActive(false);
		Poison.transform.SetParent(GameObject.Find("UI").transform, false);

		//鈍足
		SlowFoot = Instantiate((GameObject)Resources.Load("Prefab/StateSlowFoot"));
		SlowFoot.SetActive(false);
		SlowFoot.transform.SetParent(GameObject.Find("UI").transform, false);

		//やけど
		Fire = Instantiate((GameObject)Resources.Load("Prefab/StateFire"));
		Fire.SetActive(false);
		Fire.transform.SetParent(GameObject.Find("UI").transform, false);

		//背景
		BG = new GameObject("BackGround").AddComponent<Image>();

		//親設定
		BG.transform.SetParent(BCanvas.transform, false);
		BG.transform.localScale = new Vector2(12, 6.5f);

		//スプライト読み込み
		BGSprite = Resources.Load<Sprite>("Map/Back");

		//スプライト設定
		BG.sprite = BGSprite;
		Color color = BG.color;
		color.a = 0.5f;
		BG.color = color;

		//各ステータスを取得
		player = GameObject.Find("Player(Clone)");
		Player = player.GetComponent<Player>();
	}

	private void GenerateCanvas()
	{
		//Canvasアタッチ
		Canvas = gameObject.AddComponent<Canvas>();
		GameObject back = new GameObject("BCanvas");
		BCanvas = back.AddComponent<Canvas>();
		CanvasScaler scaler = back.AddComponent<CanvasScaler>();
		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;

		//カメラに表示させる
		Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		BCanvas.renderMode = RenderMode.ScreenSpaceCamera;

		//カメラ設定
		BCanvas.worldCamera = Camera.main;

		//表示順番
		Canvas.sortingOrder = 8;
		BCanvas.sortingOrder = 0;

		//表示設定
		canvasScaler = gameObject.AddComponent<CanvasScaler>();
		canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
	}

	//更新
	public void Update()
	{
		Poison.SetActive(Player.state.trap[0].isState);
		SlowFoot.SetActive(Player.state.trap[1].isState);
		Fire.SetActive(Player.state.trap[2].isState);
	}
}
