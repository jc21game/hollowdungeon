using UnityEngine;
using UnityEngine.UI;
using ScoreManager;

//画面の色変更
public class DisplayOut : MonoBehaviour
{
	//色
	public Color[] color;

	//スタート
	public void Start()
	{
		//色
		switch (Score.GetMyDungeon())
		{
			case IconBase.Icon.DUNGEON_1:
				if (Score.GetFloor() < Score.MaxFloor)
				{
					gameObject.GetComponent<Image>().color = color[0];
				}
				else
				{
					gameObject.SetActive(false);
				}
				break;

			case IconBase.Icon.DUNGEON_2:
				if (Score.GetFloor() < Score.ChangeFloor)
				{
					gameObject.GetComponent<Image>().color = color[1];
				}
				else if (Score.GetFloor() < Score.MaxFloor)
				{
					gameObject.GetComponent<Image>().color = color[2];
				}
				else
				{
					gameObject.SetActive(false);
				}
				break;
		}

	}

	//更新
	public void Update()
	{
	}
}
