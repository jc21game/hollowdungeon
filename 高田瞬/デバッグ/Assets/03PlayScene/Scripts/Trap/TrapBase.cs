using UnityEngine;

public class TrapBase
{
	//マネージャー取得用
	protected PlaySceneManager manager = null;

	//ターン数(状態異常を患うターン数)
	public int turn = 6;

	//最大
	public int Max = 5;

	//ターン数(残り何ターンで治るか)
	protected int nowTurn = 0;

	//かかっているか
	public bool isState = false;

	//初期化
	public virtual void Start()
	{
		//取得
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
	}

	//更新
	public virtual void Update()
	{

	}

	//実行
	public virtual void Execute()
	{
	}

	//ターン経過
	public virtual void Turn()
	{
		//かかっている場合
		if (nowTurn > 0)
		{
			//罹患ターン減少
			if (--nowTurn == 0)
			{
				//治る
				isState = false;
			}
		}
	}

	//ターン
	public int NowTurn()
	{
		return nowTurn;
	}
}
