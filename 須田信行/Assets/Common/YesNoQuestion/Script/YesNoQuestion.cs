using UnityEngine;
using UnityEngine.UI;

public class YesNoQuestion : MonoBehaviour
{
	//数字
	private static float Zero = 0.0f;
	private static int Thirty = 30;

	//Object
	private static GameObject Question = null;

	//クエスチョン用のCanvas : CanvasScaler : ImageBox : ImageArrow : Text1 : Text2 : BoxSprite : ArrowSprite
	private static Canvas Canvas = null;
	private static CanvasScaler CanvasScaler = null;
	private static Image ImageBox = null;
	private static Image ImageArrow = null;
	private static Text YesText = null;
	private static Text NoText = null;
	private static Sprite BoxSprite = null;
	private static Sprite ArrowSprite = null;

	//描画優先度
	private static int Priority = (int)Zero;

	//ImgageBoxのXY座標
	private static float QuestionXPosition = Zero;
	private static float QuestionYPosition = Zero;

	//IgmageBoxのサイズ
	private static float QuestionWidth = Zero;
	private static float QuestionHeight = Zero;

	//透明度
	private static float Alpha = Zero;

	//TextのXY座標
	private static float XPosition = Zero;
	private static float YPosition = Zero;

	//YesテキストのXY領域
	private static float XRegion = Zero;
	private static float YRegion = Zero;

	//フォントサイズ
	private static int FontSize = (int)Zero;

	//テキスト内容
	private static string Yes = "はい";
	private static string No = "いいえ";

	//現在の状態
	private static bool YesOrNo = true;

	//ImageArrowのXY座標
	private static float ArrowXPosition = Zero;
	private static float ArrowYPosition = Zero;

	//ImageArrowのサイズ
	private static float ArrowWidth = Zero;
	private static float ArrowHeight = Zero;

	//ImageArrowの回転
	private static float ArrowRotateZ = Zero;

	//初期化
	void Start()
	{

	}

	//変数初期化
	private static void VariableInitialize()
	{
		//Object
		Question = null;

		//クエスチョン用のCanvas : CanvasScaler : ImageBox : ImageArrow : Text1 : Text2 : BoxSprite : ArrowSprite
		Canvas = null;
		CanvasScaler = null;
		ImageBox = null;
		ImageArrow = null;
		YesText = null;
		NoText = null;
		BoxSprite = null;
		ArrowSprite = null;

		//描画優先度
		Priority = 7000;

		//ImgageBoxのXY座標
		QuestionXPosition = Screen.width / 3;
		QuestionYPosition = Zero;

		//IgmageBoxのサイズ
		QuestionWidth = Screen.width / 5;
		QuestionHeight = Screen.height / 4;

		//透明度
		Alpha = 0.7f;

		//TextのXY座標
		XPosition = QuestionWidth / 5;
		YPosition = QuestionHeight / 4;

		//YesテキストのXY領域
		XRegion = QuestionWidth / 5 * 3;
		YRegion = QuestionHeight / 2;

		//フォントサイズ
		FontSize = Screen.width / Thirty;

		//テキスト内容
		Yes = "はい";
		No = "いいえ";

		//現在の状態
		YesOrNo = true;

		//ImageArrowのXY座標
		ArrowXPosition = QuestionXPosition - QuestionWidth / 4;
		ArrowYPosition = YPosition;

		//ImageArrowのサイズ
		ArrowWidth = QuestionHeight / 2;
		ArrowHeight = QuestionWidth / 3;

		//ImageArrowの回転
		ArrowRotateZ = 180.0f;
	}

	//Canvas生成
	private static void CanvasGeneration()
	{
		//変数初期化
		VariableInitialize();

		//----------------Canvas生成部----------------//

		//オブジェクト名設定
		Question = new GameObject("YesNoQuestion");

		//Canvas獲得、生成
		Canvas = Question.AddComponent<Canvas>();

		//CanvasScaler獲得
		CanvasScaler = Question.AddComponent<CanvasScaler>();

		//描画優先度が有効なモードに変更
		Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//ReferenceResolutionの設定
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//画面サイズに合わせてサイズ調整するモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

		//ソースをアタッチ
		Question.AddComponent<YesNoQuestion>();

		//描画優先度設定
		Canvas.sortingOrder = Priority;

	}

	//Image生成
	private static void ImageGeneration()
	{
		//もしCanvasがいなければ生成
		if (Question == null)
		{
			CanvasGeneration();
		}

		//----------------ImageBox生成部----------------//

		//Imgae名設定
		ImageBox = new GameObject("YesNoImageBox").AddComponent<Image>();

		//Imageの親にCanvasを設定
		ImageBox.transform.SetParent(Canvas.transform, false);

		//Imgageの位置を設定
		ImageBox.rectTransform.anchoredPosition = new Vector2(QuestionXPosition, QuestionYPosition);

		//Imageのサイズ調整
		ImageBox.rectTransform.sizeDelta = new Vector2(QuestionWidth, QuestionHeight);

		//Imgageの透明度設定
		ImageBox.color = new Color(ImageBox.color.r, ImageBox.color.g, ImageBox.color.b, Alpha);

		//画像をImgageにアタッチ
		BoxSprite = Resources.Load<Sprite>("MessageBoxImage");
		ImageBox.sprite = BoxSprite;


		//----------------------------------------------//

		//---------------ImageArrow生成部---------------//

		//Image名設定
		ImageArrow = new GameObject("YesNoImageArrow").AddComponent<Image>();

		//Imageの親にCanvasを設定
		ImageArrow.transform.SetParent(Canvas.transform, false);

		//Imageの位置を設定
		ImageArrow.rectTransform.anchoredPosition = new Vector2(ArrowXPosition, ArrowYPosition); ;

		//Imageのサイズ調整
		ImageArrow.rectTransform.sizeDelta = new Vector2(ArrowWidth, ArrowHeight);

		//画像をImageにアタッチ
		ArrowSprite = Resources.Load<Sprite>("Arrowpng");
		ImageArrow.sprite = ArrowSprite;

		//色変更
		ImageArrow.color = Color.yellow;

		//Imageを回転
		ImageArrow.transform.Rotate(new Vector3(Zero, Zero, ArrowRotateZ));

		//----------------------------------------------//

	}

	//Text生成
	private static void TextGeneration()
	{
		//もしImgageがなかったら生成
		if (ImageBox == null)
		{
			ImageGeneration();
		}

		//----------------Text生成部----------------//

		//Text名設定
		YesText = new GameObject("YesText").AddComponent<Text>();
		NoText = new GameObject("NoText").AddComponent<Text>();

		//Imageを親に設定
		YesText.transform.SetParent(ImageBox.transform, false);
		NoText.transform.SetParent(ImageBox.transform, false);

		//Textの位置調整
		YesText.rectTransform.anchoredPosition = new Vector2(XPosition, YPosition);
		NoText.rectTransform.anchoredPosition = new Vector2(XPosition, -YPosition);

		//Text領域を変更
		YesText.rectTransform.sizeDelta = new Vector2(XRegion, YRegion);
		NoText.rectTransform.sizeDelta = new Vector2(XRegion, YRegion);

		//フォント変更
		YesText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
		NoText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

		//フォントサイズを変更
		YesText.fontSize = FontSize;
		NoText.fontSize = FontSize;

		//フォントカラーを変更
		YesText.color = Color.white;
		NoText.color = Color.white;

		//テキスト内容設定
		YesText.text = Yes;
		NoText.text = No;

		//テキストのレイアウト変更
		YesText.alignment = TextAnchor.MiddleLeft;
		NoText.alignment = TextAnchor.MiddleLeft;

	}

	//クエスチョンボックスを作成
	public static void QuestionGeneration()
	{
		//生成
		TextGeneration();

		//初期状態なので非表示
		QuestionHide();
	}

	//クエスチョンボックスを非表示
	public static void QuestionHide()
	{
		ImageBox.enabled = false;
		ImageArrow.enabled = false;
		YesText.enabled = false;
		NoText.enabled = false;

		//ImageArrowの位置を「はい」の位置に初期化
		YesOrNo = true;
	}

	//クエスチョンボックスを表示
	public static void QuestionDisplay()
	{
		ImageBox.enabled = true;
		ImageArrow.enabled = true;
		YesText.enabled = true;
		NoText.enabled = true;
	}

	//ImageArrowの位置を入れ替える
	public static void ArrowChange()
	{
		//もし「はい」の状態だったら「いいえ」にする
		if (YesOrNo == true)
		{
			//「いいえ」に変更
			YesOrNo = false;
		}
		else
		{
			//「はい」に変更
			YesOrNo = true;
		}
	}

	//はい、いいえを返す
	public static bool GetArrow()
	{
		return YesOrNo;
	}

	private void Update()
	{
		//ImageArrowの位置設定
		switch (YesOrNo)
		{
			case true:

				//ImageArrowの位置変更
				ImageArrow.rectTransform.anchoredPosition = new Vector2(ArrowXPosition, YPosition);
				break;
			case false:

				//ImageArrowの位置変更
				ImageArrow.rectTransform.anchoredPosition = new Vector2(ArrowXPosition, -YPosition);
				break;
		}
	}
}
