using System.IO;
using UnityEngine;

public class AchievementSaveLoad : MonoBehaviour
{
	//読み込み用
	public static string[] strS = new string[17];

	//格納用
	public static string[][] splitStrS = new string[17][];

	//所持してる個数
	public static int[] countS = new int[17];

	//獲得した月
	public static float[] MonthS = new float[17];

	//獲得した日
	public static float[] DayS = new float[17];

	//セーブ先のパス
	public static string[] SaveS = {"", "Save/1.txt",
									"Save/2.txt",
									"Save/4.txt",
									"Save/4.txt",
									"Save/6.txt",
									"Save/6.txt",
									"Save/7.txt",
									"Save/8.txt",
									"Save/9.txt",
									"Save/10.txt",
									"Save/11.txt",
									"Save/12.txt",
									"Save/13.txt",
									"Save/14.txt",
									"Save/15.txt",
									"Save/16.txt" };

	//初期化
	void Start()
	{
	}

	//更新
	void Update()
	{

	}

	//アチーブ情報読み込み
	public static void AchievementRoad(int number)
	{
		string path = Path.Combine(Application.streamingAssetsPath, SaveS[number]);
		strS[number] = File.ReadAllText(path);
		splitStrS[number] = strS[number].Split(',');
		countS[number] = int.Parse(splitStrS[number][0]);

		switch (number)
		{
			case 1:
				LevelOfAchievement.AchievementList.DungeonTraversal_Poison = countS[number];
				break;
			case 2:
				LevelOfAchievement.AchievementList.MonsterHunter_Poison = countS[number];
				break;
			case 3:
				LevelOfAchievement.AchievementList.Dr_Potion_Poison = countS[number];
				break;
			case 4:
				LevelOfAchievement.AchievementList.ClumsyPerson_Poison = countS[number];
				break;
			case 5:
				LevelOfAchievement.AchievementList.Diamond_Poison = countS[number];
				break;
			case 6:
				LevelOfAchievement.AchievementList.GameOver_Poison = countS[number];
				break;
			case 7:
				LevelOfAchievement.AchievementList.Talented_Poison = countS[number];
				break;
			case 8:
				LevelOfAchievement.AchievementList.GreatAdventurer_Poison = countS[number];
				break;
			case 9:
				LevelOfAchievement.AchievementList.DungeonTraversal_FlameIce = countS[number];
				break;
			case 10:
				LevelOfAchievement.AchievementList.MonsterHunter_FlameIce = countS[number];
				break;
			case 11:
				LevelOfAchievement.AchievementList.Dr_Potion_FlameIce = countS[number];
				break;
			case 12:
				LevelOfAchievement.AchievementList.ClumsyPerson_FlameIce = countS[number];
				break;
			case 13:
				LevelOfAchievement.AchievementList.Diamond_FlameIce = countS[number];
				break;
			case 14:
				LevelOfAchievement.AchievementList.GameOver_FlameIce = countS[number];
				break;
			case 15:
				LevelOfAchievement.AchievementList.Talented_FlameIce = countS[number];
				break;
			case 16:
				LevelOfAchievement.AchievementList.GreatAdventurer_FlameIce = countS[number];
				break;
		}

		MonthS[number] = int.Parse(splitStrS[number][1]);
		DayS[number] = int.Parse(splitStrS[number][2]);
	}

	//アチーブ情報書き込み
	public static void AchievementWrite(int number)
	{
		string path = Path.Combine(Application.streamingAssetsPath, SaveS[number]);
		File.WriteAllText(path, countS[number].ToString() + ',' + MonthS[number].ToString() + ',' + DayS[number].ToString());
	}
}