using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AchievementPeaple : Achievement
{
	//アチーブのオブジェクト
	private static GameObject AppealObject = null;

	//背景イメージ
	private static Image Image = null;
	private static Image AchieveTMPImage = null;

	//キャンバス
	private Canvas Canvas = null;
	private Canvas canvas = null;

	//GameObject
	private GameObject ac = null;
	private GameObject AchieveTMP = null;
	private GameObject AchieveTMPInstance = null;

	//TMP
	private TextMeshProUGUI AchievementTMP = null;

	//前の画像のパス
	private static string DuplicatePath = "";

	//現在の画像のパス
	private static string NowDuplicatePath = "";

	//初期値
	private Vector2[] initVector;
	private Color initColor;

	//画像読み込み
	private Sprite sprite = null;

	//前のパスの設定
	public static void SetDuplicatePath(string path)
	{
		DuplicatePath = path;
	}

	private static string GetDuplicatePath()
	{
		return DuplicatePath;
	}

	//現在のパスの設定
	public static void SetNowDuplicatePath(string path)
	{
		NowDuplicatePath = path;
	}

	private static string GetNowDuplicatePath()
	{
		return NowDuplicatePath;
	}

	//初期化
	protected override void Start()
	{
		ac = new GameObject("Acheve");
		canvas = ac.AddComponent<Canvas>();
		canvas.renderMode = RenderMode.ScreenSpaceCamera;
		canvas.sortingOrder = 1000000;

		//キャンバス確保
		Canvas = GameObject.Find("Canvas(Clone)").GetComponent<Canvas>();

		//TMP
		AchieveTMP = (GameObject)Resources.Load("MessageTextMeshPro");
		AchieveTMPInstance = Instantiate(AchieveTMP);
		AchievementTMP = AchieveTMPInstance.GetComponent<TextMeshProUGUI>();
		AchievementTMP.alignment = TextAlignmentOptions.Center;

		//イメージ作成
		Image = new GameObject("BackImage").AddComponent<Image>();
		BaseImage = new GameObject("AppealImage").AddComponent<Image>();
		AchieveTMPImage = new GameObject("AchievementTMPImage").AddComponent<Image>();

		//画像読み込み
		sprite = Resources.Load<Sprite>("AchieveIcon");

		//親設定
		Image.transform.SetParent(canvas.transform, false);
		BaseImage.transform.SetParent(canvas.transform, false);
		AchieveTMPImage.transform.SetParent(canvas.transform, false);
		AchievementTMP.transform.SetParent(AchieveTMPImage.transform, false);

		//初期値
		initVector = new Vector2[3];
		initVector[0] = new Vector2(0, Screen.height / 5);
		initVector[1] = new Vector2(Screen.width / 5,Screen.width / 5);
		initVector[2] = new Vector2(Screen.width / 2.5f,Screen.width / 2);
		initColor = new Color(255, 255, 255);

		//位置調整
		BaseImage.rectTransform.anchoredPosition = initVector[0];
		Image.rectTransform.anchoredPosition = new Vector2();
		AchieveTMPImage.rectTransform.anchoredPosition = new Vector2(0, -Screen.height / 5);

		//サイズ調整
		BaseImage.rectTransform.sizeDelta = initVector[1];
		Image.rectTransform.sizeDelta = initVector[2];
		AchieveTMPImage.rectTransform.sizeDelta = new Vector2(Screen.width  / 3.5f, Screen.width / 6);
		AchievementTMP.rectTransform.sizeDelta = new Vector2(Screen.width / 3.5f, Screen.width / 6);

		//透明化
		BaseImage.color = new Color(BaseImage.color.r, BaseImage.color.g, BaseImage.color.b, 0);
		AchieveTMPImage.color = new Color(Color.white.r, Color.white.g, Color.white.b, 0);

		//色設定
		Image.color = initColor;
		Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 0);

		//画像設定
		Image.sprite = sprite;

		//TMPの詳細設定
		AchievementTMP.fontSize = 45;
		AchievementTMP.color = Color.black;
		AchievementTMP.text = "";
		//AchievementTMP.alignment = ;

		//いろいろ生成
		Initialize(BaseImage, GetNowDuplicatePath());
	}

	//更新
	protected override void Update()
	{
		//パスが変わった
		if (GetDuplicatePath() != GetNowDuplicatePath())
		{
			//前のパスを更新
			SetDuplicatePath(GetNowDuplicatePath());

			//透明度変更
			BaseImage.color = new Color(BaseImage.color.r, BaseImage.color.g, BaseImage.color.b, 100);
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 100);
			AchieveTMPImage.color = new Color(AchieveTMPImage.color.r, AchieveTMPImage.color.g, AchieveTMPImage.color.b, 100);

            //テキスト内容変更
            switch (GetNowDuplicatePath())
            {
                case "TreasureChest":
                    AchievementTMP.text = "-名称-\nダンジョン踏破\n-詳細-\nダンジョンをクリア";
                    break;
                case "Trophy":
                    AchievementTMP.text = "-名称-\nモンスターハンター\n-詳細-\n全モンスターの盗伐";
                    break;
                case "Apple":
                    AchievementTMP.text = "-名称-\nポーション博士\n-詳細-\n全ポーションの使用";
                    break;
                case "Rhombus":
                    AchievementTMP.text = "-名称-\nドジっ子\n-詳細-\n2つの罠にかかる";
                    break;
                case "Heart":
                    AchievementTMP.text = "-名称-\n金剛\n-詳細-\n行動しないで5回被弾";
                    break;
                case "Weak":
                    AchievementTMP.text = "-名称-\n死んでしまった!!\n-詳細-\nゲームオーバーになる";
                    break;
                case "Medal":
                    AchievementTMP.text = "-名称-\n才能豊か\n-詳細-\n全武器でクリア";
                    break;
                case "Crown":
					AchievementTMP.text = "-名称-\n一人前の冒険者!!\n-詳細-\n7つアチーブメント取得";
					break;
			}

			//画像群セット
			Initialize(BaseImage, GetNowDuplicatePath());
		}

		if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.FADE_OUT_NOW)
		{
			canvas.enabled = false;
		}

		base.Update();
	}

	//アピールオブジェクト生成
	public static void AppealGeneration()
	{
		//アチーブの作成
		AppealObject = new GameObject("AppealObject");

		//コンポーネントを追加
		AppealObject.AddComponent<AchievementPeaple>();
	}

	//画像群セット
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	//いろいろ生成
	protected override void Initialize(Image image, string path)
	{
		base.Initialize(image, path);
	}

	//クリックされたら
	protected override void OnClick()
	{
		base.OnClick();
	}
}
