using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MessageWindowManager : MonoBehaviour
{
	//数字
	private static int Zero = 0;
	private static int One = 1;
	private static int MaxText = 5;
	private static float UpperCalculation = 10.0f;
	private static float LowerCalculation = 40.0f * 3.0f;

	//現在の画面の縦
	private static float NowScreenHeight = Zero;

	//一瞬前の画面の縦
	private static float PastScreenHeight = Zero;

	//オブジェクト
	private static GameObject MessageWindow;

	//メッセージボックス用のCanvas:Image:TextMeshPro
	//サイズ調整用のCanvasScaler
	private static GameObject TextMeshPro = null;
	private static GameObject TMPInstance = null;
	private static Canvas Canvas = null;
	private static CanvasScaler CanvasScaler = null;
	private static Image ImageBox = null;
	private static Image ImageArrow = null;
	private static TextMeshProUGUI TextMeshProUGUI = null;

	//描画優先度
	private static int Priority = Zero;

	//メッセージボックスの縦横比
	private static float MessageBoxWidth = Zero;
	private static float MessageBoxHeight = Zero;

	//メッセージボックスの位置
	private static float MessageBoxXPosition = Zero;
	private static float MessageBoxYPosition = Zero;

	//メッセージボックスの透明度
	private static float Alpha = Zero;

	//画像
	private static Sprite SpriteBox = null;
	private static Sprite SpriteArrow = null;

	//Textの位置調整
	private static float TextYPosition = Zero;

	//TextとImageの間隔
	private static float Difference = Zero;

	//Fontサイズ
	private static int FontSize = Zero;

	//テキストがセットされた回数
	private static int SetTextNumber = Zero;

	//表示するテキストの置き場所
	private static string MessageText = "";

	//メッセージボックスにセットするテキスト群
	private static string[] MessageList = { "", "", "", "", "" };

	//コマを動かす方向
	private static bool HuwahuwaState = false;

	//コマの縦横比
	private static float MessageArrowWidthHeight = Zero;

	//コマの位置
	private static float MessageArrowXPosition = Zero;
	private static float MessageArrowYPosition = Zero;

	//フワフワ上限、下限
	private static float UpperHuwahuwa = Zero;
	private static float LowerHuwahuwa = Zero;

	//フワフワ加減の除数
	private static float HuwahuwaDivisor = Zero;

	//フワフワ加減
	private static float HuwahuwaValue = Zero;

	//初期化
	void Start()
	{

	}

	//変数初期化
	private static void VariableInitialize()
	{
		//現在の画面の縦
		NowScreenHeight = Screen.height;

		//一瞬前の画面の縦
		PastScreenHeight = Screen.height;

		//描画優先度
		Priority = 7000;

		//メッセージボックスの縦横比
		MessageBoxWidth = Screen.width / 5 * 4;
		MessageBoxHeight = Screen.height / 7 * 2;

		//メッセージボックスの位置
		MessageBoxXPosition = Zero;
		MessageBoxYPosition = -(Screen.height / 2 - Screen.height / 6);

		//メッセージボックスの透明度
		Alpha = 0.7f;

		//Textの位置調整
		TextYPosition = Screen.width / 60;

		//TextとImageの間隔
		Difference = Screen.height / 10;

		//Fontサイズ
		FontSize = (int)(MessageBoxHeight / 9);

		//テキストがセットされた回数
		SetTextNumber = 0;

		//コマを動かす方向
		HuwahuwaState = false;

		//コマの縦横比
		MessageArrowWidthHeight = Screen.height / 10;

		//コマの位置
		MessageArrowXPosition = Screen.width / 3;
		MessageArrowYPosition = -Screen.height / 7 * 3;

		//フワフワ上限、下限
		UpperHuwahuwa = Screen.height / 10;
		LowerHuwahuwa = Screen.height / 40 * 3;

		//フワフワ加減の除数
		HuwahuwaDivisor = 1000.0f;

		//フワフワ加減
		HuwahuwaValue = Screen.height / HuwahuwaDivisor;
	}

	//Canvas生成
	private static void CanvasGeneration()
	{
		//変数初期化
		VariableInitialize();

		//----------------Canvas生成部----------------//

		//Canvas名設定
		MessageWindow = new GameObject("MessageCanvas");

		//Canvasを獲得、生成
		Canvas = MessageWindow.AddComponent<Canvas>();

		//CanvasScalerを獲得、生成
		CanvasScaler = MessageWindow.AddComponent<CanvasScaler>();

		//描画優先度が有効なモードに変更
		Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//ReferenceResolutionの設定
		//これを設定しないと、CanvasのデフォルトサイズがScreenサイズなのに
		//実際の画面構成はCanvasScalerのデフォルト値が反映されややこしくなる
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//画面サイズの変更に合わせて自動でサイズ変更するモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

		//MessageCanvasにこのソースをアタッチ
		MessageWindow.AddComponent<MessageWindowManager>();

		//描画優先度設定
		Canvas.sortingOrder = Priority;
	}

	//Image生成
	public static void ImageGeneration()
	{
		//MessageWindowがnullだったら
		if (MessageWindow == null)
		{
			//生成する
			CanvasGeneration();
		}

		//----------------ImageBox生成部----------------//

		//Image名設定
		ImageBox = new GameObject("MessageImage").AddComponent<Image>();

		//Imageの親にCanvasを設定
		ImageBox.transform.SetParent(Canvas.transform, false);

		//Imageの位置を設定
		ImageBox.rectTransform.anchoredPosition = new Vector2(MessageBoxXPosition, MessageBoxYPosition);

		//サイズの調整
		ImageBox.rectTransform.sizeDelta = new Vector2(MessageBoxWidth, MessageBoxHeight);

		//Imageの透明度設定
		ImageBox.color = new Color(ImageBox.color.r, ImageBox.color.g, ImageBox.color.b, Alpha);

		//画像をImageにアタッチ
		SpriteBox = Resources.Load<Sprite>("MessageBoxImageFull");
		ImageBox.sprite = SpriteBox;

		//----------------------------------------------//

		//Textがいなければ生成
		if (TextMeshProUGUI == null)
		{
			//生成
			TextGeneration();
		}
	}

	//ImageArrow生成
	public static void ArrowGeneration()
	{
		//MessageWindowがnullだったら
		if (MessageWindow == null)
		{
			//生成する
			CanvasGeneration();
		}
		//---------------Imagearrow生成部---------------//

		//Image名設定
		ImageArrow = new GameObject("MessageArrow").AddComponent<Image>();

		//Imageの親にCanvasを設定
		ImageArrow.transform.SetParent(Canvas.transform, false);

		//Imageの位置を設定
		ImageArrow.rectTransform.anchoredPosition = new Vector2(MessageArrowXPosition + Screen.width / 30, MessageArrowYPosition);

		//回転
		ImageArrow.transform.Rotate(Zero, Zero, 90);

		//サイズ調整
		ImageArrow.rectTransform.sizeDelta = new Vector2(MessageArrowWidthHeight, MessageArrowWidthHeight * 2);

		//画像をImageにアタッチ
		SpriteArrow = Resources.Load<Sprite>("Arrowpng");
		ImageArrow.sprite = SpriteArrow;
		//----------------------------------------------//
	}

	//Text内容リセット
	public static void TextReset()
	{
		//テキストセット回数リセット
		SetTextNumber = Zero;

		//セットしているテキスト内容をリセットする
		for (int i = Zero; i < MessageList.Length; i++)
		{
			MessageList[i] = "";
		}

		//テキスト反映
		TextOrganize();
	}

	//Text内容整理
	private static void TextOrganize()
	{
		//テキスト内容をリセット
		MessageText = "";

		//テキスト内容を構築
		for (int i = Zero; i < MessageList.Length; i++)
		{
			//2行目以降の処理
			if (i > Zero)
			{
				MessageText += "\n" + MessageList[i];
			}

			//1行目のみ前に改行ナシ
			else
			{
				MessageText += MessageList[i];
			}
		}

		//テキスト内容更新
		TextMeshProUGUI.text = MessageText;
	}

	//Text生成
	private static void TextGeneration()
	{
		//----------------Text生成部----------------//

		//Text名設定
		TextMeshPro = (GameObject)Resources.Load("MessageTextMeshPro");

		//インスタンス作成
		TMPInstance = Instantiate(TextMeshPro);

		//コンポーネント追加
		TextMeshProUGUI = TMPInstance.GetComponent<TextMeshProUGUI>();

		//Imageを親に設定
		TextMeshProUGUI.transform.SetParent(ImageBox.transform, false);

		//Textの位置調整
		TextMeshProUGUI.rectTransform.anchoredPosition = new Vector2(Zero, -TextYPosition);

		//Text領域を変更
		TextMeshProUGUI.rectTransform.sizeDelta = new Vector2(MessageBoxWidth - Difference, MessageBoxHeight);

		//フォントサイズを変更
		TextMeshProUGUI.fontSize = FontSize;

		//フォントカラーを変更
		TextMeshProUGUI.color = Color.white;

		//Text整理
		TextOrganize();

		//初期状態なので非表示
		MessageImageHide();
	}

	//テキスト内容変更
	public static void MessageTextChange(string message)
	{
		//ImageとArrowを表示
		MessageImageDisplay();

		//新しいテキストをセット
		//セット回数が5行以下
		if (SetTextNumber < MessageList.Length)
		{
			//セット回数行にテキストセット
			MessageList[SetTextNumber] = message;
		}
		//5行以上
		else if (SetTextNumber > MessageList.Length - One)
		{
			//行送り
			MessageLineSpacing();

			//5行目にテキストセット
			MessageList[MessageList.Length - One] = message;
		}

		//Text整理
		TextOrganize();

		//テキストセット回数整理
		if (SetTextNumber < MaxText)
		{
			//テキストセット回数増加
			SetTextNumber++;
		}
		else
		{
			//規定値に設定
			SetTextNumber = MaxText;
		}
	}

	//Image非表示
	public static void MessageImageHide()
	{
		//Imageを非表示
		ImageBox.enabled = false;

		//テキストリセット
		TextReset();
	}

	//Image表示
	public static void MessageImageDisplay()
	{
		//Imageを表示
		ImageBox.enabled = true;
	}

	//ImageArrow非表示
	public static void ImageArrowHide()
	{
		ImageArrow.enabled = false;
	}

	//ImageArrow表示
	public static void ImageArrowDisplay()
	{
		ImageArrow.enabled = true;
	}

	//行送り
	private static void MessageLineSpacing()
	{
		//テキストを並び替え
		for (int i = Zero; i < MessageList.Length - One; i++)
		{
			//テキスト内容を1行繰り上げ
			MessageList[i] = MessageList[i + One];
		}
	}

	//行流し
	public static void MessageFlowing()
	{
		//5行以降だったら5行目を空白に
		if (SetTextNumber >= MaxText)
		{
			MessageTextChange("");
		}
		//4行目以下だったら行送り
		else if (SetTextNumber < MaxText)
		{
			MessageLineSpacing();
		}

		//規定値より大きければ減少
		if (SetTextNumber > Zero)
		{
			//セット回数減少
			SetTextNumber--;
		}

		//Text整理
		TextOrganize();

		//テキストがなくなったら非表示
		if (SetTextNumber <= Zero)
		{
			MessageImageHide();
		}
	}

	//透明度を変更
	public static void AlphaChenge(float alpha)
	{
		ImageBox.color = new Color(ImageBox.color.r, ImageBox.color.g, ImageBox.color.b, alpha);
	}

	//更新
	private void Update()
	{
		//現在の画面の縦を確保
		NowScreenHeight = Screen.height;

		//画面サイズが変わった時の処理
		if (PastScreenHeight != NowScreenHeight)
		{
			//過去の画面サイズを更新
			PastScreenHeight = NowScreenHeight;

			//フワフワ加減を画面サイズに合わせて調整
			HuwahuwaValue = NowScreenHeight / HuwahuwaDivisor;

			//フワフワの上限、下限を変更
			UpperHuwahuwa = NowScreenHeight / UpperCalculation;
			LowerHuwahuwa = NowScreenHeight / LowerCalculation;
		}

		//ImageArrowがいたら
		if (ImageArrow != null)
		{
			//上へフワフワ
			if (HuwahuwaState == true)
			{
				//規定値より下なら上へフワフワ
				if (ImageArrow.rectTransform.position.y < UpperHuwahuwa)
				{
					ImageArrow.transform.position = new Vector3(ImageArrow.transform.position.x, ImageArrow.transform.position.y + HuwahuwaValue, ImageArrow.transform.position.z);
				}
				//規定値を超えたら下へフワフワさせる
				else
				{
					HuwahuwaState = false;
					ImageArrow.transform.position = new Vector3(ImageArrow.transform.position.x, UpperHuwahuwa, ImageArrow.transform.position.z);
				}
			}
			//下へフワフワ
			else if (HuwahuwaState == false)
			{
				//規定値より大きければ下へフワフワ
				if (ImageArrow.rectTransform.position.y > LowerHuwahuwa)
				{
					ImageArrow.transform.position = new Vector3(ImageArrow.transform.position.x, ImageArrow.transform.position.y - HuwahuwaValue, ImageArrow.transform.position.z);
				}
				//規定値を下回ったら上へフワフワさせる
				else
				{
					HuwahuwaState = true;
					ImageArrow.transform.position = new Vector3(ImageArrow.transform.position.x, LowerHuwahuwa, ImageArrow.transform.position.z);
				}
			}
		}
	}

	//親を無理やり変更
	public static void ChangeForciblyParent(GameObject parent)
	{
		//それぞれが存在したら
		if(ImageBox != null)
		{
			ImageBox.transform.SetParent(parent.transform, true);
		}
		if (ImageArrow != null)
		{
			ImageArrow.transform.SetParent(parent.transform, true);
		}
	}

	//戻す
	public static void ChangeForciblyParent()
	{
		if(ImageBox != null)
		{
			ImageBox.transform.SetParent(Canvas.transform, false);
		}
		if(ImageArrow != null)
		{
			ImageArrow.transform.SetParent(Canvas.transform, false);
		}
	}
}
