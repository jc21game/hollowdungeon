using UnityEngine;
using ScoreManager;

//キャラクターベース
public class CharacterBase : MonoBehaviour
{
	//オブジェクト
	protected PlaySceneManager manager = null;
	protected Map map = null;
	protected GameObject objects = null;

	//方向と足
	protected DIR dir = null;
	protected float count = 0;
	protected const float maxCount = 1;

	//スプライト
	public Sprite[] sprites;
	protected SpriteRenderer sprite = null;
	public Vector3 size;

	//ステータス
	public string NAME = "";
	public int MAX_HP = 0;
	public int HP = 0;
	public int ATC = 0;
	public int DEF = 0;
	public int EXP = 0;
	public int RANK = 0;
	public bool LIFE = false;

	//状態異常
	public State state = null;

	//現在の状態
	protected int nowState = 0;

	//攻撃中か
	public int weapon = 0;      //(NONE, 剣, 斧, 弓)
	protected bool isAttack = false;
	private bool isArrowAttack = true;
	protected float attackTime = 0;
	protected float approach = 0.25f;
	protected float attackPos = 0.25f;
	protected Vector3 basePosition;
	private int powerSward = 2;
	private int powerAxe = 3;
	private int powerArrow = 2;
	private GameObject arrow = null;
	private Vector3 arrowFinPos;
	private float arrowMaxDistance = 3;
	private float arrowSpeed = 0.5f;
	private float finPosDistance = 0.5f;
	protected bool isMove = false;

	//トレントに呼ばれたか
	public bool isColl = false;

	//経験値を落とすか
	protected bool isDropExp = true;

	//番号
	protected int number = 0;

	//単位
	protected int unit = 64;

	//視野
	protected const int view = 2;

	//初期化
	public virtual void Start()
	{
		//オブジェクト
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();
		map = GameObject.Find("Map").GetComponent<Map>();
		objects = GameObject.Find("Object");
		transform.SetParent(objects.transform, false);

		//生成
		number = manager.AddChar(gameObject.GetComponent<CharacterBase>());

		//生命
		LIFE = true;

		//方向と足
		dir = new DIR();

		//キャラクター設定
		sprite = gameObject.GetComponent<SpriteRenderer>();
		sprite.sprite = sprites[dir.GetNumber()];

		//初期設定
		transform.localScale = size;

		//表示
		sprite.sortingOrder = (int)(transform.position.x - transform.position.y * map.GetWidth());

		//初期位置
		basePosition = transform.position;

		//生成
		state = new State();

		//敵は生成時に能力を変える
		switch (NAME)
		{
			case "スライム":
				RANK = 1 * Score.GetFloor();
				MAX_HP = HP = 4 + Score.GetFloor() * RANK - 1;
				ATC = 1 + Score.GetFloor() * RANK - 1;
				DEF = 2 + Score.GetFloor() * RANK - 1;
				if(Score.GetFloor() - 1 > 0)
				{
					EXP =(int)(10 * (Score.GetFloor() - 1) * 1.6);
				}
				else
				{
					EXP = 10;
				}
				break;
			case "妖精":
				RANK = 0 * Score.GetFloor();
				MAX_HP = HP = 3 + Score.GetFloor() * Score.GetFloor() - 1;
				ATC = 0;
				DEF = 3 + Score.GetFloor() * Score.GetFloor() - 1;
				if (Score.GetFloor() - 1 > 0)
				{
					EXP = (int)(10 * (Score.GetFloor() - 1) * 1.6);
				}
				else
				{
					EXP = 10;
				}
				break;
			case "天使":
				RANK = 2 * Score.GetFloor();
				MAX_HP = HP = 5 + Score.GetFloor() * RANK - 1;
				ATC = 4 + Score.GetFloor() * RANK - 1;
				DEF = 4 + Score.GetFloor() * RANK - 1;
				if (Score.GetFloor() - 1 > 0)
				{
					EXP = (int)(25 * (Score.GetFloor() - 1) * 1.6);
				}
				else
				{
					EXP = 25;
				}
				break;
			case "ナイト":
				RANK = 3 * Score.GetFloor();
				MAX_HP = HP = 6 + Score.GetFloor() * RANK - 1;
				ATC = 5 + Score.GetFloor() * RANK - 1;
				DEF = 5 + Score.GetFloor() * RANK - 1;
				if (Score.GetFloor() - 1 > 0)
				{
					EXP = (int)(45 * (Score.GetFloor() - 1) * 1.6);
				}
				else
				{
					EXP = 45;
				}
				break;
			case "ミノタウロス":
				RANK = 4 * Score.GetFloor();
				MAX_HP = HP = 8 + Score.GetFloor() * RANK - 1;
				ATC = 7 + Score.GetFloor() * RANK - 1;
				DEF = 6 + Score.GetFloor() * RANK - 1;
				if (Score.GetFloor() - 1 > 0)
				{
					EXP = (int)(60 * (Score.GetFloor() - 1) * 1.6);
				}
				else
				{
					EXP = 60;
				}
				break;
			case "トレント(ボス)":
				RANK = 5 * Score.GetFloor();
				MAX_HP = HP = 200;
				ATC = 150;
				DEF = 150;
				EXP = 0;
				break;
			default:
				break;
		}
	}

	//更新
	public virtual void Update()
	{
		//時間測定
		if ((count -= Time.deltaTime) < 0)
		{
			//足をSTOPに設定
			dir.ResetFoot();
			sprite.sprite = sprites[dir.GetNumber()];
			count = 0;
		}

		//見た目
		sprite.sprite = sprites[dir.GetNumber()];
	}

	//実行
	public virtual bool Execute()
	{
		//処理なし
		return false;
	}

	//弓の攻撃範囲にいるか
	protected bool IsArrowRange(DIR.Dir dir)
	{
		//座標
		arrowFinPos = GetFront(dir);

		//矢を生成
		arrow = new GameObject();
		arrow.transform.position = transform.position;

		//壁かプレイヤーに当たるまでまたは3マス
		while (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= arrowMaxDistance)
		{
			//壁の場合
			if (map.GetMap(arrowFinPos) == map.mapInfomation.WALL)
			{
				//処理終了
				break;
			}

			//キャラクターかつプレイヤーの場合
			CharacterBase character = manager.GetCharacter(arrowFinPos);
			if (character != null && character.NAME == "Player")
			{
				//処理終了
				Destroy(arrow);
				return true;
			}

			//移動
			switch (dir)
			{
				case DIR.Dir.TOP: arrowFinPos.y++; break;
				case DIR.Dir.RIGHT: arrowFinPos.x++; break;
				case DIR.Dir.LEFT: arrowFinPos.x--; break;
				case DIR.Dir.BOTTOM: arrowFinPos.y--; break;
			}
		}

		//いない
		Destroy(arrow);
		return false;
	}

	//攻撃
	protected virtual bool Attack()
	{
		//攻撃中ではない
		if (!isAttack)
		{
			//足をリセット
			dir.ResetFoot();

			//攻撃範囲および攻撃
			switch (weapon)
			{
				case 1: AttackSword(); break;
				case 2: AttackAxe(); break;
				case 3: AttackArrow(); break;
			}

			//攻撃
			isAttack = true;
			if (NAME == "Player")
			{
				switch (weapon)
				{
					case 1: SoundManager.PlaySE("SwordAttack"); break;
					case 2: SoundManager.PlaySE("AxeAttack"); break;
					case 3: SoundManager.PlaySE("BowAttack"); break;
				}
			}
			else
			{
				switch (weapon)
				{
					case 1: SoundManager.PlaySE("EnemySword"); break;
					case 2: SoundManager.PlaySE("EnemyAxe"); break;
					case 3: SoundManager.PlaySE("EnemyBow"); break;
				}

				if (NAME == "トレント(ボス)")
				{
					SoundManager.PlaySE("EnemyAxe");
				}


			}
			basePosition = transform.position;
		}

		//攻撃中
		if (isAttack)
		{
			//攻撃モーション
			AttackMotion();

			//攻撃中
			return true;
		}

		//処理なし
		return false;
	}

	//攻撃(剣)
	private void AttackSword()
	{
		//座標
		Vector2 vector = GetFront();

		//敵
		CharacterBase character;

		//左正面に敵がいる場合
		switch (dir.dir)
		{
			case DIR.Dir.TOP: vector.x--; break;
			case DIR.Dir.RIGHT: vector.y++; break;
			case DIR.Dir.LEFT: vector.y--; break;
			case DIR.Dir.BOTTOM: vector.x++; break;
		}
		character = manager.GetCharacter(vector);
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward, RANK, NAME);
		}

		//正面に敵がいる場合
		character = manager.GetCharacter(GetFront());
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward, RANK, NAME);
		}

		//右正面に敵がいる場合
		vector = GetFront();
		switch (dir.dir)
		{
			case DIR.Dir.TOP: vector.x++; break;
			case DIR.Dir.RIGHT: vector.y--; break;
			case DIR.Dir.LEFT: vector.y++; break;
			case DIR.Dir.BOTTOM: vector.x--; break;
		}
		character = manager.GetCharacter(vector);
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerSward, RANK, NAME);
		}
	}

	//攻撃(斧)
	private void AttackAxe()
	{
		//正面に敵がいる場合
		CharacterBase character = manager.GetCharacter(GetFront());
		if (character != null)
		{
			//攻撃
			character.Hit(ATC + powerAxe, RANK, NAME);
		}
	}

	//攻撃(弓)
	private void AttackArrow()
	{
		//座標
		arrowFinPos = GetFront();

		//矢を生成
		arrow = Instantiate((GameObject)Resources.Load("Prefab/Arrow"));
		arrow.transform.position = transform.position;
		arrow.GetComponent<SpriteRenderer>().sortingOrder = gameObject.GetComponent<SpriteRenderer>().sortingOrder;
		isArrowAttack = true;

		//回転
		switch (dir.dir)
		{
			case DIR.Dir.TOP: arrow.transform.Rotate(new Vector3(0, 0, 90)); break;
			case DIR.Dir.RIGHT: arrow.transform.Rotate(new Vector3(0, 0, 0)); break;
			case DIR.Dir.LEFT: arrow.transform.Rotate(new Vector3(0, 0, 180)); break;
			case DIR.Dir.BOTTOM: arrow.transform.Rotate(new Vector3(0, 0, 270)); break;
		}

		//壁かキャラクターに当たるまでまたは3マス
		while (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= arrowMaxDistance)
		{
			//壁の場合
			if (map.GetMap(arrowFinPos) == map.mapInfomation.WALL)
			{
				//処理終了
				break;
			}

			//キャラクターの場合
			CharacterBase character = manager.GetCharacter(arrowFinPos);
			if (character != null)
			{
				//攻撃
				character.Hit(ATC + powerArrow, RANK, NAME);

				//処理終了
				break;
			}

			//移動
			switch (dir.dir)
			{
				case DIR.Dir.TOP: arrowFinPos.y++; break;
				case DIR.Dir.RIGHT: arrowFinPos.x++; break;
				case DIR.Dir.LEFT: arrowFinPos.x--; break;
				case DIR.Dir.BOTTOM: arrowFinPos.y--; break;
			}
		}
	}

	//攻撃モーション
	private void AttackMotion()
	{
		//迫る
		if ((attackTime += Time.deltaTime) <= approach)
		{
			//座標
			Vector3 vector = basePosition;

			//方向
			switch (dir.dir)
			{
				//上
				case DIR.Dir.TOP:
					vector.y += attackPos;
					break;

				//右
				case DIR.Dir.RIGHT:
					vector.x += attackPos;
					break;

				//左
				case DIR.Dir.LEFT:
					vector.x -= attackPos;
					break;

				//下
				case DIR.Dir.BOTTOM:
					vector.y -= attackPos;
					break;
			}

			//近づく
			transform.position = vector;
		}

		//終了
		else
		{
			//元の位置
			transform.position = basePosition;

			//武器が弓以外
			if (weapon != (int)IconBase.Icon.BOW)
			{
				//初期化
				attackTime = 0;
				isAttack = false;
			}

			//弓
			else if (!isArrowAttack)
			{
				//初期化
				attackTime = 0;
				isAttack = false;
			}
		}

		//矢
		if (weapon == (int)IconBase.Icon.BOW && isArrowAttack)
		{
			//位置
			Vector3 vector = arrow.transform.position;

			//移動
			switch (dir.dir)
			{
				case DIR.Dir.TOP: vector.y += arrowSpeed; break;
				case DIR.Dir.RIGHT: vector.x += arrowSpeed; break;
				case DIR.Dir.LEFT: vector.x -= arrowSpeed; break;
				case DIR.Dir.BOTTOM: vector.y -= arrowSpeed; break;
			}
			arrow.transform.position = vector;

			//到着
			if (Mathf.Sqrt(Mathf.Abs((arrow.transform.position - arrowFinPos).sqrMagnitude)) <= finPosDistance)
			{
				//矢の処理終了
				isArrowAttack = false;
				Destroy(arrow);
			}
		}
	}

	//移動
	protected virtual bool Move()
	{
		//処理なし
		return false;
	}

	//攻撃を受ける
	public virtual void Hit(int damage, int rank, string name, string text = "")
	{
		//定数
		float LV = Score.GetLv();
		float A = damage;
		float B = DEF;
		float FLOOR = Score.GetFloor();
		float WEAPON = 0;

		switch (Score.GetMyWeapon())
		{
			case IconBase.Icon.SWORD:
				WEAPON = 2;
				break;
			case IconBase.Icon.AXE:
				WEAPON = 3;
				break;
			case IconBase.Icon.BOW:
				WEAPON = 1;
				break;
		}

		//ダメージ量
		int plDM = (int)(A * LV / B + A * LV * 0.1 / B + (WEAPON + LV));
		int enDM = (int)(A / (B * 0.1) / (10 - FLOOR) * FLOOR) + rank;

		//メッセージ
		if (name == "")
		{
			//メッセージ送信
			manager.SetMessage(text);

			//更新
			ScoreManager.Score.SetCoveredDamage(damage);
		}

		//プレイヤー
		else if (NAME == "Player")
		{
			//メッセージ送信
			manager.SetMessage("プレイヤーは" + name + "から" + enDM.ToString() + "ポイントのダメージを受けた。");

			//更新
			ScoreManager.Score.SetCoveredDamage(enDM);
		}

		//プレイヤーの攻撃
		else if (name == "Player")
		{
			//メッセージ送信
			manager.SetMessage("プレイヤーは" + NAME + "に" + plDM.ToString() + "ポイントのダメージを与えた。");

			//更新
			ScoreManager.Score.SetGivingDamage(plDM);
		}

		//敵
		else if (name != "")
		{
			//メッセージ送信
			manager.SetMessage(name + "は" + NAME + "に" + enDM.ToString() + "ポイントのダメージを与えた。");
			isDropExp = false;
		}

		//自身がトレント
		if (NAME == "トレント(ボス)")
		{
			isDropExp = false;
		}

		//ダメージを受ける
		if(NAME == "Player" && name == "")
		{
			if ((HP -= damage) <= 0)
			{
				//死亡
				HP = 0;
				LIFE = false;

				//プレイヤーの攻撃によって倒された場合
				if (name == "Player")
				{
					//更新
					ScoreManager.Score.SetKillNum(1);
				}
			}
		}
		else if (NAME == "Player")
		{
			if ((HP -= enDM) <= 0)
			{
				//死亡
				HP = 0;
				LIFE = false;

				//プレイヤーの攻撃によって倒された場合
				if (name == "Player")
				{
					//更新
					ScoreManager.Score.SetKillNum(1);
				}
			}
		}
		else
		{
			if ((HP -= plDM) <= 0)
			{
				//死亡
				HP = 0;
				LIFE = false;
			}
		}
	}

	//死亡
	public virtual void Death()
	{
		//経験値取得
		if (isDropExp)
		{
			manager.GetCharacter(0).GetComponent<Player>().GetEXP(EXP);
		}

		//死亡判定
		Score.HuntEnemy(NAME);

		//死亡
		Destroy(gameObject);
	}

	//回復
	public virtual void Heal(int heal)
	{
		//回復量
		int healPoint = heal;
		if (HP + heal > MAX_HP)
		{
			heal = MAX_HP - HP;
		}

		//SE再生
		SoundManager.PlaySE("Heal");

		//回復
		if (NAME == "Player")
		{
			if (HP == MAX_HP)
			{
				manager.SetMessage("プレイヤーはHPが満タンだった！！");
			}
			else
			{
				manager.SetMessage("プレイヤーは" + heal + "ポイントHPを回復した！！");
			}
		}
		else
		{
			if (HP == MAX_HP)
			{
				manager.SetMessage(NAME + "はHPが満タンだった！！");
			}
			else
			{
				manager.SetMessage(NAME + "は" + heal + "ポイントHPを回復した！！");
			}
		}
		HP += heal;
	}

	//視野内にPLいるか
	protected bool IsPlayer()
	{
		//自身のマスが部屋で、プレイヤーの乗っているマスと同じ部屋だった場合
		if (map.GetMap(transform.position) >= 2 && map.GetMap(transform.position) == map.GetMap(manager.GetPosition(0)))
		{
			//いる
			return true;
		}

		//正方形内にプレイヤーがいる場合
		for (int x = -view; x <= view; x++)
		{
			for (int y = -view; y <= view; y++)
			{
				if (manager.GetPosition(0).x == transform.position.x + x && manager.GetPosition(0).y == transform.position.y + y)
				{
					//いる
					return true;
				}
			}
		}

		//いない
		return false;
	}

	//番号取得
	public int GetNumber()
	{
		return number;
	}

	//目の前の座標
	protected Vector2 GetFront(bool isReverse = false)
	{
		//座標
		return GetFront(dir.dir, isReverse);
	}
	protected Vector2 GetFront(DIR.Dir dir, bool isReverse = false)
	{
		//座標
		Vector2 vector = transform.position;

		//方向
		int dirction = 1;

		//逆方向
		if (isReverse)
		{
			//逆方向
			dirction = -1;
		}

		//方向
		switch (dir)
		{
			//上
			case DIR.Dir.TOP:
				vector.y += dirction;
				break;

			//右
			case DIR.Dir.RIGHT:
				vector.x += dirction;
				break;

			//左
			case DIR.Dir.LEFT:
				vector.x -= dirction;
				break;

			//下
			case DIR.Dir.BOTTOM:
				vector.y -= dirction;
				break;
		}

		//座標
		return vector;
	}

	//方向と足
	protected class DIR
	{
		//方向
		public enum Dir
		{
			TOP = 0,
			RIGHT = 3,
			LEFT = 6,
			BOTTOM = 9
		}
		public Dir dir = Dir.BOTTOM;
		public const int DIRCTIONS = 4;

		//足
		private enum Foot
		{
			STOP = 0,
			RIGHT,
			LEFT,
			MAX
		}
		private Foot foot = Foot.STOP;

		//方向チェンジ
		public void DirChange(Dir dir)
		{
			//方向が切り替わる場合
			if (this.dir != dir)
			{
				//足リセット
				ResetFoot();
			}

			//方向チェンジ
			this.dir = dir;
		}

		//足を進める
		public void AddFoot()
		{
			//足変更
			if (++foot == Foot.MAX)
			{
				//右に設定
				foot = Foot.RIGHT;
			}
		}

		//足リセット
		public void ResetFoot()
		{
			//停止に設定
			foot = Foot.STOP;
		}

		//番号取得
		public int GetNumber()
		{
			//方向 + 足
			return (int)dir + (int)foot;
		}

		//0〜3を方向に変換
		public Dir ConvertDir(int i)
		{
			//番号
			switch (i)
			{
				case 0: i = 0; break;
				case 1: i = 3; break;
				case 2: i = 6; break;
				case 3: i = 9; break;
			}

			//終了
			return (Dir)i;
		}
	}

	//混乱時の移動
	protected bool ConfMove()
	{
		//移動先
		dir.dir = dir.ConvertDir(Random.Range(0, DIR.DIRCTIONS));

		if (map.IsMove(GetFront(dir.dir)))
		{
			//カウント開始
			count = maxCount;

			//移動
			dir.DirChange(dir.dir);
			dir.AddFoot();
			transform.position = GetFront(dir.dir);
			manager.Move();

			//向き
			sprite.sprite = sprites[dir.GetNumber()];

			//処理終了
			return true;
		}

		//処理なし
		return Move();
	}

	//状態異常管理
	public class State
	{
		//状態
		public TrapBase[] trap = null;

		//健康
		public int health = 0;

		//毒
		public int poison = 1;

		//鈍足
		public int slow = 2;

		//炎(やけど)
		public int burn = 3;

		//氷(滑る)
		public int slide = 4;

		//コンストラクタ
		public State()
		{
			trap = new TrapBase[4];
			trap[0] = new Poison();
			trap[1] = new Slow();
			trap[2] = new Burn();
			trap[3] = new Slide();
		}
	}
}