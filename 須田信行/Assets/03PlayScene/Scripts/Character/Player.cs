using ScoreManager;
using UnityEngine;

//プレイヤー
public class Player : CharacterBase
{
	//ステータス
	public int LV = 0;
	public int nextEXP = 0;
	public int autoHEAL = 0;
	public int autoHEALTurn = 0;
	public int skillPoint = 0;
	public int h = 0;
	public int a = 0;
	public int d = 0;

	//残りターン数(鈍足)
	private int slowTurn = 0;

	//状態異常フラグ(0:かかっていない　1:行動できるターン　2:行動できないターン)
	private int slowFlg = 0;

	//前回の行動
	private BeforeAct act = null;
	public int action = 0;

	//ダッシュ
	public bool isDush = false;

	//カメラ
	private Camera mainCamera = null;
	private int camPos = -10;

	//アイテム
	public Item item = null;

	//ステータス上昇値
	public int UP_HP = 0;
	public int UP_AT = 0;
	public int UP_DF = 0;
	public float UP_NE = 0;

	//行動しない
	private int stopTime = 0;

	//最終フロアの初期位置
	public Vector3 initPos;

	//デバッグフラグ
	private bool dFlg = false;

	//初期化
	public override void Start()
	{
		//親
		base.Start();

		//アイテム
		item = new Item(manager);

		//武器の設定
		SetWeapon();

		//前回の行動
		act = new BeforeAct();

		//階層が5階の場合
		if (Score.GetFloor() == Score.MaxFloor)
		{
			//座標変更
			transform.position = basePosition = initPos;

			//ミニマップ更新
			manager.Move();
		}

		//カメラ
		mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		CameraMove();
	}

	//更新
	public override void Update()
	{
		//スコアマネージャーにデータを渡す(基礎ステータス)
		SetBaseStatus();

		//デバック対応キー
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			item.GREEN++;
			item.RED++;
			item.BLUE++;
			item.WHITE++;
		}
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			HP++;
			MAX_HP = HP;
		}
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			ATC++;
		}
		if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			DEF++;
		}
		if (Input.GetKeyDown(KeyCode.Alpha5))
		{
			skillPoint++;
		}
		if (Input.GetKeyDown(KeyCode.Alpha6))
		{
			HP++;
			MAX_HP = HP;
			ATC++;
			DEF++;
		}
		if (Input.GetKeyDown(KeyCode.Alpha7))
		{
			LV = 100;
			HP = 999;
			MAX_HP = HP;
			ATC = 999;
			DEF = 999;
			dFlg = true;
		}
		if (Input.GetKeyDown(KeyCode.Alpha8))
		{
			HP = MAX_HP;
		}

		//親
		base.Update();
	}

	//実行
	public override bool Execute()
	{
		//外部から呼び出された場合
		if (isColl)
		{
			//行動なし
			isColl = false;
			manager.Move();
			return true;
		}

		//鈍足になったターン
		if ((state.trap[1].isState && state.trap[1].NowTurn() == state.trap[1].Max))
		{
			//フラグ変更
			slowFlg = 1;

			//ターン数セット
			slowTurn = state.trap[1].NowTurn();
		}

		//治ったら(鈍足)
		if (state.trap[1].isState == false)
		{
			slowFlg = 0;
			slowTurn = 0;
		}

		//行動できる/できない切り替え(鈍足)
		if (slowTurn - 1 == state.trap[1].NowTurn())
		{
			//ターン数更新
			slowTurn = state.trap[1].NowTurn();

			//フラグ変更
			switch (slowFlg)
			{
				case 1:
					slowFlg = 2;
					manager.SetMessage("鈍足状態(残り" + state.trap[1].NowTurn().ToString() + "ターン)のため行動できない！！");
					SoundManager.PlaySE("Slow");
					break;
				case 2:
					slowFlg = 1;
					manager.SetMessage("鈍足状態(残り" + state.trap[1].NowTurn().ToString() + "ターン)");
					manager.SetMessage("鈍足状態(残り" + state.trap[1].NowTurn().ToString() + "ターン)");
					break;
			}
		}

		//かかっていない　または　かかっているが行動できるターン
		if (slowFlg == 0 || slowFlg == 1)
		{
			//メニューを開いていない
			if (action != act.MENU)
			{
				//攻撃
				if (Attack())
				{
					//攻撃中
					if (isAttack)
					{
						//行動中
						return false;
					}

					//行動終了
					return true;
				}

				//移動
				if (Move())
				{
					//行動終了
					return true;
				}

				//待機
				if (Input.GetKey(KeyCode.C))
				{
					//前回の行動がSTOP以外
					if (action != act.STOP)
					{
						//行動をSTOPに変更
						action = act.STOP;

						//回数初期化
						stopTime = 0;
					}

					//行動終了
					return true;
				}
			}

			//メニュー
			if (Menu())
			{
				//行動終了
				return true;
			}
		}
		else if (slowFlg == 2)
		{
			//行動終了
			return true;
		}

		//処理なし
		return base.Execute();
	}

	//メニュー
	private bool Menu()
	{
		//Xキー入力かメニュー
		if (Input.GetKeyDown(KeyCode.X) || action == act.MENU)
		{
			//メニュー
			if (GameObject.Find("Menu(Clone)").GetComponent<Menu>().Execute())
			{
				//行動終了
				return true;
			}

			//行動
			action = GameObject.Find("Menu(Clone)").GetComponent<Menu>().GetState();
		}

		//処理なし
		return false;
	}

	//攻撃
	protected override bool Attack()
	{
		//Z入力または攻撃中
		if (Input.GetKeyDown(KeyCode.Z) || isAttack)
		{
			//行動変更
			action = act.ATTACK;

			//攻撃
			return base.Attack();
		}

		//処理なし
		return false;
	}

	//移動
	protected override bool Move()
	{
		//座標を記憶
		Vector3 pos = transform.position;

		//向き変えをするか
		bool isChange = Input.GetKey(KeyCode.LeftShift);

		//前行動が移動じゃない場合
		if (action != act.MOVE)
		{
			//足をリセット
			dir.ResetFoot();
		}

		//ダッシュ中ではなく複数キー入力している場合じゃない場合
		if (!isDush && (
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.LeftArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)) ||
			(Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.RightAlt) && Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.DownArrow)))
			)
		{
			//処理なし
			isDush = false;
			sprite.sprite = sprites[dir.GetNumber()];
			return base.Move();
		}

		//上
		if ((Input.GetKey(KeyCode.UpArrow) && !isDush && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.TOP))
		{
			//上
			pos.y++;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.TOP);
		}

		//右
		if ((Input.GetKey(KeyCode.RightArrow) && !isDush && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.RIGHT))
		{
			//右
			pos.x++;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.RIGHT);
		}

		//左
		if ((Input.GetKey(KeyCode.LeftArrow) && !isDush && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.LEFT))
		{
			//左
			pos.x--;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.LEFT);
		}

		//下
		if ((Input.GetKey(KeyCode.DownArrow) && !isDush && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow)) || (isDush && dir.dir == (CharacterBase.DIR.Dir)DIR.Dir.BOTTOM))
		{
			//下
			pos.y--;

			//移動
			dir.DirChange(CharacterBase.DIR.Dir.BOTTOM);
		}

		//移動可能だった場合(向きのみ変更ではない場合)
		if (map.IsMove((Vector2)pos) && !isChange)
		{
			//移動
			transform.position = pos;
			dir.AddFoot();
			sprite.sprite = sprites[dir.GetNumber()];

			//氷チェック(スライド)
			IceCheck();

			//表示
			sprite.sortingOrder = (int)(transform.position.x - transform.position.y * unit);

			//カメラ追従
			CameraMove();

			//カウント開始
			count = maxCount;

			//行動
			action = act.MOVE;

			//ダッシュに切り替え
			if (Input.GetKey(KeyCode.LeftControl))
			{
				//ダッシュ
				isDush = true;
			}

			//ダッシュ中
			if (isDush)
			{
				//八方
				Vector2[] vector = new Vector2[8] { transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position };
				vector[0].y++; vector[1].x++; vector[2].x--; vector[3].y--;
				vector[4].y++; vector[4].x++;
				vector[5].y++; vector[5].x--;
				vector[6].y--; vector[6].x++;
				vector[7].y--; vector[7].x--;

				//八方にオブジェクトが存在するか
				for (int i = 0; i < 8; i++)
				{
					//オブジェクト
					for (int j = 0; j < manager.GetMaxChar(); j++)
					{
						//いる場合
						if (vector[i] == manager.GetPosition(j) || vector[i] == map.GetStairsPosition() || (Vector2)transform.position == map.GetStairsPosition())
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}

					//アイテム
					for (int j = 0; j < map.maxPotions; j++)
					{
						//いる場合
						if (vector[i] == (Vector2)map.potions[j].transform.position || (Vector2)transform.position == (Vector2)map.potions[j].transform.position)
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}

					//ギミック
					if (map.GetTrap((int)vector[i].x, -(int)vector[i].y) > 0)
					{
						//ダッシュ終了
						isDush = false;
						break;
					}
				}

				//通路の本数
				int aisleNum = 0;

				//自身が通路で後ろも通路、前が壁の場合
				if (map.GetMap(transform.position) == map.mapInfomation.AISLE &&
					map.GetMap(GetFront(true)) == map.mapInfomation.AISLE &&
					map.GetMap(GetFront()) == map.mapInfomation.WALL)
				{
					//四方
					for (int i = 0; i < DIR.DIRCTIONS; i++)
					{
						//後ろの場合
						if (vector[i] == GetFront(true))
						{
							//処理なし
							continue;
						}

						//通路の場合
						if (map.GetMap(vector[i]) == map.mapInfomation.AISLE)
						{
							//通路の本数を増やす
							aisleNum++;
						}
					}

					//通路の本数が1本の場合
					if (aisleNum == 1)
					{
						//四方
						for (int i = 0; i < DIR.DIRCTIONS; i++)
						{
							//後ろの場合
							if (vector[i] == GetFront(true))
							{
								//処理なし
								continue;
							}

							//通路の場合
							if (map.GetMap(vector[i]) == map.mapInfomation.AISLE)
							{
								//方向
								switch (i)
								{
									case 0: dir.dir = CharacterBase.DIR.Dir.TOP; break;
									case 1: dir.dir = CharacterBase.DIR.Dir.RIGHT; break;
									case 2: dir.dir = CharacterBase.DIR.Dir.LEFT; break;
									case 3: dir.dir = CharacterBase.DIR.Dir.BOTTOM; break;
								}
								break;
							}
						}
					}
				}

				//方向を変えていない場合
				if (aisleNum != 1)
				{
					//移動可能か
					for (int i = 0; i < DIR.DIRCTIONS; i++)
					{
						//自身の左右が通路の場合
						//または自身と自身の目の前が一緒ではなく自身の前が壁ではない場合
						//または自身と自身の後ろが一緒ではなく自身の後ろが通路で壁ではない場合
						if ((
							(dir.dir == DIR.Dir.TOP || dir.dir == DIR.Dir.BOTTOM) &&
							(map.GetMap(GetFront(DIR.Dir.LEFT)) == map.mapInfomation.AISLE || map.GetMap(GetFront(DIR.Dir.RIGHT)) == map.mapInfomation.AISLE)
							) ||
							(
							(dir.dir == DIR.Dir.LEFT || dir.dir == DIR.Dir.RIGHT) &&
							(map.GetMap(GetFront(DIR.Dir.TOP)) == map.mapInfomation.AISLE || map.GetMap(GetFront(DIR.Dir.BOTTOM)) == map.mapInfomation.AISLE)
							) ||
							(
							((map.GetMap(transform.position) != map.GetMap(GetFront()) && map.GetMap(GetFront()) != map.mapInfomation.WALL) ||
							(map.GetMap(transform.position) != map.GetMap(GetFront(true)) && map.GetMap(GetFront(true)) == map.mapInfomation.AISLE)
							)
							))
						{
							//ダッシュ終了
							isDush = false;
							break;
						}
					}
				}
			}

			//ダメージマスを踏んだら
			if (map.GetTrap((int)transform.position.x, -(int)transform.position.y) == 1)
			{
				if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1)
				{
					//状態更新(毒状態)
					state.trap[0].Execute();
					manager.SetMessage("プレイヤーは毒になった！！");
				}
				else
				{
					//状態更新(やけど状態)
					state.trap[2].Execute();
					manager.SetMessage("プレイヤーはやけどになった！！");
				}

				//更新
				Score.SetCovredTrap(1);
				Score.OnTrap(0);

				//ダッシュ終了
				isDush = false;
			}

			//スローマスを踏んだら
			if (map.GetTrap((int)transform.position.x, -(int)transform.position.y) == 2)
			{
				//状態更新(鈍足状態)
				state.trap[1].Execute();
				manager.SetMessage("プレイヤーは鈍足になった！！(鈍足状態(残り" + (state.trap[1].NowTurn() - 1).ToString() + "ターン)");
				SoundManager.PlaySE("Slow");

				//更新
				Score.SetCovredTrap(1);
				Score.OnTrap(1);

				//ダッシュ終了
				isDush = false;
			}

			//行動終了
			return true;
		}

		//処理なし
		isDush = false;
		sprite.sprite = sprites[dir.GetNumber()];
		return base.Move();
	}

	//氷チェック(スライド)
	private void IceCheck()
	{
		//プレイヤーの移動反映
		manager.MovePlayer();

		//自身のマスが氷か(true)
		if (map.GetTrap((int)transform.position.x, -(int)transform.position.y) == 3)
		{
			//状態更新(スライド状態)
			state.trap[3].Execute();

			//更新
			Score.SetCovredTrap(1);
			Score.OnTrap(1);

			SoundManager.PlaySE("Ice");

			//次のマスが壁か(false)
			if (map.IsMove(GetFront()))
			{
				//移動
				transform.position = GetFront();
				IceCheck();
			}
		}
	}

	//攻撃を受ける
	public override void Hit(int damage, int rank, string name, string text = "")
	{
		//ダッシュが切れる
		isDush = false;

		//行動がSTOP
		if (action == act.STOP)
		{
			stopTime++;
			Score.Vajra(stopTime);
		}

		//攻撃を受ける
		base.Hit(damage, rank, name, text);
	}

	//死亡
	public override void Death()
	{
	}

	//経験値の取得
	public void GetEXP(int EXP)
	{
		//メッセージ送信
		manager.SetMessage("プレイヤーは" + EXP.ToString() + "の経験値を手に入れた。");

		//取得した経験値
		Score.SetHaveAllExp(EXP);

		//経験値取得
		if ((this.EXP += EXP) >= nextEXP)
		{
			//経験値分
			while (this.EXP >= nextEXP)
			{
				//上昇
				LV++;

				//ステータスを最大にしていない場合
				if (!dFlg)
				{
					switch (Score.GetMyWeapon())
					{
						case IconBase.Icon.SWORD:
							MAX_HP = 20 + (LV - 1) * 5 + h;
							HP = Score.GetHp() + 5;
							ATC = 5 + (LV - 1) * 5 + a;
							DEF = 5 + (LV - 1) * 7 + d;
							break;
						case IconBase.Icon.AXE:
							MAX_HP = 20 + (LV - 1) * 5 + h;
							HP = Score.GetHp() + 5;
							ATC = 5 + (LV - 1) * 7 + a;
							DEF = 5 + (LV - 1) * 5 + d;
							break;
						case IconBase.Icon.BOW:
							MAX_HP = 20 + (LV - 1) * 7 + h;
							HP = Score.GetHp() + 7;
							ATC = 5 + (LV - 1) * 5 + a;
							DEF = 5 + (LV - 1) * 5 + d;
							break;
					}
				}

				this.EXP -= nextEXP;
				nextEXP = (int)(nextEXP * 1.1);
				skillPoint += 2;

			}
			//サウンド
			SoundManager.SoundManagerGeneration();

			//再生
			SoundManager.PlaySE("LevelUp");

			//メッセージ送信
			manager.SetMessage("プレイヤーはレベル" + LV + "に上がった!");
		}
	}

	//武器の設定
	private void SetWeapon()
	{
		//武器パス
		string path = "Player/IMG_0";
		int baseNumber = 101;
		int imageNumber = 12;

		//基礎ステータス取得
		GetBaseStatus();

		//画像のアタッチ
		for (int i = 0; i < imageNumber; i++)
		{
			//アタッチ
			sprites[i] = Resources.Load<Sprite>(path + (baseNumber + i + (weapon - 1) * imageNumber).ToString());
		}
	}

	//カメラ追従
	private void CameraMove()
	{
		//カメラの位置
		Vector3 pos = transform.position;
		pos.z = camPos;

		//カメラの移動
		mainCamera.transform.position = pos;
	}

	//自身の周りで一番若いキャラを返す(妖精以外)
	public int GetAroundCharacter()
	{
		//番号
		int ret = 100;

		//八方
		Vector2[] vector = new Vector2[8] { transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position, transform.position };
		vector[0].y++; vector[1].x++; vector[2].x--; vector[3].y--;
		vector[4].y++; vector[4].x++;
		vector[5].y++; vector[5].x--;
		vector[6].y--; vector[6].x++;
		vector[7].y--; vector[7].x--;

		//周り
		for (int i = 0; i < 8; i++)
		{
			//オブジェクト
			for (int j = 0; j < manager.GetMaxChar(); j++)
			{
				//いる場合
				if (vector[i] == manager.GetPosition(j))
				{
					if (manager.GetCharacter(vector[i]).GetNumber() < ret && manager.GetCharacter(vector[i]).NAME != "妖精")
					{
						ret = manager.GetCharacter(vector[i]).GetNumber();
					}
				}
			}
		}

		//いない
		if (ret == 100)
		{
			return 0;
		}
		return ret;
	}

	//ポーション経過
	public void AddTurn()
	{
		//アイテムのターン経過
		if (item.turnR > 0)
		{
			if (--item.turnR == 0)
			{
				ATC -= manager.potions[1].GetComponent<ItemBase>().efficacy;
			}
		}

		if (item.turnB > 0)
		{
			if (--item.turnB == 0)
			{
				DEF -= manager.potions[2].GetComponent<ItemBase>().efficacy;
			}
		}
	}

	//前回の行動
	private class BeforeAct
	{
		//なし
		public int NONE = 0;

		//移動
		public int MOVE = 1;

		//攻撃
		public int ATTACK = 2;

		//メニュー
		public int MENU = 3;

		//待機
		public int STOP = 4;
	}

	//アイテム
	public class Item
	{
		private PlaySceneManager manager = null;

		//回復
		public int GREEN = 0;
		public string GREEN_POTION = "緑色のポーション";

		//攻撃
		public int RED = 0;
		public int turnR = 0;
		public string RED_POTION = "赤色のポーション";

		//防御
		public int BLUE = 0;
		public int turnB = 0;
		public string BLUE_POTION = "青色のポーション";

		//経験値
		public int YELLOW = 0;
		public string YELLOW_POTION = "黄色のポーション";

		//スキルポイント
		public int WHITE = 0;
		public string WHITE_POTION = "白色のポーション";

		//コンストラクタ
		public Item(PlaySceneManager manager)
		{
			this.manager = manager;
		}

		//アイテム名取得
		public string GetItemName(int number)
		{
			//アイテム
			switch (number)
			{
				case 0:
					return GREEN_POTION;
				case 1:
					return RED_POTION;
				case 2:
					return BLUE_POTION;
				case 3:
					return YELLOW_POTION;
				case 4:
					return WHITE_POTION;
				default:
					return null;
			}
		}

		//アイテム取得
		public void GetItem(int number)
		{
			//アイテム
			SoundManager.PlaySE("Get");
			switch (number)
			{
				case 0:
					GREEN++;
					break;
				case 1:
					RED++;
					break;
				case 2:
					BLUE++;
					break;
				case 4:
					WHITE++;
					break;
			}
		}

		//アイテム使用
		public void UseItem(int number)
		{
			//アイテム
			MessageWindowManager.TextReset();
			switch (number)
			{
				case 0:
					GREEN--;
					manager.SetMessage("プレイヤーは" + GREEN_POTION + "を使用した。");
					Score.AddUseGreenPotion();
					break;
				case 1:
					RED--;
					manager.SetMessage("プレイヤーは" + RED_POTION + "を使用した。");
					Score.AddUseRedPotion();
					SoundManager.PlaySE("Heal");
					break;
				case 2:
					BLUE--;
					manager.SetMessage("プレイヤーは" + BLUE_POTION + "を使用した。");
					Score.AddUseBluePotion();
					SoundManager.PlaySE("Heal");
					break;
				case 3:
					WHITE--;
					manager.SetMessage("プレイヤーは" + WHITE_POTION + "を使用した。");
					Score.AddUseWhitePotion();
					SoundManager.PlaySE("Heal");
					break;
			}

			//効能実行
			manager.potions[number].GetComponent<ItemBase>().Execute();
			Debug.Log("");
		}
	}

	//基礎ステータス渡し
	public void SetBaseStatus()
	{
		//レベル
		Score.SetLv(LV);

		//HP
		Score.SetMHp(MAX_HP);
		Score.SetHp(HP);

		//攻撃力
		Score.SetAttack(ATC);

		//防御力
		Score.SetDefence(DEF);

		//スキルポイント
		Score.SetSkillPt(skillPoint);

		//緑色ポーションの所持数
		Score.SetGreenPotion(item.GREEN);

		//赤色ポーションの所持数
		Score.SetRedPotion(item.RED);

		//青色ポーションの所持数
		Score.SetBluePotion(item.BLUE);

		//黄色ポーションの所持数
		Score.SetYellowPotion(item.YELLOW);

		//白色ポーションの所持数
		Score.SetWhitePotion(item.WHITE);

		//取得した経験値
		Score.SetHaveExp(EXP);

		//次のレベルまでの経験値
		Score.SetNextExp(nextEXP);

		//上昇能力
		Score.SetAddHP(h);
		Score.SetAddATC(a);
		Score.SetAddDEF(d);
	}

	//基礎ステータス取得
	public void GetBaseStatus()
	{
		//レベル
		LV = Score.GetLv();

		//HP
		MAX_HP = Score.GetMHp();
		HP = Score.GetHp();

		//攻撃力
		ATC = Score.GetAttack();

		//防御力
		DEF = Score.GetDefence();

		//スキルポイント
		skillPoint = Score.GetSkillPt();

		//緑色ポーションの所持数
		item.GREEN = Score.GetGreenPotion();

		//赤色ポーションの所持数
		item.RED = Score.GetRedPotion();

		//青色ポーションの所持数
		item.BLUE = Score.GetBluePotion();

		//黄色ポーションの所持数
		item.YELLOW = Score.GetYellowPotion();

		//白色ポーションの所持数
		item.WHITE = Score.GetWhitePotion();

		//取得した経験値
		EXP = Score.GetHaveExp();

		//次のレベルまでの経験値
		nextEXP = Score.GetNextExp();

		//武器
		weapon = (int)Score.GetMyWeapon();

		//上昇能力
		h = Score.GetAddHP();
		a = Score.GetAddATC();
		d = Score.GetAddDEF();
	}
}
