using UnityEngine;
using UnityEngine.UI;

public class ATurn : MonoBehaviour
{
	//プレイヤー
	private Player player = null;
	//自身のテキスト
	private Text text = null;

	//初期化
	private void Start()
	{
		//プレイヤー取得
		player = GameObject.Find("Player(Clone)").GetComponent<Player>();

		//自身
		text = gameObject.GetComponent<Text>();
	}

	//更新
	public void Update()
	{
		//ターン
		text.text = player.item.turnR.ToString();
	}
}
