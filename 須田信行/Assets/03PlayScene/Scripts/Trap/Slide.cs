public class Slide : TrapBase
{
	//初期化
	public override void Start()
	{
		//罹患ターン設定
		turn = 1;

		//マネージャー取得
		base.Start();
	}

	//更新
	public override void Update()
	{

	}

	//実行
	public override void Execute()
	{
		//罹患ターンをセット
		nowTurn = turn;

		//かかる
		isState = true;
	}
}
