using UnityEngine;

//防御ポーション
public class DeffensePotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.DEFFENCE);
		efficacy = 5;
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//防御
		Player player = GameObject.Find("Player(Clone)").GetComponent<Player>();
			Debug.Log("a");
		if (player.item.turnB == 0)
		{
			efficacy = (int)((float)player.LV / 2 * 5);
			player.DEF += efficacy;
		}
		player.item.turnB = Max;
	}
}
