using UnityEngine;

//スキルポーション
public class SkillPotion : ItemBase
{
	//初期化
	public override void Start()
	{
		efficacy = 3;
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//スキルポイント
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).GetComponent<Player>().skillPoint += efficacy;
	}
}
