using UnityEngine;
using UnityEngine.UI;

public class EXPBar : MonoBehaviour
{
	//コンポーネント取得用
	private Slider expBar = null;
	public LevelText lvText = null;

	//経験値管理用
	private int exp = 0;

	//次のレベルまでの経験値管理用
	private int nextExp = 0;

	//マネージャー
	private PlaySceneManager manager = null;

	//初期化
	public void Start()
	{
		//コンポーネント取得
		expBar = this.GetComponent<Slider>();
		lvText = GameObject.Find("LevelText(Clone)").GetComponent<LevelText>();

		//マネージャー
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();

		//初期化
		exp = 0;
		nextExp = manager.GetCharacter(0).GetComponent<Player>().nextEXP;
	}

	//更新
	public void Update()
	{
		//プレイヤーの経験値確認
		exp = manager.GetCharacter(0).EXP;
		nextExp = manager.GetCharacter(0).GetComponent<Player>().nextEXP;

		//経験値を設定
		expBar.value = exp;

		//次のレベルまでの経験値を設定
		expBar.maxValue = nextExp;
	}

	//経験値を取得
	public int GetExp() { return exp; }

	//次のレベルまでの経験値取得
	public int GetNextExp() { return nextExp; }
}
