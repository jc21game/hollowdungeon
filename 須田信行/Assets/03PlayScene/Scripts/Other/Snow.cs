using UnityEngine;

//雪の落下
public class Snow : MonoBehaviour
{
	//位置
	private RectTransform snow = null;

	//落下速度
	public float speedX = 0;
	public float speedY = 0;
	public float valX = 0;
	public float valY = 0;

	//生存位置
	private (float min, float max) x = (0, 0);
	private (float min, float max) y = (0, 0);

	//スタート
	public void Start()
	{
		//生存位置
		x = (-560, 650);
		y = (350, -350);

		//アタッチ
		snow = gameObject.GetComponent<RectTransform>();

		//初期位置
		snow.anchoredPosition = new Vector3(Random.Range(x.min, x.max), y.min, 0);

		//サイズ
		float size = Random.Range(0, 0.1f);
		speedY -= (snow.sizeDelta.y - size) / 2f;
		snow.sizeDelta = new Vector2(snow.sizeDelta.x - size, snow.sizeDelta.y - size);

		//落下方向
		speedX -= Random.Range(0, valX);
	}

	//更新
	public void Update()
	{
		//落下
		Vector3 vector = snow.anchoredPosition;
		vector.x -= speedX;
		vector.y -= speedY;
		snow.anchoredPosition = vector;

		//生存限界
		if (vector.y <= y.max)
		{
			Destroy(gameObject);
		}
	}
}
