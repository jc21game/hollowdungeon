using UnityEngine;
using ScoreManager;

//エフェクトを発生させる
public class MakeEffect : MonoBehaviour
{
	//プレハブ
	public GameObject snow = null;
	public GameObject fire = null;
	public int probability = 0;

	//スタート
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//生成
		if (Random.Range(0, 100) >= 100 - probability)
		{
			//氷
			if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_2 && Score.GetFloor() < Score.ChangeFloor)
			{
				GameObject game = Instantiate(snow);
				game.transform.SetParent(transform);
			}
			//炎
			else if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_2 && Score.GetFloor() < Score.MaxFloor)
			{
				GameObject game = Instantiate(fire);
				game.transform.SetParent(transform);
			}
		}
	}
}
