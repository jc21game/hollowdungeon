using UnityEngine;

//右矢印
public class GaidoRight : MonoBehaviour
{
	//ガイドスクリプト
	private GaidoScript gaido = null;

	//マネージャー
	private TitleSceneManager manager = null;

	//スタート
	public void Start()
	{
		//ガイドスクリプト
		gaido = GameObject.Find("Gaido").GetComponent<GaidoScript>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	public void Update()
	{
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//ページを進める
		gaido.GoPage();
		manager.SelectMenu();
	}
}
