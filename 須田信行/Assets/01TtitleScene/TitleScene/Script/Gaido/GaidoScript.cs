using UnityEngine;
using UnityEngine.UI;

//ガイドの処理
public class GaidoScript : MonoBehaviour
{
	//ページ
	public Sprite[] pages = null;
	public string[] description = null;
	private int page = 0;
	private Image image = null;

	//矢印
	private GaidoLeft left = null;
	private GaidoRight right = null;

	//スタート
	public void Start()
	{
		//イメージ
		image = gameObject.GetComponent<Image>();

		//矢印
		left = GameObject.Find("GaidoLeft").GetComponent<GaidoLeft>();
		right = GameObject.Find("GaidoRight").GetComponent<GaidoRight>();
	}

	//更新
	public void Update()
	{
		//画像の切り替え
		image.sprite = pages[page];

		//両方アクティブ
		left.gameObject.SetActive(true);
		right.gameObject.SetActive(true);

		//矢印の非アクティブ化
		if (page == 0)
		{
			left.gameObject.SetActive(false);
		}
		if (page == pages.Length - 1)
		{
			right.gameObject.SetActive(false);
		}

		//テキスト表示
		MessageWindowManager.TextReset();
		string[] str = description[page].Split('.');
		for (int i = 0; i < str.Length; i++)
		{
			MessageWindowManager.MessageTextChange(str[i]);
		}
	}

	//ページを進める
	public void GoPage()
	{
		//進める
		page++;
	}

	//ページを戻す
	public void BackPage()
	{
		//戻す
		page--;
	}

	//ページリセット
	public void ResetPage()
	{
		page = 0;
	}
}
