using UnityEngine;

//左矢印
public class GaidoLeft : MonoBehaviour
{
	//ガイドスクリプト
	private GaidoScript gaido = null;

	//マネージャー
	private TitleSceneManager manager = null;

	//スタート
	public void Start()
	{
		//ガイドスクリプト
		gaido = GameObject.Find("Gaido").GetComponent<GaidoScript>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	public void Update()
	{
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//ページを戻す
		gaido.BackPage();
		manager.SelectMenu();
	}
}
