using UnityEngine;

public class ConfigButton : MonoBehaviour
{
	//ボタン
	private GameObject outArea = null;

	//設定画面
	private GameObject config = null;

	//ほかのボタンの状態
	private GaidoButton GaidoButton = null;
	private AchievementButton AchievementButton = null;
	private GameEndButton GameEndButton = null;

	//表示フラグ
	public bool flg = false;

	//マネージャー
	private TitleSceneManager manager = null;

	//初期化
	void Start()
	{
		//取得
		outArea = GameObject.Find("OutArea");
		config = GameObject.Find("Config");
		GaidoButton = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();
		AchievementButton = GameObject.Find("AchievementButton").GetComponent<AchievementButton>();
		GameEndButton = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();

		//非表示
		outArea.SetActive(flg);
		config.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		outArea.SetActive(flg);
		config.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//ほかのボタンがOnじゃない
		if (GaidoButton.flg != true && AchievementButton.flg != true && GameEndButton.flg != true)
		{
			//フラグ切り替え
			if (flg)
				flg = false;
			else
				flg = true;
			manager.SelectMenu();
		}
	}
}
