using UnityEngine;

public class GameEndArea : MonoBehaviour
{
	//ボタン
	private GameEndButton button = null;

	//マネージャー
	private TitleSceneManager manager = null;

	//初期化
	private void Start()
	{
		//取得
		button = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	private void Update()
	{

	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//フラグ変更
		button.flg = false;
		manager.SelectMenu();
	}
}
