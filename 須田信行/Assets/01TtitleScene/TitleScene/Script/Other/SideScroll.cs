	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

//横スクロール
public class SideScroll : MonoBehaviour
{
	//再生成位置
	private const float speed = 3;
	private const float limit = -24.47f;
	private const float move = 24.86f;

	private TitleSceneManager Title;

	//初期化
	public void Start()
	{
		Title = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();
	}

	//更新
	public void Update()
	{
		if (Title.IsScroll)
		{
			//移動
			transform.position -= new Vector3(Time.deltaTime * speed, 0);
			if (transform.position.x <= limit)
			{
				transform.position = new Vector3(move, 0);
			}
		}
	}
}
