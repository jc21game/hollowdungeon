using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    private GameObject PickUpArea = null;

    //設定画面
    private GameObject PickUpButton = null;

    //表示フラグ
    public bool flg = false;

    //初期化
    void Start()
    {
        //取得
        PickUpArea = GameObject.Find("PickUpArea");
        PickUpButton = GameObject.Find("PickUp");


        PickUpArea.SetActive(flg);
        PickUpButton.SetActive(flg);
    }

    //更新
    void Update()
    {
        //表示切り替え
        PickUpArea.SetActive(flg);
        PickUpButton.SetActive(flg);
    }
}
