using LevelOfAchievement;
using UnityEngine;
using UnityEngine.UI;

public class Duplicate : Achievement
{
	//前の画像のパス
	private static string DuplicatePath = "";

	//現在の画像のパス
	private static string NowDuplicatePath = "";

	//所持フラグ
	private bool Flg = false;

	//前のパスの設定
	public static void SetDuplicatePath(string path)
	{
		DuplicatePath = path;
	}

	private static string GetDuplicatePath()
	{
		return DuplicatePath;
	}

	//現在のパスの設定
	public static void SetNowDuplicatePath(string path)
	{
		NowDuplicatePath = path;
	}

	private static string GetNowDuplicatePath()
	{
		return NowDuplicatePath;
	}

	//初期化
	protected override void Start()
	{
		base.Start();
	}

	//更新
	protected override void Update()
	{
		//画像のパスが違ったら
		if (GetDuplicatePath() != GetNowDuplicatePath())
		{
			//明るさ初期化
			Flg = false;

			//前のパスを更新
			SetDuplicatePath(GetNowDuplicatePath());

			//テキスト更新
			AchieveTMP.SetNowAchievement(GetNowDuplicatePath());

			//いろいろ生成
			Initialize(BaseImage, NowDuplicatePath);

			//現在のページ
			if (Title.Icon == IconBase.Icon.DUNGEON_1)
			{
				//所持していたら明るい
				switch (GetDuplicatePath())
				{
					case "TreasureChest":
						if (AchievementList.GetDungeonTraversal_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Trophy":
						if (AchievementList.GetMonsterHunter_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Apple":
						if (AchievementList.GetDr_Potion_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Rhombus":
						if (AchievementList.GetClumsyPerson_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Heart":
						if (AchievementList.GetDiamond_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Weak":
						if (AchievementList.GetGameOver_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Medal":
						if (AchievementList.GetTalented_Poison() > 0)
						{
							Flg = true;
						}
						break;
					case "Crown":
						if (AchievementList.GetGreatAdventurer_Poison() > 0)
						{
							Flg = true;
						}
						break;
				}
			}
			else if(Title.Icon == IconBase.Icon.DUNGEON_2)
			{
				//所持していたら明るい
				switch (GetDuplicatePath())
				{
					case "TreasureChest":
						if (AchievementList.GetDungeonTraversal_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Trophy":
						if (AchievementList.GetMonsterHunter_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Apple":
						if (AchievementList.GetDr_Potion_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Rhombus":
						if (AchievementList.GetClumsyPerson_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Heart":
						if (AchievementList.GetDiamond_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Weak":
						if (AchievementList.GetGameOver_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Medal":
						if (AchievementList.GetTalented_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
					case "Crown":
						if (AchievementList.GetGreatAdventurer_FlameIce() > 0)
						{
							Flg = true;
						}
						break;
				}
			}
			

			if (Flg)
			{
				BaseImage.color = Color.white;
			}
			else
			{
				BaseImage.color = Color.black;
			}
		}

		base.Update();

	}

	//いろいろ生成
	protected override void Initialize(Image image, string path)
	{
		base.Initialize(image, path);
	}

	//画像格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}
}
