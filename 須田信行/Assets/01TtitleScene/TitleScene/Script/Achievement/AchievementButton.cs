using UnityEngine;

public class AchievementButton : MonoBehaviour
{
	private GameObject AchievementArea = null;

	//設定画面
	private GameObject Achievement = null;

	//ほかのボタン
	private ConfigButton ConfigButton = null;
	private GaidoButton GaidoButton = null;
	private GameEndButton GameEndButton = null;

	//マネージャー
	private TitleSceneManager manager = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//取得
		AchievementArea = GameObject.Find("AchievementArea");
		Achievement = GameObject.Find("Achievement");
		ConfigButton = GameObject.Find("ConfigButton").GetComponent<ConfigButton>();
		GaidoButton = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();
		GameEndButton = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
		manager = GameObject.Find("TitleSceneManager").GetComponent<TitleSceneManager>();

		AchievementArea.SetActive(flg);
		Achievement.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		AchievementArea.SetActive(flg);
		Achievement.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//ほかのボタンがOnじゃない
		if (ConfigButton.flg != true && GaidoButton.flg != true && GameEndButton.flg != true)
		{
			//フラグ切り替え
			if (flg)
				flg = false;
			else
				flg = true;
			manager.SelectMenu();
		}
	}
}
