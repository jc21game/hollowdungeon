﻿using System;

namespace ConsoleApp1
{
	class Program
	{
		//フィールド
		private enum FIELD
		{
			WALL,
			FLOOR,
			ROOM,
			TEST_MAX
		}
		private FIELD[,] field;
		public int fieldSize = 24;

		//フィールドサイズ(正方形)
		public int size = 128;

		//線を引く方向
		private enum LineDir
		{
			VERTICAL,
			HORIZONTAL
		}

		//フィールド生成用位置(LEFT, TOP, RIGHT, BOTTOM)
		private enum Rect
		{
			LEFT,
			TOP,
			RIGHT,
			BOTTOM,
			RECT_MAX
		}
		private int[,] rect;
		int[,] smallRect;

		//部屋の個数と通路同士の最低距離と通路から部屋までの距離
		private int rooms = 0;
		public int distanceToAisle = 10;
		public int betweenAisleAndRoom = 4;

		//テスト
		static void Main(string[] args)
		{
			Program program = new Program();
		}

		//コンストラクタ
		public Program()
		{
			//部屋数設定
			//rooms = Random.Range(5, 9);
			rooms = new Random().Next(5, 9);
			rooms = 5;

			//フィールド作成(部屋数 * フィールドの広さ)
			field = new FIELD[size, size];
			Create();
		}

		//フィールド作成
		private void Create()
		{
			//フィールド生成用位置初期化({ { Left, Top }, { Right, Bottom } })
			rect = new int[2, 2] { { 0, 0 }, { size, size } };

			//通路の区切りを付けた際の狭い方
			smallRect = new int[2, 2];

			//通路方向(垂直方向からスタート)
			LineDir line = LineDir.VERTICAL;

			//通路位置
			int aislePos = 0;

			//ランダム
			Random random = new Random();

			//部屋の位置
			int left = 0;
			int right = 0;
			int top = 0;
			int bottom = 0;

			//フィールド初期化
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					field[i, j] = FIELD.WALL;
				}
			}

			//通路の作成(通路 = 部屋数 - 1)
			for (int i = 0; i < rooms - 1; i++)
			{
				//smallRectに元のrectをコピー
				for(Rect r = 0; r < Rect.RECT_MAX; r++)
				{
					//コピー
					SetSmallRect(r, GetRect(r));
				}

				//生成方向
				switch (line)
				{
					//垂直
					case LineDir.VERTICAL:
					{
						//位置設定
						aislePos = random.Next(GetRect(Rect.TOP) + distanceToAisle, GetRect(Rect.BOTTOM) - distanceToAisle);

						//通路作成
						for (int j = GetRect(Rect.LEFT) + 1; j < GetRect(Rect.RIGHT); j++)
						{
							//壁を通路に置き換え
							field[aislePos, j] = FIELD.FLOOR;
						}

						//生成幅を狭める
						//下側が広い場合
						if (aislePos - GetRect(Rect.TOP) <= GetRect(Rect.BOTTOM) - aislePos)
						{
							//rectのTOPをposに更新
							SetRect(Rect.TOP, aislePos);

							//smallRectのBOTTOMをrectのTOPに更新
							SetSmallRect(Rect.BOTTOM, GetRect(Rect.TOP));
						}

						//上側が広い場合
						else
						{
							//rectのBottomをposに更新
							SetRect(Rect.BOTTOM, aislePos);

							//smallRectのTOPをrectのBOTTOMに更新
							SetSmallRect(Rect.TOP, GetRect(Rect.BOTTOM));
						}

						//切り替え
						line = LineDir.HORIZONTAL;
						break;
					}

					//水平
					case LineDir.HORIZONTAL:
					{
						//位置設定
						aislePos = random.Next(GetRect(Rect.LEFT) + distanceToAisle, GetRect(Rect.RIGHT) - distanceToAisle);

						//通路作成
						for(int j = GetRect(Rect.TOP) + 1; j < GetRect(Rect.BOTTOM); j++)
						{
							//壁を通路に置き換え
							field[j, aislePos] = FIELD.FLOOR;
						}

						//生成幅を狭める
						//右側が広い場合
						if(aislePos - GetRect(Rect.LEFT) <= GetRect(Rect.RIGHT) - aislePos)
						{
							//rectのLEFTをposに更新
							SetRect(Rect.LEFT, aislePos);

							//smallRectのRIGHTをrectのLEFTに更新
							SetSmallRect(Rect.RIGHT, GetRect(Rect.LEFT));
						}

						//左側が広い場合
						else
						{
							//rectのRIGHTをposに更新
							SetRect(Rect.RIGHT, aislePos);

							//smallRectのLEFTをrectのRIGHTに更新
							SetSmallRect(Rect.LEFT, GetRect(Rect.RIGHT));
						}

						//切り替え
						line = LineDir.VERTICAL;
						break;
					}
				}

				//部屋の作成
				//全部屋中今回生成しようとしている部屋の位置が一番左の場合
				if(GetSmallRect(Rect.LEFT) == 0)
				{
					//一マス分左から部屋を生成
					left = random.Next(GetSmallRect(Rect.LEFT) + betweenAisleAndRoom - 1,
						(GetSmallRect(Rect.RIGHT) - GetSmallRect(Rect.LEFT)) / 2 + GetSmallRect(Rect.LEFT) - betweenAisleAndRoom);
				}
				//一番左ではない場合
				else
				{
					//普通に生成
					left = random.Next(GetSmallRect(Rect.LEFT) + betweenAisleAndRoom,
						(GetSmallRect(Rect.RIGHT) - GetSmallRect(Rect.LEFT)) / 2 + GetSmallRect(Rect.LEFT) - betweenAisleAndRoom);
				}
				right = random.Next((GetSmallRect(Rect.RIGHT) - GetSmallRect(Rect.LEFT)) / 2 + GetSmallRect(Rect.LEFT) + betweenAisleAndRoom,
					GetSmallRect(Rect.RIGHT) - betweenAisleAndRoom);

				//全部屋中今回生成しようとしている部屋の位置が一番上の場合
				if (GetSmallRect(Rect.TOP) == 0)
				{
					//一マス分上から部屋を生成
					top = random.Next(GetSmallRect(Rect.TOP) + betweenAisleAndRoom - 1,
						(GetSmallRect(Rect.BOTTOM) - GetSmallRect(Rect.TOP)) / 2 + GetSmallRect(Rect.TOP) - betweenAisleAndRoom);
				}

				//一番上ではない場合
				else
				{
					//普通に生成
					top = random.Next(GetSmallRect(Rect.TOP) + betweenAisleAndRoom,
						(GetSmallRect(Rect.BOTTOM) - GetSmallRect(Rect.TOP)) / 2 + GetSmallRect(Rect.TOP) - betweenAisleAndRoom);
				}
				bottom = random.Next((GetSmallRect(Rect.BOTTOM) - GetSmallRect(Rect.TOP)) / 2 + GetSmallRect(Rect.TOP) + betweenAisleAndRoom,
					GetSmallRect(Rect.BOTTOM) - betweenAisleAndRoom);

				//部屋の作成
				for (int r = left; r <= right; r++)
				{
					for(int s = top; s <= bottom; s++)
					{
						//壁を部屋に書き換え
						field[s, r] = FIELD.ROOM;
					}
				}
			}

			//最後の一部屋の作成
			//全部屋中今回生成しようとしている部屋の位置が一番左の場合
			if (GetSmallRect(Rect.LEFT) == 0)
			{
				//一マス分左から部屋を生成
				left = random.Next(GetRect(Rect.LEFT) + betweenAisleAndRoom - 1,
					(GetRect(Rect.RIGHT) - GetRect(Rect.LEFT)) / 2 + GetRect(Rect.LEFT) - betweenAisleAndRoom);
			}
			//一番左ではない場合
			else
			{
				//普通に生成
				left = random.Next(GetRect(Rect.LEFT) + betweenAisleAndRoom,
					(GetRect(Rect.RIGHT) - GetRect(Rect.LEFT)) / 2 + GetRect(Rect.LEFT) - betweenAisleAndRoom);
			}
			right = random.Next((GetRect(Rect.RIGHT) - GetRect(Rect.LEFT)) / 2 + GetRect(Rect.LEFT) + betweenAisleAndRoom,
				GetRect(Rect.RIGHT) - betweenAisleAndRoom);

			//全部屋中今回生成しようとしている部屋の位置が一番上の場合
			if (GetRect(Rect.TOP) == 0)
			{
				//一マス分上から部屋を生成
				top = random.Next(GetRect(Rect.TOP) + betweenAisleAndRoom - 1,
					(GetRect(Rect.BOTTOM) - GetRect(Rect.TOP)) / 2 + GetRect(Rect.TOP) - betweenAisleAndRoom);
			}

			//一番上ではない場合
			else
			{
				//普通に生成
				top = random.Next(GetRect(Rect.TOP) + betweenAisleAndRoom,
					(GetRect(Rect.BOTTOM) - GetRect(Rect.TOP)) / 2 + GetRect(Rect.TOP) - betweenAisleAndRoom);
			}
			bottom = random.Next((GetRect(Rect.BOTTOM) - GetRect(Rect.TOP)) / 2 + GetRect(Rect.TOP) + betweenAisleAndRoom,
				GetRect(Rect.BOTTOM) - betweenAisleAndRoom);

			//部屋の作成
			for (int r = left; r <= right; r++)
			{
				for (int s = top; s <= bottom; s++)
				{
					//壁を部屋に書き換え
					field[s, r] = FIELD.ROOM;
				}
			}

			//描画
			Draw();

			Console.WriteLine(size);
		}

		//テスト
		private void Draw()
		{
			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					switch(field[i, j])
					{
						case FIELD.WALL: Console.Write("■");break;
						case FIELD.FLOOR: Console.Write("□");break;
						case FIELD.ROOM: Console.Write("◇");break;
					}
				}
				Console.WriteLine();
			}
			Console.WriteLine();
		}

		//rectの中身取得
		private int GetRect(Rect r)
		{
			//対象
			switch (r)
			{
				case Rect.LEFT: return rect[0, 0];
				case Rect.TOP: return rect[0, 1];
				case Rect.RIGHT: return rect[1, 0];
				case Rect.BOTTOM: return rect[1, 1];
				default:return 0;
			}
		}

		//rectの中身設定
		private void SetRect(Rect r, int i)
		{
			//対象
			switch (r)
			{
				case Rect.LEFT: rect[0, 0] = i;break;
				case Rect.TOP: rect[0, 1] = i; break;
				case Rect.RIGHT: rect[1, 0] = i; break;
				case Rect.BOTTOM: rect[1, 1] = i; break;
				default: break;
			}
		}

		//smallRectの中身取得
		private int GetSmallRect(Rect r)
		{
			//対象
			switch (r)
			{
				case Rect.LEFT: return smallRect[0, 0];
				case Rect.TOP: return smallRect[0, 1];
				case Rect.RIGHT: return smallRect[1, 0];
				case Rect.BOTTOM: return smallRect[1, 1];
				default:return 0;
			}
		}

		//smallRectの中身設定
		private void SetSmallRect(Rect r, int i)
		{
			//対象
			switch (r)
			{
				case Rect.LEFT: smallRect[0, 0] = i;break;
				case Rect.TOP: smallRect[0, 1] = i; break;
				case Rect.RIGHT: smallRect[1, 0] = i; break;
				case Rect.BOTTOM: smallRect[1, 1] = i; break;
				default: break;
			}
		}
	}
}
