using UnityEngine;
using SceneManager;

//スプラッシュ
public class SplashSceneManager : MonoBehaviour
{
	//フェード時間 Unity側で設定
	public float FadeInTime = 0;
	public float FadeOutTime = 0;
	
	//FPS
	private readonly int FPS = 60;

	//アチーブメント数
	private readonly int AchievementNumber = 17;

	private Scene Scene = null;

	//初期化
	public void Start()
	{
		//フレームレート設定
		Application.targetFrameRate = FPS;

		//シーンマネージャー
		Scene = new Scene();
		Scene.SetNowScene(SCENES.SPLASH_SCENE);

		//最初真っ暗な画面にする
		FadeScript.Darkening();

		//フェードインでスプラッシュシーンを表示する
		FadeScript.FadeIn();

		//アチーブメントの読み込み
		for (int i = 1; i < AchievementNumber; i++)
		{
			AchievementSaveLoad.AchievementRoad(i);
		}

		//FadeOutTime秒後にフェードアウトする
		Invoke("FadeOutPlay", FadeOutTime);

		//FadeInTime秒後にタイトルに移動する
		Invoke("ChangeTitle", FadeInTime);

	}

	//更新
	public void Update()
	{
		//Zキー入力でスプラッシュを飛ばす
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//タイトルシーンに移動
			Scene.SetNextScene(SCENES.TITLE_SCENE);
		}
	}

	//タイトルシーンに移動する関数
	public void ChangeTitle()
	{
		//タイトルシーンに移動
		Scene.SetNextScene(SCENES.TITLE_SCENE);
	}

	//フェードアウトする関数
	public void FadeOutPlay()
	{
		//フェードアウトする
		FadeScript.FadeOut();
	}
}
