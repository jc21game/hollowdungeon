using UnityEngine;
using UnityEngine.UI;

//タイトル変動
public class SelectImage : MonoBehaviour
{
	//画像
	public Sprite[] sprites = null;

	//初期化
	void Start()
	{
		//ランダムで指定
		gameObject.GetComponent<Image>().sprite = sprites[Random.Range(0, sprites.Length)];
	}

	//更新
	void Update()
	{
	}
}
