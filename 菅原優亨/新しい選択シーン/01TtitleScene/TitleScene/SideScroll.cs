using UnityEngine;

//横スクロール
public class SideScroll : MonoBehaviour
{
	//再生成位置
	private float speed = 3;

	//限界位置
	private float LimitPosition = -24.47f;

	//変更位置
	private float Position = 24.86f;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//移動
		transform.position -= new Vector3(Time.deltaTime * speed, 0);

		//規定値を超過したら位置を更新
		if (transform.position.x <= LimitPosition)
		{
			transform.position = new Vector3(Position, 0);
		}

	}
}
