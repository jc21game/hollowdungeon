using UnityEngine;

public class YesButton : MonoBehaviour
{
	//ボタン
	private GaidoButton button = null;

	public void Start()
	{
		//取得
		button = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();
	}

	public void OnClick()
	{
		Application.OpenURL("https://youtu.be/ROS1XS_mUd4");

		//フラグ変更
		button.flg = false;
	}
}
