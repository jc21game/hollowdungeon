using UnityEngine;

public class GaidoArea : MonoBehaviour
{
	//ボタン
	private GaidoButton button = null;

	//初期化
	void Start()
	{
		//取得
		button = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();
	}

	//更新
	void Update()
	{
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//フラグ変更
		button.flg = false;
	}
}