using TMPro;
using UnityEngine;

public class DungeonNameTMP : MonoBehaviour
{
	//自身のオブジェクト
	private TextMeshProUGUI DungeonName = null;

	//アチーブ
	private TitleAchievement TitleAchievement = null;

	//初期化
	void Start()
	{
		//確保
		DungeonName = GameObject.Find("DungeonNameTMP").GetComponent<TextMeshProUGUI>();
		TitleAchievement = GameObject.Find("Achievement").GetComponent<TitleAchievement>();
	}

	//更新
	void Update()
	{
		if (TitleAchievement.Icon == IconBase.Icon.DUNGEON_1)
		{
			DungeonName.text = "毒沼のダンジョン";
		}
		else if (TitleAchievement.Icon == IconBase.Icon.DUNGEON_2)
		{
			DungeonName.text = "氷炎のダンジョン";
		}
	}
}
