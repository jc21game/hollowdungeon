using UnityEngine;

public class LeftArrowButton : MonoBehaviour
{
	//左矢印
	private GameObject LeftArrow = null;

	//オブジェクト
	private TitleAchievement TitleAchievement = null;

	//表示フラグ
	public bool flg = false;

	//初期化
	void Start()
	{
		//取得
		LeftArrow = GameObject.Find("LeftArrow");
		TitleAchievement = GameObject.Find("Achievement").GetComponent<TitleAchievement>();

		//アクティブ反映
		LeftArrow.SetActive(flg);
	}

	//更新
	void Update()
	{
		//表示切り替え
		LeftArrow.SetActive(flg);
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		TitleAchievement.Icon = IconBase.Icon.DUNGEON_1;
	}
}
