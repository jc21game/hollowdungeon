using UnityEngine;

public class PickUpArea : MonoBehaviour
{
	//ボタン
	private PickUp button = null;

	//初期化
	void Start()
	{
		//取得
		button = GameObject.Find("PickUp").GetComponent<PickUp>();
	}

	//更新
	void Update()
	{
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//フラグ変更
		button.flg = false;
	}
}
