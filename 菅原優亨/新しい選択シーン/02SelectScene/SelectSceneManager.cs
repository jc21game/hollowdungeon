using SceneManager;
using UnityEngine;
using UnityEngine.UI;

public class SelectSceneManager : MonoBehaviour
{
	//数字
	private static readonly float Zero = 0;
	private static readonly float Five = 5.0f;
	private static readonly float Ten = 10.0f;
	private static readonly float Twelve = 12.0f;
	private static readonly float Thirteen = 13.0f;
	private static readonly float Twenty = 21.0f;
	private static readonly float Inversion = 180.0f;

	//選択状態
	public enum SelectState
	{
		NONE,               //待機
		WEAPON_SELECT,      //武器選択
		DUNGEON_SELECT,     //ダンジョン選択
		RETURN_SELECT,      //選択を戻る
		END                 //終了
	}
	private static SelectState State = SelectState.NONE;

	//決定した武器
	private static IconBase.Icon Weapon = IconBase.Icon.NONE;

	//決定したダンジョン
	private static IconBase.Icon Dungeon = IconBase.Icon.NONE;

	//現在の選択アイコン
	private static IconBase.Icon NowIcon = IconBase.Icon.NONE;

	//シーンマネージャー
	private static Scene scene = null;

	//ゲームオブジェクト
	private static GameObject CanvasObject = null;

	//表示用Canvas : CanvasScaler
	private static Canvas WeaponSelectCanvas = null;
	private static CanvasScaler CanvasScaler = null;

	//タイトルテキスト
	private static Text TitleText = null;

	//タイトルテキストのXY座標
	private static float TitleXPosition = Zero;
	private static float TitleYPosition = Zero;

	//タイトルテキストの領域
	private static float TitleYRegion = Zero;

	//矢印
	private static Image LeftArrow = null;
	private static Image RightArrow = null;

	//矢印のY座標
	private static float ArrowYPosition = Zero;

	//左矢印のX座標
	private static float ArrowXPosition = Zero;

	//左矢印のサイズ
	private static float ArrowWidth = Zero;
	private static float ArrowHeight = Zero;

	//右矢印の回転角度
	private static float RightArrowRotateX = Zero;
	private static float RightArrowRotateZ = Zero;

	//フォントサイズ
	private static int FontSize = (int)Zero;

	//テキスト
	private static readonly string SelectWeaponText = "武器選択";
	private static readonly string SelectDungeonText = "ダンジョン選択";

	//質問文
	private static readonly string WeaponDecision = "武器を決定しますか？";
	private static readonly string DungeonDecision = "ダンジョンを決定しますか？";
	private static readonly string SelectReturn = "武器選択に戻りますか？";

	//各説明文
	private static readonly string SwordText = "剣：前方1マス、左右2マスの計3マスが攻撃範囲。攻撃力は4\n武器説明：冒険者の多くが最初に使う剣。誰にでも使いやすいように加工が施されている";
	private static readonly string AxeText = "斧：前方1マスが攻撃範囲。攻撃力は5\n武器説明：力自慢の冒険者が好んで使う斧。一撃が重く、モンスターに有効な打撃を与えることができる";
	private static readonly string BowText = "弓：前方3マスが攻撃範囲。攻撃力は3。\n武器説明：一般的に普及されている弓。冒険者から狩人にまで幅広い分野で使われている";
	private static readonly string Dungeon1Text = "ダンジョン1は沼地が特徴のステージ";
	private static readonly string Dungeon2Text = "ダンジョン2は氷と炎が特徴のステージ";

	//BGM
	private static readonly string BGM = "Sounds/BGM/Weapons";

	//SEパス
	private static readonly string ChoiseSE = "Sounds/SE/choice";
	private static readonly string ClickSE = "Sounds/SE/click";

	//クエスチョン
	private static bool Question = false;

	//決定武器のセッター
	private static void SetWeapon(IconBase.Icon weapon)
	{
		Weapon = weapon;
	}

	//決定武器のゲッター
	public static IconBase.Icon GetWeapon()
	{
		return Weapon;
	}

	//決定ダンジョンのセッター
	private static void SetDungeon(IconBase.Icon dungeon)
	{
		Dungeon = dungeon;
	}

	//決定ダンジョンのゲッター
	public static IconBase.Icon GetDungeon()
	{
		return Dungeon;
	}

	//現在の選択アイコンのセッター
	private static void SetNowIcon(IconBase.Icon icon)
	{
		NowIcon = icon;
	}

	//現在の選択アイコンのゲッター
	public static IconBase.Icon GetNowIcon()
	{
		return NowIcon;
	}

	//現状のセッター
	private static void SetSelect(SelectState selectState)
	{
		State = selectState;
	}

	//現状のゲッター
	public static SelectState GetSelect()
	{
		return State;
	}

	//初期化
	void Start()
	{
		//現在のシーンを登録
		scene = new Scene();
		scene.SetNowScene(SCENES.SELECT_SCENE);

		//サウンドマネージャー生成
		SoundManager.SoundManagerGeneration();

		//再生
		SoundManager.PlayBGM(BGM);

		//各変数セット

		//タイトルテキストのXY座標
		TitleXPosition = Zero;
		TitleYPosition = Screen.height / Twelve * Five;

		//タイトルテキストの領域
		TitleYRegion = Screen.height / Ten;

		//矢印のY座標
		ArrowYPosition = -Screen.width / Thirteen;

		//左矢印のX座標
		ArrowXPosition = -Screen.height / Five;

		//左矢印のサイズ
		ArrowWidth = Screen.width / Five;
		ArrowHeight = Screen.height / Five;

		//右矢印の回転角度
		RightArrowRotateX = Inversion;
		RightArrowRotateZ = Inversion;

		//フォントサイズ
		FontSize = Screen.width / (int)Twenty;

		//Canvas生成
		CanvasGeneration();

		//フェードイン開始
		FadeScript.FadeIn();

		//タイトル生成
		TitleGeneration();

		//矢印生成
		ArrowGeneration();

		//武器選択状態に変更
		SetSelect(SelectState.WEAPON_SELECT);

		//最初の３つのアイコンを作成

		//剣アイコン作成
		IconSword.SwordGeneration(WeaponSelectCanvas);

		//斧アイコン作成
		IconAxe.AxeGeneration(WeaponSelectCanvas);

		//弓アイコン作成
		IconBow.BowGeneration(WeaponSelectCanvas);

		//ダンジョン1生成
		IconDungeon1.Dungeon1Generation();

		//ダンジョン2生成
		IconDungeon2.Dungeon2Generation();

		//ダンジョンアイコンを非表示
		IconBase.DungeonIconHide();

		//テキストボックス生成
		MessageWindowManager.ImageGeneration();

		//テキストボックス内のテキストを剣に設定
		MessageWindowManager.MessageTextChange(SwordText);

		//初期状態として剣を選択
		SetNowIcon(IconBase.Icon.SWORD);

		//クエスチョン可能状態に変更
		Question = false;

		//クエスチョンボックスを生成
		YesNoQuestion.QuestionGeneration();
	}

	//Canvas生成
	private static void CanvasGeneration()
	{
		//-----------------Canvas生成部-----------------//

		//Canvas名設定
		CanvasObject = new GameObject("WeaponSelectCanvas");

		//Canvas生成
		WeaponSelectCanvas = CanvasObject.AddComponent<Canvas>();

		//親をシーンオブジェクトに設定
		WeaponSelectCanvas.transform.SetParent(CanvasObject.transform, false);

		//CanvasScalerを獲得、生成
		CanvasScaler = CanvasObject.AddComponent<CanvasScaler>();

		//描画優先度が有効なモードに変更
		WeaponSelectCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//ReferenceResolutionの設定
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//画面サイズに合わせて自動でサイズ調整するモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
	}

	//タイトルテキスト生成
	private static void TitleGeneration()
	{
		//------------------Text生成部------------------//

		//タイトルテキスト生成
		TitleText = new GameObject("TitleText").AddComponent<Text>();

		//Canvasを親に設定
		TitleText.transform.SetParent(WeaponSelectCanvas.transform, false);

		//TitleTextの位置調整
		TitleText.rectTransform.anchoredPosition = new Vector2(TitleXPosition, TitleYPosition);

		//Text領域を変更
		TitleText.rectTransform.sizeDelta = new Vector2(Screen.width, TitleYRegion);

		//フォント変更
		TitleText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

		//フォントサイズ変更
		TitleText.fontSize = FontSize;

		//フォントカラー変更
		TitleText.color = Color.white;

		//テキストセット
		TitleText.text = SelectWeaponText;

		//テキストのレイアウト変更
		TitleText.alignment = TextAnchor.MiddleCenter;
	}

	//矢印生成
	private static void ArrowGeneration()
	{
		//------------------左Arrow生成部------------------//

		//矢印生成
		LeftArrow = new GameObject("LeftArrowImage").AddComponent<Image>();

		//Canvasを親に設定
		LeftArrow.transform.SetParent(WeaponSelectCanvas.transform, false);

		//矢印の位置調整
		LeftArrow.rectTransform.anchoredPosition = new Vector2(ArrowXPosition, ArrowYPosition);

		//矢印のサイズ調整
		LeftArrow.rectTransform.sizeDelta = new Vector2(ArrowWidth, ArrowHeight);

		//Arrowに画像をアタッチ
		LeftArrow.sprite = Resources.Load<Sprite>("Arrowpng");

		//------------------右Arrow生成部------------------//

		//矢印生成
		RightArrow = new GameObject("RightArrowImage").AddComponent<Image>();

		//Canvasを親に設定
		RightArrow.transform.SetParent(WeaponSelectCanvas.transform, false);

		//矢印の位置調整
		RightArrow.rectTransform.anchoredPosition = new Vector2(-ArrowXPosition, ArrowYPosition);

		//矢印のサイズ調整
		RightArrow.rectTransform.sizeDelta = new Vector2(ArrowWidth, ArrowHeight);

		//矢印回転
		RightArrow.transform.Rotate(new Vector3(RightArrowRotateX, Zero, RightArrowRotateZ));

		//Arrowに画像をアタッチ
		RightArrow.sprite = Resources.Load<Sprite>("Arrowpng");
	}

	//更新
	void Update()
	{
		//-------------------------------入力受付-------------------------------//

		//クエスチョンしていない
		if (!Question)
		{
			//選択中のものを左ずらし
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//SE再生
				SoundManager.PlaySE(ChoiseSE);
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				//現在セットしているアイコンを送信、入れ替え
				ShiftLeft(GetNowIcon());
			}

			//選択中のものを右ずらし
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//SE再生
				SoundManager.PlaySE(ChoiseSE);
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				//現在セットしているアイコンを送信、入れ替え
				ShiftRight(GetNowIcon());
			}

			//選択決定
			if (Input.GetKeyDown(KeyCode.Z))
			{
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//SE再生
				SoundManager.PlaySE(ClickSE);
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				//クエスチョンボックスを表示
				YesNoQuestion.QuestionDisplay();

				//クエスチョン状態に変更
				Question = true;

				//武器選択
				if (GetSelect() == SelectState.WEAPON_SELECT)
				{
					//テキストをリセット
					MessageWindowManager.TextReset();

					//テキストをセット
					MessageWindowManager.MessageTextChange(WeaponDecision);
				}

				//ダンジョン選択
				else if (GetSelect() == SelectState.DUNGEON_SELECT)
				{
					//テキストをリセット
					MessageWindowManager.TextReset();

					//テキストをセット
					MessageWindowManager.MessageTextChange(DungeonDecision);
				}
			}

			//選択解除
			if (Input.GetKeyDown(KeyCode.X))
			{
				//選択解除
				if (GetSelect() == SelectState.DUNGEON_SELECT)
				{
					//状態を変更
					SetSelect(SelectState.RETURN_SELECT);

					//テキストをリセット
					MessageWindowManager.TextReset();

					//テキストをセット
					MessageWindowManager.MessageTextChange(SelectReturn);

					//クエスチョンボックスを表示
					YesNoQuestion.QuestionDisplay();

					//クエスチョン状態に変更
					Question = true;
				}
			}
		}

		//クエスチョンしている
		else if (Question)
		{
			//武器、ダンジョン選択中なら
			if (GetSelect() != SelectState.END && GetSelect() != SelectState.NONE)
			{
				//「はい」「いいえ」切り替え
				if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
				{
					//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//SE再生
					SoundManager.PlaySE(ChoiseSE);
					//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

					YesNoQuestion.ArrowChange();
				}
			}

			//戻る
			if (Input.GetKeyDown(KeyCode.X))
			{
				//武器選択解除だったら
				if (GetSelect() == SelectState.RETURN_SELECT)
				{
					//ダンジョン選択状態に戻す
					SetSelect(SelectState.DUNGEON_SELECT);
				}

				//クエスチョンボックスを非表示
				YesNoQuestion.QuestionHide();

				//クエスチョン可能状態に変更
				Question = false;

				//テキストをリセット
				MessageWindowManager.TextReset();

				//テキストを選択しているものに戻す
				switch (GetNowIcon())
				{
					case IconBase.Icon.SWORD:
						MessageWindowManager.MessageTextChange(SwordText);
						break;
					case IconBase.Icon.AXE:
						MessageWindowManager.MessageTextChange(AxeText);
						break;
					case IconBase.Icon.BOW:
						MessageWindowManager.MessageTextChange(BowText);
						break;
					case IconBase.Icon.DUNGEON_1:
						MessageWindowManager.MessageTextChange(Dungeon1Text);
						break;
					case IconBase.Icon.DUNGEON_2:
						MessageWindowManager.MessageTextChange(Dungeon2Text);
						break;
				}
			}

			//選択決定
			if (Input.GetKeyDown(KeyCode.Z))
			{
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//SE再生
				SoundManager.PlaySE(ClickSE);
				//----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				//クエスチョンに「はい」で答えた
				if (YesNoQuestion.GetArrow())
				{
					//選択しているものを正式決定
					if (GetSelect() == SelectState.WEAPON_SELECT)
					{
						//武器を正式決定
						SetWeapon(GetNowIcon());

						//状態切り替え
						SetSelect(SelectState.DUNGEON_SELECT);

						//クエスチョンボックスを非表示
						YesNoQuestion.QuestionHide();

						//タイトルテキスト変更
						TitleText.text = SelectDungeonText;

						//武器アイコン非表示
						IconBase.WeaponIconHide();

						//各ダンジョンアイコン表示
						IconBase.DungeonIconDisplay();

						//テキストリセット
						MessageWindowManager.TextReset();

						//初期状態としてダンジョン1テキストをセット
						MessageWindowManager.MessageTextChange(Dungeon1Text);

						//ダンジョン1をセット
						SetNowIcon(IconBase.Icon.DUNGEON_1);

						//クエスチョン可能状態に変更
						Question = false;

					}
					else if (GetSelect() == SelectState.DUNGEON_SELECT)
					{
						//ダンジョンを正式決定
						SetDungeon(GetNowIcon());

						//クエスチョンボックスを非表示
						YesNoQuestion.QuestionHide();

						//状態切り替え
						SetSelect(SelectState.END);
					}

					//選択解除
					else if (GetSelect() == SelectState.RETURN_SELECT)
					{
						//状態を武器選択に変更
						SetSelect(SelectState.WEAPON_SELECT);

						//各アイコンを再表示
						IconBase.WeaponIconDisplay();

						//テキストリセット
						MessageWindowManager.TextReset();

						//テキストを剣に変更
						MessageWindowManager.MessageTextChange(SwordText);

						//選択武器を剣に設定
						SetNowIcon(IconBase.Icon.SWORD);

						//矢印の表示
						LeftArrow.enabled = true;
						RightArrow.enabled = true;

						//クエスチョンボックスを非表示
						YesNoQuestion.QuestionHide();

						//タイトルテキストを変更
						TitleText.text = SelectWeaponText;

						//ダンジョンアイコンを非表示
						IconBase.DungeonIconHide();

						//クエスチョン可能状態に変更
						Question = false;

					}
				}
				//クエスチョンに「いいえ」で答えた
				else if (!YesNoQuestion.GetArrow())
				{
					//クエスチョンボックスを非表示
					YesNoQuestion.QuestionHide();

					//クエスチョン可能状態に変更
					Question = false;

					//テキストをリセット
					MessageWindowManager.TextReset();

					//テキストを選択しているものに戻す
					switch (GetNowIcon())
					{
						case IconBase.Icon.SWORD:
							MessageWindowManager.MessageTextChange(SwordText);
							break;
						case IconBase.Icon.AXE:
							MessageWindowManager.MessageTextChange(AxeText);
							break;
						case IconBase.Icon.BOW:
							MessageWindowManager.MessageTextChange(BowText);
							break;
						case IconBase.Icon.DUNGEON_1:
							MessageWindowManager.MessageTextChange(Dungeon1Text);
							break;
						case IconBase.Icon.DUNGEON_2:
							MessageWindowManager.MessageTextChange(Dungeon2Text);
							break;
					}

					//武器選択解除だったら
					if (GetSelect() == SelectState.RETURN_SELECT)
					{
						//ダンジョン選択状態に戻す
						SetSelect(SelectState.DUNGEON_SELECT);
					}
				}
			}
		}

		//----------------------------------------------------------------------//

		//準備
		if (GetSelect() == SelectState.END)
		{
			//決定した武器、ダンジョンを登録
			ScoreManager.Score.SetMyWeapon(GetWeapon());
			ScoreManager.Score.SetMyDungeon(GetDungeon());

			//フェードアウト
			FadeScript.FadeOut();

			//準備完了
			SetSelect(SelectState.NONE);
		}

		//シーン遷移
		if (GetSelect() == SelectState.NONE)
		{
			//フェード終了
			if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
			{
				//シーン切り替え
				scene.SetNextScene(SCENES.PLAY_SCENE);
			}
		}

		//矢印の非表示
		if (GetSelect() == SelectState.DUNGEON_SELECT)
		{
			//ダンジョン1を選択中
			if (GetNowIcon() == IconBase.Icon.DUNGEON_1)
			{
				//左矢印非表示
				LeftArrow.enabled = false;

				//右矢印表示
				RightArrow.enabled = true;
			}

			//ダンジョン2を選択中
			else if (GetNowIcon() == IconBase.Icon.DUNGEON_2)
			{
				//右矢印非表示
				RightArrow.enabled = false;

				//左矢印表示
				LeftArrow.enabled = true;
			}
		}
	}

	//右入力
	private static void ShiftLeft(IconBase.Icon icon)
	{
		//選択中のアイコン
		switch (icon)
		{
			//選択中：剣
			case IconBase.Icon.SWORD:

				//選択を弓に変更
				SetNowIcon(IconBase.Icon.BOW);

				//テキストリセット
				MessageWindowManager.TextReset();

				//弓の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(BowText);

				break;

			//選択中：斧
			case IconBase.Icon.AXE:

				//選択を剣に変更
				SetNowIcon(IconBase.Icon.SWORD);

				//テキストリセット
				MessageWindowManager.TextReset();

				//剣の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(SwordText);

				break;

			//選択中：弓
			case IconBase.Icon.BOW:

				//選択を斧に変更
				SetNowIcon(IconBase.Icon.AXE);

				//テキストリセット
				MessageWindowManager.TextReset();

				//斧の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(AxeText);

				break;

			//選択中：ダンジョン1
			case IconBase.Icon.DUNGEON_1:

				//選択をダンジョン2に変更
				SetNowIcon(IconBase.Icon.DUNGEON_2);

				//テキストリセット
				MessageWindowManager.TextReset();

				//ダンジョン2の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(Dungeon2Text);

				break;

			//選択中：ダンジョン2
			case IconBase.Icon.DUNGEON_2:

				/* 端なのでナシ */

				break;
		}
	}

	//左入力
	private static void ShiftRight(IconBase.Icon icon)
	{
		switch (icon)
		{
			//選択中：剣
			case IconBase.Icon.SWORD:

				//選択武器を斧に変更
				SetNowIcon(IconBase.Icon.AXE);

				//テキストリセット
				MessageWindowManager.TextReset();

				//斧の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(AxeText);

				break;

			//選択中：斧
			case IconBase.Icon.AXE:

				//選択武器を弓に変更
				SetNowIcon(IconBase.Icon.BOW);

				//テキストリセット
				MessageWindowManager.TextReset();

				//弓の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(BowText);

				break;

			//選択中：弓
			case IconBase.Icon.BOW:

				//選択武器を剣に変更
				SetNowIcon(IconBase.Icon.SWORD);

				//テキストリセット
				MessageWindowManager.TextReset();

				//剣の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(SwordText);

				break;

			//選択中：ダンジョン1
			case IconBase.Icon.DUNGEON_1:

				/* 端なのでナシ */

				break;

			//選択中：ダンジョン2
			case IconBase.Icon.DUNGEON_2:

				//選択武器をダンジョン1に変更
				SetNowIcon(IconBase.Icon.DUNGEON_1);

				//テキストリセット
				MessageWindowManager.TextReset();

				//ダンジョン1の説明テキストに切り替え
				MessageWindowManager.MessageTextChange(Dungeon1Text);

				break;
		}
	}
}
