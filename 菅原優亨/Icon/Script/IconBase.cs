using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBase : MonoBehaviour
{
    //おまえは誰？
    public enum Icon
	{
        NONE,       //なし
        SWORD,      //剣
        AXE,        //斧
        BOW,        //弓
        DUNGEON_1,  //ダンジョン1
        DUNGEON_2   //ダンジョン2
	}

    //拡縮状況
    public enum SCALE_STATE
	{
        NONE,           //なし
        SCALE_BIG,      //大きく
        SCALE_SMALE,    //小さく
        SCALE_USU       //均す
	}


    //各オブジェクト
    private static GameObject SwordBase;
    private static GameObject AxeBase;
    private static GameObject BowBase;
    private static GameObject Dungeon1Base;
    private static GameObject Dungeon2Base;

    //自身のImage
    protected static Image ImageSword;
    protected static Image ImageAxe;
    protected static Image ImageBow;
    protected static Image ImageDungeon1;
    protected static Image ImageDungeon2;

    //アイコンのY座標
    protected static readonly float IconYPosition = Screen.height / 10;

    //センターのX座標
    protected static readonly float CenterXPosition = .0f;

    //ライトのX座標
    protected static readonly float RightXPosition = Screen.width / 4;

    //レフトのX座標
    protected static readonly float LeftXPosition = -Screen.width / 4;

    //アイコンの標準サイズ
    protected static readonly float IconWidthHeight = Screen.height / 3;

    //アイコンの上限サイズ
    protected static readonly float IconMaxSize = IconWidthHeight * 1.75f;

    //アイコンの下限サイズ
    protected static readonly float IconMinSize = IconWidthHeight * 1.25f;

    //拡縮率
    protected static readonly float ScaleRate = IconMinSize / 1000;

    //描画優先度
    protected static readonly float IconMain = 20.0f;
    protected static readonly float IconSub = 10.0f;


    //アイコン作成
    public static void IconGeneration(Canvas canvas, Icon icon)
	{
		switch (icon)
		{
            case Icon.SWORD:
                SwordBase = new GameObject("SwordIcon");
                ImageSword = SwordBase.AddComponent<Image>();
                ImageSword.rectTransform.SetParent(canvas.transform, false);
                SwordBase.AddComponent<IconSword>();
                break;
            case Icon.AXE:
                AxeBase = new GameObject("AxeIcon");
                ImageAxe = AxeBase.AddComponent<Image>();
                ImageAxe.rectTransform.SetParent(canvas.transform, false);
                AxeBase.AddComponent<IconAxe>();
                break;
            case Icon.BOW:
                BowBase = new GameObject("BowIcon");
                ImageBow = BowBase.AddComponent<Image>();
                ImageBow.rectTransform.SetParent(canvas.transform, false);
                BowBase.AddComponent<IconBow>();
                break;
            case Icon.DUNGEON_1:
                Dungeon1Base = new GameObject("Dungeon1Icon");
                ImageDungeon1 = Dungeon1Base.AddComponent<Image>();
                ImageDungeon1.rectTransform.SetParent(canvas.transform, false);
                Dungeon1Base.AddComponent<IconDungeon1>();
                break;
            case Icon.DUNGEON_2:
                Dungeon2Base = new GameObject("Dungeon2Icon");
                ImageDungeon2 = Dungeon2Base.AddComponent<Image>();
                ImageDungeon2.rectTransform.SetParent(canvas.transform, false);
                Dungeon2Base.AddComponent<IconDungeon2>();
                break;
		}

	}

    //拡縮
    protected virtual void Scale(Image image,SCALE_STATE state)
	{
        //拡縮状況が「均す」だったら
        if (state == SCALE_STATE.SCALE_USU)
        {
            //標準サイズに変更
            image.rectTransform.sizeDelta = new Vector2(IconWidthHeight, IconWidthHeight);
        }
        //拡縮状況が「大きく」だったら
        else if (state == SCALE_STATE.SCALE_BIG)
        {
            image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x + ScaleRate, image.rectTransform.sizeDelta.y + ScaleRate);
        }
        //拡縮状況が「小さく」だったら
        else if (state == SCALE_STATE.SCALE_SMALE)
        {
            image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x - ScaleRate, image.rectTransform.sizeDelta.y - ScaleRate);
        }
        //その他
        else
        {
        }
    }

    //アイコン非表示
    public static void WeaponIconHide()
	{
        ImageSword.enabled = false;
        ImageAxe.enabled = false;
        ImageBow.enabled = false;

	}

    public static void DungeonIconHide()
	{
        ImageDungeon1.enabled = false;
        ImageDungeon2.enabled = false;
    }
}
