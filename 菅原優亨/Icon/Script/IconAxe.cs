using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconAxe : IconBase
{
	//自身の番号
	private static Icon IconNumber = Icon.AXE;

	//自身の拡縮状況
	private static SCALE_STATE IconState = SCALE_STATE.NONE;

	//初期化
	void Start()
	{
		//画像セット
		ImageAxe.sprite = Resources.Load<Sprite>("主人公正面(斧)");

		//サイズ調整
		SetScaleState(SCALE_STATE.SCALE_USU);

		//拡縮
		Scale(ImageAxe, GetScaleState());

		//初期座標に移動
		ImageAxe.rectTransform.anchoredPosition = new Vector2(LeftXPosition, IconYPosition);

	}

	//更新
	void Update()
	{
		//自身が選択されている
		if (WeaponSelectSceneManager.GetNowIcon() == IconNumber)
		{
			//自身をセンターに移動
			ImageAxe.rectTransform.anchoredPosition = new Vector2(CenterXPosition, IconYPosition);

			//直前まで「均す」だったら変更
			if (GetScaleState() == SCALE_STATE.SCALE_USU)
			{
				//拡縮状況を「大きく」に変更
				SetScaleState(SCALE_STATE.SCALE_BIG);

				//描画優先度を変更

			}
		}
		//選択されていない
		else
		{
			//現在選択されている武器に合わせて位置を変更
			switch (WeaponSelectSceneManager.GetNowIcon())
			{
					//剣選択
				case Icon.SWORD:
					//画面左
					ImageAxe.rectTransform.anchoredPosition = new Vector2(LeftXPosition, IconYPosition);
					break;
				//斧選択
				case Icon.AXE:
					//自身なのでなし
					break;
				//弓選択
				case Icon.BOW:
					//画面右
					ImageAxe.rectTransform.anchoredPosition = new Vector2(RightXPosition, IconYPosition);
					break;
				//ダンジョン1選択
				case Icon.DUNGEON_1:
					//関係なし
					break;
				//ダンジョン2選択
				case Icon.DUNGEON_2:
					//関係なし
					break;
				case Icon.NONE:
					//なし
					break;
			}

			//拡縮状況を「均す」に変更
			SetScaleState(SCALE_STATE.SCALE_USU);

			//拡縮
			Scale(ImageAxe, GetScaleState());
		}


		//「大きく」か「小さく」だったら拡縮自動起動
		if (GetScaleState() == SCALE_STATE.SCALE_BIG || GetScaleState() == SCALE_STATE.SCALE_SMALE)
		{
			//拡縮
			Scale(ImageAxe, GetScaleState());
		}

		//サイズが規定値を超えたら拡縮状況を「小さく」に変更
		if (ImageAxe.rectTransform.sizeDelta.x > IconMaxSize)
		{
			SetScaleState(SCALE_STATE.SCALE_SMALE);
		}

		//サイズが規定値より下回ったら拡縮状況を「大きく」に変更
		if (ImageAxe.rectTransform.sizeDelta.x < IconMinSize)
		{
			SetScaleState(SCALE_STATE.SCALE_BIG);
		}
	}

	//自身の拡縮状態のセッター
	private static void SetScaleState(SCALE_STATE state)
	{
		IconState = state;
	}

	//自身の拡縮状況のゲッター
	private static SCALE_STATE GetScaleState()
	{
		return IconState;
	}

	//拡縮
	protected override void Scale(Image image, SCALE_STATE state)
	{
		base.Scale(image, state);
	}
}
