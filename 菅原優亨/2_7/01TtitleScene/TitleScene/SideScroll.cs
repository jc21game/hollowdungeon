	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

//横スクロール
public class SideScroll : MonoBehaviour
{
	//再生成位置
	private float speed = 3;

	//初期化
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//移動
		transform.position -= new Vector3(Time.deltaTime * speed, 0);
		if(transform.position.x	<= -24.47)
		{
			transform.position = new Vector3(24.86f,0);
		}
	}


}
