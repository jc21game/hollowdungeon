using UnityEngine;
using SceneManager;
using LevelOfAchievement;
//タイトルシーン
public class TitleSceneManager : MonoBehaviour
{
	//シーンマネージャー
	private Scene Scene = null;

	//武器選択シーンへ移動する時間
	public float invokeTime = 0;

	//キャンバス
	public Canvas canvas = null;
	public GameObject scroll = null;
	public GameObject arasuji = null;

	//BGMバー
	private BGMBar bgmBar = null;

	//SEバー
	private SEBar seBar = null;

	//あらすじ表示
	bool DispraySynopsis = false;

	//初期化
	public void Start()
	{
		//シーン登録
		Scene = new Scene();
		Scene.SetNowScene(SCENES.TITLE_SCENE);

		//獲得フラグ初期化
		AchievementList.FlgReset();

		//フェード画像初期化
		FadeScript.Darkening();
		FadeScript.FadeIn();

		//生成
		Instantiate(canvas);
		Instantiate(scroll);

		//レイヤー順番設定
		arasuji.GetComponent<SpriteRenderer>().sortingOrder = 3;

		//サウンドマネージャー生成
		SoundManager.SoundManagerGeneration();

		//取得
		bgmBar = GameObject.Find("BGMBar").GetComponent<BGMBar>();
		seBar = GameObject.Find("SEBar").GetComponent<SEBar>();

		//各スライダーの初期設定
		bgmBar.Initialize();
		seBar.Initialize();

		//再生
		SoundManager.PlayBGM("Sounds/Title");

		//初期化
		ScoreManager.Score.Initialize();
	}

	//更新
	public void Update()
	{
		//Zキーを押したらフェードアウトして武器選択シーンに移動する
		if (Input.GetKeyDown(KeyCode.Z))
		{
			if (DispraySynopsis)
			{
				//フェードアウト
				FadeScript.FadeOut();

				//4秒後に武器選択シーンに移動する
				Invoke("ChangeWeapon", invokeTime);
			}
			else
			{
				Instantiate(arasuji);
				Destroy(GameObject.Find("Canvas(Clone)"));
				//あらすじ表示した
				DispraySynopsis = true;
			}
		}

		//書き込みチェック
		//SoundManager.CheckVolume();
	}

	//武器選択シーンに移動する関数
	public void ChangeWeapon()
	{
		//武器選択シーンに移動
		UnityEngine.SceneManagement.SceneManager.LoadScene("WeaponSelectScene");
	}
}
