using TMPro;
using UnityEngine;
using LevelOfAchievement;

public class AchieveTMP : MonoBehaviour
{
	//オブジェクト
	private TitleAchievement Achievement = null;

	//アチーブ詳細テキスト
	private TextMeshProUGUI TMP = null;

	//現在のアチーブ
	private static string NowAchievement = "";

	//セッター
	public static void SetNowAchievement(string achievement)
	{
		NowAchievement = achievement;
	}

	//ゲッター
	public static string GetNowAchievement()
	{
		return NowAchievement;
	}

	//初期化
	void Start()
	{
		//自信を確保
		TMP = this.GetComponent<TextMeshProUGUI>();

		//確保
		Achievement = GameObject.Find("Achievement").GetComponent<TitleAchievement>();
	}

	//更新
	void Update()
	{
		//現在のアチーブに合わせてテキスト変更
		switch (GetNowAchievement())
		{
			case "TreasureChest":
				TMP.text = AchievementList.DungeonTraversalText(Achievement.Icon);
				break;
			case "Trophy":
				TMP.text = AchievementList.MonsterHunterText(Achievement.Icon);
				break;
			case "Apple":
				TMP.text = AchievementList.Dr_PotionText(Achievement.Icon);
				break;
			case "Rhombus":
				TMP.text = AchievementList.ClumsyPersonText(Achievement.Icon);
				break;
			case "Heart":
				TMP.text = AchievementList.DiamondText(Achievement.Icon);
				break;
			case "Weak":
				TMP.text = AchievementList.GameOverText(Achievement.Icon);
				break;
			case "Medal":
				TMP.text = AchievementList.TalentedText(Achievement.Icon);
				break;
			case "Crown":
				TMP.text = AchievementList.GreatAdventurerText(Achievement.Icon);
				break;
		}
	}
}
