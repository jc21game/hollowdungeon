using UnityEngine;

public class NoEndButton : MonoBehaviour
{
	//オブジェクト
	private GameEndButton GameEnd = null;

	//初期化
	void Start()
	{
		GameEnd = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
	}

	//更新
	void Update()
	{

	}

	public void OnClick()
	{
		GameEnd.flg = false;
	}
}
