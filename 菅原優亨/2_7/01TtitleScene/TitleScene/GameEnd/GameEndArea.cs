using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndArea : MonoBehaviour
{
	//ボタン
	private GameEndButton button = null;

	//初期化
	private void Start()
	{
		//取得
		button = GameObject.Find("GameEndButton").GetComponent<GameEndButton>();
	}

	//更新
	private void Update()
	{
		
	}

	//ボタンをクリックされた
	public void OnClick()
	{
		//フラグ変更
		button.flg = false;
	}
}
