HierarchicalDisplay 使用方法

プレイシーンでのみ使用

階段を降りて、階層が変化するときに使用する

現状シーン内部での処理を僕は知らないので、プレイヤーやアイテム、マップ、敵
等に対する処理はだれかわかる人に任せます

その処理を置いてほしい場所にはコメントが入ってるのでそこに書いてください

使用する際の処理

public class PlaySceneManager : MonoBehaviour
{
	void Update()
	{
		if(...)
		{
			HierarchicalDisplay.HierarchyChange("ダンジョン名");
		}
	}
}