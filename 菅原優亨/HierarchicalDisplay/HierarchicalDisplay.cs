using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HierarchicalDisplay : MonoBehaviour
{

    enum HIEARARCHY_STATE
	{
        NONE,           //待機
        H_FADE_IN,      //フェードイン
        H_FADE_NABLE,   //メイン
        H_FADE_ENABLE,  //退場開始
        H_FADE_OUT      //フェードアウト
	}
    private static HIEARARCHY_STATE HierarchyState = HIEARARCHY_STATE.NONE;

    //テキスト表示用のObject : Canvas : Text : CanvasScaler
    private static GameObject HierarchyObject;
    private static Canvas HierarchyCanvas;
    private static Text DungeonText;
    private static Text HierarchyText;
    private static CanvasScaler CanvasScaler;

    //Canvasの描画優先度
    private static int Priority = 200;

    //Text(ダンジョン名)の位置
    private static readonly float DungeonXPosition = -Screen.width / 6;
    private static readonly float DungeonYPositon = 0.0f;

    //Text(階層)の位置
    private static readonly float HierarchyXPosition = Screen.width / 3;
    private static readonly float HierarchyYPosition = 0.0f;

    //Text(ダンジョン名)の領域
    private static readonly float DungeonXRegion = Screen.width / 3 * 2;
    private static readonly float DungeonYRegion = Screen.height / 5;

    //Text(階層)の領域
    private static readonly float HierarchyXRegion = Screen.width / 3;
    private static readonly float HierarchyYRegion = Screen.height / 5;

    //フォントサイズ
    private static readonly int FontSize = Screen.height /7;

    //テキストの透明度
    private static float Alpha = 0.0f;

    //透明度の下限、上限
    private static readonly float LowerAlpha = 0.0f;
    private static readonly float UpperAlpha = 1.0f;

    //フェードにかかる時間
    private static readonly float FadeTime = 3.0f;

    //メイン状態にしておく時間
    private static float NableTime = 1.0f;

    //ダンジョン名
    private static string DungeonName = "";

    //現在の階層
    private static int NowHierarchy = 0;

	//Canvas生成
	private static void CanvasGeneration()
	{
        //----------------Canvas生成部----------------//

        //Canvas名設定
        HierarchyObject = new GameObject("HierarchyCanvas");

        //Canvasを獲得、生成
        HierarchyCanvas = HierarchyObject.AddComponent<Canvas>();

        //CanvasScalerを獲得、生成
        CanvasScaler = HierarchyObject.AddComponent<CanvasScaler>();

        //描画優先度が有効なモードに変更
        HierarchyCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

        //ReferenceResolutionの設定
        //これを設定しないと、CanvasのデフォルトサイズがScreenサイズなのに
        //実際の画面構成はCanvasScalerのデフォルト値が反映されややこしくなる
        CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        //画面サイズの変更に合わせて自動でサイズ変更するモードに変更
        CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

        //HierarchicalDisplayをアタッチ
        HierarchyObject.AddComponent<HierarchicalDisplay>();

        //描画優先度設定
        HierarchyCanvas.sortingOrder = Priority;
    }

    //Text生成
    private static void TextGeneration()
	{
        //Canvasがいないなら生成
        if (HierarchyCanvas == null)
		{
            CanvasGeneration();
		}

        //----------------Text生成部----------------//

        //Text名設定
        DungeonText = new GameObject("DungeonText").AddComponent<Text>();
        HierarchyText = new GameObject("HierarchyText").AddComponent<Text>();

        //Canvasを親に設定
        DungeonText.transform.SetParent(HierarchyCanvas.transform, false);
        HierarchyText.transform.SetParent(HierarchyCanvas.transform, false);

        //Textの位置調整
        DungeonText.rectTransform.anchoredPosition = new Vector2(DungeonXPosition, DungeonYPositon);
        HierarchyText.rectTransform.anchoredPosition = new Vector2(HierarchyXPosition, HierarchyYPosition);

        //Text領域を変更
        DungeonText.rectTransform.sizeDelta = new Vector2(DungeonXRegion, DungeonYRegion);
        HierarchyText.rectTransform.sizeDelta = new Vector2(HierarchyXRegion, HierarchyYRegion);

        //フォントを変更
        DungeonText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        HierarchyText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        //フォントサイズを変更
        DungeonText.fontSize = FontSize;
        HierarchyText.fontSize = FontSize;

        //フォントカラーを変更
        DungeonText.color = Color.white;
        HierarchyText.color = Color.white;

        //テキストセット
        DungeonText.text = DungeonName;
		HierarchyText.text = NowHierarchy + "F";

        //テキストレイアウト変更
        DungeonText.alignment = TextAnchor.MiddleCenter;
        HierarchyText.alignment = TextAnchor.MiddleCenter;

        //色設定
        DungeonText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);
        HierarchyText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);
    }

	//階層切り替え
	//引数：ダンジョン名、現在の階層番号
	public static void HierarchyChange(string dungeonname)
	{
        //ダンジョン名更新
        DungeonName = dungeonname;

        //現在の階層を更新
        NowHierarchy = ScoreManager.Score.GetFloor();

        //状態をフェードインに切り替え
        HierarchyState = HIEARARCHY_STATE.H_FADE_IN;

        //透明度を整理
        Alpha = LowerAlpha;

        //テキストを生成していなければ生成
        if(HierarchyText == null)
		{
            //テキスト生成
            TextGeneration();
        }

        //画面をフェードアウト
        FadeScript.FadeOut();
    }



    //更新
    private void Update()
	{
        //階層変更が行われたら
        if (HierarchyState == HIEARARCHY_STATE.H_FADE_IN)
        {

            //経過時間から透明度計算
            Alpha += Time.deltaTime / FadeTime;

            //透明度が規定値を超えたら終了
            if (Alpha > UpperAlpha)
            {
                //透明度を整理
                Alpha = UpperAlpha;

                //状態を変更
                HierarchyState = HIEARARCHY_STATE.H_FADE_NABLE;

            }

            //テキストをだんだん表示
            DungeonText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);
            HierarchyText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);

        }

        //メイン状態にしておく
        if(HierarchyState == HIEARARCHY_STATE.H_FADE_NABLE)
		{
            //経過時間計測
            NableTime -= Time.deltaTime;

            //ゲームデータを更新する
            /*
             * マップの更新や敵、アイテム、プレイヤーの位置情報の更新
             * 終わったらフラグか何かを返す
             */

            //時間が過ぎた
            if(NableTime < LowerAlpha /* && マップの更新が終わったフラグ */)
			{
                //時間を戻す
                NableTime = UpperAlpha;

                //テキストをフェードアウトさせる
                HierarchyState = HIEARARCHY_STATE.H_FADE_ENABLE;

			}
		}

        //画面のフェードイン開始
        if(HierarchyState == HIEARARCHY_STATE.H_FADE_ENABLE)
		{
            //画面をフェードイン
            FadeScript.FadeIn();

            //状態変更
            HierarchyState = HIEARARCHY_STATE.H_FADE_OUT;
        }

        //フェードアウト開始
        if(HierarchyState == HIEARARCHY_STATE.H_FADE_OUT)
		{

            //経過時間から透明度計算
            Alpha -= Time.deltaTime / FadeTime;

            //透明度が規定値を超えたら終了
            if (Alpha < LowerAlpha)
            {
                //透明度を整理
                Alpha = LowerAlpha;

                //状態を変更
                HierarchyState = HIEARARCHY_STATE.NONE;

            }

            //テキストをだんだん非表示
            DungeonText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);
            HierarchyText.color = new Color(Color.white.r, Color.white.g, Color.white.b, Alpha);

        }
	}
}
