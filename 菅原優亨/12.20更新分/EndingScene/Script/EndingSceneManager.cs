using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScoreManager;
using SceneManager;


public class EndingSceneManager : MonoBehaviour
{
    //シーンマネージャー
    private Scene Scene = null;

    //キャンバスオブジェクト
    private GameObject CanvasObject = null;

    //キャンバス
    private Canvas Canvas = null;

    //キャンバススケーラー
    private CanvasScaler CanvasScaler = null;

    //イメージ
    private Image Image = null;

    //リザルトテキスト
    private Text ResultText = null;

    //リザルトテキストのXY座標
    private float ResultTextXPosition = 0;
    private float ResultTextYPosition = -Screen.height / 9 * 3;

    //リザルトテキストの領域
    private float ResultTextXRegion = Screen.width;
    private float ResultTextYRegion = Screen.height / 10 * 3;

    //シーン画像のXY座標
    private readonly float EndingImageXPosition = .0f;
    private readonly float EndingImageYPosition = Screen.height / 7;

    //シーン画像のサイズ
    private readonly float EndingImageWidht = Screen.width / 2;
    private readonly float EndingImageHeight = Screen.height / 5 * 3;
    //シーン背景画像のサイズ
    private readonly float EndingBackWidht = Screen.width / 2 + Screen.width / 60;
    private readonly float EndingBackHeight = Screen.height / 5 * 3 + Screen.height / 40;

    //セットするテキスト群
    private string Page1 = "";
    private string Page2 = "";
    private string Page3 = "";

    //セットされるテキスト群
    private string Hp = "HP:";
    private string Lv = "LV:";
    private string Attack = "攻撃力:";
    private string Defence = "防御力:";
    private string Floor = "階層:";
    private string Turn = "総ターン数:";
    private string CoveredRecovery = "総回復量:";
    private string CoveredDamage = "総被ダメージ:";
    private string GivingDamage = "総与ダメージ:";
    private string CoveredAttack = "総被ダメージ回数:";
    private string GivingAttack = "総与攻撃回数:";
    private string HaveItem = "総アイテム取得数:";
    private string UseItem = "総アイテム使用回数:";


    //初期化
    void Start()
    {
        //シーン登録
        Scene = new Scene();
        Scene.SetNowScene(SCENES.ENDING_SCENE);

        //フェードイン
        FadeScript.FadeIn();

        //シーン画像生成
        ImageGeneration();

        //サウンドマネージャー生成
        SoundManager.SoundManagerGeneration();

        //再生
        SoundManager.PlayBGM("Assets/04EndingScene/Sounds/BGM/Ending.mp3");

        //スコアを表示するための準備
        Hp += Score.GetHp();                // HP
        Lv += Score.GetAttack();            // 攻撃力
        Attack += Score.GetDefence();           // 防御力
        Defence += Score.GetLv();                // Lv
        Floor += Score.GetFloor();             // 階層
        Turn += Score.GetTurn();              // 総ターン数
        CoveredRecovery += Score.GetCoveredRecovery();   // 総回復量
        CoveredDamage += Score.GetCoveredDamage();     // 総破ダメージ
        GivingDamage += Score.GetGivingDamage();      // 総与ダメージ
        CoveredAttack += Score.GetCoveredAttack();     // 総破ダメージ回数
        GivingAttack += Score.GetGivingAttack();      // 総与攻撃回数
        HaveItem += Score.GetHaveItem();          // 総アイテム取得数
        UseItem += Score.GetUseItem();           // 総アイテム使用回数

        //セットするテキストをまとめる
        Page1 += "今回の記録" + "\n" + "\n" + Hp + "         " + Lv + "\n" + "\n" + Attack + "         " + Defence;
        Page2 += "今回の記録" + "\n" + "\n" + Floor + "         " + Turn + "\n" + "\n" + CoveredRecovery + "         " + CoveredDamage;
        Page3 += "今回の記録" + "\n" + "\n" + GivingDamage + "         " + CoveredAttack + "\n" + "\n" + GivingAttack + "         " + HaveItem + "         " + UseItem;

        //テキスト生成
        Page1TextGeneration();
    }

    //更新
    void Update()
    {
		//テキストを入れ替え
		if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
		{
            if(ResultText.text == Page1)
			{
                ResultText.text = Page2;
            }
            else if(ResultText.text == Page2)
			{
                ResultText.text = Page3;
			}
            else if(ResultText.text == Page3)
			{
                //フェードアウト
                FadeScript.FadeOut();

                //4秒後にタイトルシーンに移動する
                Invoke("ChangeTitle", 4.0f);
            }
            
		}
    }

    //キャンバス生成
    private void CanvasGeneration()
	{
        //キャンバスオブジェクト生成
        CanvasObject = new GameObject("EndingCanvas");

        //Canvas生成
        Canvas = CanvasObject.AddComponent<Canvas>();

        //親をキャンバスオブジェクトに設定
        Canvas.transform.SetParent(CanvasObject.transform, false);

        //CanvasScalerを獲得、生成
        CanvasScaler = CanvasObject.AddComponent<CanvasScaler>();

        //描画優先度が有効なモードに変更
        Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        //ReferenceResolutionの設定
        CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        //画面サイズに合わせて自動でサイズ調整
        CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

	}

    //イメージ生成
    private void ImageGeneration()
	{
        //キャンバス
        if(Canvas == null)
		{
            CanvasGeneration();
		}

        //--------------------------画像背景--------------------------//

        //新しいイメージを生成
        Image = new GameObject("Endingback").AddComponent<Image>();

        //親をキャンバスに設定
        Image.transform.SetParent(Canvas.transform, false);

        //位置設定
        Image.rectTransform.anchoredPosition = new Vector2(EndingImageXPosition, EndingImageYPosition);

        //サイズ調整
        Image.rectTransform.sizeDelta = new Vector2(EndingBackWidht, EndingBackHeight);

        //画像セット
        Image.sprite = Resources.Load<Sprite>("");

        //--------------------------画像本体--------------------------//

        //新しいイメージを生成
        Image = new GameObject("EndingImage").AddComponent<Image>();

        //親をキャンバスに設定
        Image.transform.SetParent(Canvas.transform, false);

        //位置設定
        Image.rectTransform.anchoredPosition = new Vector2(EndingImageXPosition, EndingImageYPosition);

        //サイズ調整
        Image.rectTransform.sizeDelta = new Vector2(EndingImageWidht, EndingImageHeight);

        //画像セット
        Image.sprite = Resources.Load<Sprite>("クリアシーン");

        
    }

    //リザルト1ページ目
    private void Page1TextGeneration()
	{
        //リザルトテキスト生成
        ResultText = new GameObject("ResultText").AddComponent<Text>();

        //キャンバスを親に設定
        ResultText.transform.SetParent(Canvas.transform, false);

        //リザルトテキストの位置調整
        ResultText.rectTransform.anchoredPosition = new Vector2(ResultTextXPosition, ResultTextYPosition);

        //テキスト領域を変更
        ResultText.rectTransform.sizeDelta = new Vector2(ResultTextXRegion, ResultTextYRegion);

        //フォント変更
        ResultText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;

        //フォントサイズ変更
        ResultText.fontSize = 50;

        //フォントカラー変更
        ResultText.color = Color.white;

        //テキストセット
        ResultText.text = Page1;

        //テキストのレイアウト変更
        ResultText.alignment = TextAnchor.MiddleCenter;
	}


    //タイトルシーンに移動する関数
    public void ChangeTitle()
    {
        //タイトルシーンに移動
        Scene.SetNextScene(SCENES.TITLE_SCENE);
    }


}

