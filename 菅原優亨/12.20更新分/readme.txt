サウンドマネージャー変更点
①サウンドマネージャーをプレハブではなく、スクリプトで生成できるようにした
②上記に伴い、Resoucesにあったサウンドマネージャープレハブを削除
③上記に伴い、各シーンに記述されていた下記を削除、代わりにサウンドマネージャー生成関数を呼び出し

※オーディオミキサーの位置を下記に移動してください
Common/SoundManager/Resources

【削除項目】
・変数宣言
　　
//サウンドオブジェクト
private GameObject SoundObject = null;

//インスタンス
private GameObject SoundInstance = null;

//サウンドマネージャー
private SoundManager SoundManager = null;

・下記サウンドマネージャー使用に必要だった処理
//サウンドプレハブを取得
SoundObject = (GameObject)Resources.Load("SoundManager");

//インスタンス生成
SoundInstance = Instantiate(SoundObject);

//コンポーネント追加
SoundManager = SoundInstance.GetComponent<SoundManager>();


【代替処理】
・サウンドマネージャー生成
//サウンドマネージャー生成
SoundManager.SoundManagerGeneration();