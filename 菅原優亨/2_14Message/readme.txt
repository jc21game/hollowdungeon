変更点
・public関数：ArrowGeneration()を追加
内容：もともとImageGeneration()にまとめていた部分を摘出

・public関数：ImageArrowHide()、ImageArrowDisplay()を追加
内容：もともとMessageImageHide()、MessageImageDisplay()にまとめていたImageArrowの非表示表示を摘出

・Update内のHuwahuwaState関連の処理にif(ImageArrow != null)を追加

・ChangeForciblyParent(GameObject parent)とChangeForciblyParent()の処理と調整
内容：もともと裸だった処理にif(ImageBox != null)とif(ImageArrow != null)をそれぞれ追加