using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconDungeon2 : IconBase
{
	//自身の番号
	private static Icon IconNumber = Icon.DUNGEON_2;

	//自身の拡縮状況
	private static SCALE_STATE IconState = SCALE_STATE.NONE;

	//初期化
	void Start()
	{
		//画像セット
		ImageDungeon2.sprite = Resources.Load<Sprite>("ダンジョン2の代用2");

		//サイズ調整
		SetScaleState(SCALE_STATE.SCALE_USU);

		//拡縮
		Scale(ImageDungeon2, GetScaleState());

		//初期座標に移動
		ImageDungeon2.rectTransform.anchoredPosition = new Vector2(RightXPosition, IconYPosition);

	}

	//更新
	void Update()
	{
		//自身が選択されている
		if (WeaponSelectSceneManager.GetNowIcon() == IconNumber)
		{
			//自身をセンターに移動
			ImageDungeon2.rectTransform.anchoredPosition = new Vector2(CenterXPosition, IconYPosition);

			//直前まで「均す」だったら変更
			if (GetScaleState() == SCALE_STATE.SCALE_USU)
			{
				//拡縮状況を「大きく」に変更
				SetScaleState(SCALE_STATE.SCALE_BIG);

				//描画優先度を変更

			}
		}
		//選択されていない
		else
		{
			//現在選択されている武器に合わせて位置を変更
			switch (WeaponSelectSceneManager.GetNowIcon())
			{
				case Icon.SWORD:
					//関係なし
					break;
				case Icon.AXE:
					//関係なし
					break;
				case Icon.BOW:
					//関係なし
					break;
				case Icon.DUNGEON_1:
					//右に設置
					ImageDungeon2.rectTransform.anchoredPosition = new Vector2(RightXPosition, IconYPosition);
					break;
				case Icon.DUNGEON_2:
					//自身だから無し
					break;
				case Icon.NONE:
					//なし
					break;
			}

			//拡縮状況を「均す」に変更
			SetScaleState(SCALE_STATE.SCALE_USU);

			//拡縮
			Scale(ImageDungeon2, GetScaleState());
		}

		//「大きく」か「小さく」だったら拡縮自動起動
		if(GetScaleState() == SCALE_STATE.SCALE_BIG || GetScaleState() == SCALE_STATE.SCALE_SMALE)
		{
			//拡縮
			Scale(ImageDungeon2, GetScaleState());
		}

		//サイズが規定値を超えたら拡縮状況を「小さく」に変更
		if (ImageDungeon2.rectTransform.sizeDelta.x > IconMaxSize)
		{
			SetScaleState(SCALE_STATE.SCALE_SMALE);
		}

		//サイズが規定値より下回ったら拡縮状況を「大きく」に変更
		if (ImageDungeon2.rectTransform.sizeDelta.x < IconMinSize)
		{
			SetScaleState(SCALE_STATE.SCALE_BIG);
		}


	}

	//自身の拡縮状態のセッター
	private static void SetScaleState(SCALE_STATE state)
	{
		IconState = state;
	}

	//自身の拡縮状況のゲッター
	private static SCALE_STATE GetScaleState()
	{
		return IconState;
	}

	//拡縮
	protected override void Scale(Image image, SCALE_STATE state)
	{
		base.Scale(image, state);
	}
}
