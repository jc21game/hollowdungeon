変更内容：
①武器選択シーンで各変数の中身が実数だったものを動的に修正
②プレイシーンで変更されていたフレームレートをスプラッシュに移動
③メッセージボックスのフワフワの挙動を修正
④クエスチョンボックスのフォントサイズ修正