using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndButton : MonoBehaviour
{
	//範囲外
	private GameObject GameEndArea = null;

	//自分
	private GameObject GameEnd = null;

	//ほかのボタンの状態
	private ConfigButton ConfigButton = null;
	private GaidoButton GaidoButton = null;
	private AchievementButton Achievement = null;

	//表示フラグ
	public bool flg = false;

    //初期化
    void Start()
    {
		//取得
		GameEndArea = GameObject.Find("GameEndArea");
		GameEnd = GameObject.Find("GameEnd");
		ConfigButton = GameObject.Find("ConfigButton").GetComponent<ConfigButton>();
		GaidoButton = GameObject.Find("GaidoButton").GetComponent<GaidoButton>();
		Achievement = GameObject.Find("AchievementButton").GetComponent<AchievementButton>();

		//非表示
		GameEndArea.SetActive(flg);
		GameEnd.SetActive(flg);
	}

    //更新
    void Update()
    {
		//表示切替
		GameEndArea.SetActive(flg);
		GameEnd.SetActive(flg);

    }

    //ボタンをクリックされた
    public void OnClick()
	{
		//ほかのボタンがOnじゃない
		if (GaidoButton.flg != true && Achievement.flg != true && ConfigButton.flg != true)
		{
			//フラグ切り替え
			if (flg)
				flg = false;
			else
				flg = true;
		}
    }
}
