using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CharacterBase : MonoBehaviour
{
    //自分の行動状態
    protected enum NOW_STATE
	{
        NONE,           //処理待機
        THINK_STATE,    //思考中
        ATK_STATE,      //攻撃中
        MOVE_STATE,     //移動中
        END_STATE       //行動終了
	}
    protected NOW_STATE NowState = NOW_STATE.NONE;

    //攻撃タイプ
    protected enum ATC_TYPE
	{
        NONE,   //待機
        SWORD,  //剣
        AXE,    //斧
        BOW     //弓
	}

    //正面方向
    protected enum FRONT_DIR
	{
        NONE,   //未設定
        UP,     //上方向
        RIGHT,  //右方向
        LEFT,    //左方向
        DOWN   //下方向
	}
    protected FRONT_DIR CharacterFront = FRONT_DIR.NONE;

    //プレイヤー座標との差分
    protected int Xdif = 0;
    protected int Ydif = 0;

    //ステータス(初期値)
    public string characterName = "";
    public int initLv = 0;
    public int initHp = 0;
    public int initAtc = 0;
    public int initDef = 0;
    public int initExp = 0;
    private  ATC_TYPE initAtcType = ATC_TYPE.NONE;

    //ステータス現在
    protected int lv = 0;
    protected int hp = 0;
    protected int atc = 0;
    protected int def = 0;
    protected int exp = 0;

    //最大HP
    protected int maxHp = 0;

    //攻撃タイプ
    protected ATC_TYPE atcType = ATC_TYPE.NONE;

    //状態異常


    //各種ステータスを取得
    protected virtual string GetCharacterName()
    {
        return characterName; 
    }
    protected virtual int GetLv() 
    {
        return lv;
    }
    protected virtual int GetHp() 
    { 
        return hp;
    }
    protected virtual int GetAtc()
    {
        return atc; 
    }
    protected virtual int GetDef()
    { 
        return def;
    }
    protected virtual int GetExp() 
    { 
        return exp; 
    }
    protected virtual int GetMaxHp() 
    { 
        return maxHp;
    }
    protected virtual NOW_STATE GetState()
	{
        return NowState;
	}
    protected virtual ATC_TYPE GetAtcType() 
    { 
        return atcType;
    }
    protected virtual FRONT_DIR GetCharaFront()
	{
        return CharacterFront;
	}

    //各種ステータスを設定
    protected virtual void SetCharacterName(string name)
    {
        characterName = name;
    }

    protected virtual void SetLv(int value)
	{
        lv = value;
	}

    protected virtual void SetHp(int value)
    {
        hp = value;
    }
    protected virtual void SetAtc(int value)
    {
        atc = value;
    }
    protected virtual void SetDef(int value)
    {
        def = value;
    }
    protected virtual void SetExp(int value)
    {
        exp = value;
    }
    protected virtual void SetMaxHp(int value)
    {
        maxHp = value;
    }
    protected virtual void SetNowState(NOW_STATE state)
	{
        NowState = state;
	}
    protected virtual void SetAtcType(ATC_TYPE type)
    {
        atcType = type;
    }
    protected virtual void SetCharaFront(FRONT_DIR front)
	{
        CharacterFront = front;
	}
    //初期化
    protected virtual void Start()
    {
        //初期ステータス割り当て
        lv = initLv;
        hp = maxHp = initHp;
        atc = initAtc;
        def = initDef;
        exp = initExp;
        atcType = initAtcType;
    }

    //更新
    protected virtual void Update()
    {
        //自身が部屋にいたら && 出口が設定されていない
        if(/*自身が部屋にいる*/ 1 == 1 && /*出口未設定*/ 1 == 1)
		{
            //出口設定
            ExitConfiguration();
		}

        //行動
        Execute();

        //状態更新
        SetNowState(NOW_STATE.THINK_STATE);
    }

    //出口設定
    protected virtual void ExitConfiguration()
	{
        //出口を設定する

	}

    //最短経路検出処理
    protected virtual void ShortestPath()
	{

	}

    //行動
    protected virtual void Execute()
    {
		//自身の攻撃範囲内の確認
        if(/*攻撃範囲内にプレイヤーがいる*/ 1 == 1)
		{
            //攻撃
            Attack();

            //状態更新
            SetNowState(NOW_STATE.ATK_STATE);
		}
        //攻撃範囲内にプレイヤーがいない
		else
		{
            //移動先チェック
            //DestinationCheck();

            //状態更新
            SetNowState(NOW_STATE.MOVE_STATE);
		}
    }

    //プレイヤーとの位置関係を確認する
    protected virtual bool PositionalRelationShip(GameObject player)
	{
        //X軸の差分を確保
        Xdif = (int)(transform.position.x - player.transform.position.x);

        //Y軸の差分を確保
        Ydif = (int)(transform.position.y - player.transform.position.y);

        //索敵範囲内にプレイヤーがいる
        if(/**/1==1)
		{
            return true;
		}

        //索敵範囲内にプレイヤーがいない
		else
		{
            return false;
		}

    }

    //移動先チェック
    protected virtual void DestinationCheck()
    {
        CsvReader csvReader = null;

        // プレイヤーとの位置関係を確認する
        if (/*PositionalRelationShip(/* プレイヤーのGameObject)*/ 1 == 1)
        {
            //プレイヤーへの最短経路を出して移動
        }

        //プレイヤーが索敵範囲にいませんでした
        else
        {
            int CanProceed = 0;
            bool CanBack = false;

            //自身の前後左右に隙間がある！
            /*前後左右に埋まってないマスがある！*/

            //自身の前左右を確認
            //上
            if (csvReader.GetDataInt((int)transform.position.x, (int)transform.position.y + 1) == /* 通れる */ 1)
            {
                CanProceed++;
            }
            //右
            if (csvReader.GetDataInt((int)transform.position.x + 1, (int)transform.position.y) == /* 通れる */ 1)
            {
                CanProceed++;
            }
            //左
            if (csvReader.GetDataInt((int)transform.position.x - 1, (int)transform.position.y) == /* 通れる */ 1)
            {
                CanProceed++;
            }
            //した
            if (csvReader.GetDataInt((int)transform.position.x, (int)transform.position.y - 1) == /* 通れる */ 1)
            {
                CanBack = true;
            }

            //隙間がありましたか？
            if (/*隙間がありました！*/CanProceed > 0 || CanBack == true)
            {
                //自身は部屋にいますか？
                if (/*はい、います*/ csvReader.GetDataInt((int)transform.position.x, (int)transform.position.y) > 3)
                {
                    //設定されている出口に向かって移動
                    //最短経路探査処理に従い移動
                }
                //いいえ、いません
                else
                {
                    //ここは進行可能方向が2つ以上ですか?
                    if (CanProceed > 2)
                    {
                        //後方以外に進めますか？
                        if (/*はい、進めます*/ CanProceed > 0)
                        {
                            //進行可能方向に進むに進む
                        }
                        //いいえ、進めません
                        else
                        {
                            //後方に進む
                            Move(transform.position.x, transform.position.y - 1);
                        }
                    }
                    //はい、複数方向に進めます
                    else
                    {
                        //ランダム（後方を除く）な方向に進む
                    }
                }
            }


            //いいえ、隙間がありません
            else
            {
                //動けないので移動しません
                Move(transform.position.x,transform.position.y);
            }

        }

        //状態更新
        SetNowState(NOW_STATE.END_STATE);
    }



    //移動
    protected virtual void Move(float x, float y)
    {
        transform.position = new Vector3(transform.position.x + x, transform.position.y + y, 0);
    }

    //攻撃
    protected virtual void Attack()
    {
        /*攻撃処理*/

        //行動終了
        SetNowState(NOW_STATE.END_STATE);
    }

    //ダメージを受ける
    protected virtual void OnDamage(int damage)
    {
        //ダメージ分HP減少
        hp -= damage;

        //HPが下回った場合
        if (hp <= 0)
        {
            //HPを0に設定
            hp = 0;

            //死亡
            Deth();
        }
    }

    //回復
    protected virtual void OnRecovery(int recovery)
    {
        //回復分HPを増やす
        hp += recovery;

        //HPが最大体力を超えた場合
        if (hp >= maxHp)
        {
            //HPを最大体力にする
            hp = maxHp;
        }
    }

    //死亡
    protected virtual void Deth()
    {
    }
}