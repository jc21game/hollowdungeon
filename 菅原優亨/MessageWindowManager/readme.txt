MessageWindowManager 使用方法

public class ...Manager : MonoBehaviour
{
	void Start()
	{
		//生成
		MessageWindowManager.ImageGeneration();
	}

	void ...()
	{
		//テキストセット
		MessageWindowManager.MessageTextChange("test");
	}
}

テキストを新しくセットしたい関数内で、テキストセットの関数を実行してセットしてください
テキストをリセットしたい場合はTextReset関数を実行してください