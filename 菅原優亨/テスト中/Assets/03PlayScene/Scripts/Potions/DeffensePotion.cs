using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//防御ポーション
public class DeffensePotion : ItemBase
{
	//初期化
	public override void Start()
	{
		//アタッチ
		Attach(EFFECT.DEFFENCE);
	}

	//更新
	public override void Update()
	{
	}

	//効果
	public override void Execute()
	{
		//防御
		GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GetCharacter(0).DEF += efficacy;
	}
}
