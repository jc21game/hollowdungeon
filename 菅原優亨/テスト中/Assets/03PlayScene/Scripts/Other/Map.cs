using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//マップ
public class Map : MonoBehaviour
{
	//マップ情報
	public MAP mapInfomation = null;

	//壁
	private GameObject wall = null;

	//床
	private GameObject room = null;

	//階段
	private GameObject stairs = null;
	public Vector2 stairsPosition;

	//マップ情報
	private int[,] map;

	//マップ情報の置き場所
	public CSVReader Csv = null;

	//マネージャー
	private PlaySceneManager manager = null;

	//アイテム
	public GameObject[] potions = null;
	public int maxPotions = 0;

	//初期化
	void Start()
	{
		//マップ情報
		mapInfomation = new MAP();

		//壁プレハブを取得
		wall = (GameObject)Resources.Load("Map/Wall");

		//床プレハブを取得
		room = (GameObject)Resources.Load("Map/Room");

		//階段プレハブを取得
		stairs = (GameObject)Resources.Load("Map/Stairs");
		stairsPosition = new Vector2(0, 0);
		stairs.GetComponent<SpriteRenderer>().sortingOrder = 1;

		//場所づくり
		Csv = new CSVReader();

		//マネージャー
		manager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();

		//マップ情報読み込み
		Csv.LoadCsv("CSV_MAP");

		//マップ生成
		MakeCSVMap();

		//アイテム生成
		ItemCreate();
	}

	//更新
	void Update()
	{

	}

	//マップ生成
	private void MakeCSVMap()
    {
		//容量づくり
		map = new int[Csv.GetCsvWidth(), Csv.GetCsvHeight()];

		//書き込み
		for (int x = 0; x < Csv.GetCsvWidth(); x++)
		{
			for (int y = 0; y < Csv.GetCsvHeight(); y++)
			{
				map[x, y] = Csv.GetDataInt(x, y);
			}
		}

		//マップ生成
		for (int y = 0; y < map.GetLength(1); y++)
		{
			for (int x = 0; x < map.GetLength(0); x++)
			{
				//壁生成
				if (map[x, y] == mapInfomation.WALL)
				{
					Instantiate(wall, new Vector3(x, -y, 0), Quaternion.identity).transform.SetParent(gameObject.transform, false);
				}

				//床生成
				else
				{
					Instantiate(room, new Vector3(x, -y, 0), Quaternion.identity).transform.SetParent(gameObject.transform, false);
				}
			}
		}

		//位置
		while (true)
		{
			//位置決定
			stairsPosition.x = Random.Range(1, Csv.GetCsvWidth() - 1);
			stairsPosition.y = Random.Range(-Csv.GetCsvHeight() + 1, -1);

			//部屋以外の場合
			if (map[(int)stairsPosition.x, -(int)stairsPosition.y] < mapInfomation.ROOM)
			{
				//NO
				continue;
			}

			//OK
			break;
		}

		//生成
		GameObject game = Instantiate(stairs, new Vector3(stairsPosition.x, stairsPosition.y, 0), Quaternion.identity);
		game.transform.SetParent(manager.objects.transform, false);
		game.transform.localScale = new Vector3(1.5f, 1.5f, 1);
	}

	//アイテム生成
	private void ItemCreate()
	{
		//個数
		maxPotions = Random.Range(0, 13);
		potions = new GameObject[maxPotions];

		//座標
		Vector2 position;

		//生成
		for (int i = 0; i < maxPotions; i++)
		{
			//位置
			position = Spawn(-1);

			//アイテムの種類
			potions[i] = Instantiate(manager.potions[Random.Range(0, manager.potions.Length)]);

			//親
			potions[i].transform.SetParent(manager.objects.transform, false);

			//座標
			potions[i].transform.position = position;

			//オーダー
			potions[i].GetComponent<SpriteRenderer>().sortingOrder = 1;
		}
	}

	//生成
	public Vector2 Spawn(int num)
	{
		//生成位置
		Vector2 position = new Vector2(0, 0);

		//位置
		while(true)
        {
			//位置決定
			position.x = Random.Range(1, Csv.GetCsvWidth() - 1);
			position.y = Random.Range(-Csv.GetCsvHeight() + 1, -1);

			//部屋以外の場合
			if (map[(int)position.x, -(int)position.y] < mapInfomation.ROOM)
			{
				//NO
				continue;
			}

			//階段の場合
			if(position.x == stairs.transform.position.x && position.y == stairs.transform.position.y)
			{
				//NO
				continue;
			}

			//アイテム
			if (num == -1)
			{
				//OK
				break;
			}

			//キャラクターが存在する場合
			for (int number = 0; number < manager.GetMaxChar(); number++)
			{
				//キャラクターが存在する場合
				if (position == manager.GetPosition(number))
				{
					//NO
					continue;
				}
			}

			//プレイヤー以外がプレイヤーと同じ部屋の場合
			if (num != 0 && GetMap(manager.GetPosition(num)) == GetMap(manager.GetPosition(0)))
			{
				//NO
				continue;
			}

			//OK
			break;
		}

		//生成位置を返す
		return position;
	}

	//移動可能か
	public bool IsMove(Vector2 position)
	{
		//壁の場合
		if (map[(int)position.x, -(int)position.y] == mapInfomation.WALL)
		{
			//NO
			return false;
		}

		//キャラクターが存在する場合
		for (int number = 0; number < manager.GetMaxChar(); number++)
		{
			//キャラクターが存在する場合
			if (position == manager.GetPosition(number))
			{
				//NO
				return false;
			}
		}

		//OK
		return true;
	}

	//マップ情報取得
	public int GetMap(Vector2 vector)
	{
		//マップ情報
		return GetMap((int)vector.x, -(int)vector.y);
	}
	public int GetMap(int x, int y)
	{
		//マップ情報
		return map[x, y];
	}
	public int GetWidth()
	{
		//幅
		return Csv.GetCsvWidth();
	}
	public int GetHeight()
	{
		//高さ
		return Csv.GetCsvHeight();
	}

	//階段の位置
	public Vector2 GetStairsPosition()
	{
		//階段の位置
		return stairsPosition;
	}

	//マップ情報
	public class MAP
    {
		//壁
		public int WALL = 0;

		//通路
		public int AISLE = 1;

		//部屋
		public int ROOM = 2;
    }

	//画面に映る範囲か
	public bool IsExist(Vector2 vector)
	{
		//w:9 h:5以内の場合
		if (Mathf.Abs(manager.GetPosition(0).x - vector.x) <= 9 && Mathf.Abs(manager.GetPosition(0).y - vector.y) <= 5)
		{
			//映る
			return true;
		}

		//映っていない
		return false;
	}

	//ポーションの削除
	public void Remove(int number)
	{
		Destroy(potions[number]);

		//移動
		for (; number < maxPotions - 1; number++)
		{
			//移動
			potions[number] = potions[number + 1];
		}

		maxPotions--;
	}
}
