using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
	//プレイシーンマネージャー
	private PlaySceneManager PlaySceneManager = null;
	//マップオブジェクト
	private Map map = null;

	//Canvas
	private Canvas Canvas = null;

	//CanvasScaler
	private CanvasScaler CanvasScaler = null;

	//キャラクター
	private GameObject[] character = null;
	private Image[] Character = null;

	//部屋&通路管理
	private Image[,] minimap = new Image[64, 64];

	//アイテム
	private GameObject[] potions = null;
	private Image[] Potions = null;

	//部屋画像
	private Sprite room = null;

	//通路画像
	private Sprite aisle = null;

	//階段画像
	private Sprite stairs = null;

	//プレイヤー画像
	private Sprite player = null;

	//敵画像
	private Sprite enemy = null;

	//敵画像
	private Sprite item = null;

	//表示サイズ
	private float size = 3.5f;

	//表示位置高さ
	private const float Height = 150;
	private float height = 150;

	//表示位置幅
	private const float Width = -400;
	private float width = -400;

	//表示の透明度
	private float alpha = 0.5f;

	//プレイヤーが見える範囲
	private int area = 2;

	//開放マス
	private bool[,] open;

	//オープンするミニマップの探索
	private int[,] search = new int[,]
	{
		{-1,0 },
		{0,1 },
		{ 1,0},
		{ 0,-1}
	};

	//時間
	private float nowTime = 0.5f;
	private float maxTime = 0.5f;

	//初期化
	void Start()
	{
		//プレイシーンマネージャーを見つける
		PlaySceneManager = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();

		//mapを見つける
		map = GameObject.Find("Map").GetComponent<Map>();

		open = new bool[/*map.GetMapHeight(), map.GetMapWidth()*/64, 64];
		character = new GameObject[PlaySceneManager.characters];
		Character = new Image[PlaySceneManager.characters];
		potions = new GameObject[map.potions.Length];
		Potions = new Image[map.potions.Length];

		//Canvas作成
		GenerateCanvas();

		//MiniMap作成
		GenerateMiniMap();
	}

	private void GenerateCanvas()
	{
		//Canvasアタッチ
		Canvas = gameObject.AddComponent<Canvas>();
		CanvasScaler = gameObject.AddComponent<CanvasScaler>();

		//カメラに表示させる
		Canvas.renderMode = RenderMode.ScreenSpaceCamera;

		//カメラの情報をCanvasに入れる
		Canvas.worldCamera = Camera.main;

		//CanvasScalerを固定
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;


		//表示順番
		Canvas.sortingOrder = 6500;
	}

	private void GenerateMiniMap()
	{
		for (int y = 0; y < /*map.GetMapHeight()*/64; y++)
		{
			for (int x = 0; x < /*map.GetMapWidth()*/64; x++)
			{
				minimap[x, y] = new GameObject("MiniMap").AddComponent<Image>();

				//親をCanvasに設定
				minimap[x, y].transform.SetParent(gameObject.transform, false);

				//表示位置
				minimap[x, y].rectTransform.anchoredPosition = new Vector2(width, height);

				//表示大きさ
				minimap[x, y].rectTransform.sizeDelta = new Vector2(size, size);


				//通路
				if (map.GetMap(x, y) == 1)
				{
					//画像データの読み込み
					aisle = Resources.Load<Sprite>("MiniMap/aisle");
					//spriteに読み込んだ画像を入れる
					minimap[x, y].sprite = aisle;
				}

				//階段
				else if(x == map.stairsPosition.x && -y == map.stairsPosition.y)
				{
					//画像データの読み込み
					stairs = Resources.Load<Sprite>("MiniMap/stair");
					//spriteに読み込んだ画像を入れる
					minimap[x, y].sprite = stairs;
				}

				//部屋
				else if (map.GetMap(x, y) >= 2)
				{
					//画像データの読み込み
					room = Resources.Load<Sprite>("MiniMap/room");
					//spriteに読み込んだ画像を入れる
					minimap[x, y].sprite = room;
				}

				if (map.GetMap(x, y) > 0)
				{
					//表示の透明度
					minimap[x, y].color = new Color(minimap[x, y].color.r, minimap[x, y].color.g, minimap[x, y].color.b, alpha);
				}
				else
				{
					//壁の場合表示しない
					minimap[x, y].color = new Color(minimap[x, y].color.r, minimap[x, y].color.g, minimap[x, y].color.b, 0);
				}



				//size分横にずれる
				width += size;

				//非表示
				minimap[x, y].enabled = false;
			}
			//幅を初期値に戻す
			width = Width;

			//size分下にずれる
			height -= size;
		}

		//アイテム
		for (int i = 0; i < map.maxPotions; i++)
		{
			//Itemを作成し、Imageをアタッチ
			potions[i] = new GameObject("MiniMapPotion");
			Potions[i] = potions[i].AddComponent<Image>();

			//親をCanvasに設定
			Potions[i].transform.SetParent(Canvas.transform, false);

			//表示位置
			Potions[i].rectTransform.anchoredPosition = new Vector2(-400 + size * map.potions[i].transform.position.x, 150 + size * map.potions[i].transform.position.y);

			//表示大きさ
			Potions[i].rectTransform.sizeDelta = new Vector2(size, size);

			//画像データの読み込み
			item = Resources.Load<Sprite>("MiniMap/item");

			//表示の透明度
			Potions[i].color = new Color(255, 255, 0, alpha + 50);

			//spriteに読み込んだ画像を入れる
			Potions[i].sprite = item;

			//非表示
			Potions[i].enabled = false;
		}
	}

	public void GenerateObject(int val, float x, float y)
	{
		//プレイヤー
		if (val == 0)
		{
			//Playerを作成し、Imageをアタッチ
			character[val] = new GameObject("MiniMapPlayer");
			Character[val] = character[val].AddComponent<Image>();

			//親をCanvasに設定
			Character[val].transform.SetParent(Canvas.transform, false);

			//表示位置
			Character[val].rectTransform.anchoredPosition = new Vector2(-400 + size * x, 150 + size * -y);

			//表示大きさ
			Character[val].rectTransform.sizeDelta = new Vector2(size, size);

			//画像データの読み込み
			player = Resources.Load<Sprite>("MiniMap/char");

			//表示の透明度
			Character[val].color = new Color(255, 255, 0, alpha + 50);

			//spriteに読み込んだ画像を入れる
			Character[val].sprite = player;

			//部屋にいるときに部屋の周囲に通路があれば１マス開ける
			OpenAisleAroundRoom();

			//周囲開放
			AroundSearch(new Vector2(x, -y));
		}

		//敵
		else if (val > 0)
		{
			//Enemyを作成し、Imageをアタッチ
			character[val] = new GameObject("MiniMapEnemy");
			Character[val] = character[val].AddComponent<Image>();

			//親をCanvasに設定
			Character[val].transform.SetParent(Canvas.transform, false);

			//表示位置
			Character[val].rectTransform.anchoredPosition = new Vector2(-400 + size * x, 150 + size * -y);

			//表示大きさ
			Character[val].rectTransform.sizeDelta = new Vector2(size, size);

			//画像データの読み込み
			enemy = Resources.Load<Sprite>("MiniMap/villain");

			//表示の透明度
			Character[val].color = new Color(255, 255, 0, alpha + 50);

			//spriteに読み込んだ画像を入れる
			Character[val].sprite = enemy;

			Character[val].enabled = false;
		}

		//マップ開放
		PlayerMove();
	}

	private void OpenAisleAroundRoom()
	{
		int f = map.GetMap(PlaySceneManager.GetPosition(0));
		if (f > 1)
		{
			for (int x = 0; x < 64; x++)
			{
				for (int y = 0; y < 64; y++)
				{
					if (f == map.GetMap(x, y))
					{
						for (int r = 0; r < 4; r++)
						{
							if (map.GetMap(x + search[r, 0], y + search[r, 1]) == 1)
							{
								open[x + search[r, 0], y + search[r, 1]] = true;
							}
						}
					}
				}
			}
		}
	}

	private void AroundSearch(Vector2 vector2)
	{
		for (int x = -2; x < 3; x++)
		{
			for (int y = -2; y < 3; y++)
			{
				open[(int)vector2.x + x, -(int)vector2.y + y] = true;
			}
		}
	}

	//移動
	public void Move(int numver, Vector2 vector)
	{
		Character[numver].rectTransform.anchoredPosition = new Vector2(-400 + size * vector.x, 150 + size * vector.y);
	}

	//部屋に入ったらその部屋のミニマップを開放
	private void OpenMap(Vector2 vector)
	{
		//その場所が部屋なら
		if (map.GetMap(vector) >= 2)
		{
			for (int x = 0; x < 64; x++)
			{
				for (int y = 0; y < 64; y++)
				{
					//自身がいる部屋情報と一致したら
					if (map.GetMap(x, y) == map.GetMap(vector))
					{
						open[x, y] = true;
					}
				}
			}
		}
		//それ以外はその場所だけ
		else
		{
			open[(int)vector.x, -(int)vector.y] = true;
		}
	}

	//プレイヤーの移動
	public void PlayerMove()
	{
		Move(0, PlaySceneManager.GetPosition(0));

		//周囲開放
		AroundSearch(PlaySceneManager.GetPosition(0));
		//部屋に入ったとき周囲を見て通路があれば１マス開ける
		OpenAisleAroundRoom();
		//ミニマップの開放
		OpenMap(PlaySceneManager.GetPosition(0));

		for (int r = 1; r < PlaySceneManager.GetMaxChar(); r++)
		{
			//プレイヤーが部屋に存在しその部屋内に敵が現れた場合
			if (map.GetMap(PlaySceneManager.GetPosition(0)) >= map.mapInfomation.ROOM &&
				map.GetMap(PlaySceneManager.GetPosition(0)) == map.GetMap(PlaySceneManager.GetPosition(r)))
			{
				Character[r].enabled = true;
			}
		}

		for (int x = (int)PlaySceneManager.GetPosition(0).x - area; x < (int)PlaySceneManager.GetPosition(0).x + area; x++)
		{
			for (int y = (int)PlaySceneManager.GetPosition(0).x - area; y < (int)PlaySceneManager.GetPosition(0).y + area; y++)
			{
				open[x, y] = true;
			}
		}

		for (int x = 0; x < 64; x++)
		{
			for (int y = 0; y < 64; y++)
			{
				//null以外かつプレイヤーが通ったら表示
				if (open[x, y] && minimap[x, y] != null)
				{
					minimap[x, y].enabled = true;
				}
			}
		}

		//アイテムを開く
		for (int i = 0; i < map.maxPotions; i++)
		{
			//プレイヤーが部屋に存在しその部屋内にアイテムが存在している場合場合
			if (map.GetMap(PlaySceneManager.GetPosition(0)) >= map.mapInfomation.ROOM &&
				map.GetMap(PlaySceneManager.GetPosition(0)) == map.GetMap(map.potions[i].transform.position))
			{
				//表示
				Potions[i].enabled = true;
			}

			//足元にアイテムがある場合
			if(PlaySceneManager.GetPosition(0) == (Vector2)map.potions[i].transform.position)
			{
				//取得
				PlaySceneManager.GetCharacter(0).GetComponent<Player>().item.GetItem((int)map.potions[i].GetComponent<ItemBase>().effect);
				PlaySceneManager.SetMessage("プレイヤーは" + PlaySceneManager.GetCharacter(0).GetComponent<Player>().item.GetItemName((int)map.potions[i].GetComponent<ItemBase>().effect) + "を手に入れた！");

				//アイテム削除
				PotionRemove(i);
			}
		}

		//足元が階段
		if (PlaySceneManager.GetPosition(0) == map.stairsPosition)
		{
			PlaySceneManager.MoveNextFloor();
		}
	}

	public void Death(int number)
	{
		Destroy(character[number]);
		Destroy(Character[number]);
		//移動
		for (; number < PlaySceneManager.GetMaxChar() - 1; number++)
		{
			//移動
			character[number] = character[number + 1];
			Character[number] = Character[number + 1];
		}
	}

	//アイテムの削除
	private void PotionRemove(int number)
	{
		Destroy(potions[number]);
		Destroy(Potions[number]);
		//移動
		for (int n = number; n < map.maxPotions - 1; n++)
		{
			//移動
			potions[n] = potions[n + 1];
			Potions[n] = Potions[n + 1];
		}
		map.Remove(number);
	}

	//更新
	void Update()
	{
		//時間経過
		if ((nowTime -= Time.deltaTime) <= 0)
		{
			//時間初期化
			nowTime = maxTime;

			//プレイヤーの点滅
			Color color = Character[0].color;

			//0->1
			if (color.a == 0)
			{
				color.a = 1;
			}

			//1->0
			else
			{
				color.a = 0;
			}

			//変更
			Character[0].color = color;
		}
	}
}