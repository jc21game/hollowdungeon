using SceneManager;
using UnityEngine;

//タイトルシーン
public class TitleSceneManager : MonoBehaviour
{
	//シーンマネージャー
	private Scene Scene = null;

	//武器選択シーンへ移動する時間
	public float invokeTime = 0;

	//キャンバス
	public Canvas canvas = null;

	//背景画像
	public GameObject game = null;

	//BGMバー
	private BGMBar bgmBar = null;

	//SEバー
	private SEBar seBar = null;

	//アチーブメントのXY座標
	private static int AB_1X = 0;
	private static int AB_2X = 0;
	private static int AB_3X = 0;
	private static int AB_4X = 0;
	private static int A_Y = 0;
	private static int B_Y = 0;

	//アチーブメントサイズ
	private static int Size = 0;

	//初期化
	public void Start()
	{
		//シーン登録
		Scene = new Scene();
		Scene.SetNowScene(SCENES.TITLE_SCENE);

		//アイコン位置初期化
		VariableInitialize();

		//フェード画像初期化
		FadeScript.Darkening();
		FadeScript.FadeIn();

		//生成
		Instantiate(canvas);
		Instantiate(game);

		//サウンドマネージャー生成
		SoundManager.SoundManagerGeneration();

		//取得
		bgmBar = GameObject.Find("BGMBar").GetComponent<BGMBar>();
		seBar = GameObject.Find("SEBar").GetComponent<SEBar>();

		//各スライダーの初期設定
		bgmBar.Initialize();
		seBar.Initialize();

		//再生
		SoundManager.PlayBGM("Sounds/BGM/Play_BGM_1");

		////アチーブメント配置
		//TreasureChest.TreasureChestGeneration(AB_1X, A_Y, Size, Size);      //宝箱
		//Trophy.TrophyGeneration(AB_2X, A_Y, Size, Size);                    //トロフィー
		//Apple.AppleGeneration(AB_3X, A_Y, Size, Size);						//リンゴ
		//Rhombus.RhombusGeneration(AB_4X, A_Y, Size, Size);					//ラミエル
		//Heart.HeartGeneration(AB_1X, B_Y, Size, Size);						//ハート
		//Weak.WeakGeneration(AB_2X, B_Y, Size, Size);                        //スライム
		//Sword.SwordGeneration(AB_3X, B_Y, Size, Size);						//剣
		//Crown.CrownGeneration(AB_4X, B_Y, Size, Size);						//王冠

	}

	//更新
	public void Update()
	{
		//Zキーを押したらフェードアウトして武器選択シーンに移動する
		if (Input.GetKeyDown(KeyCode.Z))
		{
			//フェードアウト
			FadeScript.FadeOut();

			//4秒後に武器選択シーンに移動する
			Invoke("ChangeWeapon", invokeTime);
		}

		if (Input.GetKeyDown("k"))
		{
			SoundManager.PlaySE("Sounds/SE/SE_Item_1");
		}

		//書き込みチェック
		SoundManager.CheckVolume();
	}

	//武器選択シーンに移動する関数
	public void ChangeWeapon()
	{
		//武器選択シーンに移動
		Scene.SetNextScene(SCENES.SELECT_WEAPON_SCENE);
	}

	//アイコン座標、サイズ初期化
	private void VariableInitialize()
	{
		//アチーブメントのサイズ
		Size = (int)(Screen.height / 3.5);

		//アチーブメントのXY座標
		AB_1X = -Screen.width / 10 * 3;
		AB_2X = -Screen.width / 10;
		AB_3X = Screen.width / 10;
		AB_4X = Screen.width / 10 * 3;
		A_Y = Screen.height / 3;
		B_Y = 0;
	}

}
