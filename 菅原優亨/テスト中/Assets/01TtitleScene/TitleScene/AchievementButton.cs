using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementButton : MonoBehaviour
{
    private GameObject AchievementArea = null;

    //設定画面
    private GameObject Achievement = null;

    //表示フラグ
    public bool flg = false;

    //初期化
    void Start()
    {
        //取得
        AchievementArea = GameObject.Find("AchievementArea");
        Achievement = GameObject.Find("Achievement");


        AchievementArea.SetActive(flg);
        Achievement.SetActive(flg);
    }

    //更新
    void Update()
    {
        //表示切り替え
        AchievementArea.SetActive(flg);
        Achievement.SetActive(flg);
    }

    //ボタンをクリックされた
    public void OnClick()
    {

        //フラグ切り替え
        if (flg)
            flg = false;
        else
            flg = true;
    }
}
