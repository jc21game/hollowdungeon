using UnityEngine;
using UnityEngine.UI;

public class IconSword : IconBase
{
	//自身のオブジェクトを作成
	private static GameObject BaseObject = null;

	//キャンバスオブジェクト
	private static GameObject SwordCanvasObject = null;

	//キャンバス
	private static Canvas SwordCanvas = null;

	//キャンバスソリューション
	private static CanvasScaler CanvasScaler = null;

	//自身のイメージを作成
	private static Image BaseImage = null;

	//自身の番号
	private static new readonly Icon IconNumber = Icon.SWORD;

	//自身の拡縮状況
	private static SCALE_STATE IconState = SCALE_STATE.NONE;

	//初期化
	void Start()
	{
		//画像セット
		BaseImage.sprite = Resources.Load<Sprite>("主人公正面(剣)");

		//サイズ調整
		SetScaleState(SCALE_STATE.SCALE_USU);

		//拡縮
		Scale(BaseImage, GetScaleState());

		//初期座標に移動
		BaseImage.rectTransform.anchoredPosition = new Vector2(CenterXPosition, IconYPosition);
	}

	//更新
	void Update()
	{

		//自身が選択されている
		if (WeaponSelectSceneManager.GetNowIcon() == IconNumber)
		{
			//直前まで「均す」だったら変更
			if (GetScaleState() == SCALE_STATE.SCALE_USU)
			{
				//拡縮状況を「大きく」に変更
				SetScaleState(SCALE_STATE.SCALE_BIG);
			}

			//サイズが規定値を超えたら拡縮状況を「小さく」に変更
			if (BaseImage.rectTransform.sizeDelta.x > IconMaxSize)
			{
				SetScaleState(SCALE_STATE.SCALE_SMALE);
			}

			//サイズが規定値より下回ったら拡縮状況を「大きく」に変更
			if (BaseImage.rectTransform.sizeDelta.x < IconMinSize)
			{
				SetScaleState(SCALE_STATE.SCALE_BIG);
			}
		}
		//選択されていない
		else
		{
			//拡縮状況を「均す」に変更
			SetScaleState(SCALE_STATE.SCALE_USU);

			//拡縮
			Scale(BaseImage, GetScaleState());
		}

		//「大きく」か「小さく」だったら拡縮自動起動
		if (GetScaleState() == SCALE_STATE.SCALE_BIG || GetScaleState() == SCALE_STATE.SCALE_SMALE)
		{
			//拡縮
			Scale(BaseImage, GetScaleState());
		}
	}

	//自身の拡縮状態のセッター
	private static void SetScaleState(SCALE_STATE state)
	{
		IconState = state;
	}

	//自身の拡縮状況のゲッター
	private static SCALE_STATE GetScaleState()
	{
		return IconState;
	}

	//拡縮
	protected override void Scale(Image image, SCALE_STATE state)
	{
		base.Scale(image, state);
	}

	//キャンバス生成
	private static void SwordCanvasGeneration(Canvas canvas)
	{
		//-----------------Canvas生成部-----------------//

		//Canvas名設定
		SwordCanvasObject = new GameObject("SwordCanvas");

		//Canvas生成
		SwordCanvas = SwordCanvasObject.AddComponent<Canvas>();

		//親をキャンバスに設定
		SwordCanvas.transform.SetParent(canvas.transform, false);

		//CanvasScalerを獲得、生成
		CanvasScaler = SwordCanvasObject.AddComponent<CanvasScaler>();

		//描画優先度が有効なモードに変更
		SwordCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//ReferenceResolutionの設定
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//画面サイズに合わせて自動でサイズ調整するモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

		//描画優先度を有効化
		SwordCanvas.overrideSorting = true;
	}

	//剣アイコン生成
	public static void SwordGeneration(Canvas canvas)
	{
		//オブジェクトが未生成だったら
		if (SwordCanvasObject == null)
		{
			SwordCanvasGeneration(canvas);
		}

		//オブジェクトを作成
		BaseObject = new GameObject("SwordIcon");

		//コンポーネント追加
		BaseObject.AddComponent<IconSword>();

		//コンポーネント追加
		BaseImage = BaseObject.AddComponent<Image>();

		//親を設定
		BaseImage.rectTransform.SetParent(SwordCanvas.transform, false);
	}

	//非表示
	public static void SwordHide()
	{
		BaseImage.enabled = false;
	}

	//表示
	public static void SwordDisplay()
	{
		BaseImage.enabled = true;
	}
}
