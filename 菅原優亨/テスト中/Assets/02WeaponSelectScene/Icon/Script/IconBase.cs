using UnityEngine;
using UnityEngine.UI;

public class IconBase : MonoBehaviour
{
	//おまえは誰？
	public enum Icon
	{
		NONE,       //なし
		SWORD,      //剣
		AXE,        //斧
		BOW,        //弓
		DUNGEON_1,  //ダンジョン1
		DUNGEON_2   //ダンジョン2
	}

	//自身の番号
	protected Icon IconNumber = Icon.NONE;

	//拡縮状況
	public enum SCALE_STATE
	{
		NONE,           //なし
		SCALE_BIG,      //大きく
		SCALE_SMALE,    //小さく
		SCALE_USU       //均す
	}
	protected SCALE_STATE MyState = SCALE_STATE.NONE;

	//アイコンのY座標
	protected static readonly float IconYPosition = Screen.height / 10;

	//センターのX座標
	protected static readonly float CenterXPosition = .0f;

	//ライトのX座標
	protected static readonly float RightXPosition = Screen.width / 4;

	//レフトのX座標
	protected static readonly float LeftXPosition = -Screen.width / 4;

	//ダンジョン1のX座標
	protected static readonly float Dungeon1XPosition = -Screen.width / 6f;

	//ダンジョン2のX座標
	protected static readonly float Dungeon2XPosition = Screen.width / 6f;

	//アイコンの標準サイズ
	protected static readonly float IconWidthHeight = Screen.height / 3;

	//アイコンの上限サイズ
	protected static readonly float IconMaxSize = IconWidthHeight * 1.75f;

	//アイコンの下限サイズ
	protected static readonly float IconMinSize = IconWidthHeight * 1.25f;

	//拡縮率
	protected static readonly float ScaleRate = IconMinSize / 100;

	//拡縮
	protected virtual void Scale(Image image, SCALE_STATE state)
	{
		//拡縮状況が「均す」だったら
		if (state == SCALE_STATE.SCALE_USU)
		{
			//標準サイズに変更
			image.rectTransform.sizeDelta = new Vector2(IconWidthHeight, IconWidthHeight);
		}
		//拡縮状況が「大きく」だったら
		else if (state == SCALE_STATE.SCALE_BIG)
		{
			image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x + ScaleRate, image.rectTransform.sizeDelta.y + ScaleRate);
		}
		//拡縮状況が「小さく」だったら
		else if (state == SCALE_STATE.SCALE_SMALE)
		{
			image.rectTransform.sizeDelta = new Vector2(image.rectTransform.sizeDelta.x - ScaleRate, image.rectTransform.sizeDelta.y - ScaleRate);
		}
		//その他
		else
		{
		}
	}

	//アイコン非表示
	public static void WeaponIconHide()
	{
		IconSword.SwordHide();
		IconAxe.AxeHide();
		IconBow.BowHide();
	}

	public static void DungeonIconHide()
	{
		IconDungeon1.Dungeon1Hide();
		IconDungeon2.Dungeon2Hide();
	}

	//アイコン表示
	public static void WeaponIconDisplay()
	{
		IconSword.SwordDisplay();
		IconAxe.AxeDisplay();
		IconBow.BowDisplay();
	}

	public static void DungeonIconDisplay()
	{
		IconDungeon1.Dungeon1Display();
		IconDungeon2.Dungeon2Display();
	}
}
