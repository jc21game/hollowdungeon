using ScoreManager;
namespace LevelOfAchievement
{
	//アチーブメント管理
	public class AchievementList
	{
		//------------------アチーブメント------------------//
		//アチーブ名：ダンジョン踏破
		//条件：ダンジョンクリア
		//宝箱
		private static int DungeonTraversal = 0;

		//アチーブ名：モンスターハンター
		//条件：すべてのモンスター(ボスを含む)を倒す
		//トロフィー
		private static int MonsterHunter = 0;

		//アチーブ名：ポーション博士
		//条件：すべてのポーションを入手、使用
		//リンゴ
		private static int Dr_Potion = 0;

		//アチーブ名：ドジっ子
		//条件：すべてのダンジョンギミックを踏む
		//ラミエル
		private static int ClumsyPerson = 0;

		//アチーブ名：金剛
		//条件：モンスターからの攻撃を連続5ターン受ける
		//ハート
		private static int Diamond = 0;

		//アチーブ名：死んでしまった！！
		//条件：ゲームオーバーになる
		//スライム
		private static int GameOver = 0;

		//アチーブ名：才能豊か
		//条件：それぞれの武器でダンジョンをクリアする
		//さいころ
		private static int Talented = 0;

		//アチーブ名：一人前の冒険者!!
		//条件：すべてのアチーブメントを獲得する
		private static int GreatAdventurer = 0;

		//------------------アチーブのゲッター------------------//
		public static int GetDungeonTraversal()
		{
			return DungeonTraversal;
		}
		public static int GetMonsterHunter()
		{
			return MonsterHunter;
		}
		public static int GetDr_Potion()
		{
			return Dr_Potion;
		}
		public static int GetClumsyPerson()
		{
			return ClumsyPerson;
		}
		public static int GetDiamond()
		{
			return Diamond;
		}
		public static int GetGameOver()
		{
			return GameOver;
		}
		public static int GetTalented()
		{
			return Talented;
		}
		public static int GetGreatAdventurer()
		{
			return GreatAdventurer;
		}
		

		//------------------アチーブ増加------------------//

		//アチーブ獲得チェック
		public static void AchievementCheck()
		{
			//ダンジョン踏破
			//生き残っているので
			if (Score.GetHp() > 0)
			{
				DungeonTraversal++;
			}
			
			//モンスターハンター
			if (1 == 1)
			{
				MonsterHunter++;
			}
			
			
			//ポーション博士
			if (Score.GetUseGreenPotion() > 0 && Score.GetUseRedPotion() > 0 &&
				Score.GetUseBluePotion() > 0 && Score.GetUseYellowPotion() > 0 &&
				Score.GetUseWhitePotion() > 0)
			{
				Dr_Potion++;
			}
			
			
			//ドジっ子
			if (1 == 1)
			{
				ClumsyPerson++;
			}
			
			
			//金剛
			if (1 == 1)
			{
				Diamond++;
			}
			
			//ゲームオーバー
			if(Score.GetHp() < 1)
			{
				GameOver++;
			}

			//才能豊か
			if (Score.GetSwordFlg() && Score.GetAxeFlg() && Score.GetBowFlg())
			{
				Talented++;
			}

			//一人前の冒険者!!
			if (GetDungeonTraversal() > 0 && GetMonsterHunter() > 0 && GetDr_Potion() > 0 &&
				GetClumsyPerson() > 0 && GetDiamond() > 0 &&
				GetTalented() > 0 && GetGameOver() > 0)
			{
				GreatAdventurer++;
			}
		}

		//アチーブ獲得状況のリセット
		static public void AchieveReset()
		{

		}
	}
}