using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LevelOfAchievement;

public class TreasureChest : Achievement
{


	//画像のパス
	private static readonly string TreasureChestPath = "TreasureChest";

	//初期化
	protected override void Start()
	{
		BaseImage = this.GetComponent<Image>();

		//BaseImage.sprite = Resources.Load<Sprite>("TreasureChest/0");

		Initialize(TreasureChestPath);
	}

	//いろいろ生成
	protected override void Initialize(string path)
	{
		base.Initialize(path);
	}

	//更新
	protected override void Update()
	{
		base.Update();
		BaseImage.sprite = Resources.Load<Sprite>("TreasureChest/1");

		//所持している
		//踏破
		if (AchievementList.GetDungeonTraversal() > 0)
		{
			BaseImage.color = Color.white;
		}
		else
		{
			BaseImage.color = Color.black;
		}
	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	public void OnClick()
	{
	}
}
