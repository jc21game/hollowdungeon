using UnityEngine;
using UnityEngine.UI;
using LevelOfAchievement;

public class Crown : Achievement
{


	//画像のパス
	private static readonly string CrownPath = "Crown";




	//初期化
	protected override void Start()
	{
		Initialize(CrownPath);
	}

	//いろいろ生成
	protected override void Initialize(string path)
	{
		base.Initialize(path);
	}

	//更新
	protected override void Update()
	{
		base.Update();

		//所持している
		if (AchievementList.GetGreatAdventurer() > 0)
		{
			BaseImage.color = Color.white;
		}
		else
		{
			BaseImage.color = Color.black;
		}

	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}


	public void OnClick()
	{

	}
}
