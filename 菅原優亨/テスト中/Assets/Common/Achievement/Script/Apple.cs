using UnityEngine;
using UnityEngine.UI;
using LevelOfAchievement;

public class Apple : Achievement
{


	//画像のパス
	private static readonly string ApplePath = "Apple";


	

	//初期化
	protected override void Start()
	{
		Initialize(ApplePath);
	}

	//いろいろ生成
	protected override void Initialize(string path)
	{
		base.Initialize(path);
	}

	//更新
	protected override void Update()
	{
		base.Update();

		//所持している
		//ポーション
		if (AchievementList.GetDr_Potion() > 0)
		{
			BaseImage.color = Color.white;
		}
		else
		{
			BaseImage.color = Color.black;
		}

	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}


	public void OnClick()
	{

	}
}
