using UnityEngine;
using UnityEngine.UI;
using LevelOfAchievement;

public class Achievement : MonoBehaviour
{
	//枚数
	protected int Sheets = 20;

	//イメージ
	protected Image BaseImage = null;

	//パス
	protected string Path = "/";

	//画像群
	protected Sprite[] Sprits = null;

	//現在の番号
	protected int Number = 0;

	//回転速度・・・3で1秒1回転
	protected float Speed = 4.5f;

	//画像群セット
	protected virtual void ImageTest(string name)
	{
		for (int i = 0; i < Sheets; i++)
		{
			//格納
			Sprits[i] = Resources.Load<Sprite>(name + (i).ToString());
		}
	}

	//初期化
	protected virtual void Start()
	{
	}

	//いろいろ生成
	protected virtual void Initialize(string path)
	{
		//画像群初期化
		Sprits = new Sprite[Sheets];

		//Image生成
		BaseImage = new GameObject(path + "Image").AddComponent<Image>();

		//パスをセット
		Path = path + Path;

		//画像群セット
		ImageTest(Path);

		//画像セット
		BaseImage.sprite = Sprits[Number];
	}

	//更新
	protected virtual void Update()
	{
		//番号増加
		Number++;

		//規定値を超過したら初期化
		if (Number > (Sheets - 1) * Speed)
		{
			Number = 0;
		}

		//画像入れ替え
		BaseImage.sprite = Sprits[(int)(Number / Speed)];
		Debug.Log(BaseImage.sprite.name);
	}

}
