using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//紙吹雪生成
public class MakeConfetti : MonoBehaviour
{
	//プレハブ
	public GameObject confettiRight = null;
	public GameObject confettiLeft = null;
	public int probability = 0;

	//スタート
	public void Start()
	{
	}

	//更新
	public void Update()
	{
		//生成
		if (Random.Range(0, 100) >= 100 - probability)
		{
			//紙吹雪
			GameObject game = Instantiate(confettiRight);
			game.transform.SetParent(transform);
		}
		if (Random.Range(0, 100) >= 100 - probability)
		{
			//紙吹雪
			GameObject game = Instantiate(confettiLeft);
			game.transform.SetParent(transform);
		}
	}
}
