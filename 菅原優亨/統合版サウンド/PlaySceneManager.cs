using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaySceneManager : MonoBehaviour
{
    public static bool flg = true;

    private MiniMap miniMap = null;

    //プレイヤー用
    private Player play = null;

    //エネミー用
    private Enemy enemy = null;

    //ボス用
    private Boss boss = null;

    //サウンドオブジェクト
    private GameObject SoundObject = null;

    //インスタンス
    private GameObject SoundInstance = null;

    //サウンドマネージャー
    private SoundManager SoundManager = null;

    //キャラクターを格納する配列(25体分)
    private CharacterBase[] chrArray = null;

    //追加されているキャラクター数
    private int charNum = 0;

    //行動順を管理する変数
    private int turn = 0;

    //初期化
    public void Start()
    {
        //フェードイン開始
        FadeScript.Darkening();
        FadeScript.FadeIn();

        //プレイヤー取得
        play = GameObject.Find("Player").GetComponent<Player>();

        //ミニマップ取得
        miniMap = GameObject.Find("MiniMap").GetComponent<MiniMap>();

        //エネミー取得
        enemy = GameObject.Find("Enemy").GetComponent<Enemy>();

        //ボス取得
        boss = GameObject.Find("Boss").GetComponent<Boss>();

        //サウンドプレハブを取得
        SoundObject = (GameObject)Resources.Load("SoundManager");

        //インスタンス生成
        SoundInstance = Instantiate(SoundObject);

        //コンポーネント追加
        SoundManager = SoundInstance.GetComponent<SoundManager>();

        //配列生成
        chrArray = new CharacterBase[25];

        //プレイヤーを追加
        Add(play);

        //エネミーを追加
        Add(enemy);

        //ボス追加
        //Add(boss);

        //テスト用
        chrArray[0].SetCharacterName("プレイヤー");
        chrArray[1].SetCharacterName("エネミー");
        //chrArray[2].SetCharacterName("ボス");

        //再生
        SoundManager.PlayBGM("Assets/03PlayScene/Sounds/BGM_1.mp3");
    }

    //更新
    public void Update()
    {
        
        //前のキャラクターの行動が終了していたら次のキャラクターを行動させる(配列順)
        if (chrArray[turn].Execute())
        {
            //テスト用
            Debug.Log(chrArray[turn].GetCharacterName() + "のターン終了");

            //順番更新
            turn++;

            //配列に追加されているすべてのキャラクターの行動が終了したら
            if (turn == charNum)
            {
                //順番リセット
                turn = 0;
            }
        }

        //スペースを押したらミニマップ以外非表示
        flg ^= Input.GetKeyDown(KeyCode.Space);
        Color color = miniMap.GetComponent<Image>().color;
        if(flg)
        {
            color.a = 255;
        }
        else
        {
            color.a = 0;
        }
        miniMap.GetComponent<Image>().color = color;
    }

    //配列に追加する処理
    //引数：追加したいキャラクター
    public void Add(CharacterBase character)
    {
        //追加されたキャラクターにコンポーネントを取得させる
        character.playScene = GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>();

        //自身の配列番号を記憶
        character.SetNumber(charNum);

        //キャラクターを追加
        chrArray[charNum++] = character;
    }

    //配列から削除する処理
    //引数：削除したいキャラクターの配列番号
    public void Remove(int num)
    {
        //キャラクターを削除
        chrArray[num] = null;

        //削除した分を前に詰める
        for (int i = num; i < 24; i++)
        {
            //前に詰める
            chrArray[i] = chrArray[i + 1];
        }

        //キャラクター数を減らす
        charNum--;

        //行動順がキャラクターの数を上回ったら(死亡したキャラクター以降の行動がすべて終了したら)
        if (turn >= charNum)
        {
            //順番リセット
            turn = 0;
        }
    }
}
