using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class SoundManager : MonoBehaviour
{
	//再生用
	private AudioSource audioSource = null;

	//BGM用
	private AudioClip bgm = null;

	//SE用
	private AudioClip se = null;

	//初期化
	public void Start()
	{
		//初期化
		se = null;
		bgm = null;
	}

	//更新
	public void Update()
	{
	}

	//再生したいBGMを指定
	//引数：Assetsからの絶対パス
	public void PlayBGM(string path)
	{
		//コンポーネントを取得
		audioSource = this.GetComponent<AudioSource>();

		//ファイルを設定
		bgm = AssetDatabase.LoadAssetAtPath<AudioClip>(path);

		//再生する音を指定
		audioSource.clip = bgm;

		//ループ設定
		audioSource.loop = true;

		//再生
		audioSource.Play();
	}

	//再生されているBGMを停止
	public void StopBGM()
	{
		//停止
		audioSource.Stop();
	}

	//再生したいSEを指定
	//引数：Assetsからの絶対パス
	public void PlaySE(string path)
	{
		//ファイルを設定
		se = AssetDatabase.LoadAssetAtPath<AudioClip>(path);

		//再生
		audioSource.PlayOneShot(se);
	}
}