FadeScript 使用方法

public class ...Manager : MonoBehaviour
{
	void Start()
	{
		FadeScript. ..();
	}
}

FadeScriptはpublic classで作ったのでnewはなし
ある処理は
①真っ暗にする
②フェードイン
③フェードアウト

フェードインしたいときは事前に真っ暗にしておいてください
暗転処理を使っても、フェードアウトの後でも可