using UnityEngine;
using UnityEngine.UI;

//フェードイン・フェードアウトを行うクラス
public class FadeScript : MonoBehaviour
{
	//数字
	private const int Zero = 0;
	private const int One = 1;
	private const int Three = 3;
	private const int Dim = 9;
	private const int MaxPriority = 9999;

	//フェード処理の状態
	public enum FADE_STATE
	{
		NONE,           //待機状態
		FADE_IN_NOW,    //フェードイン中...
		FADE_IN_END,    //フェードイン終了
		FADE_OUT_NOW,   //フェードアウト中...
		FADE_OUT_END,   //フェードアウト終了
		FADE_OUT_DIM	//半透明
	}

	//生成するCanvasとImage
	private static Canvas Canvas = null;
	private static CanvasScaler CanvasScaler = null;
	private static Image Image;

	//フェード状態管理用変数
	private static FADE_STATE FadeState = FADE_STATE.NONE;

	//フェード更新にかかる時間
	private static float FadeTime = Zero;

	//現在のα値
	private static float Alpha = Zero;

	//α値の下限、上限
	private static float LowerAlpha = Zero;
	private static float UpperAlpha = Zero;

	//レイヤー優先度
	private static int Priority = Zero;

	//初期化
	private void Start()
	{

	}

	//変数初期化
	private static void VariableInitialize()
	{
		//生成するCanvasとImage
		Canvas = null;
		CanvasScaler = null;
		Image = null;

		//フェード状態管理用変数
		FadeState = FADE_STATE.NONE;

		//フェード更新にかかる時間
		FadeTime = Three;

		//現在のα値
		Alpha = Zero;

		//α値の下限、上限
		LowerAlpha = Zero;
		UpperAlpha = One;

		//レイヤー優先度
		Priority = MaxPriority;

	}

	//フェード用のCanvas・Image生成
	private static void Generation()
	{
		//変数初期化
		VariableInitialize();

		//----------------Canvas生成部----------------//

		//Canvas名設定
		GameObject FadeObject = new GameObject("FadeCanvas");

		//Canvas生成
		Canvas = FadeObject.AddComponent<Canvas>();

		//コンポーネントを追加
		CanvasScaler = FadeObject.AddComponent<CanvasScaler>();

		//スケールモードをReferenceResolutionに合わせてサイズが変わるモードに変更
		CanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

		//ReferenceResolutionのサイズ調整
		CanvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		//コンポーネントを追加
		FadeObject.AddComponent<GraphicRaycaster>();

		//描画優先度が有効な設定に切り替え
		Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

		//コンポーネントを追加
		FadeObject.AddComponent<FadeScript>();

		//最前面に設定
		Canvas.sortingOrder = Priority;

		//----------------Image生成部----------------//

		//Image名設定
		Image = new GameObject("FadeImage").AddComponent<Image>();

		//Imageの親にCanvasを設定
		Image.transform.SetParent(Canvas.transform, false);

		//Imageの位置をzeroオリジンに設定
		Image.rectTransform.anchoredPosition = Vector3.zero;

		//サイズ調整 画面の縦横
		Image.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);

	}

	//画面を真っ暗にしておく処理
	public static void Darkening()
	{
		//Imageがいなければ生成
		if (Image == null)
		{
			Generation();
		}

		//Imageを表示
		Image.enabled = true;

		//画面を真っ暗にする
		Alpha = UpperAlpha;

		//色設定：黒
		Image.color = Color.black;

		//フェード状態を待機中に変更
		FadeState = FADE_STATE.NONE;

	}

	//フェードイン開始
	public static void FadeIn()
	{
		//シーンにImageが生成されていなければ生成する
		if (Image == null)
		{
			Generation();
		}

		//すでにフェードインしていたら実行しない
		if (Alpha != LowerAlpha || Image.color != Color.black)
		{
			//α値を規定値に設定
			Alpha = UpperAlpha;

			//色設定：黒
			Image.color = Color.black;

			//フェード状態をフェードイン中に変更
			FadeState = FADE_STATE.FADE_IN_NOW;
		}
	}

	//フェードアウト開始
	public static void FadeOut()
	{
		//シーンにImageが生成されていなければ生成する
		if (Image == null)
		{
			Generation();
		}

		//すでにフェードアウトしていたら実行しない
		if (Alpha != UpperAlpha)
		{
			//色設定：無色
			Image.color = Color.clear;

			//Canvasをシーンに表示させる
			Canvas.enabled = true;

			//フェード状態をフェードアウト中に変更
			FadeState = FADE_STATE.FADE_OUT_NOW;
		}
	}

	//半透明に
	public static void ImageDim()
	{
		//シーンにImageが生成されていなければ生成する
		if (Image == null)
		{
			Generation();
		}

		//半透明じゃなかったら
		if(GetFadeState() != FADE_STATE.FADE_OUT_DIM)
		{
			//描画優先度をメニューに下に
			Canvas.sortingOrder = Dim;

			//Canvasを表示
			Canvas.enabled = true;

			//透明度を変更
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 0.5f);

			//状態を半透明に
			FadeState = FADE_STATE.FADE_OUT_DIM;
		}
	}

	//明るく
	public static void ImageFlush()
	{
		//シーンにImageが生成されていなければ生成する
		if (Image == null)
		{
			Generation();
		}

		//半透明だったら
		if(GetFadeState() == FADE_STATE.FADE_OUT_DIM)
		{
			//透明度を変更
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, 1.0f);

			//描画優先度を最前面に
			Canvas.sortingOrder = Priority;

			//状態を待機に
			FadeState = FADE_STATE.NONE;

			//Canvasを非表示
			Canvas.enabled = false;
		}
	}

	//更新
	//画面にCanvas:Imageがいれば自動発動
	private void Update()
	{
		//フェードイン中なら
		if (FadeState == FADE_STATE.FADE_IN_NOW)
		{
			//経過時間から透明度計算
			Alpha -= Time.deltaTime / FadeTime;

			//透明度が規定値を下回ったら終了
			if (Alpha <= LowerAlpha)
			{
				//フェード状態をフェードイン終了に変更
				FadeState = FADE_STATE.FADE_IN_END;

				//透明度を整理
				Alpha = LowerAlpha;

				//Canvasを非表示
				Canvas.enabled = false;
			}

			//Imageの色・透明度設定
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, Alpha);
		}

		//フェードアウト中なら
		if (FadeState == FADE_STATE.FADE_OUT_NOW)
		{
			//経過時間から透明度計算
			Alpha += Time.deltaTime / FadeTime;

			//透明度が規定値を上回ったら終了
			if (Alpha >= UpperAlpha)
			{
				//フェード状態をフェードアウト終了に変更
				FadeState = FADE_STATE.FADE_OUT_END;

				//透明度を整理
				Alpha = UpperAlpha;
			}

			//Imageの色・透明度設定
			Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, Alpha);
		}

		//Imageのサイズを常に画面サイズに調整
		Image.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);

		//フェード状態がフェードイン終了、もしくはフェードアウト終了だったら
		if (FadeState == FADE_STATE.FADE_IN_END || FadeState == FADE_STATE.FADE_OUT_END)
		{
			//フェード状態を待機に変更
			FadeState = FADE_STATE.NONE;
		}
	}

	//現在のフェード状態を獲得する関数
	public static FADE_STATE GetFadeState()
	{
		//フェード状態を返す
		return FadeState;
	}
}
