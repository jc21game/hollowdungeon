using LevelOfAchievement;
using ScoreManager;
using UnityEngine;
using UnityEngine.UI;

//ゲームオーバーシーン
public class GameOverSceneManager : MonoBehaviour
{
	//テキスト
	public Text[] texts = null;
	private int countFirst = 3;
	private int countSecond = 8;

	//カウント
	private int Count = 0;

	//初期化
	void Start()
	{
		//フェードイン
		FadeScript.FadeIn();

		//再生
		SoundManager.SoundManagerGeneration();
		//SoundManager.PlayBGM("Assets/04EndingScene/Sounds/BGM/Ending.mp3");

		//スコアを表示するための準備
		Resources.Load("Canvas");
		texts[0].text = "HP:" + Score.GetHp().ToString();
		texts[1].text = "Lv:" + Score.GetLv().ToString();
		texts[2].text = "攻撃力:" + Score.GetAttack().ToString();
		texts[3].text = "防御力:" + Score.GetDefence().ToString();
		texts[4].text = "階層:" + Score.GetFloor().ToString();
		texts[5].text = "総ターン数:" + Score.GetTurn().ToString();
		texts[6].text = "総回復量:" + Score.GetCoveredRecovery().ToString();
		texts[7].text = "総被ダメージ:" + Score.GetCoveredDamage().ToString();
		texts[8].text = "総与ダメージ:" + Score.GetGivingDamage().ToString();
		texts[9].text = "総被攻撃回数:" + Score.GetCoveredAttack().ToString();
		texts[10].text = "総与攻撃回数:" + Score.GetGivingAttack().ToString();
		texts[11].text = "総アイテム取得数:" + Score.GetHaveItem().ToString();
		texts[12].text = "総アイテム使用回数:" + Score.GetUseItem().ToString();

		//生成
		MessageWindowManager.ImageGeneration();
		MessageWindowManager.MessageImageDisplay();

		//アチーブ確認
		AchievementList.AchievementCheck();

		//生成
		AchievementPeaple.AppealGeneration();
	}

	//更新
	void Update()
	{
		//カウントを増やす
		if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
		{
			Count++;
		}

		//消す
		for (int i = 0; i < texts.Length; i++)
		{
			texts[i].gameObject.SetActive(false);
		}

		//状態
		switch (Count)
		{
			//0-3
			case 0:
				{
					//表示
					for (int i = 0; i <= countFirst; i++)
					{
						texts[i].gameObject.SetActive(true);
					}

					//終了
					break;
				}

			//4-8
			case 1:
				{
					//表示
					for (int i = countFirst + 1; i <= countSecond; i++)
					{
						texts[i].gameObject.SetActive(true);
					}

					//終了
					break;
				}

			//9-12
			case 2:
				{
					//表示
					for (int i = countSecond + 1; i < texts.Length; i++)
					{
						texts[i].gameObject.SetActive(true);
					}

					//終了
					break;
				}
			case 3:
				{
					if (AchievementList.GetDungeonTraversal_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("TreasureChest");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 4:
				{
					if (AchievementList.GetMonsterHunter_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Trophy");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 5:
				{
					if (AchievementList.GetDr_Potion_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Apple");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 6:
				{
					if (AchievementList.GetClumsyPerson_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Rhombus");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 7:
				{
					if (AchievementList.GetDiamond_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Heart");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 8:
				{
					if (AchievementList.GetGameOver_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Weak");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 9:
				{
					if (AchievementList.GetTalented_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Medal");
					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			case 10:
				{
					if (AchievementList.GetGreatAdventurer_Flg())
					{
						AchievementPeaple.SetNowDuplicatePath("Crown");

					}
					else
					{
						Count++;
					}

					//終了
					break;
				}

			//終了
			default:
				{
					////表示
					//for (int i = countSecond + 1; i < texts.Length; i++)
					//{
					//	texts[i].gameObject.SetActive(true);
					//}

					//フェードアウト
					FadeScript.FadeOut();

					//完全にフェードアウトが終了した場合
					if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
					{
						//タイトルシーンに移動
						UnityEngine.SceneManagement.SceneManager.LoadScene("TitleScene");
					}

					//終了
					break;
				}
		}
	}
}
