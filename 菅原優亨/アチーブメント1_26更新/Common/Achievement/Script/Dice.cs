using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LevelOfAchievement;

public class Dice : Achievement
{
	//自身のオブジェクト
	private static GameObject DiceGameObject = null;

	//自身のXY座標
	private static float XPosition = 0;
	private static float YPosition = 0;

	//自身のサイズ
	private static float Width = 0;
	private static float Height = 0;

	//画像のパス
	private static readonly string DicePath = "Sword";

	//表示非表示のフラグ
	private static bool Flag = false;

	//セッター
	private static void SetXPosition(float X)
	{
		XPosition = X;
	}
	private static void SetYPosition(float Y)
	{
		YPosition = Y;
	}
	private static void SetWidth(float width)
	{
		Width = width;
	}
	private static void SetHeight(float height)
	{
		Height = height;
	}

	//ゲッター
	private static float GetXPosition()
	{
		return XPosition;
	}
	private static float GetYPosition()
	{
		return YPosition;
	}
	private static float GetWidth()
	{
		return Width;
	}
	private static float GetHeight()
	{
		return Height;
	}

	//フラグのゲッター
	private static bool GetFlag()
	{
		return Flag;
	}

	//初期化
	protected override void Start()
	{
		Initialize(DiceGameObject, GetXPosition(), GetYPosition(), GetWidth(), GetHeight(), DicePath);
	}

	//いろいろ生成
	protected override void Initialize(GameObject gameObject, float X, float Y, float width, float height, string path)
	{
		base.Initialize(gameObject, X, Y, width, height, path);
	}

	//更新
	protected override void Update()
	{
		base.Update();

		//所持している
		//全部気
		if (AchievementList.GetTalented() > 0)
		{
			BaseImage.color = Color.white;
		}
		else
		{
			BaseImage.color = Color.black;
		}

		//表示非表示
		if (GetFlag())
		{
			Display();
		}
		else
		{
			Hide();
		}
	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	//Canvas生成
	private static void CanvasGeneration(float Xposition, float Yposition, float width, float height)
	{
		//-----------------Canvas生成部-----------------//

		//Canvas名設定
		DiceGameObject = new GameObject("SwordObject");

		//コンポーネントを追加
		DiceGameObject.AddComponent<Dice>();

		//自身の位置を設定
		SetXPosition(Xposition);
		SetYPosition(Yposition);
		SetWidth(width);
		SetHeight(height);
	}

	//アイコン生成
	//引数：X座標　Y座標　幅　高さ
	public static void DiceGeneration(float Xposition, float Yposition, float width, float height)
	{
		//Canvas生成
		if (DiceGameObject == null)
		{
			CanvasGeneration(Xposition, Yposition, width, height);
		}

		HideFlagOn();


	}

	//表示
	public static void HideFlagOn()
	{
		Flag = true;
	}
	protected override void Display()
	{
		base.Display();
	}

	//非表示
	public static void HideFlagOff()
	{
		Flag = false;
	}
	protected override void Hide()
	{
		base.Hide();
	}
}
