using LevelOfAchievement;
using UnityEngine;
using UnityEngine.UI;

public class Apple : Achievement
{
	//画像のパス
	private static readonly string ApplePath = "Apple";

	//初期化
	protected override void Start()
	{
		base.Start();

		//いろいろ生成
		Initialize(BaseImage, ApplePath);
	}

	//いろいろ生成
	protected override void Initialize(Image image, string path)
	{
		base.Initialize(image, path);
	}

	//更新
	protected override void Update()
	{
		base.Update();

		//現在のページ
		if (Title.Icon == IconBase.Icon.DUNGEON_1)
		{
			//所持している
			//踏破
			if (AchievementList.GetDr_Potion_Poison() > 0)
			{
				BaseImage.color = Color.white;
			}
			else
			{
				BaseImage.color = Color.black;
			}
		}
		else if (Title.Icon == IconBase.Icon.DUNGEON_2)
		{
			//所持している
			//踏破
			if (AchievementList.GetDr_Potion_FlameIce() > 0)
			{
				BaseImage.color = Color.white;
			}
			else
			{
				BaseImage.color = Color.black;
			}
		}
	}

	//画像を格納
	protected override void ImageTest(string name)
	{
		base.ImageTest(name);
	}

	//クリックされたら
	protected override void OnClick()
	{
		Debug.Log("Apple");
		base.OnClick();

		//画像セット
		Duplicate.SetNowDuplicatePath(ApplePath);
	}
}
