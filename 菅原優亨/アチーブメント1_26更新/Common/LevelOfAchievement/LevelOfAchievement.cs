using ScoreManager;
namespace LevelOfAchievement
{
	//アチーブメント管理
	public class AchievementList
	{
		//------------------アチーブメント------------------//
		//アチーブ名：ダンジョン踏破
		//条件：ダンジョンクリア
		//宝箱
		private static int DungeonTraversal_Poison = 0;
		private static int DungeonTraversal_FlameIce = 0;
		private static bool DungeonTraversal_Flg = false;

		//アチーブ名：モンスターハンター
		//条件：すべてのモンスター(ボスを含む)を倒す
		//トロフィー
		private static int MonsterHunter_Poison = 0;
		private static int MonsterHunter_FlameIce = 0;
		private static bool MonsterHunter_Flg = false;

		//アチーブ名：ポーション博士
		//条件：すべてのポーションを入手、使用
		//リンゴ
		private static int Dr_Potion_Poison = 0;
		private static int Dr_Potion_FlameIce = 0;
		private static bool Dr_Potion_Flg = false;

		//アチーブ名：ドジっ子
		//条件：すべてのダンジョンギミックを踏む
		//ラミエル
		private static int ClumsyPerson_Poison = 0;
		private static int ClumsyPerson_FlameIce = 0;
		private static bool ClumsyPerson_Flg = false;

		//アチーブ名：金剛
		//条件：モンスターからの攻撃を連続5ターン受ける
		//ハート
		private static int Diamond_Poison = 0;
		private static int Diamond_FlameIce = 0;
		private static bool Diamond_Flg = false;

		//アチーブ名：死んでしまった！！
		//条件：ゲームオーバーになる
		//スライム
		private static int GameOver_Poison = 0;
		private static int GameOver_FlameIce = 0;
		private static bool GameOver_Flg = false;

		//アチーブ名：才能豊か
		//条件：それぞれの武器でダンジョンをクリアする
		//さいころ
		private static int Talented_Poison = 0;
		private static int Talented_FlameIce = 0;
		private static bool Talented_Flg = false;

		//アチーブ名：一人前の冒険者!!
		//条件：すべてのアチーブメントを獲得する
		private static int GreatAdventurer_Poison = 0;
		private static int GreatAdventurer_FlameIce = 0;
		private static bool GreatAdventurer_Flg = false;

		//------------------アチーブのゲッター------------------//
		public static int GetDungeonTraversal_Poison()
		{
			return DungeonTraversal_Poison;
		}
		public static int GetDungeonTraversal_FlameIce()
		{
			return DungeonTraversal_FlameIce;
		}
		public static bool GetDungeonTraversal_Flg()
		{
			return DungeonTraversal_Flg;
		}
		public static int GetMonsterHunter_Poison()
		{
			return MonsterHunter_Poison;
		}
		public static int GetMonsterHunter_FlameIce()
		{
			return MonsterHunter_FlameIce;
		}
		public static bool GetMonsterHunter_Flg()
		{
			return MonsterHunter_Flg;
		}
		public static int GetDr_Potion_Poison()
		{
			return Dr_Potion_Poison;
		}
		public static int GetDr_Potion_FlameIce()
		{
			return Dr_Potion_FlameIce;
		}
		public static bool GetDr_Potion_Flg()
		{
			return Dr_Potion_Flg;
		}
		public static int GetClumsyPerson_Poison()
		{
			return ClumsyPerson_Poison;
		}
		public static int GetClumsyPerson_FlameIce()
		{
			return ClumsyPerson_FlameIce;
		}
		public static bool GetClumsyPerson_Flg()
		{
			return ClumsyPerson_Flg;
		}
		public static int GetDiamond_Poison()
		{
			return Diamond_Poison;
		}
		public static int GetDiamond_FlameIce()
		{
			return Diamond_FlameIce;
		}
		public static bool GetDiamond_Flg()
		{
			return Diamond_Flg;
		}
		public static int GetGameOver_Poison()
		{
			return GameOver_Poison;
		}
		public static int GetGameOver_FlameIce()
		{
			return GameOver_FlameIce;
		}
		public static bool GetGameOver_Flg()
		{
			return GameOver_Flg;
		}
		public static int GetTalented_Poison()
		{
			return Talented_Poison;
		}
		public static int GetTalented_FlameIce()
		{
			return Talented_FlameIce;
		}
		public static bool GetTalented_Flg()
		{
			return Talented_Flg;
		}
		public static int GetGreatAdventurer_Poison()
		{
			return GreatAdventurer_Poison;
		}
		public static int GetGreatAdventurer_FlameIce()
		{
			return GreatAdventurer_FlameIce;
		}
		public static bool GetGreatAdventurer_Flg()
		{
			return GreatAdventurer_Flg;
		}

		//フラグの初期化
		public static void FlgReset()
		{
			DungeonTraversal_Flg = false;
			MonsterHunter_Flg = false;
			Dr_Potion_Flg = false;
			ClumsyPerson_Flg = false;
			Diamond_Flg = false;
			GameOver_Flg = false;
			Talented_Flg = false;
			GreatAdventurer_Flg = false;
		}

		//------------------アチーブ増加------------------//

		//アチーブ獲得チェック
		public static void AchievementCheck()
		{
			//挑んでいるダンジョン
			if (Score.GetMyDungeon() == IconBase.Icon.DUNGEON_1)
			{
				//ダンジョン踏破
				//生き残っているので
				if (Score.GetHp() > 0)
				{
					DungeonTraversal_Poison++;
					DungeonTraversal_Flg = true;
				}

				//モンスターハンター
				if (1 == 1)
				{
					MonsterHunter_Poison++;
					MonsterHunter_Flg = true;
				}


				//ポーション博士
				if (Score.GetUseGreenPotion() > 0 && Score.GetUseRedPotion() > 0 &&
					Score.GetUseBluePotion() > 0 && Score.GetUseYellowPotion() > 0 &&
					Score.GetUseWhitePotion() > 0)
				{
					Dr_Potion_Poison++;
					Dr_Potion_Flg = true;
				}


				//ドジっ子
				if (1 == 1)
				{
					ClumsyPerson_Poison++;
					ClumsyPerson_Flg = true;
				}


				//金剛
				if (1 == 1)
				{
					Diamond_Poison++;
					Diamond_Flg = true;
				}

				//ゲームオーバー
				if (Score.GetHp() < 1)
				{
					GameOver_Poison++;
					GameOver_Flg = true;
				}

				//才能豊か
				if (Score.GetSwordFlg() && Score.GetAxeFlg() && Score.GetBowFlg())
				{
					Talented_Poison++;
					Talented_Flg = true;
				}

				//一人前の冒険者!!
				if (GetDungeonTraversal_Poison() > 0 && GetMonsterHunter_Poison() > 0 && GetDr_Potion_Poison() > 0 &&
					GetClumsyPerson_Poison() > 0 && GetDiamond_Poison() > 0 &&
					GetTalented_Poison() > 0 && GetGameOver_Poison() > 0)
				{
					GreatAdventurer_Poison++;
					GreatAdventurer_Flg = true;
				}
			}
			else
			{
				//ダンジョン踏破
				//生き残っているので
				if (Score.GetHp() > 0)
				{
					DungeonTraversal_FlameIce++;
					DungeonTraversal_Flg = true;
				}

				//モンスターハンター
				if (1 == 1)
				{
					MonsterHunter_FlameIce++;
					MonsterHunter_Flg = true;
				}


				//ポーション博士
				if (Score.GetUseGreenPotion() > 0 && Score.GetUseRedPotion() > 0 &&
					Score.GetUseBluePotion() > 0 && Score.GetUseYellowPotion() > 0 &&
					Score.GetUseWhitePotion() > 0)
				{
					Dr_Potion_FlameIce++;
					Dr_Potion_Flg = true;
				}


				//ドジっ子
				if (1 == 1)
				{
					ClumsyPerson_FlameIce++;
					ClumsyPerson_Flg = true;
				}


				//金剛
				if (1 == 1)
				{
					Diamond_FlameIce++;
					Diamond_Flg = true;
				}

				//ゲームオーバー
				if (Score.GetHp() < 1)
				{
					GameOver_FlameIce++;
					GameOver_Flg = true;
				}

				//才能豊か
				if (Score.GetSwordFlg() && Score.GetAxeFlg() && Score.GetBowFlg())
				{
					Talented_FlameIce++;
					Talented_Flg = true;
				}

				//一人前の冒険者!!
				if (GetDungeonTraversal_FlameIce() > 0 && GetMonsterHunter_FlameIce() > 0 && GetDr_Potion_FlameIce() > 0 &&
					GetClumsyPerson_FlameIce() > 0 && GetDiamond_FlameIce() > 0 &&
					GetTalented_FlameIce() > 0 && GetGameOver_FlameIce() > 0)
				{
					GreatAdventurer_FlameIce++;
					GreatAdventurer_Flg = true;
				}
			}

		}

		//アチーブ獲得状況のリセット
		static public void AchieveReset()
		{

		}

		//アチーブ詳細テキスト
		static public string DungeonTraversalText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ダンジョン踏破\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetDungeonTraversal_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetDungeonTraversal_FlameIce().ToString() + "炎氷";
			}
			//アチーブ名：ダンジョン踏破
			return text;
		}
		static public string MonsterHunterText(IconBase.Icon icon)
		{
			//テキスト
			string text = "モンスターハンター\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetMonsterHunter_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetMonsterHunter_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：モンスターハンター
			return text;
		}
		static public string Dr_PotionText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ポーション博士\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetDr_Potion_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetDr_Potion_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：ポーション博士
			return text;
		}
		static public string ClumsyPersonText(IconBase.Icon icon)
		{
			//テキスト
			string text = "ドジっ子\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetClumsyPerson_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetClumsyPerson_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：ドジっ子
			return text;
		}
		static public string DiamondText(IconBase.Icon icon)
		{
			//テキスト
			string text = "金剛\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetDiamond_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetDiamond_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：金剛
			return text;
		}
		static public string GameOverText(IconBase.Icon icon)
		{
			//テキスト
			string text = "死んでしまった!!\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetGameOver_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetGameOver_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：死んでしまった!!
			return text;
		}
		static public string TalentedText(IconBase.Icon icon)
		{
			//テキスト
			string text = "才能豊か\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetTalented_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetTalented_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：才能豊か
			return text;
		}
		static public string GreatAdventurerText(IconBase.Icon icon)
		{
			//テキスト
			string text = "一人前の冒険者!!\n" + "獲得日：\n獲得数：";

			//ダンジョンに合わせて変更
			if (icon == IconBase.Icon.DUNGEON_1)
			{
				text += GetGreatAdventurer_Poison().ToString() + "毒";
			}
			else if (icon == IconBase.Icon.DUNGEON_2)
			{
				text += GetGreatAdventurer_FlameIce().ToString() + "炎氷";
			}

			//アチーブ名：一人前の冒険者!!
			return text;
		}
	}
}