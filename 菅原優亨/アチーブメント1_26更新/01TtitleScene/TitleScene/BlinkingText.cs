using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{
    //カラー
    private Color color;

    //規定値を超えるまたは下回った場合に切り替える
    private bool branch = false;

    //変更α値
    private float a;
    //設定α値
    public float seta;

    //初期化
    private void Start()
    {
        //カラー設定
        color = this.GetComponent<Image>().color;
        //α値設定
        a = color.a;
    }

    //更新
    private void Update()
    {
        //α値が超えたら
        if(a >= 1)
		{
            branch = false;
		}
        //α値が下回ったら
        else if(a <= 0)
		{
            branch = true;
		}

        //trueで上昇、falseで下降
        if(branch)
		{
            a += seta;
		}
        else
		{
            a -= seta;
		}

        //α値を反映
        color.a = a;
        this.GetComponent<Image>().color = color;
    }
}
