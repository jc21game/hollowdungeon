using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementArea : MonoBehaviour
{
    //ボタン
    private AchievementButton button = null;

    //初期化
    void Start()
    {
        //取得
        button = GameObject.Find("AchievementButton").GetComponent<AchievementButton>();
    }

    //更新
    void Update()
    {
    }

    //ボタンをクリックされた
    public void OnClick()
    {

        //フラグ変更
        button.flg = false;
    }
}
