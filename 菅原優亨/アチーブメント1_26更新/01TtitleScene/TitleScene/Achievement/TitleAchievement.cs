using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleAchievement : MonoBehaviour
{
	//オブジェクト
	private GameObject Achievement = null;
	private RightArrowButton Right = null;
	private LeftArrowButton Left = null;
	private Image Image = null;

	//ダンジョン
	public IconBase.Icon Icon = IconBase.Icon.DUNGEON_1;

	//初期化
	private void Start()
	{
		//オブジェクト確保
		Achievement = GameObject.Find("Achievement");
		Right = GameObject.Find("RightArrow").GetComponent<RightArrowButton>();
		Left = GameObject.Find("LeftArrow").GetComponent<LeftArrowButton>();
		Image = this.GetComponent<Image>();

		Icon = IconBase.Icon.DUNGEON_1;
	}

	//更新
	private void Update()
	{
		//自身の色を変更
		switch (Icon)
		{
			case IconBase.Icon.DUNGEON_1:
				//色変更
				Image.color = Color.magenta;
				
				//右矢印表示
				Right.gameObject.SetActive(true);
				Right.flg = true;

				//左矢印非表示
				Left.gameObject.SetActive(false);
				Left.flg = false;
				break;
			case IconBase.Icon.DUNGEON_2:
				//色変更
				Image.color = Color.red;

				//左矢印表示
				Left.gameObject.SetActive(true);
				Left.flg = true;

				//右矢印
				Right.gameObject.SetActive(false);
				Right.flg = false;
				break;
		}
	}
}
