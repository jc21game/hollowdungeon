using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{
    private Image image;
    private bool AddSub = false;
    private float a = 0;
    public float changea = 0;
    //初期化
    private　void Start()
    {
        image = this.GetComponent<Image>();
        a = image.color.a;
    }

    //更新
    private void Update()
    {
        if(a >= 1)
		{
            AddSub = false;
		}
        else if(a <= 0)
		{
            AddSub = true;
		}

        if(AddSub)
		{
            a += changea;
		}
        else
		{
            a -= changea;
		}

        Color color = image.color;
        color.a = a;
        image.color = color;
        
    }
}
