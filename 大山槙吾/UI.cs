using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
	//Canvas
	private Canvas Canvas = null;
	private Canvas BCanvas = null;
	private CanvasScaler canvasScaler = null;

	//背景
	private Image BG = null;
	private Sprite BGSprite = null;

	//状態異常
	private Image Poison = null;
	private Image SlowFoot = null;

	//初期化
	public void Start()
	{
		//Canvas生成
		GenerateCanvas();

		//レベルUI
		GameObject LevelText = Instantiate((GameObject)Resources.Load("Prefab/LevelText"));
		LevelText.transform.SetParent(GameObject.Find("UI").transform, false);

		//階層UI
		GameObject FloorText = Instantiate((GameObject)Resources.Load("Prefab/FloorText"));
		FloorText.transform.SetParent(GameObject.Find("UI").transform, false);

		//HPバーUI
		GameObject HPBar = Instantiate((GameObject)Resources.Load("Prefab/HPBar"));
		HPBar.transform.SetParent(GameObject.Find("UI").transform, false);

		//経験値バーUI
		GameObject EXPBar = Instantiate((GameObject)Resources.Load("Prefab/EXPBar"));
		EXPBar.transform.SetParent(GameObject.Find("UI").transform, false);

		//HPテキスト
		GameObject HPText = Instantiate((GameObject)Resources.Load("Prefab/HPText"));
		HPText.transform.SetParent(GameObject.Find("UI").transform, false);

		//経験値テキスト
		GameObject EXPText = Instantiate((GameObject)Resources.Load("Prefab/EXPText"));
		EXPText.transform.SetParent(GameObject.Find("UI").transform, false);

		//背景
		BG = new GameObject("BackGround").AddComponent<Image>();

		//親設定
		BG.transform.SetParent(BCanvas.transform, false);
		BG.transform.localScale = new Vector2(12, 6.5f);

		//スプライト読み込み
		BGSprite = Resources.Load<Sprite>("Map/Back");

		//スプライト設定
		BG.sprite = BGSprite;
		Color color = BG.color;
		color.a = 0.5f;
		BG.color = color;

		//Image初期化
		Poison = new GameObject("poison").AddComponent<Image>();
		SlowFoot = new GameObject("slowfoot").AddComponent<Image>();
		Poison.transform.SetParent(Canvas.transform, false);
		SlowFoot.transform.SetParent(Canvas.transform, false);
		//Imageの位置を設定
		Poison.rectTransform.anchoredPosition = new Vector2(2, 0);
		SlowFoot.rectTransform.anchoredPosition = new Vector2(4, 0);
		//サイズの調整
		Poison.rectTransform.sizeDelta = new Vector2(4, 4);
		SlowFoot.rectTransform.sizeDelta = new Vector2(4, 4);

		Poison.GetComponent<>
	}

	private void GenerateCanvas()
	{
		//Canvasアタッチ
		Canvas = gameObject.AddComponent<Canvas>();
		GameObject back = new GameObject("BCanvas");
		BCanvas = back.AddComponent<Canvas>();
		CanvasScaler scaler = back.AddComponent<CanvasScaler>();
		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;

		//カメラに表示させる
		Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		BCanvas.renderMode = RenderMode.ScreenSpaceCamera;

		//カメラ設定
		BCanvas.worldCamera = Camera.main;

		//表示順番
		Canvas.sortingOrder = 8;
		BCanvas.sortingOrder = 0;

		//表示設定
		canvasScaler = gameObject.AddComponent<CanvasScaler>();
		canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
	}

	//更新
	public void Update()
	{
	}
}
