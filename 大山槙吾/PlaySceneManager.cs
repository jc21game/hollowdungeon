using SceneManager;
using UnityEngine;
using UnityEngine.UI;

//プレイシーン
public class PlaySceneManager : MonoBehaviour
{
	//階層画像
	public Sprite[] floors = null;
	private Sprite sprite = null;
	Image image = null;
	Color color;

	//マップオブジェクト
	private GameObject map = null;
	private Map mapScript = null;
	private GameObject miniMap = null;
	private MiniMap minimapScript = null;

	//キャラクター
	public GameObject objects = null;
	public int characters = 25;
	private CharacterBase[] characterArray;
	public CharacterBase[] allCharacter;
	public CharacterBase trant;

	//キャラクター情報
	private int execute = 0;
	private int maxChar = 0;

	//キャラクター生成確立
	private int generate = 2;

	//アイテム
	public GameObject[] potions = null;

	//シーンマネージャー
	private Scene scene = null;

	//UI
	private GameObject ui = null;

	//ゲーム途中終了
	public bool gameFin = false;
	public bool gameClear = false;

	//時間
	private float count = 0;
	private float maxCount = 5;

	//ゲームスタート
	private bool initialize = false;
	private bool isStart = false;
	private bool fin = false;

	//初期化
	public void Start()
	{
		//フレームレート設定
		Application.targetFrameRate = 60;

		//画面真っ暗
		FadeScript.Darkening();

		//真っ暗な時間
		count = maxCount;

		//イメージ生成
		image = new GameObject("Floor").AddComponent<Image>();

		color = image.color;
		color.a = 0;
		image.color = color;

	}

	//更新
	public void Update()
	{
		//フロア移動
		if (fin)
		{
			NextFloor();
		}

		//階層表示
		if (!isStart)
		{
			//階層の表示
			 sprite = floors[ScoreManager.Score.GetFloor() - 1];

			if(image.color.a < 1)
			{
				color.a += 0.008f;
				image.color = color;
			}

			image.rectTransform.sizeDelta = new Vector2(600, 500);

			image.transform.SetParent(GameObject.Find("FadeCanvas").transform, false);
			image.sprite = sprite;

			//一定時間でメッセージウィンドウ初期化
			if ((count -= Time.deltaTime) < 0)
			{
				//ゲーム開始
				isStart = true;
			}
		}

		//ゲーム開始
		else if (isStart)
		{
			//初期化
			if (!initialize)
			{
				//フェードイン開始
				FadeScript.FadeIn();

				//シーン
				scene = new Scene();
				scene.SetNowScene(SCENES.PLAY_SCENE);

				//マップオブジェクト生成
				map = new GameObject("Map");

				//ソースをアタッチ
				mapScript = map.AddComponent<Map>();

				//ミニマップオブジェクトの生成
				miniMap = new GameObject("MiniMap");
				minimapScript = miniMap.AddComponent<MiniMap>();

				//キャラクター配列
				characterArray = new CharacterBase[characters];

				//生成
				objects = new GameObject("Object");
				Instantiate(Resources.Load("Prefab/Player"));
				Instantiate(Resources.Load("Prefab/Menu"));

				//敵の生成
				for (int i = Random.Range(0, 6); i > 0; i--)
				{
					//生成
					Instantiate(allCharacter[Random.Range(0, allCharacter.Length)]);
				}
				Instantiate(trant);

				//UI
				ui = new GameObject("UI");
				ui.AddComponent<UI>();

				//テキストボックス生成
				MessageWindowManager.ImageGeneration();

				//クエスチョンボックスを生成
				YesNoQuestion.QuestionGeneration();

				//初期化終了
				initialize = true;
			}

			//α値減少
			if (image.color.a > 0)
			{
				color = image.color;
				color.a -= 0.006f;
				image.color = color;
			}

			//フロア移動
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//フェード
				GameFinStart();

				fin = true;
				return;
			}

			//一定時間でメッセージウィンドウ初期化
			if ((count -= Time.deltaTime) < 0)
			{
				//初期化
				MessageWindowManager.MessageImageHide();
			}

			//人数なし
			if (maxChar <= 0)
			{
				return;
			}

			//ゲーム途中終了
			if (gameFin)
			{
				//シーン遷移
				GameFin((int)SCENES.TITLE_SCENE);

				//処理終了
				return;
			}

			//ゲームクリア
			if (gameClear)
			{
				//シーン遷移
				GameFin((int)SCENES.ENDING_SCENE);

				//処理終了
				return;
			}

			//プレイヤーの死亡
			if (!characterArray[0].LIFE)
			{
				//シーン遷移
				GameFin((int)SCENES.OVER_SCENE);

				//処理終了
				return;
			}

			//実行
			if (characterArray[execute].Execute())
			{
				//プレイヤーの死亡
				if (!characterArray[0].LIFE)
				{
					//フェード
					GameFinStart();

					//処理終了
					return;
				}

				//死亡しているか
				for (int i = 1; i < maxChar; i++)
				{
					//死亡
					if (!characterArray[i].LIFE)
					{
						//削除
						Remove(i--);
					}
				}

				//次へ進む
				if (++execute == maxChar)
				{
					//初期化
					execute = 0;

					//キャラクターの生成
					if (Random.Range(0, 100) == generate)
					{
						//生成
						Instantiate(allCharacter[Random.Range(0, allCharacter.Length)]);
					}

					//プレイヤーの移動反映
					miniMap.GetComponent<MiniMap>().PlayerMove();

					//自動回復
					GetCharacter(0).GetComponent<Player>().AutoHeal();
				}
			}
		}
	}

	//最大人数
	public int GetMaxChar()
	{
		//最大人数
		return maxChar;
	}

	//追加
	public void AddChar(CharacterBase character)
	{
		//追加
		characterArray[maxChar++] = character;

		//座標
		character.transform.position = mapScript.Spawn(maxChar - 1);

		//ミニマップ
		minimapScript.GenerateObject(maxChar - 1, character.transform.position.x, -character.transform.position.y);
	}

	//削除
	public void Remove(int number)
	{
		//死亡
		characterArray[number].Death();
		minimapScript.Death(number);

		//移動
		for (; number < maxChar - 1; number++)
		{
			//移動
			characterArray[number] = characterArray[number + 1];
		}

		//実行を戻すか
		if (number <= execute)
		{
			//戻す
			execute--;
		}

		//最大人数
		maxChar--;
	}

	//座標取得
	public Vector2 GetPosition(int number)
	{
		//座標
		return characterArray[number].transform.position;
	}

	//一致する座標に存在するキャラクターを返す	
	public CharacterBase GetCharacter(Vector2 vector)
	{
		//全キャラ回す
		for (int number = 0; number < maxChar; number++)
		{
			//座標が同じ場合
			if ((Vector2)characterArray[number].transform.position == vector)
			{
				//キャラクターを返す
				return characterArray[number];
			}
		}

		//いない場合
		return null;
	}

	//キャラクターを返す
	public CharacterBase GetCharacter(int number)
	{
		//キャラクターを返す
		return characterArray[number];
	}

	//キャラクターの移動
	public void Move()
	{
		//キャラクターの移動
		miniMap.GetComponent<MiniMap>().Move(execute, characterArray[execute].transform.position);
	}

	//ゲーム終了
	public void GameFinStart()
	{
		//フェード
		FadeScript.FadeOut();
	}
	public void GameFin(int nextScene)
	{
		//フェード終了
		if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
		{
			//シーン移動
			scene.SetNextScene((SCENES)nextScene);
		}
	}

	//メッセージ送信
	public void SetMessage(string message)
	{
		count = maxCount;
		MessageWindowManager.MessageTextChange(message);
	}

	//メッセージリセット
	public void ResetMessage()
	{
		MessageWindowManager.TextReset();
	}

	//メッセージ永続表示
	public void CountMax()
	{
		count = 1;
	}

	//メッセージ非表示
	public void CountMin()
	{
		count = 0;
	}

	//階層移動
	public void NextFloor()
	{
		//フェード終了
		if (FadeScript.GetFadeState() == FadeScript.FADE_STATE.NONE)
		{
			//階層変更
			ScoreManager.Score.SetFloor(ScoreManager.Score.GetFloor() + 1);
		}

		//シーン遷移
		GameFin((int)SCENES.PLAY_SCENE);
	}
}