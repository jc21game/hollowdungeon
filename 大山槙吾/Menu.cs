using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	//オーダー
	private const int order = 10;

	//インスペクターウィンドウからゲームオブジェクトを設定する
	public GameObject MenuPanel = null;
	public GameObject ItemPanel = null;
	public GameObject StatusPanel = null;
	//選択時の背景色の変更するためのオブジェクト
	public GameObject Item = null;
	public GameObject Status = null;
	public GameObject End = null;
	//大体のメニューに表示するステータス
	public GameObject MainStatusText = null;
	//選択時に背景色の変更するためのオブジェクト
	public GameObject Green = null;
	public GameObject Red = null;
	public GameObject Blue = null;
	public GameObject Yellow = null;
	public GameObject White = null;
	//各アイテムのテキスト変更
	public GameObject GreenText = null;
	public GameObject RedText = null;
	public GameObject BlueText = null;
	public GameObject YellowText = null;
	public GameObject WhiteText = null;
	//選択時の背景色の変更するためのオブジェクト
	public GameObject HP = null;
	public GameObject ATTACK = null;
	public GameObject DEFENSE = null;
	//ポイントを振った時の値を表示
	public GameObject HPText = null;
	public GameObject ATTACKText = null;
	public GameObject DEFENSEText = null;
	//ステータスメニューに表示するステータス
	public GameObject SubStatusText = null;
	//各メニューフラグ
	public static bool menu = false;
	public static bool item = false;
	public static bool status = false;
	public static bool end = false;

	//選択時の背景色を変化させる(非選択時の場合は背景色を背景と同化させる)
	Color Select = new Color(255f, 255f, 0f, 0.5f);
	Color NotSelect = new Color(82f, 125f, 220f, 0f);
	//メニュー表示時各項目の非選択時の背景色(白)
	Color Back = new Color(255f, 255f, 255f, 1f);

	//各アイテムごとのテキスト書き換え
	private Text GText = null;
	private Text RText = null;
	private Text BText = null;
	private Text WText = null;

	//選択している項目番号
	private int Number = 0;
	private Text HText = null;
	private Text AText = null;
	private Text DText = null;

	//各ステータスの加算値と初期のステータス
	private int H;
	private int A;
	private int D;

	//プレイヤー
	private GameObject player = null;
	private Player Player = null;

	//ターン終了
	private bool fin = false;

	//メッセージ
	private bool doOnce = false;
	private string[] potion = null;

	//初期化
	private void Start()
	{
		//テキストを書き込むための設定
		GText = GreenText.GetComponent<Text>();
		RText = RedText.GetComponent<Text>();
		BText = BlueText.GetComponent<Text>();
		WText = WhiteText.GetComponent<Text>();
		HText = HPText.GetComponent<Text>();
		AText = ATTACKText.GetComponent<Text>();
		DText = DEFENSEText.GetComponent<Text>();

		//各ステータスを取得
		player = GameObject.Find("Player(Clone)");
		Player = player.GetComponent<Player>();

		H = Player.h;
		A = Player.a;
		D = Player.d;

		//ステータス状態(初期)
		HText.text = H.ToString();
		AText.text = A.ToString();
		DText.text = D.ToString();

		//オーダー
		gameObject.GetComponent<Canvas>().sortingOrder = order;

		//テキスト
		potion = new string[4]
		{
			"HPを50ポイント回復。",
			"攻撃力を10ターンすこし上昇。",
			"防御力を10ターンすこし上昇。",
			"スキルポイントを3ポイント取得。",
		};
	}

	//更新
	private void Update()
	{
		//trueなら表示,falseなら非表示(２つ以上trueになることはない)
		MenuPanel.SetActive(menu);
		ItemPanel.SetActive(item);
		StatusPanel.SetActive(status);
	}

	//行動
	public bool Execute()
	{
		//Xが押された時の各挙動
		if (Input.GetKeyDown(KeyCode.X))
		{
			//何も開いていない
			if (!menu && !item && !status && !end)
			{
				menu = true;
				MessageWindowManager.MessageImageHide();
				Number = 1;

				//半透明に
				FadeScript.ImageDim();
			}
			//メニューのみ
			else if (menu && !item && !status && !end)
			{
				menu = false;
				Number = 1;
				MessageWindowManager.MessageImageHide();

				//明るく
				FadeScript.ImageFlush();
			}
			//アイテムのみ
			else if (!menu && item && !status && !end)
			{
				menu = true;
				item = false;
				Number = 1;
				MessageWindowManager.MessageImageHide();
			}
			//ステータスのみ
			else if (!menu && !item && status && !end)
			{
				menu = true;
				status = false;
				Number = 2;
			}
		}

		//メニューが開いていたら
		if (menu && !end)
		{
			//カーソル移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number >= 3)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}

			//カーソル移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number <= 1)
				{
					Number = 3;
				}
				else
				{
					Number--;
				}
			}

			//決定
			if (Input.GetKeyDown(KeyCode.Z))
			{
				//SE再生
				SoundManager.PlaySE("Select");

				if (Number != 3)
				{
					menu = false;
				}
				else
				{
					doOnce = true;
				}
				SelectMenu(Number);
				return false;
			}

			//カーソルの色変更
			switch (Number)
			{
				case 1:
					Item.GetComponent<Image>().color = Select;
					Status.GetComponent<Image>().color = Back;
					End.GetComponent<Image>().color = Back;
					break;
				case 2:
					Item.GetComponent<Image>().color = Back;
					Status.GetComponent<Image>().color = Select;
					End.GetComponent<Image>().color = Back;
					break;
				case 3:
					Item.GetComponent<Image>().color = Back;
					Status.GetComponent<Image>().color = Back;
					End.GetComponent<Image>().color = Select;
					break;
			}
		}

		//アイテムメニューが開いていたら
		if (item)
		{
			//アイテムメニューテキストの表示
			GText.text = Player.item.GREEN_POTION + "　　" + Player.item.GREEN + "個";
			RText.text = Player.item.RED_POTION + "　　" + Player.item.RED + "個";
			BText.text = Player.item.BLUE_POTION + "　　" + Player.item.BLUE + "個";
			WText.text = Player.item.WHITE_POTION + "　　" + Player.item.WHITE + "個";

			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number >= 4)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number <= 1)
				{
					Number = 4;
				}
				else
				{
					Number--;
				}
			}
			//カーソルの色変更
			switch (Number)
			{
				case 1:
					Green.GetComponent<Image>().color = Select;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 2:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = Select;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 3:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = Select;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = NotSelect;
					break;
				case 4:
					Green.GetComponent<Image>().color = NotSelect;
					Red.GetComponent<Image>().color = NotSelect;
					Blue.GetComponent<Image>().color = NotSelect;
					Yellow.GetComponent<Image>().color = NotSelect;
					White.GetComponent<Image>().color = Select;
					break;
			}

			//テキスト
			MessageWindowManager.MessageImageHide();
			MessageWindowManager.MessageTextChange(potion[Number - 1]);

			//アイテムの使用
			if (Input.GetKeyDown(KeyCode.Z))
			{
				//SE再生
				SoundManager.PlaySE("Select");

				//使用
				bool use = false;
				switch (Number)
				{
					case 1: if (Player.item.GREEN > 0) use = true; break;
					case 2: if (Player.item.RED > 0) use = true; break;
					case 3: if (Player.item.BLUE > 0) use = true; break;
					case 4: if (Player.item.WHITE > 0) use = true; break;
				}

				//使用
				if (use)
				{
					//ターン経過
					fin = true;

					//使用
					Player.item.UseItem(Number - 1);
					Debug.Log(Player.DEF);
				}
			}
		}

		//ステータスメニューが開いていたら
		if (status)
		{
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number >= 3)
				{
					Number = 1;
				}
				else
				{
					Number++;
				}
			}
			//カーソルの移動
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				if (Number <= 1)
				{
					Number = 3;
				}
				else
				{
					Number--;
				}
			}
			//カーソルの色変更
			switch (Number)
			{
				case 1:
					HP.GetComponent<Image>().color = Select;
					ATTACK.GetComponent<Image>().color = NotSelect;
					DEFENSE.GetComponent<Image>().color = NotSelect;
					break;
				case 2:
					HP.GetComponent<Image>().color = NotSelect;
					ATTACK.GetComponent<Image>().color = Select;
					DEFENSE.GetComponent<Image>().color = NotSelect;
					break;
				case 3:
					HP.GetComponent<Image>().color = NotSelect;
					ATTACK.GetComponent<Image>().color = NotSelect;
					DEFENSE.GetComponent<Image>().color = Select;
					break;

			}

			//ステータスパラメータの変更
			if (Player.skillPoint > 0)
			{
				switch (Number)
				{
					case 1:
						if (Input.GetKeyDown(KeyCode.RightArrow))
						{
							//SE再生
							SoundManager.PlaySE("Choice");

							H++;
							Player.h++;
							Player.HP++;
							Player.MAX_HP++;
							Player.skillPoint--;
						}
						break;
					case 2:
						if (Input.GetKeyDown(KeyCode.RightArrow))
						{
							//SE再生
							SoundManager.PlaySE("Choice");

							A++;
							Player.a++;
							Player.ATC++;
							Player.skillPoint--;
						}
						break;
					case 3:
						if (Input.GetKeyDown(KeyCode.RightArrow))
						{
							//SE再生
							SoundManager.PlaySE("Choice");

							D++;
							Player.d++;
							Player.DEF++;
							Player.skillPoint--;
						}
						break;
				}
			}

			//ポイント振り分け
			HText.text = H.ToString();
			AText.text = A.ToString();
			DText.text = D.ToString();
		}

		//end
		if (end)
		{
			if (doOnce)
			{
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().ResetMessage();
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().SetMessage("ゲームを終了します。");
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().SetMessage("よろしいですか？");
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().SetMessage("注意！！ゲームを終了すると、初めからのスタートとなります。");
			}
			GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMax();

			//クエスチョンボックスを表示
			YesNoQuestion.QuestionDisplay();

			//「はい」「いいえ」切り替え
			if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))
			{
				//SE再生
				SoundManager.PlaySE("Choice");

				YesNoQuestion.ArrowChange();
			}

			//選択決定
			else if (Input.GetKeyDown(KeyCode.Z))
			{
				//SE再生
				SoundManager.PlaySE("Select");

				//Yes
				if (YesNoQuestion.GetArrow())
				{
					//明るく
					FadeScript.ImageFlush();

					//menuをfalse
					menu = false;

					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().GameFinStart();
					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().gameFin = true;
				}

				//No
				else
				{
					end = false;
					Number = 3;
					GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMin();
				}

				//クエスチョンボックスを非表示
				YesNoQuestion.QuestionHide();
			}

			else if (Input.GetKeyDown(KeyCode.X))
			{
				//クエスチョンボックスを非表示
				YesNoQuestion.QuestionHide();
				end = false;
				Number = 3;
				GameObject.Find("PlaySceneManager").GetComponent<PlaySceneManager>().CountMin();
			}
		}

		//ターン終了時
		if (fin)
		{
			menu = item = status = end = false;
			fin = false;
			Player.h = H;
			Player.a = A;
			Player.d = D;

			//明るく
			FadeScript.ImageFlush();
			return true;
		}

		//ターン終了していない
		return false;
	}


	//メニュー切り替え
	private void SelectMenu(int Val)
	{
		Number = 1;
		switch (Val)
		{
			case 1: item = true; break;
			case 2: status = true; break;
			case 3: end = true; break;
		}
	}

	//ステート
	public int GetState()
	{
		//状態
		if (menu || item || status || end)
			return 3;
		return 0;
	}
}
